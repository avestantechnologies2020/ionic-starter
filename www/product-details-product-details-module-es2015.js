(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-details-product-details-module"],{

/***/ "./src/app/product/details/product-details.model.ts":
/*!**********************************************************!*\
  !*** ./src/app/product/details/product-details.model.ts ***!
  \**********************************************************/
/*! exports provided: ProductDetailsModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsModel", function() { return ProductDetailsModel; });
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../shell/data-store */ "./src/app/shell/data-store.ts");

class ProductDetailsModel extends _shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"] {
    constructor() {
        super();
        this.showcaseImages = [
            {
                type: '',
                source: ''
            },
            {
                type: '',
                source: ''
            },
            {
                type: '',
                source: ''
            }
        ];
        this.colorVariants = [
            {
                name: '',
                value: '',
                default: false
            },
            {
                name: '',
                value: '',
                default: false
            },
            {
                name: '',
                value: '',
                default: false
            }
        ];
        this.sizeVariants = [
            {
                name: '',
                value: '',
                default: false
            },
            {
                name: '',
                value: '',
                default: false
            },
            {
                name: '',
                value: '',
                default: false
            }
        ];
        this.relatedProducts = [
            {
                id: null
            },
            {
                id: null
            }
        ];
    }
}


/***/ }),

/***/ "./src/app/product/details/product-details.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/product/details/product-details.module.ts ***!
  \***********************************************************/
/*! exports provided: ProductDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsPageModule", function() { return ProductDetailsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _product_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../product.service */ "./src/app/product/product.service.ts");
/* harmony import */ var _product_details_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./product-details.page */ "./src/app/product/details/product-details.page.ts");
/* harmony import */ var _product_details_resolver__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./product-details.resolver */ "./src/app/product/details/product-details.resolver.ts");












const routes = [
    {
        path: '',
        component: _product_details_page__WEBPACK_IMPORTED_MODULE_8__["ProductDetailsPage"],
        resolve: {
            data: _product_details_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductDetailsResolver"]
        }
    }
];
class ProductDetailsPageModule {
}
ProductDetailsPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: ProductDetailsPageModule });
ProductDetailsPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function ProductDetailsPageModule_Factory(t) { return new (t || ProductDetailsPageModule)(); }, providers: [
        _product_details_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductDetailsResolver"],
        _product_service__WEBPACK_IMPORTED_MODULE_7__["ProductService"]
    ], imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ProductDetailsPageModule, { declarations: [_product_details_page__WEBPACK_IMPORTED_MODULE_8__["ProductDetailsPage"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductDetailsPageModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [
                    _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                    _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
                    _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]
                ],
                declarations: [
                    _product_details_page__WEBPACK_IMPORTED_MODULE_8__["ProductDetailsPage"]
                ],
                providers: [
                    _product_details_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductDetailsResolver"],
                    _product_service__WEBPACK_IMPORTED_MODULE_7__["ProductService"]
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/product/details/product-details.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/product/details/product-details.page.ts ***!
  \*********************************************************/
/*! exports provided: ProductDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsPage", function() { return ProductDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _categories_categories_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../categories/categories.page */ "./src/app/categories/categories.page.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/firebase-analytics/ngx */ "./node_modules/@ionic-native/firebase-analytics/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/downloader/ngx */ "./node_modules/@ionic-native/downloader/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _popup_compare_popup_compare__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../popup-compare/popup-compare */ "./src/app/popup-compare/popup-compare.ts");
/* harmony import */ var _popup_reason_to_buy_popup_reason_to_buy__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../popup-reason-to-buy/popup-reason-to-buy */ "./src/app/popup-reason-to-buy/popup-reason-to-buy.ts");
/* harmony import */ var _popup_compare_same_category_popup_compare_same_category__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../popup-compare-same-category/popup-compare-same-category */ "./src/app/popup-compare-same-category/popup-compare-same-category.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../shell/image-shell/image-shell.component */ "./src/app/shell/image-shell/image-shell.component.ts");































const _c0 = ["slides"];
function ProductDetailsPage_ng_container_9_ion_row_5_ion_col_1_Template(rf, ctx) { if (rf & 1) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-col", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-icon", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_ion_row_5_ion_col_1_Template_ion_icon_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r10); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r9.clickOnFavourites(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ProductDetailsPage_ng_container_9_ion_row_5_ion_col_2_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-col", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-icon", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_ion_row_5_ion_col_2_Template_ion_icon_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r11.clickOnFavourites(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c1 = function (a0, a1) { return { "centered-image": a0, "fill-image": a1 }; };
function ProductDetailsPage_ng_container_9_ion_row_5_ion_slide_6_Template(rf, ctx) { if (rf & 1) {
    const _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-slide", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-row", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_ion_row_5_ion_slide_6_Template_ion_row_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r15); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r14.openZoomPopup(ctx_r14.images); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "app-image-shell", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const image_r13 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](2, _c1, image_r13.type === "square", image_r13.type === "fill"))("src", image_r13);
} }
function ProductDetailsPage_ng_container_9_ion_row_5_Template(rf, ctx) { if (rf & 1) {
    const _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-row", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ProductDetailsPage_ng_container_9_ion_row_5_ion_col_1_Template, 2, 0, "ion-col", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ProductDetailsPage_ng_container_9_ion_row_5_ion_col_2_Template, 2, 0, "ion-col", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "ion-col", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "ion-slides", 26, 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ionSlideDidChange", function ProductDetailsPage_ng_container_9_ion_row_5_Template_ion_slides_ionSlideDidChange_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r17); const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r16.getIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ProductDetailsPage_ng_container_9_ion_row_5_ion_slide_6_Template, 3, 5, "ion-slide", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.favouritesFlage == false);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.favouritesFlage == true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r1.slidesOptions);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r1.images);
} }
function ProductDetailsPage_ng_container_9_ion_row_6_ion_col_1_Template(rf, ctx) { if (rf & 1) {
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-col", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-icon", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_ion_row_6_ion_col_1_Template_ion_icon_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r23); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r22.clickOnFavourites(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ProductDetailsPage_ng_container_9_ion_row_6_ion_col_2_Template(rf, ctx) { if (rf & 1) {
    const _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-col", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-icon", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_ion_row_6_ion_col_2_Template_ion_icon_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r25); const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r24.clickOnFavourites(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ProductDetailsPage_ng_container_9_ion_row_6_ion_slide_6_Template(rf, ctx) { if (rf & 1) {
    const _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-slide", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-row", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_ion_row_6_ion_slide_6_Template_ion_row_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r28); const ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r27.openZoomPopup(ctx_r27.images); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "app-image-shell", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const image_r26 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](2, _c1, image_r26.type === "square", image_r26.type === "fill"))("src", image_r26);
} }
function ProductDetailsPage_ng_container_9_ion_row_6_Template(rf, ctx) { if (rf & 1) {
    const _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-row", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ProductDetailsPage_ng_container_9_ion_row_6_ion_col_1_Template, 2, 0, "ion-col", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ProductDetailsPage_ng_container_9_ion_row_6_ion_col_2_Template, 2, 0, "ion-col", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "ion-col", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "ion-slides", 34, 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ionSlideDidChange", function ProductDetailsPage_ng_container_9_ion_row_6_Template_ion_slides_ionSlideDidChange_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r30); const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r29.getIndex(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ProductDetailsPage_ng_container_9_ion_row_6_ion_slide_6_Template, 3, 5, "ion-slide", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.favouritesFlage == false);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.favouritesFlage == true);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx_r2.slidesOptions);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r2.images);
} }
function ProductDetailsPage_ng_container_9_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" MRP: \u20B9", ctx_r3.formatNumber(ctx_r3.MRP), " ");
} }
function ProductDetailsPage_ng_container_9_ion_row_24_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-row", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-col", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "ion-col", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r31 = ctx.$implicit;
    const i_r32 = ctx.index;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("background", i_r32 % 2 === 0 ? "rgba(175, 202, 214)" : "rgb(227, 235, 239)", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefaultStyleSanitizer"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r31.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r31.value);
} }
function ProductDetailsPage_ng_container_9_Template(rf, ctx) { if (rf & 1) {
    const _r34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-row", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-col");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, ProductDetailsPage_ng_container_9_ion_row_5_Template, 7, 4, "ion-row", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ProductDetailsPage_ng_container_9_ion_row_6_Template, 7, 4, "ion-row", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "ion-row", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "ion-col", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, ProductDetailsPage_ng_container_9_div_11_Template, 3, 1, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "ion-col", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_Template_ion_col_click_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r34); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r33.shareMethod(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](13, "img", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "Share");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "ion-row", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "ion-col", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "ion-button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductDetailsPage_ng_container_9_Template_ion_button_click_18_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r34); const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r35.indicatorDownload(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, "Download");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "h3", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, "Product Specifications");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, ProductDetailsPage_ng_container_9_ion_row_24_Template, 7, 4, "ion-row", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r0.categoryName);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r0.images.length == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r0.images.length != 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r0.productName);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r0.MRP != null && ctx_r0.MRP != "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r0.productDetails);
} }
const _c2 = function () { return ["/favourites"]; };
const _c3 = function () { return ["/categories"]; };
// import { ZoomPopup } from '../../zoom-popup/zoom-popup';
// import { PopupZoomPage } from '../../popup-zoom/popup-zoom.page';
class ProductDetailsPage {
    constructor(route, alertController, http, downloader, storage, androidPermissions, loadingController, socialSharing, toastCtrl, myapp, categoriesPage, platform, firebaseAnalytics, inAppBrowser, modalCtrl, router) {
        this.route = route;
        this.alertController = alertController;
        this.http = http;
        this.downloader = downloader;
        this.storage = storage;
        this.androidPermissions = androidPermissions;
        this.loadingController = loadingController;
        this.socialSharing = socialSharing;
        this.toastCtrl = toastCtrl;
        this.myapp = myapp;
        this.categoriesPage = categoriesPage;
        this.platform = platform;
        this.firebaseAnalytics = firebaseAnalytics;
        this.inAppBrowser = inAppBrowser;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.colorVariants = [];
        this.sizeVariants = [];
        this.productId = '';
        this.title = '';
        this.productName = '';
        this.productDetails = [];
        this.MRP = "";
        this.shareDetails = [];
        this.images = [];
        this.favouritesFlage = false;
        this.SKUCode = "";
        this.item = {
            title: "",
            ID: "",
            Image: "",
            ProductTitle: "",
            SKUCode: "",
            MRP: "",
            productSpecifications: [],
            uspList: []
        };
        this.slidesOptions = {
            zoom: {
                toggle: false // Disable zooming to prevent weird double tap zomming on slide images
            }
        };
        this.categoryName = '';
        this.reasonsToBuyFlage = false;
        this.reasonsToBuyImageArray = [];
        this.uspList = [];
        this.showPopup = false;
        this.productId = this.route.snapshot.params.productId;
        this.route.queryParams.subscribe(params => {
            this.categoryName = params.title;
            this.shareDetails = params;
            if (params && params.title) {
                this.title = params.title;
                let param = {};
                if (params.title == "Air Conditioners") {
                    param = { air_conditioner: this.productId };
                }
                else if (params.title == "Air Coolers") {
                    param = { air_cooler: this.productId };
                }
                else if (params.title == "Air Purifiers") {
                    param = { air_purifier: this.productId };
                }
                else if (params.title == "Water Purifiers") {
                    param = { water_purifier: this.productId };
                    // param = { water_purifier: '184' }
                }
                this.loadingController.create({
                    message: 'Please wait',
                }).then((res) => {
                    res.present();
                    if (navigator.onLine) {
                        this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/product_details', param).subscribe((response) => {
                            Object.keys(response).map(key => {
                                this.product_detail = response[key].product_details;
                                this.setData(response[key].product_details);
                                this.getPdfLink();
                                res.dismiss();
                            });
                        }, err => {
                            res.dismiss();
                            this.presentToastInternert("No internet connection. Please try again later.");
                            console.log("err.........", JSON.stringify(err));
                        });
                    }
                    else {
                        res.dismiss();
                        this.presentToastInternert("No internet connection. Please try again later.");
                    }
                });
            }
        });
    }
    get isShell() {
        return (this.details && this.details.isShell) ? true : false;
    }
    ngOnInit() { }
    ionViewWillLeave() { }
    ionViewWillEnter() {
        //  console.log("item", this.item);
        //  console.log("ionViewWillEnter productId", this.productId);
        let data = [];
        // this.storage.get('compareList').then((val) => {
        //   console.log("compareList", val);
        //   if (val.length != 0) {
        //     data = val;
        //     for (let index in data) {
        //       if (data[index].ID == this.productId) {
        //        console.log("Already added to compare");
        //        this.btnCompareText = "Go to Compare";
        //       }else {
        //         console.log("add to compare");
        //         this.btnCompareText = "Add to Compare";
        //        }
        //     }  
        //   } else{
        //     console.log("no compareList here");
        //     this.btnCompareText = "Add to Compare";
        //   }
        // });
    }
    getIndex() {
        this.slides.getActiveIndex().then((index) => {
            console.log("index", index);
            this.currentImageIndex = index;
        });
    }
    formatNumber(number) {
        return new Intl.NumberFormat('en-IN').format(number);
    }
    getPdfLink() {
        this.route.queryParams.subscribe(params => {
            this.shareDetails = params;
            if (params && params.title) {
                this.title = params.title;
                let param = {};
                if (params.title == "Air Conditioners") {
                    param = { air_conditioner: this.productId };
                }
                else if (params.title == "Air Coolers") {
                    param = { air_cooler: this.productId };
                }
                else if (params.title == "Air Purifiers") {
                    param = { air_purifier: this.productId };
                }
                else if (params.title == "Water Purifiers") {
                    param = { water_purifier: this.productId };
                }
                this.loadingController.create({
                    message: 'Please wait',
                }).then((res) => {
                    res.present();
                    if (navigator.onLine) {
                        this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/product_pdf/download', param).subscribe((response) => {
                            Object.keys(response).map(key => {
                                this.pdf_link = response[key].pdf_link;
                                res.dismiss();
                            });
                        }, err => {
                            res.dismiss();
                            console.log("err.........", JSON.stringify(err));
                        });
                    }
                    else {
                        res.dismiss();
                        console.log("no internat connection");
                    }
                });
            }
        });
    }
    setData(value) {
        this.productDetails = [];
        this.images = value.Image;
        for (let data in value) {
            if (data == "product_name") {
                this.productName = value[data][1];
                this.item.ProductTitle = value[data][1];
            }
            if (data == "id") {
                this.item.ID = value[data][1];
            }
            if (data == "sku_model_number") {
                this.item.SKUCode = value[data][1];
                this.SKUCode = value[data][1];
                if (this.platform.is('cordova')) {
                    this.firebaseAnalytics.logEvent('product_view', { product: this.SKUCode })
                        .then((res) => console.log(res))
                        .catch((error) => console.error(error));
                }
            }
            this.item.Image = this.images[0];
            if (data == "mrp") {
                this.item.MRP = value[data][1];
                this.MRP = value[data][1];
            }
            if (data == "mrp_product") {
                this.item.MRP = value[data][1];
                this.MRP = value[data][1];
            }
            if (data == "reasons_to_buy") {
                this.reasonsToBuyFlage = true;
                this.reasonsToBuyImageArray = value[data];
            }
            if (data == "brochures") {
                this.brochures = value[data];
            }
            if (data != "Image" && data != "id" && data != "mrp" && data != "brochures" && data != "reasons_to_buy" && data != "mrp_product") {
                let textValue = "";
                if (value[data][1] == "") {
                    textValue = "-";
                }
                else {
                    textValue = value[data][1];
                }
                let object = {
                    title: value[data][0],
                    value: textValue
                };
                this.productDetails.push(object);
            }
        }
        this.item.productSpecifications = this.productDetails;
        console.log("this.productDetails", this.productDetails);
        if (value.usp)
            this.uspList = value.usp;
        console.log("uspList", this.uspList);
        this.item.uspList = this.uspList;
        let data = [];
        this.storage.get('favouriteList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    for (let index in data) {
                        if (data[index].ID == this.item.ID) {
                            count = 1;
                            console.log("favouriteList 1");
                        }
                    }
                    if (count == 0) {
                        this.favouritesFlage = false;
                        console.log("favouriteList 2");
                    }
                    else {
                        this.favouritesFlage = true;
                        console.log("favouriteList 3");
                    }
                }
                else {
                    this.favouritesFlage = false;
                    console.log("favouriteList 5");
                }
            }
            else {
                this.favouritesFlage = false;
                console.log("favouriteList 6");
            }
        });
        let data_new = [];
        this.storage.get('compareList').then((val) => {
            console.log("compareList", val);
            if (val != null) {
                if (val.length != 0) {
                    data_new = val;
                    let count = 0;
                    for (let index in data_new) {
                        if (data_new[index].ID == this.item.ID) {
                            count = 1;
                        }
                    }
                    if (count == 0) {
                        this.btnCompareText = "Add to Compare";
                    }
                    else {
                        this.btnCompareText = "Go to Compare";
                    }
                }
                else {
                    this.btnCompareText = "Add to Compare";
                }
            }
            else {
                this.btnCompareText = "Add to Compare";
            }
        });
    }
    clickOnFavourites() {
        if (this.title == "Air Conditioners") {
            this.storeDataInAirConditioner(this.item);
        }
        else if (this.title == "Air Coolers") {
            this.storeDataInAirCooler(this.item);
        }
        else if (this.title == "Air Purifiers") {
            this.storeDataInAirPurifier(this.item);
        }
        else if (this.title == "Water Purifiers") {
            this.storeDataInWaterConditioner(this.item);
        }
        let data = [];
        let item = this.item;
        this.storage.get('favouriteList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    let removeItemIndex = 0;
                    for (let index in data) {
                        if (data[index].ID == item.ID) {
                            count = 1;
                            removeItemIndex = Number(index);
                        }
                    }
                    if (count == 0) {
                        this.favouritesFlage = true;
                        data.push(item);
                        this.storage.set('favouriteList', data);
                    }
                    else {
                        //remove product from Favourites list
                        this.favouritesFlage = false;
                        data.splice(removeItemIndex, 1);
                        this.storage.set('favouriteList', data);
                    }
                }
                else {
                    this.favouritesFlage = true;
                    data.push(item);
                    this.storage.set('favouriteList', data);
                }
            }
            else {
                this.favouritesFlage = true;
                data.push(item);
                this.storage.set('favouriteList', data);
            }
        });
    }
    storeDataInAirConditioner(item) {
        let data = [];
        this.storage.get('airConditionerList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    let removeItemIndex = 0;
                    for (let index in data) {
                        if (data[index].ID == item.ID) {
                            count = 1;
                            removeItemIndex = Number(index);
                        }
                    }
                    if (count == 0) {
                        data.push(item);
                        this.storage.set('airConditionerList', data);
                    }
                    else {
                        //remove product from Favourites list
                        data.splice(removeItemIndex, 1);
                        this.storage.set('airConditionerList', data);
                    }
                }
                else {
                    data.push(item);
                    this.storage.set('airConditionerList', data);
                }
            }
            else {
                data.push(item);
                this.storage.set('airConditionerList', data);
            }
        });
    }
    storeDataInAirCooler(item) {
        let data = [];
        this.storage.get('airCoolerList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    let removeItemIndex = 0;
                    for (let index in data) {
                        if (data[index].ID == item.ID) {
                            count = 1;
                            removeItemIndex = Number(index);
                        }
                    }
                    if (count == 0) {
                        data.push(item);
                        this.storage.set('airCoolerList', data);
                    }
                    else {
                        //remove product from Favourites list
                        data.splice(removeItemIndex, 1);
                        this.storage.set('airCoolerList', data);
                    }
                }
                else {
                    data.push(item);
                    this.storage.set('airCoolerList', data);
                }
            }
            else {
                data.push(item);
                this.storage.set('airCoolerList', data);
            }
        });
    }
    storeDataInAirPurifier(item) {
        let data = [];
        this.storage.get('airPurifierList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    let removeItemIndex = 0;
                    for (let index in data) {
                        if (data[index].ID == item.ID) {
                            count = 1;
                            removeItemIndex = Number(index);
                        }
                    }
                    if (count == 0) {
                        data.push(item);
                        this.storage.set('airPurifierList', data);
                    }
                    else {
                        //remove product from Favourites list
                        data.splice(removeItemIndex, 1);
                        this.storage.set('airPurifierList', data);
                    }
                }
                else {
                    data.push(item);
                    this.storage.set('airPurifierList', data);
                }
            }
            else {
                data.push(item);
                this.storage.set('airPurifierList', data);
            }
        });
    }
    storeDataInWaterConditioner(item) {
        let data = [];
        this.storage.get('waterPurifierList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    let removeItemIndex = 0;
                    for (let index in data) {
                        if (data[index].ID == item.ID) {
                            count = 1;
                            removeItemIndex = Number(index);
                        }
                    }
                    if (count == 0) {
                        data.push(item);
                        this.storage.set('waterPurifierList', data);
                    }
                    else {
                        //remove product from Favourites list
                        data.splice(removeItemIndex, 1);
                        this.storage.set('waterPurifierList', data);
                    }
                }
                else {
                    data.push(item);
                    this.storage.set('waterPurifierList', data);
                }
            }
            else {
                data.push(item);
                this.storage.set('waterPurifierList', data);
            }
        });
    }
    indicatorDownload() {
        let url = '';
        if (this.brochures == "") {
            url = this.pdf_link;
        }
        else {
            url = this.brochures;
        }
        var request = {
            uri: url,
            title: 'Blue Star',
            description: '',
            mimeType: 'application/pdf',
            visibleInDownloadsUi: true,
            notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_11__["NotificationVisibility"].VisibleNotifyCompleted,
            destinationInExternalPublicDir: {
                dirType: 'Download',
                subPath: this.item.SKUCode + '.pdf'
            }
        };
        if (this.platform.is("ios")) {
            const browser = this.inAppBrowser.create(url);
            this.firebaseAnalytics.logEvent('downloads_products', { product: this.SKUCode, category: this.title })
                .then((res) => console.log(res))
                .catch((error) => console.error(error));
        }
        else {
            this.loadingController.create({
                message: 'Please wait while downloading',
            }).then((res) => {
                res.present();
                this.downloader.download(request)
                    .then((location) => {
                    res.dismiss();
                    this.presentToast("Downloaded in device download folder");
                    this.firebaseAnalytics.logEvent('downloads_products', { product: this.SKUCode, category: this.title })
                        .then((res) => console.log(res))
                        .catch((error) => console.error(error));
                })
                    .catch((error) => {
                    res.dismiss();
                    console.error(error);
                });
            });
        }
    }
    addToCompare() {
        this.storage.get('compareList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    if (val.length < 3) {
                        this.storage.get('categaryTitle').then((val) => {
                            if (val != null) {
                                if (val == this.title) {
                                    this.openpopup(this.item);
                                }
                                else {
                                    // show popup not add to this categary 
                                    this.openpopupcomparesamecategory();
                                }
                            }
                        });
                    }
                    else {
                        // show popup limit
                        this.openCompareModal(this.item);
                    }
                }
                else {
                    this.openpopup(this.item);
                }
            }
            else {
                this.openpopup(this.item);
            }
        });
    }
    addToCompareDataInLocalStorage(item) {
        let data = [];
        this.storage.get('compareList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    for (let index in data) {
                        if (data[index].ID == item.ID) {
                            count = 1;
                            this.showAlertErrorAlreadyAdd();
                        }
                    }
                    if (count == 0) {
                        data.push(item);
                        this.storage.set('compareList', data);
                        this.btnCompareText = "Go to Compare";
                        console.log("Push 1");
                    }
                }
                else {
                    data.push(item);
                    this.storage.set('compareList', data);
                    this.btnCompareText = "Go to Compare";
                    console.log("Push 2");
                    this.storage.get('compareList').then((val) => {
                        console.log("compareList", val);
                    });
                }
            }
            else {
                data.push(item);
                this.storage.set('compareList', data);
                this.btnCompareText = "Go to Compare";
                console.log("Push 3");
            }
        });
        this.storage.set('categaryTitle', this.title);
    }
    alertAddComapreProduct() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: "Confirmation",
                header: "Are you sure you want to add compare product?",
                // message: "Are you sure you'd like to remove this product from My Products?",
                cssClass: 'variant-alert size-chooser',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            alert.dismiss();
                        }
                    }, {
                        text: 'OK',
                        handler: () => {
                            this.addToCompareDataInLocalStorage(this.item);
                            alert.dismiss();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    showAlertErrorAdd() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: "Confirmation",
                header: "Only 3 Product added in compare list",
                // message: "Are you sure you'd like to remove this product from My Products?",
                cssClass: 'variant-alert size-chooser',
                buttons: [
                    // {
                    //   text: 'Cancel',
                    //   role: 'cancel',
                    //   cssClass: 'secondary',
                    //   handler: () => {
                    //     alert.dismiss();
                    //   }
                    // }, 
                    {
                        text: 'OK',
                        handler: () => {
                            // this.removeFromFavourites(id)
                            alert.dismiss();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    showAlertErrorLimit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: "Confirmation",
                header: "Please add the product of the same category for comparison",
                // message: "Are you sure you'd like to remove this product from My Products?",
                cssClass: 'variant-alert size-chooser',
                buttons: [
                    // {
                    //   text: 'Cancel',
                    //   role: 'cancel',
                    //   cssClass: 'secondary',
                    //   handler: () => {
                    //     alert.dismiss();
                    //   }
                    // }, 
                    {
                        text: 'OK',
                        handler: () => {
                            // this.removeFromFavourites(id)
                            alert.dismiss();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    showAlertErrorAlreadyAdd() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let navigationExtras = {
                queryParams: {
                    title: this.categoryName,
                }
            };
            this.router.navigate(['compare/'], navigationExtras);
            // const alert = await this.alertController.create({
            //   header: "Product already added to compare list",
            //   cssClass: 'variant-alert size-chooser',
            //   buttons: [ 
            //     {
            //       text: 'OK',
            //       handler: () => {
            //         alert.dismiss();
            //       }
            //     }
            //   ]
            // });
            // await alert.present();
        });
    }
    presentToast(text) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: text,
                duration: 7000,
                position: 'bottom',
                cssClass: "msg-align",
            });
            toast.present();
        });
    }
    presentToastInternert(msg) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msg,
                duration: 8000,
                position: 'bottom',
                cssClass: "msg-align",
            });
            toast.present();
        });
    }
    toDataUrl(url, _this, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                callback(reader.result, _this);
            };
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }
    shareMethod() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.toDataUrl(this.item.Image, this, function (myBase64, _this) {
                _this.callMethod(myBase64);
            });
        });
    }
    callMethod(myBase64) {
        let body = `Hi there, check out this product by Blue Star!\n\nProduct Category: ${this.title}\nProduct Title: ${this.item.ProductTitle}\nSKU Code: ${this.item.SKUCode}\nClick here to get the product specification: ${this.pdf_link.replace(/\s/g, "%20")}`;
        this.socialSharing.share(body, this.item.ProductTitle.replace(/\%/g, " pc"), myBase64, null)
            .then(sucess => {
            this.firebaseAnalytics.logEvent('share_products', { product: this.item.SKUCode, category: this.title })
                .then((res) => console.log(res))
                .catch((error) => console.error(error));
        })
            .catch(err => {
            console.log(err);
        });
    }
    openCompareModal(item) {
        let data = [];
        this.storage.get('compareList').then((val) => {
            data = val;
            let count = 0;
            for (let index in data) {
                if (data[index].ID == item.ID) {
                    count = 1;
                    this.showAlertErrorAlreadyAdd();
                }
            }
            if (count == 0) {
                this.openModel();
            }
        });
    }
    openModel() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _popup_compare_popup_compare__WEBPACK_IMPORTED_MODULE_13__["PopupCompare"],
                componentProps: {},
                cssClass: "my-modal",
                backdropDismiss: false
            });
            return yield modal.present();
        });
    }
    openpopupcomparesamecategory() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _popup_compare_same_category_popup_compare_same_category__WEBPACK_IMPORTED_MODULE_15__["popupcomparesamecategory"],
                componentProps: {},
                cssClass: "my-modal",
                backdropDismiss: false
            });
            return yield modal.present();
        });
    }
    openpopup(item) {
        let data = [];
        this.storage.get('compareList').then((val) => {
            data = val;
            let count = 0;
            for (let index in data) {
                if (data[index].ID == item.ID) {
                    count = 1;
                    // console.log("if****");
                    this.showAlertErrorAlreadyAdd();
                }
            }
            if (count == 0) {
                // console.log("enter count 0");
                // this.openModel();
                // this.alertAddComapreProduct();
                this.addToCompareDataInLocalStorage(this.item);
            }
        });
    }
    // async openZoomPopup(images) {
    //   const modal = await this.modalCtrl.create({
    //     component: ZoomPopup,
    //     componentProps: {images: images}
    //   });
    //   return await modal.present();
    // }
    openZoomPopup(images) {
        let navigationExtras = {
            queryParams: {
                images: images,
                currentImageIndex: this.currentImageIndex
            }
        };
        this.router.navigate(['zoom'], navigationExtras);
    }
    openPopupReasonToBuy(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _popup_reason_to_buy_popup_reason_to_buy__WEBPACK_IMPORTED_MODULE_14__["PopupReasonToBuy"],
                componentProps: {
                    item: item
                }
            });
            return yield modal.present();
        });
    }
    // pressShowPopup(item){
    //   setTimeout(() => {
    //    this.openPopupReasonToBuy(item);
    //   }, 1000);
    // }
    close() {
        this.showPopup = false;
    }
    showUSPPopup(item) {
        this.showPopup = true;
        this.USPTitle = item[0];
        this.USPDescription = item[1];
        this.content.scrollToPoint(0, 0, 1000);
    }
    showComparePage() {
        let navigationExtras = {
            queryParams: {
                title: this.categoryName,
            }
        };
        this.router.navigate(['compare/'], navigationExtras);
    }
}
ProductDetailsPage.ɵfac = function ProductDetailsPage_Factory(t) { return new (t || ProductDetailsPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_11__["Downloader"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__["AndroidPermissions"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__["SocialSharing"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_categories_categories_page__WEBPACK_IMPORTED_MODULE_7__["CategoriesPage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_9__["FirebaseAnalytics"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__["InAppBrowser"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
ProductDetailsPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ProductDetailsPage, selectors: [["app-product-details"]], viewQuery: function ProductDetailsPage_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_c0, true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstaticViewQuery"](_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.slides = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.content = _t.first);
    } }, hostVars: 2, hostBindings: function ProductDetailsPage_HostBindings(rf, ctx) { if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("is-shell", ctx.isShell);
    } }, decls: 10, vars: 5, consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "app/categories/product"], ["src", "assets/logo.png", 1, "logo"], ["slot", "end"], ["name", "heart-outline", 1, "heart_icon", 3, "routerLink"], ["name", "home-outline", 1, "menu_icon", 3, "routerLink"], [1, "fashion-details-content"], [4, "ngIf"], [1, "main-title-row"], [1, "main-title"], ["class", "slider-row", 4, "ngIf"], [1, "product-title"], ["size", "8", 2, "margin-top", "5px", "font-weight", "600"], ["style", "font-weight: 600;color: rgb(9, 80, 157);margin-top: 5px;", 4, "ngIf"], ["size", "4", 2, "text-align", "right", "color", "#09509d", 3, "click"], ["src", "assets/share.png", 1, "icon-share"], [1, "details-purchase-options-row", 2, "padding-bottom", "0px"], ["size", "12", 1, "btn-padding-download"], ["expand", "block", 1, "select-variant-btn", 2, "font-weight", "600", "margin", "0 auto", "width", "50%", 3, "click"], [1, "details-description-wrapper", 2, "padding-bottom", "10px", "padding-top", "10px"], [1, "detail-title"], ["style", "font-size: 15px;", 3, "background", 4, "ngFor", "ngForOf"], [1, "slider-row"], ["size", "12", 4, "ngIf"], ["size", "12"], [1, "details-slides", 3, "options", "ionSlideDidChange"], ["slides", ""], ["class", "", 4, "ngFor", "ngForOf"], ["name", "heart-outline", 1, "icon-heart-black", 3, "click"], ["name", "heart", 1, "icon-heart-red", 3, "click"], [1, ""], [1, "slide-inner-row", 3, "click"], ["animation", "spinner", 1, "showcase-image", 2, "height", "127px", 3, "ngClass", "src"], ["pager", "true", 1, "details-slides", 3, "options", "ionSlideDidChange"], [2, "font-weight", "600", "color", "rgb(9, 80, 157)", "margin-top", "5px"], [2, "font-size", "15px"], ["size", "6", 2, "padding-left", "10px", "padding-right", "10px"]], template: function ProductDetailsPage_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-header");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-toolbar", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-buttons", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "ion-back-button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "img", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "ion-buttons", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "ion-icon", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "ion-icon", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "ion-content", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, ProductDetailsPage_ng_container_9_Template, 25, 6, "ng-container", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](3, _c2));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](4, _c3));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.product_detail);
    } }, directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonIcon"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["RouterLinkDelegate"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], _angular_common__WEBPACK_IMPORTED_MODULE_16__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonCol"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonButton"], _angular_common__WEBPACK_IMPORTED_MODULE_16__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlide"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_17__["ImageShellComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_16__["NgClass"]], styles: ["[_nghost-%COMP%] {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background);\n  --page-swiper-pagination-space: 30px;\n  --page-swiper-pagination-height: 18px;\n  --page-pagination-bullet-size: 10px;\n  --page-options-gutter: calc(var(--page-margin) / 2);\n  --page-related-items-gutter: calc(var(--page-margin) / 2);\n  --page-color: #cb328f;\n}\n\n.fashion-details-content[_ngcontent-%COMP%] {\n  --background: var(--page-background);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .slider-row[_ngcontent-%COMP%] {\n  border: 1px solid #09509d;\n  margin: 10px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .slider-row[_ngcontent-%COMP%]   .details-slides[_ngcontent-%COMP%] {\n  --bullet-background: #09509d;\n  --bullet-background-active: #09509d;\n  height: 100%;\n  width: 100%;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .slider-row[_ngcontent-%COMP%]   .details-slides[_ngcontent-%COMP%]   .slide-inner-row[_ngcontent-%COMP%] {\n  height: 100%;\n  width: 100%;\n  padding: 0px;\n  padding-bottom: var(--page-swiper-pagination-space);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .slider-row[_ngcontent-%COMP%]   .showcase-image[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .slider-row[_ngcontent-%COMP%]   .showcase-image.centered-image[_ngcontent-%COMP%] {\n  background-size: auto;\n  background-position: center;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .slider-row[_ngcontent-%COMP%]   .showcase-image2[_ngcontent-%COMP%] {\n  width: 100%;\n  height: auto;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .slider-row[_ngcontent-%COMP%]   .showcase-image2.centered-image[_ngcontent-%COMP%] {\n  background-size: auto;\n  background-position: center;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .description-wrapper[_ngcontent-%COMP%] {\n  padding: var(--page-margin) var(--page-margin) 0px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .description-wrapper[_ngcontent-%COMP%]   .details-name[_ngcontent-%COMP%] {\n  font-size: 18px;\n  font-weight: 500;\n  margin: 0px 0px 5px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .description-wrapper[_ngcontent-%COMP%]   .details-brand[_ngcontent-%COMP%] {\n  display: block;\n  text-transform: uppercase;\n  font-size: 14px;\n  font-weight: 400;\n  margin: 0px 0px var(--page-margin);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .description-wrapper[_ngcontent-%COMP%]   .details-price[_ngcontent-%COMP%] {\n  font-size: 18px;\n  font-weight: 500;\n  margin: 0px;\n  color: var(--page-color);\n  display: inline-block;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .description-wrapper[_ngcontent-%COMP%]   .details-sale-price[_ngcontent-%COMP%] {\n  color: var(--ion-color-medium-shade);\n  text-decoration: line-through;\n  display: inline-block;\n  margin-left: var(--page-margin);\n  font-size: 14px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-purchase-options-row[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: var(--page-options-gutter);\n  padding-top: 0px;\n  padding-left: calc(var(--page-margin) - var(--page-options-gutter));\n  padding-right: calc(var(--page-margin) - var(--page-options-gutter));\n  padding-bottom: calc(var(--page-margin) * 2);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-purchase-options-row[_ngcontent-%COMP%]   .aux-action-col[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-purchase-options-row[_ngcontent-%COMP%]   .aux-action-col[_ngcontent-%COMP%]   .size-chart-btn[_ngcontent-%COMP%] {\n  margin: 0px;\n  height: 25px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-purchase-options-row[_ngcontent-%COMP%]   .main-call-to-action-col[_ngcontent-%COMP%] {\n  padding-bottom: 0px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-purchase-options-row[_ngcontent-%COMP%]   .select-variant-btn[_ngcontent-%COMP%] {\n  margin: 0px;\n  text-transform: capitalize;\n  font-weight: 600;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-purchase-options-row[_ngcontent-%COMP%]   .add-to-cart-btn[_ngcontent-%COMP%] {\n  margin: 0px;\n  margin-top: var(--page-margin);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .detail-title[_ngcontent-%COMP%] {\n  font-size: 16px;\n  font-weight: 600;\n  color: var(--ion-color-dark-tint);\n  margin: 0px;\n  margin-bottom: calc(var(--page-margin) / 2);\n  text-align: center;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-description-wrapper[_ngcontent-%COMP%] {\n  padding-top: 0px;\n  padding-left: var(--page-margin);\n  padding-right: var(--page-margin);\n  padding-bottom: calc(var(--page-margin) * 2);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .details-description-wrapper[_ngcontent-%COMP%]   .details-description[_ngcontent-%COMP%] {\n  margin: 0px;\n  line-height: 1.3;\n  font-size: 14px;\n  color: var(--ion-color-medium-shade);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .detail-alt-title[_ngcontent-%COMP%] {\n  text-align: center;\n  font-size: 18px;\n  font-weight: 400;\n  text-transform: uppercase;\n  color: var(--ion-color-dark-shade);\n  margin: 0px;\n  margin-bottom: var(--page-margin);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%] {\n  padding-top: 0px;\n  padding-left: var(--page-margin);\n  padding-right: var(--page-margin);\n  padding-bottom: calc(var(--page-margin) * 2);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: 0px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%] {\n  margin-bottom: calc(var(--page-margin) * 2);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]:nth-child(odd) {\n  padding-right: var(--page-related-items-gutter);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]:nth-child(even) {\n  padding-left: var(--page-related-items-gutter);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%] {\n  border: 1px solid var(--ion-color-light-shade);\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%]   .image-anchor[_ngcontent-%COMP%] {\n  display: block;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]   .item-details-wrapper[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  padding: 5px 5px 0px;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%] {\n  margin: 0px;\n  margin-bottom: calc(var(--page-margin) / 2);\n  font-size: 14px;\n  font-weight: 400;\n  text-align: center;\n  min-width: 90%;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  overflow: hidden;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%]   .name-anchor[_ngcontent-%COMP%] {\n  color: var(--ion-color-dark-tint);\n  display: block;\n  text-decoration: none;\n}\n\n.fashion-details-content[_ngcontent-%COMP%]   .related-products-wrapper[_ngcontent-%COMP%]   .related-products-list[_ngcontent-%COMP%]   .related-product-item[_ngcontent-%COMP%]   .item-sale-price[_ngcontent-%COMP%] {\n  display: block;\n  font-weight: 400;\n  color: var(--page-color);\n  font-size: 16px;\n  text-align: center;\n  min-width: 30%;\n}\n\n[_nghost-%COMP%]     .details-slides .swiper-pagination {\n  height: var(--page-swiper-pagination-height);\n  line-height: 1;\n  bottom: calc((var(--page-swiper-pagination-space) - var(--page-swiper-pagination-height) ) / 2);\n}\n\n[_nghost-%COMP%]     .details-slides .swiper-pagination .swiper-pagination-bullet {\n  width: var(--page-pagination-bullet-size);\n  height: var(--page-pagination-bullet-size);\n}\n\n  .variant-alert {\n  --select-alert-color: #000;\n  --select-alert-background: #FFF;\n  --select-alert-margin: 16px;\n  --select-alert-color: var(--ion-color-lightest);\n  --select-alert-background: var(--ion-color-primary);\n  --select-alert-margin: var(--app-fair-margin);\n}\n\n  .variant-alert .alert-head {\n  padding-top: calc((var(--select-alert-margin) / 4) * 3);\n  padding-bottom: calc((var(--select-alert-margin) / 4) * 3);\n  -webkit-padding-start: var(--select-alert-margin);\n          padding-inline-start: var(--select-alert-margin);\n  -webkit-padding-end: var(--select-alert-margin);\n          padding-inline-end: var(--select-alert-margin);\n}\n\n  .variant-alert .alert-title {\n  color: var(--select-alert-color);\n}\n\n  .variant-alert .alert-head,   .variant-alert .alert-message {\n  background-color: var(--select-alert-background);\n}\n\n  .variant-alert .alert-wrapper.sc-ion-alert-ios .alert-title {\n  margin: 0px;\n}\n\n  .variant-alert .alert-wrapper.sc-ion-alert-md .alert-title {\n  font-size: 18px;\n  font-weight: 400;\n}\n\n  .variant-alert .alert-wrapper.sc-ion-alert-md .alert-button {\n  --padding-top: 0;\n  --padding-start: 0.9em;\n  --padding-end: 0.9em;\n  --padding-bottom: 0;\n  height: 2.1em;\n  font-size: 13px;\n}\n\n  .variant-alert .alert-message {\n  display: none;\n}\n\n.main-title-row[_ngcontent-%COMP%] {\n  margin: 10px;\n}\n\n.main-title[_ngcontent-%COMP%] {\n  font-size: 17px;\n  color: #09509d;\n  font-weight: bold;\n}\n\n.text-align[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\nion-header[_ngcontent-%COMP%]   ion-toolbar[_ngcontent-%COMP%]:first-of-type {\n  text-align: center;\n}\n\n.logo[_ngcontent-%COMP%] {\n  width: 70%;\n  margin-top: 5px;\n}\n\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 10px;\n}\n\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n\n.icon-heart-red[_ngcontent-%COMP%] {\n  float: right;\n  padding-right: 5px;\n  color: #09509d;\n  font-size: 25px;\n}\n\n.icon-heart-black[_ngcontent-%COMP%] {\n  float: right;\n  padding-right: 5px;\n  color: #09509d;\n  font-size: 25px;\n}\n\n.icon-share[_ngcontent-%COMP%] {\n  width: 30px;\n  margin-bottom: -7px;\n  padding-right: 3px;\n}\n\n.product-title[_ngcontent-%COMP%] {\n  border-bottom: 1px solid #eae4e4;\n  margin: 10px 15px 15px 15px;\n}\n\nul[_ngcontent-%COMP%] {\n  padding-left: 25px;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n\nli[_ngcontent-%COMP%] {\n  padding-bottom: 10px;\n}\n\n.close_icon[_ngcontent-%COMP%] {\n  font-size: 1.8em;\n  float: right;\n  color: white;\n}\n\n.row-popup[_ngcontent-%COMP%] {\n  border: 1px solid #09509d;\n  border-radius: 2%;\n  margin: 10px;\n  background-color: white;\n  position: absolute;\n  top: 30%;\n  z-index: 9;\n  width: -webkit-fill-available;\n}\n\n.col-background[_ngcontent-%COMP%] {\n  background-color: #09509d;\n}\n\n.col-margin[_ngcontent-%COMP%] {\n  margin-bottom: 10px;\n}\n\n.title-popup[_ngcontent-%COMP%] {\n  padding-left: 5px;\n  padding-top: 5px;\n  font-weight: 600;\n  color: white;\n}\n\n.popup-description[_ngcontent-%COMP%] {\n  padding: 20px 10px 20px 10px;\n}\n\n.btn-padding-compare[_ngcontent-%COMP%] {\n  padding-right: 0px;\n  padding-left: 0px;\n}\n\n.btn-padding-download[_ngcontent-%COMP%] {\n  padding-right: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvcHJvZHVjdC9kZXRhaWxzL3N0eWxlcy9wcm9kdWN0LWRldGFpbHMucGFnZS5zY3NzIiwic3JjL2FwcC9wcm9kdWN0L2RldGFpbHMvc3R5bGVzL3Byb2R1Y3QtZGV0YWlscy5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvdGhlbWUvbWl4aW5zL2lucHV0cy9zZWxlY3QtYWxlcnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNFLHFDQUFBO0VBQ0Esd0NBQUE7RUFFQSxvQ0FBQTtFQUNBLHFDQUFBO0VBQ0EsbUNBQUE7RUFFQSxtREFBQTtFQUNBLHlEQUFBO0VBRUEscUJBQUE7QUNORjs7QURVQTtFQUNFLG9DQUFBO0FDUEY7O0FEU0U7RUFFRSx5QkFBQTtFQUNBLFlBQUE7QUNSSjs7QURTSTtFQUlFLDRCQUFBO0VBQ0EsbUNBQUE7RUFFQSxZQUFBO0VBQ0EsV0FBQTtBQ1hOOztBRGFNO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsbURBQUE7QUNaUjs7QURnQkk7RUFDRSxXQUFBO0FDZE47O0FEZ0JNO0VBQ0UscUJBQUE7RUFDQSwyQkFBQTtBQ2RSOztBRGtCSTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDaEJOOztBRGlCTTtFQUNFLHFCQUFBO0VBQ0EsMkJBQUE7QUNmUjs7QURvQkU7RUFDRSxrREFBQTtBQ2xCSjs7QURvQkk7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQ2xCTjs7QURxQkk7RUFDRSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQ0FBQTtBQ25CTjs7QURzQkk7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0Esd0JBQUE7RUFDQSxxQkFBQTtBQ3BCTjs7QUR1Qkk7RUFDRSxvQ0FBQTtFQUNBLDZCQUFBO0VBQ0EscUJBQUE7RUFDQSwrQkFBQTtFQUNBLGVBQUE7QUNyQk47O0FEeUJFO0VBQ0UscURBQUE7RUFFQSxnQkFBQTtFQUNBLG1FQUFBO0VBQ0Esb0VBQUE7RUFDQSw0Q0FBQTtBQ3hCSjs7QUQwQkk7RUFDRSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDeEJOOztBRDBCTTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDeEJSOztBRDRCSTtFQUNFLG1CQUFBO0FDMUJOOztBRDZCSTtFQUNFLFdBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0FDM0JOOztBRDhCSTtFQUNFLFdBQUE7RUFDQSw4QkFBQTtBQzVCTjs7QURnQ0U7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQ0FBQTtFQUNBLFdBQUE7RUFDQSwyQ0FBQTtFQUNBLGtCQUFBO0FDOUJKOztBRGlDRTtFQUNFLGdCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQ0FBQTtFQUNBLDRDQUFBO0FDL0JKOztBRGlDSTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxvQ0FBQTtBQy9CTjs7QURtQ0U7RUFDRSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0NBQUE7RUFDQSxXQUFBO0VBQ0EsaUNBQUE7QUNqQ0o7O0FEb0NFO0VBQ0UsZ0JBQUE7RUFDQSxnQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsNENBQUE7QUNsQ0o7O0FEb0NJO0VBQ0UsOEJBQUE7QUNsQ047O0FEb0NNO0VBQ0UsMkNBQUE7QUNsQ1I7O0FEb0NRO0VBQ0UsK0NBQUE7QUNsQ1Y7O0FEcUNRO0VBQ0UsOENBQUE7QUNuQ1Y7O0FEc0NRO0VBQ0UsOENBQUE7QUNwQ1Y7O0FEc0NVO0VBQ0UsY0FBQTtBQ3BDWjs7QUR3Q1E7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUVBLG9CQUFBO0FDdkNWOztBRDBDUTtFQUNFLFdBQUE7RUFDQSwyQ0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUVBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ3pDVjs7QUQyQ1U7RUFDRSxpQ0FBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtBQ3pDWjs7QUQ2Q1E7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUMzQ1Y7O0FEdURFO0VBQ0UsNENBQUE7RUFDQSxjQUFBO0VBRUEsK0ZBQUE7QUNyREo7O0FEdURJO0VBQ0UseUNBQUE7RUFDQSwwQ0FBQTtBQ3JETjs7QUQ0REE7RUU3UEUsMEJBQUE7RUFDQSwrQkFBQTtFQUNBLDJCQUFBO0VGK1BBLCtDQUFBO0VBQ0EsbURBQUE7RUFDQSw2Q0FBQTtBQ3pERjs7QUN0TUU7RUFDRSx1REFBQTtFQUNBLDBEQUFBO0VBQ0EsaURBQUE7VUFBQSxnREFBQTtFQUNBLCtDQUFBO1VBQUEsOENBQUE7QUR3TUo7O0FDck1FO0VBQ0UsZ0NBQUE7QUR1TUo7O0FDcE1FOztFQUVFLGdEQUFBO0FEc01KOztBQ2pNSTtFQUNFLFdBQUE7QURtTU47O0FDN0xJO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FEK0xOOztBQzVMSTtFQUVFLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBRUEsYUFBQTtFQUNBLGVBQUE7QUQ0TE47O0FEK0JFO0VBQ0UsYUFBQTtBQzdCSjs7QURnQ0E7RUFDRSxZQUFBO0FDN0JGOztBRCtCQTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUM1QkY7O0FEOEJBO0VBQ0Usa0JBQUE7QUMzQkY7O0FEOEJBO0VBQ0Usa0JBQUE7QUMzQkY7O0FEOEJBO0VBRUUsVUFBQTtFQUNBLGVBQUE7QUM1QkY7O0FEOEJBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDM0JGOztBRDZCQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQzFCRjs7QUQ0QkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUN6QkY7O0FENEJBO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGVBQUE7QUMxQkY7O0FENEJBO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGVBQUE7QUMxQkY7O0FENEJBO0VBTUUsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUM5QkY7O0FEZ0NBO0VBQ0UsZ0NBQUE7RUFDQSwyQkFBQTtBQzdCRjs7QURnQ0E7RUFFRSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQzlCRjs7QURpQ0E7RUFDRSxvQkFBQTtBQzlCRjs7QURnQ0E7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FDN0JGOztBRCtCQTtFQUNFLHlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0VBQ0EsNkJBQUE7QUM1QkY7O0FEK0JBO0VBQ0UseUJBQUE7QUM1QkY7O0FEK0JBO0VBRUUsbUJBQUE7QUM3QkY7O0FEZ0NBO0VBQ0MsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQzdCRDs7QURnQ0E7RUFDRSw0QkFBQTtBQzdCRjs7QURnQ0M7RUFDQyxrQkFBQTtFQUNBLGlCQUFBO0FDN0JGOztBRGdDQztFQUNDLGtCQUFBO0FDN0JGIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdC9kZXRhaWxzL3N0eWxlcy9wcm9kdWN0LWRldGFpbHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uLy4uL3RoZW1lL21peGlucy9pbnB1dHMvc2VsZWN0LWFsZXJ0XCI7XG5cbi8vIEN1c3RvbSB2YXJpYWJsZXNcbi8vIE5vdGU6ICBUaGVzZSBvbmVzIHdlcmUgYWRkZWQgYnkgdXMgYW5kIGhhdmUgbm90aGluZyB0byBkbyB3aXRoIElvbmljIENTUyBDdXN0b20gUHJvcGVydGllc1xuOmhvc3Qge1xuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xuXG4gIC0tcGFnZS1zd2lwZXItcGFnaW5hdGlvbi1zcGFjZTogMzBweDtcbiAgLS1wYWdlLXN3aXBlci1wYWdpbmF0aW9uLWhlaWdodDogMThweDtcbiAgLS1wYWdlLXBhZ2luYXRpb24tYnVsbGV0LXNpemU6IDEwcHg7XG5cbiAgLS1wYWdlLW9wdGlvbnMtZ3V0dGVyOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAtLXBhZ2UtcmVsYXRlZC1pdGVtcy1ndXR0ZXI6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG5cbiAgLS1wYWdlLWNvbG9yOiAjY2IzMjhmO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcblxuICAuc2xpZGVyLXJvdyB7XG4gICAgLy8gYm9yZGVyOiAxcHggc29saWQgdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDk1MDlkO1xuICAgIG1hcmdpbjogMTBweDtcbiAgICAuZGV0YWlscy1zbGlkZXMge1xuICAgICAgLy8gLS1idWxsZXQtYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWRhcmstdGludCk7XG4gICAgICAvLyAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogdmFyKC0taW9uLWNvbG9yLWRhcmstdGludCk7XG4gICAgICBcbiAgICAgIC0tYnVsbGV0LWJhY2tncm91bmQ6ICMwOTUwOWQ7XG4gICAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTogIzA5NTA5ZDtcblxuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgd2lkdGg6IDEwMCU7XG5cbiAgICAgIC5zbGlkZS1pbm5lci1yb3cge1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBwYWRkaW5nOiAwcHg7XG4gICAgICAgIC8vIC5zd2lwZXItcGFnaW5hdGlvbiBzcGFjZVxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogdmFyKC0tcGFnZS1zd2lwZXItcGFnaW5hdGlvbi1zcGFjZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLnNob3djYXNlLWltYWdlIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuXG4gICAgICAmLmNlbnRlcmVkLWltYWdlIHtcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBhdXRvO1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLnNob3djYXNlLWltYWdlMiB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogYXV0bztcbiAgICAgICYuY2VudGVyZWQtaW1hZ2Uge1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGF1dG87XG4gICAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAuZGVzY3JpcHRpb24td3JhcHBlciB7XG4gICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIHZhcigtLXBhZ2UtbWFyZ2luKSAwcHg7XG5cbiAgICAuZGV0YWlscy1uYW1lIHtcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICBtYXJnaW46IDBweCAwcHggNXB4O1xuICAgIH1cblxuICAgIC5kZXRhaWxzLWJyYW5kIHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICBtYXJnaW46IDBweCAwcHggdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIH1cblxuICAgIC5kZXRhaWxzLXByaWNlIHtcbiAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICBtYXJnaW46IDBweDtcbiAgICAgIGNvbG9yOiB2YXIoLS1wYWdlLWNvbG9yKTtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB9XG5cbiAgICAuZGV0YWlscy1zYWxlLXByaWNlIHtcbiAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlKTtcbiAgICAgIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgbWFyZ2luLWxlZnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG4gIH1cblxuICAuZGV0YWlscy1wdXJjaGFzZS1vcHRpb25zLXJvdyB7XG4gICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogdmFyKC0tcGFnZS1vcHRpb25zLWd1dHRlcik7XG5cbiAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgIHBhZGRpbmctbGVmdDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLSB2YXIoLS1wYWdlLW9wdGlvbnMtZ3V0dGVyKSk7XG4gICAgcGFkZGluZy1yaWdodDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLSB2YXIoLS1wYWdlLW9wdGlvbnMtZ3V0dGVyKSk7XG4gICAgcGFkZGluZy1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMik7XG5cbiAgICAuYXV4LWFjdGlvbi1jb2wge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgICBwYWRkaW5nLXRvcDogMHB4O1xuICAgICAgcGFkZGluZy1ib3R0b206IDBweDtcblxuICAgICAgLnNpemUtY2hhcnQtYnRuIHtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAubWFpbi1jYWxsLXRvLWFjdGlvbi1jb2wge1xuICAgICAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgICB9XG5cbiAgICAuc2VsZWN0LXZhcmlhbnQtYnRuIHtcbiAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7IFxuICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICB9XG5cbiAgICAuYWRkLXRvLWNhcnQtYnRuIHtcbiAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgbWFyZ2luLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIH1cbiAgfVxuXG4gIC5kZXRhaWwtdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyay10aW50KTtcbiAgICBtYXJnaW46IDBweDtcbiAgICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgfVxuXG4gIC5kZXRhaWxzLWRlc2NyaXB0aW9uLXdyYXBwZXIge1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1sZWZ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIHBhZGRpbmctYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpO1xuXG4gICAgLmRldGFpbHMtZGVzY3JpcHRpb24ge1xuICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICBsaW5lLWhlaWdodDogMS4zO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xuICAgIH1cbiAgfVxuXG4gIC5kZXRhaWwtYWx0LXRpdGxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmstc2hhZGUpO1xuICAgIG1hcmdpbjogMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgfVxuXG4gIC5yZWxhdGVkLXByb2R1Y3RzLXdyYXBwZXIge1xuICAgIHBhZGRpbmctdG9wOiAwcHg7XG4gICAgcGFkZGluZy1sZWZ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAgIHBhZGRpbmctYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpO1xuXG4gICAgLnJlbGF0ZWQtcHJvZHVjdHMtbGlzdCB7XG4gICAgICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG5cbiAgICAgIC5yZWxhdGVkLXByb2R1Y3QtaXRlbSB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMik7XG5cbiAgICAgICAgJjpudGgtY2hpbGQob2RkKSB7XG4gICAgICAgICAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1yZWxhdGVkLWl0ZW1zLWd1dHRlcik7XG4gICAgICAgIH1cblxuICAgICAgICAmOm50aC1jaGlsZChldmVuKSB7XG4gICAgICAgICAgcGFkZGluZy1sZWZ0OiB2YXIoLS1wYWdlLXJlbGF0ZWQtaXRlbXMtZ3V0dGVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5pdGVtLWltYWdlLXdyYXBwZXIge1xuICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG5cbiAgICAgICAgICAuaW1hZ2UtYW5jaG9yIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5pdGVtLWRldGFpbHMtd3JhcHBlciB7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG5cbiAgICAgICAgICBwYWRkaW5nOiA1cHggNXB4IDBweDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5pdGVtLW5hbWUge1xuICAgICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIG1pbi13aWR0aDogOTAlO1xuXG4gICAgICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuXG4gICAgICAgICAgLm5hbWUtYW5jaG9yIHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyay10aW50KTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC5pdGVtLXNhbGUtcHJpY2Uge1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgICAgY29sb3I6IHZhcigtLXBhZ2UtY29sb3IpO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgbWluLXdpZHRoOiAzMCU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuXG4vLyBJU1NVRTogLnN3aXBlci1wYWdnaW5hdGlvbiBnZXRzIHJlbmRlcmVkIGR5bmFtaWNhbGx5LiBUaGF0IHByZXZlbnRzIHN0eWxpbmcgdGhlIGVsZW1lbnRzIHdoZW4gdXNpbmcgdGhlIGRlZmF1bHQgQW5ndWxhciBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXG4vLyAgICAgICAgKEFuZ3VsYXIgZG9lc24ndCBhZGQgYW4gJ19uZ2NvbnRlbnQnIGF0dHJpYnV0ZSB0byB0aGUgLnN3aXBlci1wYWdnaW5hdGlvbiBiZWNhdXNlIGl0J3MgZHluYW1pY2FsbHkgcmVuZGVyZWQpXG4vLyBGSVg6ICAgU2VlOiBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMzYyNjUwNzIvMTExNjk1OVxuOmhvc3QgOjpuZy1kZWVwIC5kZXRhaWxzLXNsaWRlcyB7XG4gIC5zd2lwZXItcGFnaW5hdGlvbiB7XG4gICAgaGVpZ2h0OiB2YXIoLS1wYWdlLXN3aXBlci1wYWdpbmF0aW9uLWhlaWdodCk7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgLy8gLnN3aXBlci1wYWdpbmF0aW9uIGlzIDE4cHggaGVpZ2h0LCAuc2xpZGUtaW5uZXItcm93IGhhcyA0MHB4IG9mIHBhZGRpbmctYm90dG9tID0+IGJvdHRvbSA9ICg0MHB4IC0gMThweCkvMiA9IDExcHhcbiAgICBib3R0b206IGNhbGMoKHZhcigtLXBhZ2Utc3dpcGVyLXBhZ2luYXRpb24tc3BhY2UpIC0gdmFyKC0tcGFnZS1zd2lwZXItcGFnaW5hdGlvbi1oZWlnaHQpICkgLyAyKTtcblxuICAgIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQge1xuICAgICAgd2lkdGg6IHZhcigtLXBhZ2UtcGFnaW5hdGlvbi1idWxsZXQtc2l6ZSk7XG4gICAgICBoZWlnaHQ6IHZhcigtLXBhZ2UtcGFnaW5hdGlvbi1idWxsZXQtc2l6ZSk7XG4gICAgfVxuICB9XG59XG5cbi8vIEFsZXJ0cyBhbmQgaW4gZ2VuZXJhbCBhbGwgb3ZlcmxheXMgYXJlIGF0dGFjaGVkIHRvIHRoZSBib2R5IG9yIGlvbi1hcHAgZGlyZWN0bHlcbi8vIFdlIG5lZWQgdG8gdXNlIDo6bmctZGVlcCB0byBhY2Nlc3MgaXQgZnJvbSBoZXJlXG46Om5nLWRlZXAgLnZhcmlhbnQtYWxlcnQge1xuICBAaW5jbHVkZSBzZWxlY3QtYWxlcnQoKTtcblxuICAvLyBWYXJpYWJsZXMgc2hvdWxkIGJlIGluIGEgZGVlcGVyIHNlbGVjdG9yIG9yIGFmdGVyIHRoZSBtaXhpbiBpbmNsdWRlIHRvIG92ZXJyaWRlIGRlZmF1bHQgdmFsdWVzXG4gIC0tc2VsZWN0LWFsZXJ0LWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAtLXNlbGVjdC1hbGVydC1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIC0tc2VsZWN0LWFsZXJ0LW1hcmdpbjogdmFyKC0tYXBwLWZhaXItbWFyZ2luKTtcblxuICAuYWxlcnQtbWVzc2FnZSB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxufVxuLm1haW4tdGl0bGUtcm93e1xuICBtYXJnaW46IDEwcHhcbn1cbi5tYWluLXRpdGxle1xuICBmb250LXNpemU6IDE3cHg7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi50ZXh0LWFsaWdue1xuICB0ZXh0LWFsaWduOiBjZW50ZXJcbn1cblxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBle1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sb2dve1xuICAvLyB3aWR0aDogNjUlO1xuICB3aWR0aDogNzAlO1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4ubWVudV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuLmhlYXJ0X2ljb257XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLmNvcHlfaWNvbntcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmljb24taGVhcnQtcmVke1xuICBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgLy8gcGFkZGluZy10b3A6IDVweDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cbi5pY29uLWhlYXJ0LWJsYWNre1xuICBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgLy8gcGFkZGluZy10b3A6IDVweDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cbi5pY29uLXNoYXJle1xuICAvLyBmb250LXNpemU6IDEuMmVtO1xuICAvLyAvLyBjb2xvcjogIzA5NTA5ZDtcbiAgLy8gbWFyZ2luLWJvdHRvbTogLTNweDtcbiAgLy8gcGFkZGluZy1yaWdodDogM3B4O1xuXG4gIHdpZHRoOiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiAtN3B4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG4ucHJvZHVjdC10aXRsZXtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWU0ZTQ7XG4gIG1hcmdpbjogMTBweCAxNXB4IDE1cHggMTVweDtcbn1cblxudWx7XG4gIC8vIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nLWxlZnQ6IDI1cHg7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuXG5saXtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG4uY2xvc2VfaWNvbntcbiAgZm9udC1zaXplOiAxLjhlbTtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBjb2xvcjogd2hpdGU7XG59XG4ucm93LXBvcHVwe1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDk1MDlkO1xuICBib3JkZXItcmFkaXVzOiAyJTtcbiAgbWFyZ2luOiAxMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDMwJTtcbiAgei1pbmRleDogOTtcbiAgd2lkdGg6IC13ZWJraXQtZmlsbC1hdmFpbGFibGU7XG59XG5cbi5jb2wtYmFja2dyb3VuZHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA5NTA5ZDtcbn1cblxuLmNvbC1tYXJnaW57XG4gIC8vIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi50aXRsZS1wb3B1cHtcbiBwYWRkaW5nLWxlZnQ6IDVweDtcbiBwYWRkaW5nLXRvcDogNXB4O1xuIGZvbnQtd2VpZ2h0OiA2MDA7XG4gY29sb3I6IHdoaXRlXG59XG5cbi5wb3B1cC1kZXNjcmlwdGlvbntcbiAgcGFkZGluZzogMjBweCAxMHB4IDIwcHggMTBweCA7XG4gfVxuXG4gLmJ0bi1wYWRkaW5nLWNvbXBhcmV7XG4gIHBhZGRpbmctcmlnaHQ6IDBweDtcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XG4gfVxuXG4gLmJ0bi1wYWRkaW5nLWRvd25sb2Fke1xuICBwYWRkaW5nLXJpZ2h0OiAwcHg7XG4gIC8vIHBhZGRpbmctbGVmdDogNXB4O1xuIH1cblxuLy8gIHVse1xuLy8gICAgbWFyZ2luLXRvcDogMHB4O1xuLy8gIH1cblxuIiwiOmhvc3Qge1xuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xuICAtLXBhZ2Utc3dpcGVyLXBhZ2luYXRpb24tc3BhY2U6IDMwcHg7XG4gIC0tcGFnZS1zd2lwZXItcGFnaW5hdGlvbi1oZWlnaHQ6IDE4cHg7XG4gIC0tcGFnZS1wYWdpbmF0aW9uLWJ1bGxldC1zaXplOiAxMHB4O1xuICAtLXBhZ2Utb3B0aW9ucy1ndXR0ZXI6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gIC0tcGFnZS1yZWxhdGVkLWl0ZW1zLWd1dHRlcjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgLS1wYWdlLWNvbG9yOiAjY2IzMjhmO1xufVxuXG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLXBhZ2UtYmFja2dyb3VuZCk7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLnNsaWRlci1yb3cge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDk1MDlkO1xuICBtYXJnaW46IDEwcHg7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLnNsaWRlci1yb3cgLmRldGFpbHMtc2xpZGVzIHtcbiAgLS1idWxsZXQtYmFja2dyb3VuZDogIzA5NTA5ZDtcbiAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6ICMwOTUwOWQ7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLnNsaWRlci1yb3cgLmRldGFpbHMtc2xpZGVzIC5zbGlkZS1pbm5lci1yb3cge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLXN3aXBlci1wYWdpbmF0aW9uLXNwYWNlKTtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuc2xpZGVyLXJvdyAuc2hvd2Nhc2UtaW1hZ2Uge1xuICB3aWR0aDogMTAwJTtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuc2xpZGVyLXJvdyAuc2hvd2Nhc2UtaW1hZ2UuY2VudGVyZWQtaW1hZ2Uge1xuICBiYWNrZ3JvdW5kLXNpemU6IGF1dG87XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuc2xpZGVyLXJvdyAuc2hvd2Nhc2UtaW1hZ2UyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogYXV0bztcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuc2xpZGVyLXJvdyAuc2hvd2Nhc2UtaW1hZ2UyLmNlbnRlcmVkLWltYWdlIHtcbiAgYmFja2dyb3VuZC1zaXplOiBhdXRvO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLmRlc2NyaXB0aW9uLXdyYXBwZXIge1xuICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgdmFyKC0tcGFnZS1tYXJnaW4pIDBweDtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuZGVzY3JpcHRpb24td3JhcHBlciAuZGV0YWlscy1uYW1lIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW46IDBweCAwcHggNXB4O1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5kZXNjcmlwdGlvbi13cmFwcGVyIC5kZXRhaWxzLWJyYW5kIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgbWFyZ2luOiAwcHggMHB4IHZhcigtLXBhZ2UtbWFyZ2luKTtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuZGVzY3JpcHRpb24td3JhcHBlciAuZGV0YWlscy1wcmljZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAwcHg7XG4gIGNvbG9yOiB2YXIoLS1wYWdlLWNvbG9yKTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5kZXNjcmlwdGlvbi13cmFwcGVyIC5kZXRhaWxzLXNhbGUtcHJpY2Uge1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZSk7XG4gIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuZGV0YWlscy1wdXJjaGFzZS1vcHRpb25zLXJvdyB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IHZhcigtLXBhZ2Utb3B0aW9ucy1ndXR0ZXIpO1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC0gdmFyKC0tcGFnZS1vcHRpb25zLWd1dHRlcikpO1xuICBwYWRkaW5nLXJpZ2h0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAtIHZhcigtLXBhZ2Utb3B0aW9ucy1ndXR0ZXIpKTtcbiAgcGFkZGluZy1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMik7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLmRldGFpbHMtcHVyY2hhc2Utb3B0aW9ucy1yb3cgLmF1eC1hY3Rpb24tY29sIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgcGFkZGluZy10b3A6IDBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuZGV0YWlscy1wdXJjaGFzZS1vcHRpb25zLXJvdyAuYXV4LWFjdGlvbi1jb2wgLnNpemUtY2hhcnQtYnRuIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGhlaWdodDogMjVweDtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuZGV0YWlscy1wdXJjaGFzZS1vcHRpb25zLXJvdyAubWFpbi1jYWxsLXRvLWFjdGlvbi1jb2wge1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5kZXRhaWxzLXB1cmNoYXNlLW9wdGlvbnMtcm93IC5zZWxlY3QtdmFyaWFudC1idG4ge1xuICBtYXJnaW46IDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLmRldGFpbHMtcHVyY2hhc2Utb3B0aW9ucy1yb3cgLmFkZC10by1jYXJ0LWJ0biB7XG4gIG1hcmdpbjogMHB4O1xuICBtYXJnaW4tdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLmRldGFpbC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrLXRpbnQpO1xuICBtYXJnaW46IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5kZXRhaWxzLWRlc2NyaXB0aW9uLXdyYXBwZXIge1xuICBwYWRkaW5nLXRvcDogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICBwYWRkaW5nLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAyKTtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAuZGV0YWlscy1kZXNjcmlwdGlvbi13cmFwcGVyIC5kZXRhaWxzLWRlc2NyaXB0aW9uIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjM7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5kZXRhaWwtYWx0LXRpdGxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItZGFyay1zaGFkZSk7XG4gIG1hcmdpbjogMHB4O1xuICBtYXJnaW4tYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLnJlbGF0ZWQtcHJvZHVjdHMtd3JhcHBlciB7XG4gIHBhZGRpbmctdG9wOiAwcHg7XG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICBwYWRkaW5nLXJpZ2h0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIHBhZGRpbmctYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDIpO1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5yZWxhdGVkLXByb2R1Y3RzLXdyYXBwZXIgLnJlbGF0ZWQtcHJvZHVjdHMtbGlzdCB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAucmVsYXRlZC1wcm9kdWN0cy13cmFwcGVyIC5yZWxhdGVkLXByb2R1Y3RzLWxpc3QgLnJlbGF0ZWQtcHJvZHVjdC1pdGVtIHtcbiAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAyKTtcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAucmVsYXRlZC1wcm9kdWN0cy13cmFwcGVyIC5yZWxhdGVkLXByb2R1Y3RzLWxpc3QgLnJlbGF0ZWQtcHJvZHVjdC1pdGVtOm50aC1jaGlsZChvZGQpIHtcbiAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1yZWxhdGVkLWl0ZW1zLWd1dHRlcik7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLnJlbGF0ZWQtcHJvZHVjdHMtd3JhcHBlciAucmVsYXRlZC1wcm9kdWN0cy1saXN0IC5yZWxhdGVkLXByb2R1Y3QtaXRlbTpudGgtY2hpbGQoZXZlbikge1xuICBwYWRkaW5nLWxlZnQ6IHZhcigtLXBhZ2UtcmVsYXRlZC1pdGVtcy1ndXR0ZXIpO1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5yZWxhdGVkLXByb2R1Y3RzLXdyYXBwZXIgLnJlbGF0ZWQtcHJvZHVjdHMtbGlzdCAucmVsYXRlZC1wcm9kdWN0LWl0ZW0gLml0ZW0taW1hZ2Utd3JhcHBlciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLnJlbGF0ZWQtcHJvZHVjdHMtd3JhcHBlciAucmVsYXRlZC1wcm9kdWN0cy1saXN0IC5yZWxhdGVkLXByb2R1Y3QtaXRlbSAuaXRlbS1pbWFnZS13cmFwcGVyIC5pbWFnZS1hbmNob3Ige1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5mYXNoaW9uLWRldGFpbHMtY29udGVudCAucmVsYXRlZC1wcm9kdWN0cy13cmFwcGVyIC5yZWxhdGVkLXByb2R1Y3RzLWxpc3QgLnJlbGF0ZWQtcHJvZHVjdC1pdGVtIC5pdGVtLWRldGFpbHMtd3JhcHBlciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDVweCA1cHggMHB4O1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5yZWxhdGVkLXByb2R1Y3RzLXdyYXBwZXIgLnJlbGF0ZWQtcHJvZHVjdHMtbGlzdCAucmVsYXRlZC1wcm9kdWN0LWl0ZW0gLml0ZW0tbmFtZSB7XG4gIG1hcmdpbjogMHB4O1xuICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWluLXdpZHRoOiA5MCU7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLmZhc2hpb24tZGV0YWlscy1jb250ZW50IC5yZWxhdGVkLXByb2R1Y3RzLXdyYXBwZXIgLnJlbGF0ZWQtcHJvZHVjdHMtbGlzdCAucmVsYXRlZC1wcm9kdWN0LWl0ZW0gLml0ZW0tbmFtZSAubmFtZS1hbmNob3Ige1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhcmstdGludCk7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4uZmFzaGlvbi1kZXRhaWxzLWNvbnRlbnQgLnJlbGF0ZWQtcHJvZHVjdHMtd3JhcHBlciAucmVsYXRlZC1wcm9kdWN0cy1saXN0IC5yZWxhdGVkLXByb2R1Y3QtaXRlbSAuaXRlbS1zYWxlLXByaWNlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGNvbG9yOiB2YXIoLS1wYWdlLWNvbG9yKTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1pbi13aWR0aDogMzAlO1xufVxuXG46aG9zdCA6Om5nLWRlZXAgLmRldGFpbHMtc2xpZGVzIC5zd2lwZXItcGFnaW5hdGlvbiB7XG4gIGhlaWdodDogdmFyKC0tcGFnZS1zd2lwZXItcGFnaW5hdGlvbi1oZWlnaHQpO1xuICBsaW5lLWhlaWdodDogMTtcbiAgYm90dG9tOiBjYWxjKCh2YXIoLS1wYWdlLXN3aXBlci1wYWdpbmF0aW9uLXNwYWNlKSAtIHZhcigtLXBhZ2Utc3dpcGVyLXBhZ2luYXRpb24taGVpZ2h0KSApIC8gMik7XG59XG46aG9zdCA6Om5nLWRlZXAgLmRldGFpbHMtc2xpZGVzIC5zd2lwZXItcGFnaW5hdGlvbiAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcbiAgd2lkdGg6IHZhcigtLXBhZ2UtcGFnaW5hdGlvbi1idWxsZXQtc2l6ZSk7XG4gIGhlaWdodDogdmFyKC0tcGFnZS1wYWdpbmF0aW9uLWJ1bGxldC1zaXplKTtcbn1cblxuOjpuZy1kZWVwIC52YXJpYW50LWFsZXJ0IHtcbiAgLS1zZWxlY3QtYWxlcnQtY29sb3I6ICMwMDA7XG4gIC0tc2VsZWN0LWFsZXJ0LWJhY2tncm91bmQ6ICNGRkY7XG4gIC0tc2VsZWN0LWFsZXJ0LW1hcmdpbjogMTZweDtcbiAgLS1zZWxlY3QtYWxlcnQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gIC0tc2VsZWN0LWFsZXJ0LWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgLS1zZWxlY3QtYWxlcnQtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xufVxuOjpuZy1kZWVwIC52YXJpYW50LWFsZXJ0IC5hbGVydC1oZWFkIHtcbiAgcGFkZGluZy10b3A6IGNhbGMoKHZhcigtLXNlbGVjdC1hbGVydC1tYXJnaW4pIC8gNCkgKiAzKTtcbiAgcGFkZGluZy1ib3R0b206IGNhbGMoKHZhcigtLXNlbGVjdC1hbGVydC1tYXJnaW4pIC8gNCkgKiAzKTtcbiAgcGFkZGluZy1pbmxpbmUtc3RhcnQ6IHZhcigtLXNlbGVjdC1hbGVydC1tYXJnaW4pO1xuICBwYWRkaW5nLWlubGluZS1lbmQ6IHZhcigtLXNlbGVjdC1hbGVydC1tYXJnaW4pO1xufVxuOjpuZy1kZWVwIC52YXJpYW50LWFsZXJ0IC5hbGVydC10aXRsZSB7XG4gIGNvbG9yOiB2YXIoLS1zZWxlY3QtYWxlcnQtY29sb3IpO1xufVxuOjpuZy1kZWVwIC52YXJpYW50LWFsZXJ0IC5hbGVydC1oZWFkLFxuOjpuZy1kZWVwIC52YXJpYW50LWFsZXJ0IC5hbGVydC1tZXNzYWdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tc2VsZWN0LWFsZXJ0LWJhY2tncm91bmQpO1xufVxuOjpuZy1kZWVwIC52YXJpYW50LWFsZXJ0IC5hbGVydC13cmFwcGVyLnNjLWlvbi1hbGVydC1pb3MgLmFsZXJ0LXRpdGxlIHtcbiAgbWFyZ2luOiAwcHg7XG59XG46Om5nLWRlZXAgLnZhcmlhbnQtYWxlcnQgLmFsZXJ0LXdyYXBwZXIuc2MtaW9uLWFsZXJ0LW1kIC5hbGVydC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cbjo6bmctZGVlcCAudmFyaWFudC1hbGVydCAuYWxlcnQtd3JhcHBlci5zYy1pb24tYWxlcnQtbWQgLmFsZXJ0LWJ1dHRvbiB7XG4gIC0tcGFkZGluZy10b3A6IDA7XG4gIC0tcGFkZGluZy1zdGFydDogMC45ZW07XG4gIC0tcGFkZGluZy1lbmQ6IDAuOWVtO1xuICAtLXBhZGRpbmctYm90dG9tOiAwO1xuICBoZWlnaHQ6IDIuMWVtO1xuICBmb250LXNpemU6IDEzcHg7XG59XG46Om5nLWRlZXAgLnZhcmlhbnQtYWxlcnQgLmFsZXJ0LW1lc3NhZ2Uge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubWFpbi10aXRsZS1yb3cge1xuICBtYXJnaW46IDEwcHg7XG59XG5cbi5tYWluLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi50ZXh0LWFsaWduIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5pb24taGVhZGVyIGlvbi10b29sYmFyOmZpcnN0LW9mLXR5cGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sb2dvIHtcbiAgd2lkdGg6IDcwJTtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4ubWVudV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLmNvcHlfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5pY29uLWhlYXJ0LXJlZCB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uaWNvbi1oZWFydC1ibGFjayB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uaWNvbi1zaGFyZSB7XG4gIHdpZHRoOiAzMHB4O1xuICBtYXJnaW4tYm90dG9tOiAtN3B4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG5cbi5wcm9kdWN0LXRpdGxlIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWU0ZTQ7XG4gIG1hcmdpbjogMTBweCAxNXB4IDE1cHggMTVweDtcbn1cblxudWwge1xuICBwYWRkaW5nLWxlZnQ6IDI1cHg7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuXG5saSB7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4uY2xvc2VfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMS44ZW07XG4gIGZsb2F0OiByaWdodDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4ucm93LXBvcHVwIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgYm9yZGVyLXJhZGl1czogMiU7XG4gIG1hcmdpbjogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAzMCU7XG4gIHotaW5kZXg6IDk7XG4gIHdpZHRoOiAtd2Via2l0LWZpbGwtYXZhaWxhYmxlO1xufVxuXG4uY29sLWJhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDk1MDlkO1xufVxuXG4uY29sLW1hcmdpbiB7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi50aXRsZS1wb3B1cCB7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5wb3B1cC1kZXNjcmlwdGlvbiB7XG4gIHBhZGRpbmc6IDIwcHggMTBweCAyMHB4IDEwcHg7XG59XG5cbi5idG4tcGFkZGluZy1jb21wYXJlIHtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbn1cblxuLmJ0bi1wYWRkaW5nLWRvd25sb2FkIHtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufSIsIkBtaXhpbiBzZWxlY3QtYWxlcnQoKSB7XG4gIC8vIERlZmF1bHQgdmFsdWVzXG4gIC0tc2VsZWN0LWFsZXJ0LWNvbG9yOiAjMDAwO1xuICAtLXNlbGVjdC1hbGVydC1iYWNrZ3JvdW5kOiAjRkZGO1xuICAtLXNlbGVjdC1hbGVydC1tYXJnaW46IDE2cHg7XG5cbiAgLmFsZXJ0LWhlYWQge1xuICAgIHBhZGRpbmctdG9wOiBjYWxjKCh2YXIoLS1zZWxlY3QtYWxlcnQtbWFyZ2luKSAvIDQpICogMyk7XG4gICAgcGFkZGluZy1ib3R0b206IGNhbGMoKHZhcigtLXNlbGVjdC1hbGVydC1tYXJnaW4pIC8gNCkgKiAzKTtcbiAgICBwYWRkaW5nLWlubGluZS1zdGFydDogdmFyKC0tc2VsZWN0LWFsZXJ0LW1hcmdpbik7XG4gICAgcGFkZGluZy1pbmxpbmUtZW5kOiB2YXIoLS1zZWxlY3QtYWxlcnQtbWFyZ2luKTtcbiAgfVxuXG4gIC5hbGVydC10aXRsZSB7XG4gICAgY29sb3I6IHZhcigtLXNlbGVjdC1hbGVydC1jb2xvcik7XG4gIH1cblxuICAuYWxlcnQtaGVhZCxcbiAgLmFsZXJ0LW1lc3NhZ2Uge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXNlbGVjdC1hbGVydC1iYWNrZ3JvdW5kKTtcbiAgfVxuXG4gIC8vIGlPUyBzdHlsZXNcbiAgLmFsZXJ0LXdyYXBwZXIuc2MtaW9uLWFsZXJ0LWlvcyB7XG4gICAgLmFsZXJ0LXRpdGxlIHtcbiAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cbiAgfVxuXG4gIC8vIE1hdGVyaWFsIHN0eWxlc1xuICAuYWxlcnQtd3JhcHBlci5zYy1pb24tYWxlcnQtbWQge1xuICAgIC5hbGVydC10aXRsZSB7XG4gICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICBmb250LXdlaWdodDogNDAwO1xuICAgIH1cblxuICAgIC5hbGVydC1idXR0b24ge1xuICAgICAgLy8gVmFsdWVzIHRha2VuIGZyb20gSW9uaWMgc21hbGwgYnV0dG9uIHByZXNldFxuICAgICAgLS1wYWRkaW5nLXRvcDogMDtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMC45ZW07XG4gICAgICAtLXBhZGRpbmctZW5kOiAwLjllbTtcbiAgICAgIC0tcGFkZGluZy1ib3R0b206IDA7XG5cbiAgICAgIGhlaWdodDogMi4xZW07XG4gICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgfVxuICB9XG59XG4iXX0= */", "[_nghost-%COMP%] {\n  --shell-color: #cb328f;\n  --shell-color-rgb: 203,50,143;\n}\n\napp-image-shell.showcase-image[_ngcontent-%COMP%] {\n  --image-shell-loading-background: white;\n  --image-shell-spinner-color: #5289c3;\n  background-size: contain;\n  background-position: 50% 50%;\n}\n\n.details-name[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .25);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .35);\n  --text-shell-line-height: 18px;\n  max-width: 60%;\n}\n\n.details-name[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  max-width: inherit;\n}\n\n.details-brand[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 14px;\n  max-width: 40%;\n}\n\n.details-brand[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  max-width: inherit;\n}\n\n.details-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .10);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .20);\n  --text-shell-line-height: 18px;\n  width: 50px;\n}\n\n.details-price[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  width: inherit;\n}\n\n.details-sale-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .10);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .20);\n  --text-shell-line-height: 18px;\n  width: 50px;\n}\n\n.details-sale-price[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  width: inherit;\n}\n\n.details-description[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .10);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .20);\n  --text-shell-line-height: 14px;\n}\n\n.details-description.product-code[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  max-width: 40%;\n}\n\n.details-description.product-code[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  max-width: inherit;\n}\n\napp-image-shell.related-product-image[_ngcontent-%COMP%] {\n  --image-shell-loading-background: rgba(var(--shell-color-rgb), .10);\n  --image-shell-spinner-color: rgba(var(--shell-color-rgb), .25);\n}\n\n.item-name[_ngcontent-%COMP%]   app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .25);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .35);\n  --text-shell-line-height: 14px;\n}\n\n.item-sale-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvcHJvZHVjdC9kZXRhaWxzL3N0eWxlcy9wcm9kdWN0LWRldGFpbHMuc2hlbGwuc2NzcyIsInNyYy9hcHAvcHJvZHVjdC9kZXRhaWxzL3N0eWxlcy9wcm9kdWN0LWRldGFpbHMuc2hlbGwuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHNCQUFBO0VBQ0EsNkJBQUE7QUNERjs7QURXQTtFQUtFLHVDQUFBO0VBQ0Esb0NBQUE7RUFDQSx3QkFBQTtFQUNBLDRCQUFBO0FDWkY7O0FEZUE7RUFDRSxvRUFBQTtFQUNBLCtEQUFBO0VBQ0EsOEJBQUE7RUFDQSxjQUFBO0FDWkY7O0FEY0U7RUFDRSxrQkFBQTtBQ1pKOztBRGdCQTtFQUNFLG9FQUFBO0VBQ0EsK0RBQUE7RUFDQSw4QkFBQTtFQUNBLGNBQUE7QUNiRjs7QURlRTtFQUNFLGtCQUFBO0FDYko7O0FEaUJBO0VBQ0Usb0VBQUE7RUFDQSwrREFBQTtFQUNBLDhCQUFBO0VBQ0EsV0FBQTtBQ2RGOztBRGdCRTtFQUNFLGNBQUE7QUNkSjs7QURrQkE7RUFDRSxvRUFBQTtFQUNBLCtEQUFBO0VBQ0EsOEJBQUE7RUFDQSxXQUFBO0FDZkY7O0FEaUJFO0VBQ0UsY0FBQTtBQ2ZKOztBRG1CQTtFQUNFLG9FQUFBO0VBQ0EsK0RBQUE7RUFDQSw4QkFBQTtBQ2hCRjs7QURtQkE7RUFDRSxjQUFBO0FDaEJGOztBRGtCRTtFQUNFLGtCQUFBO0FDaEJKOztBRG9CQTtFQUNFLG1FQUFBO0VBQ0EsOERBQUE7QUNqQkY7O0FEb0JBO0VBQ0Usb0VBQUE7RUFDQSwrREFBQTtFQUNBLDhCQUFBO0FDakJGOztBRG9CQTtFQUNFLG9FQUFBO0VBQ0EsK0RBQUE7RUFDQSw4QkFBQTtBQ2pCRiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QvZGV0YWlscy9zdHlsZXMvcHJvZHVjdC1kZXRhaWxzLnNoZWxsLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDdXN0b20gdmFyaWFibGVzXG4vLyBOb3RlOiAgVGhlc2Ugb25lcyB3ZXJlIGFkZGVkIGJ5IHVzIGFuZCBoYXZlIG5vdGhpbmcgdG8gZG8gd2l0aCBJb25pYyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbjpob3N0IHtcbiAgLS1zaGVsbC1jb2xvcjogI2NiMzI4ZjtcbiAgLS1zaGVsbC1jb2xvci1yZ2I6IDIwMyw1MCwxNDM7XG59XG5cbi8vIFlvdSBjYW4gYWxzbyBhcHBseSBzaGVlbCBzdHlsZXMgdG8gdGhlIGVudGlyZSBwYWdlXG46aG9zdCguaXMtc2hlbGwpIHtcbiAgLy8gaW9uLWNvbnRlbnQge1xuICAvLyAgIG9wYWNpdHk6IDAuODtcbiAgLy8gfVxufVxuXG5hcHAtaW1hZ2Utc2hlbGwuc2hvd2Nhc2UtaW1hZ2Uge1xuICAvLyAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTApO1xuICAvLyAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcblxuICAvLyAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogIzhjYjllYTtcbiAgLS1pbWFnZS1zaGVsbC1sb2FkaW5nLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6ICM1Mjg5YzM7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogNTAlIDUwJTtcbn1cblxuLmRldGFpbHMtbmFtZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yNSk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjM1KTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxOHB4O1xuICBtYXgtd2lkdGg6IDYwJTtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtYXgtd2lkdGg6IGluaGVyaXQ7XG4gIH1cbn1cblxuLmRldGFpbHMtYnJhbmQgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbiAgbWF4LXdpZHRoOiA0MCU7XG5cbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgbWF4LXdpZHRoOiBpbmhlcml0O1xuICB9XG59XG5cbi5kZXRhaWxzLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjEwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE4cHg7XG4gIHdpZHRoOiA1MHB4O1xuXG4gICYudGV4dC1sb2FkZWQge1xuICAgIHdpZHRoOiBpbmhlcml0O1xuICB9XG59XG5cbi5kZXRhaWxzLXNhbGUtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMThweDtcbiAgd2lkdGg6IDUwcHg7XG5cbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgd2lkdGg6IGluaGVyaXQ7XG4gIH1cbn1cblxuLmRldGFpbHMtZGVzY3JpcHRpb24gPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbn1cblxuLmRldGFpbHMtZGVzY3JpcHRpb24ucHJvZHVjdC1jb2RlID4gYXBwLXRleHQtc2hlbGwge1xuICBtYXgtd2lkdGg6IDQwJTtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtYXgtd2lkdGg6IGluaGVyaXQ7XG4gIH1cbn1cblxuYXBwLWltYWdlLXNoZWxsLnJlbGF0ZWQtcHJvZHVjdC1pbWFnZSB7XG4gIC0taW1hZ2Utc2hlbGwtbG9hZGluZy1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4xMCk7XG4gIC0taW1hZ2Utc2hlbGwtc3Bpbm5lci1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjUpO1xufVxuXG4uaXRlbS1uYW1lIGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yNSk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjM1KTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xufVxuXG4uaXRlbS1zYWxlLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjIwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE2cHg7XG59XG4iLCI6aG9zdCB7XG4gIC0tc2hlbGwtY29sb3I6ICNjYjMyOGY7XG4gIC0tc2hlbGwtY29sb3ItcmdiOiAyMDMsNTAsMTQzO1xufVxuXG5hcHAtaW1hZ2Utc2hlbGwuc2hvd2Nhc2UtaW1hZ2Uge1xuICAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogd2hpdGU7XG4gIC0taW1hZ2Utc2hlbGwtc3Bpbm5lci1jb2xvcjogIzUyODljMztcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCUgNTAlO1xufVxuXG4uZGV0YWlscy1uYW1lID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzUpO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE4cHg7XG4gIG1heC13aWR0aDogNjAlO1xufVxuLmRldGFpbHMtbmFtZSA+IGFwcC10ZXh0LXNoZWxsLnRleHQtbG9hZGVkIHtcbiAgbWF4LXdpZHRoOiBpbmhlcml0O1xufVxuXG4uZGV0YWlscy1icmFuZCA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yMCk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjMwKTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xuICBtYXgtd2lkdGg6IDQwJTtcbn1cbi5kZXRhaWxzLWJyYW5kID4gYXBwLXRleHQtc2hlbGwudGV4dC1sb2FkZWQge1xuICBtYXgtd2lkdGg6IGluaGVyaXQ7XG59XG5cbi5kZXRhaWxzLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjEwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE4cHg7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmRldGFpbHMtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbC50ZXh0LWxvYWRlZCB7XG4gIHdpZHRoOiBpbmhlcml0O1xufVxuXG4uZGV0YWlscy1zYWxlLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjEwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE4cHg7XG4gIHdpZHRoOiA1MHB4O1xufVxuLmRldGFpbHMtc2FsZS1wcmljZSA+IGFwcC10ZXh0LXNoZWxsLnRleHQtbG9hZGVkIHtcbiAgd2lkdGg6IGluaGVyaXQ7XG59XG5cbi5kZXRhaWxzLWRlc2NyaXB0aW9uID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjEwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE0cHg7XG59XG5cbi5kZXRhaWxzLWRlc2NyaXB0aW9uLnByb2R1Y3QtY29kZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgbWF4LXdpZHRoOiA0MCU7XG59XG4uZGV0YWlscy1kZXNjcmlwdGlvbi5wcm9kdWN0LWNvZGUgPiBhcHAtdGV4dC1zaGVsbC50ZXh0LWxvYWRlZCB7XG4gIG1heC13aWR0aDogaW5oZXJpdDtcbn1cblxuYXBwLWltYWdlLXNoZWxsLnJlbGF0ZWQtcHJvZHVjdC1pbWFnZSB7XG4gIC0taW1hZ2Utc2hlbGwtbG9hZGluZy1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4xMCk7XG4gIC0taW1hZ2Utc2hlbGwtc3Bpbm5lci1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjUpO1xufVxuXG4uaXRlbS1uYW1lIGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yNSk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjM1KTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xufVxuXG4uaXRlbS1zYWxlLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjIwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE2cHg7XG59Il19 */", "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QvZGV0YWlscy9zdHlsZXMvcHJvZHVjdC1kZXRhaWxzLmlvcy5zY3NzIn0= */", "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QvZGV0YWlscy9zdHlsZXMvcHJvZHVjdC1kZXRhaWxzLm1kLnNjc3MifQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ProductDetailsPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
                selector: 'app-product-details',
                templateUrl: './product-details.page.html',
                styleUrls: [
                    './styles/product-details.page.scss',
                    './styles/product-details.shell.scss',
                    './styles/product-details.ios.scss',
                    './styles/product-details.md.scss'
                ]
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }, { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }, { type: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_11__["Downloader"] }, { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] }, { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_10__["AndroidPermissions"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }, { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_6__["SocialSharing"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }, { type: _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"] }, { type: _categories_categories_page__WEBPACK_IMPORTED_MODULE_7__["CategoriesPage"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }, { type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_9__["FirebaseAnalytics"] }, { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_12__["InAppBrowser"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }]; }, { slides: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
            args: ['slides']
        }], content: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
            args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonContent"], { static: true }]
        }], isShell: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"],
            args: ['class.is-shell']
        }] }); })();


/***/ }),

/***/ "./src/app/product/details/product-details.resolver.ts":
/*!*************************************************************!*\
  !*** ./src/app/product/details/product-details.resolver.ts ***!
  \*************************************************************/
/*! exports provided: ProductDetailsResolver */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailsResolver", function() { return ProductDetailsResolver; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../product.service */ "./src/app/product/product.service.ts");




class ProductDetailsResolver {
    constructor(productService) {
        this.productService = productService;
    }
    resolve() {
        const dataSource = this.productService.getDetailsDataSource();
        const dataStore = this.productService.getDetailsStore(dataSource);
        return dataStore;
    }
}
ProductDetailsResolver.ɵfac = function ProductDetailsResolver_Factory(t) { return new (t || ProductDetailsResolver)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"])); };
ProductDetailsResolver.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ProductDetailsResolver, factory: ProductDetailsResolver.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductDetailsResolver, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: _product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/product/listing/product-listing.model.ts":
/*!**********************************************************!*\
  !*** ./src/app/product/listing/product-listing.model.ts ***!
  \**********************************************************/
/*! exports provided: ProductItemModel, ProductListingModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductItemModel", function() { return ProductItemModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListingModel", function() { return ProductListingModel; });
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../shell/data-store */ "./src/app/shell/data-store.ts");

class ProductItemModel {
}
class ProductListingModel extends _shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"] {
    constructor() {
        super();
        this.items = [
            new ProductItemModel(),
            new ProductItemModel(),
            new ProductItemModel(),
            new ProductItemModel()
        ];
    }
}


/***/ }),

/***/ "./src/app/product/product.service.ts":
/*!********************************************!*\
  !*** ./src/app/product/product.service.ts ***!
  \********************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _shell_data_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shell/data-store */ "./src/app/shell/data-store.ts");
/* harmony import */ var _listing_product_listing_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./listing/product-listing.model */ "./src/app/product/listing/product-listing.model.ts");
/* harmony import */ var _details_product_details_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./details/product-details.model */ "./src/app/product/details/product-details.model.ts");








class ProductService {
    constructor(http) {
        this.http = http;
    }
    getListingDataSource() {
        return this.http.get('./assets/sample-data/fashion/listing.json')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((data) => {
            // Note: HttpClient cannot know how to instantiate a class for the returned data
            // We need to properly cast types from json data
            const listing = new _listing_product_listing_model__WEBPACK_IMPORTED_MODULE_4__["ProductListingModel"]();
            // The Object.assign() method copies all enumerable own properties from one or more source objects to a target object.
            // Note: If you have non-enummerable properties, you can try a spread operator instead. listing = {...data};
            // (see: https://scotch.io/bar-talk/copying-objects-in-javascript#toc-using-spread-elements-)
            Object.assign(listing, data);
            return listing;
        }));
    }
    getListingStore(dataSource) {
        // Use cache if available
        if (!this.listingDataStore) {
            // Initialize the model specifying that it is a shell model
            const shellModel = new _listing_product_listing_model__WEBPACK_IMPORTED_MODULE_4__["ProductListingModel"]();
            this.listingDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](shellModel);
            // Trigger the loading mechanism (with shell) in the dataStore
            this.listingDataStore.load(dataSource);
        }
        return this.listingDataStore;
    }
    getDetailsDataSource() {
        return this.http.get('./assets/sample-data/fashion/details.json')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])((data) => {
            // Note: HttpClient cannot know how to instantiate a class for the returned data
            // We need to properly cast types from json data
            const details = new _details_product_details_model__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsModel"]();
            // The Object.assign() method copies all enumerable own properties from one or more source objects to a target object.
            // Note: If you have non-enummerable properties, you can try a spread operator instead. details = {...data};
            // (see: https://scotch.io/bar-talk/copying-objects-in-javascript#toc-using-spread-elements-)
            Object.assign(details, data);
            return details;
        }));
    }
    getDetailsStore(dataSource) {
        // Use cache if available
        if (!this.detailsDataStore) {
            // Initialize the model specifying that it is a shell model
            const shellModel = new _details_product_details_model__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsModel"]();
            this.detailsDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](shellModel);
            // Trigger the loading mechanism (with shell) in the dataStore
            this.detailsDataStore.load(dataSource);
        }
        return this.detailsDataStore;
    }
}
ProductService.ɵfac = function ProductService_Factory(t) { return new (t || ProductService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
ProductService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ProductService, factory: ProductService.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/shell/data-store.ts":
/*!*************************************!*\
  !*** ./src/app/shell/data-store.ts ***!
  \*************************************/
/*! exports provided: ShellModel, DataStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShellModel", function() { return ShellModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataStore", function() { return DataStore; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./config/app-shell.config */ "./src/app/shell/config/app-shell.config.ts");



class ShellModel {
    constructor() {
        this.isShell = false;
    }
}
class DataStore {
    constructor(shellModel) {
        this.shellModel = shellModel;
        // We wait on purpose 2 secs on local environment when fetching from json to simulate the backend roundtrip.
        // However, in production you should set this delay to 0 in the assets/config/app-shell.config.prod.json file.
        // tslint:disable-next-line:max-line-length
        this.networkDelay = (_config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings && _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay) ? _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay : 0;
        this.timeline = new rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"](1);
    }
    // Static function with generics
    // (ref: https://stackoverflow.com/a/24293088/1116959)
    // Append a shell (T & ShellModel) to every value (T) emmited to the timeline
    static AppendShell(dataObservable, shellModel, networkDelay = 400) {
        const delayObservable = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(true).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["delay"])(networkDelay));
        // Assign shell flag accordingly
        // (ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["combineLatest"])([
            delayObservable,
            dataObservable
        ]).pipe(
        // Dismiss unnecessary delayValue
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(([delayValue, dataValue]) => Object.assign(dataValue, { isShell: false })), 
        // Set the shell model as the initial value
        Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["startWith"])(Object.assign(shellModel, { isShell: true })));
    }
    load(dataSourceObservable) {
        const dataSourceWithShellObservable = DataStore.AppendShell(dataSourceObservable, this.shellModel, this.networkDelay);
        dataSourceWithShellObservable
            .subscribe((dataValue) => {
            this.timeline.next(dataValue);
        });
    }
    get state() {
        return this.timeline.asObservable();
    }
}


/***/ })

}]);
//# sourceMappingURL=product-details-product-details-module-es2015.js.map