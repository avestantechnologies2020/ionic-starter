(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["favourites-favourites-module"],{

/***/ "./src/app/favourites/favourites.module.ts":
/*!*************************************************!*\
  !*** ./src/app/favourites/favourites.module.ts ***!
  \*************************************************/
/*! exports provided: FavouritesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavouritesPageModule", function() { return FavouritesPageModule; });
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _favourites_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./favourites.page */ "./src/app/favourites/favourites.page.ts");









const favouritesRoutes = [
    {
        path: '',
        component: _favourites_page__WEBPACK_IMPORTED_MODULE_6__["FavouritesPage"]
    }
];
class FavouritesPageModule {
}
FavouritesPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: FavouritesPageModule });
FavouritesPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ factory: function FavouritesPageModule_Factory(t) { return new (t || FavouritesPageModule)(); }, imports: [[
            _ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(favouritesRoutes),
            _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](FavouritesPageModule, { declarations: [_favourites_page__WEBPACK_IMPORTED_MODULE_6__["FavouritesPage"]], imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
        _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](FavouritesPageModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"],
        args: [{
                imports: [
                    _ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"],
                    _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                    _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(favouritesRoutes),
                    _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"]
                ],
                declarations: [_favourites_page__WEBPACK_IMPORTED_MODULE_6__["FavouritesPage"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/favourites/favourites.page.ts":
/*!***********************************************!*\
  !*** ./src/app/favourites/favourites.page.ts ***!
  \***********************************************/
/*! exports provided: FavouritesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavouritesPage", function() { return FavouritesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/downloader/ngx */ "./node_modules/@ionic-native/downloader/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/social-sharing/ngx */ "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/firebase-analytics/ngx */ "./node_modules/@ionic-native/firebase-analytics/__ivy_ngcc__/ngx/index.js");
/* harmony import */ var _categories_categories_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./../categories/categories.page */ "./src/app/categories/categories.page.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../shell/image-shell/image-shell.component */ "./src/app/shell/image-shell/image-shell.component.ts");


























function FavouritesPage_span_24_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Total Products : ", ctx_r0.total_product, "");
} }
function FavouritesPage_div_26_p_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" MRP: \u20B9", ctx_r4.formatNumber(item_r3), "");
} }
function FavouritesPage_div_26_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_div_26_Template_div_click_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const item_r3 = ctx.$implicit; const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r6.goToProductDetalPage(item_r3.ID, item_r3.SKUCode, item_r3.ProductTitle, item_r3.Image); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "app-image-shell", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_div_26_Template_div_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const item_r3 = ctx.$implicit; const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r8.goToProductDetalPage(item_r3.ID, item_r3.SKUCode, item_r3.ProductTitle, item_r3.Image); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, FavouritesPage_div_26_p_11_Template, 2, 1, "p", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "ion-row", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "ion-col", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_div_26_Template_ion_col_click_13_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const item_r3 = ctx.$implicit; const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r9.downloadPdf(item_r3); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "ion-icon", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Download");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "ion-col", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_div_26_Template_ion_col_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const item_r3 = ctx.$implicit; const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r10.deleteImage(item_r3.ID); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "ion-icon", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20, "Remove");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "ion-col", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_div_26_Template_ion_col_click_21_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r7); const item_r3 = ctx.$implicit; const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r11.showSharePopover(item_r3.ID, item_r3); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](22, "img", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, "Share");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r3 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("src", item_r3.Image);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r3.SKUCode);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r3.ProductTitle);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r3.MRP != null && item_r3.MRP != "" || item_r3.Price != null && item_r3.Price != "");
} }
function FavouritesPage_div_27_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Please go to any product and tap on the \"heart icon\" for it to be shown here.");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return ["/categories"]; };
class FavouritesPage {
    constructor(storage, alertController, router, loadingController, myapp, http, toastCtrl, downloader, platform, inAppBrowser, firebaseAnalytics, categoriesPage, socialSharing) {
        this.storage = storage;
        this.alertController = alertController;
        this.router = router;
        this.loadingController = loadingController;
        this.myapp = myapp;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.downloader = downloader;
        this.platform = platform;
        this.inAppBrowser = inAppBrowser;
        this.firebaseAnalytics = firebaseAnalytics;
        this.categoriesPage = categoriesPage;
        this.socialSharing = socialSharing;
        this.product_listing = [];
        this.allProductList = [];
        this.airConditionerList = [];
        this.airCoolerList = [];
        this.airPurifierList = [];
        this.waterPurifierList = [];
        this.airConditionerFlage = false;
        this.airCoolerFlage = false;
        this.airPurifierFlage = false;
        this.waterPurifierFlage = false;
        this.allProductFlage = true;
        this.currentTab = "All";
        this.showPopup = false;
        this.table_air_conditioner = "air_conditioner";
        this.table_air_cooler = "air_cooler";
        this.table_air_purifier = "air_purifier";
        this.table_water_purifier = "water_purifier";
    }
    ionViewWillEnter() {
        console.log("Enter ionViewWillEnter");
        this.storage.get('favouriteList').then((val) => {
            if (val != null) {
                this.product_listing = val;
                this.allProductList = val;
                this.total_product = this.product_listing.length;
                this.product_listing.sort(function (a, b) {
                    if (Number(a.MRP) < Number(b.MRP)) {
                        return -1;
                    }
                    else if (Number(a.MRP) > Number(b.MRP)) {
                        return 1;
                    }
                    else {
                        return 0;
                    }
                });
            }
        });
        this.storage.get('airConditionerList').then((val) => {
            if (val != null) {
                this.airConditionerList = val;
            }
        });
        this.storage.get('airCoolerList').then((val) => {
            if (val != null) {
                this.airCoolerList = val;
            }
        });
        this.storage.get('airPurifierList').then((val) => {
            if (val != null) {
                this.airPurifierList = val;
            }
        });
        this.storage.get('waterPurifierList').then((val) => {
            if (val != null) {
                this.waterPurifierList = val;
            }
        });
    }
    clickOnCategary(categary) {
        if (categary == "Air Conditioners") {
            this.currentTab = "Air Conditioners";
            this.product_listing = this.airConditionerList;
            this.airConditionerFlage = true;
            this.airCoolerFlage = false;
            this.airPurifierFlage = false;
            this.waterPurifierFlage = false;
            this.allProductFlage = false;
            this.total_product = this.product_listing.length;
            this.product_listing.sort(function (a, b) {
                if (Number(a.MRP) < Number(b.MRP)) {
                    return -1;
                }
                else if (Number(a.MRP) > Number(b.MRP)) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        else if (categary == "Air Coolers") {
            this.currentTab = "Air Coolers";
            this.product_listing = this.airCoolerList;
            this.airConditionerFlage = false;
            this.airCoolerFlage = true;
            this.airPurifierFlage = false;
            this.waterPurifierFlage = false;
            this.allProductFlage = false;
            this.total_product = this.product_listing.length;
            this.product_listing.sort(function (a, b) {
                if (Number(a.MRP) < Number(b.MRP)) {
                    return -1;
                }
                else if (Number(a.MRP) > Number(b.MRP)) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        else if (categary == "Air Purifiers") {
            this.currentTab = "Air Purifiers";
            this.product_listing = this.airPurifierList;
            this.airConditionerFlage = false;
            this.airCoolerFlage = false;
            this.airPurifierFlage = true;
            this.waterPurifierFlage = false;
            this.allProductFlage = false;
            this.total_product = this.product_listing.length;
            this.product_listing.sort(function (a, b) {
                if (Number(a.MRP) < Number(b.MRP)) {
                    return -1;
                }
                else if (Number(a.MRP) > Number(b.MRP)) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        else if (categary == "Water Purifiers") {
            this.currentTab = "Water Purifiers";
            this.product_listing = this.waterPurifierList;
            this.airConditionerFlage = false;
            this.airCoolerFlage = false;
            this.airPurifierFlage = false;
            this.waterPurifierFlage = true;
            this.allProductFlage = false;
            this.total_product = this.product_listing.length;
            this.product_listing.sort(function (a, b) {
                if (Number(a.MRP) < Number(b.MRP)) {
                    return -1;
                }
                else if (Number(a.MRP) > Number(b.MRP)) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
        else if (categary == "All") {
            this.currentTab = "All";
            this.product_listing = this.allProductList;
            this.airConditionerFlage = false;
            this.airCoolerFlage = false;
            this.airPurifierFlage = false;
            this.waterPurifierFlage = false;
            this.allProductFlage = true;
            this.total_product = this.product_listing.length;
            this.product_listing.sort(function (a, b) {
                if (Number(a.MRP) < Number(b.MRP)) {
                    return -1;
                }
                else if (Number(a.MRP) > Number(b.MRP)) {
                    return 1;
                }
                else {
                    return 0;
                }
            });
        }
    }
    formatNumber(item) {
        var number = item.MRP ? item.MRP : item.Price;
        return new Intl.NumberFormat('en-IN').format(number);
    }
    ngOnInit() {
        this.product_listing = [];
        this.allProductList = [];
        this.airConditionerList = [];
        this.airCoolerList = [];
        this.airPurifierList = [];
        this.waterPurifierList = [];
    }
    deleteImage(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                // header: "Confirmation",
                header: "Are you sure you'd like to remove this product from My Products?",
                // message: "Are you sure you'd like to remove this product from My Products?",
                cssClass: 'variant-alert size-chooser',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            alert.dismiss();
                        }
                    }, {
                        text: 'OK',
                        handler: () => {
                            this.removeFromFavourites(id);
                            alert.dismiss();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    // deleteImage(){
    //   this.showPopup = true;
    //   this.content.scrollToPoint(0, 0, 1000)
    // }
    // close() {
    //   this.showPopup = false;
    // }
    removeFromFavourites(id) {
        // console.log("id", id);
        // this.showPopup = false;
        let data = [];
        this.storage.get('favouriteList').then((val) => {
            if (val != null) {
                if (val.length != 0) {
                    data = val;
                    let count = 0;
                    let removeItemIndex = 0;
                    for (let index in data) {
                        if (data[index].ID == id) {
                            count = 1;
                            removeItemIndex = Number(index);
                        }
                    }
                    if (count == 1) {
                        //remove product from Favourites list
                        data.splice(removeItemIndex, 1);
                        this.product_listing = data;
                        this.allProductList = data;
                        this.storage.set('favouriteList', data);
                        this.total_product = this.total_product - 1;
                    }
                }
            }
            let count1 = 0;
            let count2 = 0;
            let count3 = 0;
            let count4 = 0;
            let removeItemIndex1 = 0;
            let removeItemIndex2 = 0;
            let removeItemIndex3 = 0;
            let removeItemIndex4 = 0;
            for (let index in this.airConditionerList) {
                if (this.airConditionerList[index].ID == id) {
                    count1 = 1;
                    removeItemIndex1 = Number(index);
                }
            }
            if (count1 == 1) {
                this.airConditionerList.splice(removeItemIndex1, 1);
                this.storage.set('airConditionerList', this.airConditionerList);
            }
            for (let index in this.airCoolerList) {
                if (this.airCoolerList[index].ID == id) {
                    count2 = 1;
                    removeItemIndex2 = Number(index);
                }
            }
            if (count2 == 1) {
                this.airCoolerList.splice(removeItemIndex2, 1);
                this.storage.set('airCoolerList', this.airCoolerList);
            }
            for (let index in this.airPurifierList) {
                if (this.airPurifierList[index].ID == id) {
                    count3 = 1;
                    removeItemIndex3 = Number(index);
                }
            }
            if (count3 == 1) {
                this.airPurifierList.splice(removeItemIndex3, 1);
                this.storage.set('airPurifierList', this.airPurifierList);
            }
            for (let index in this.waterPurifierList) {
                if (this.waterPurifierList[index].ID == id) {
                    count4 = 1;
                    removeItemIndex4 = Number(index);
                }
            }
            if (count4 == 1) {
                this.waterPurifierList.splice(removeItemIndex4, 1);
                this.storage.set('waterPurifierList', this.waterPurifierList);
            }
            if (this.currentTab == "Air Conditioners") {
                this.product_listing = this.airConditionerList;
            }
            else if (this.currentTab == "Air Coolers") {
                this.product_listing = this.airCoolerList;
            }
            else if (this.currentTab == "Air Purifiers") {
                this.product_listing = this.airPurifierList;
            }
            else if (this.currentTab == "Water Purifiers") {
                this.product_listing = this.waterPurifierList;
            }
            else if (this.currentTab == "All") {
                this.product_listing = this.allProductList;
            }
        });
    }
    // removeFromFavourites(id) {
    //   let data = []
    //   this.storage.get('favouriteList').then((val) => {
    //     if (val != null) {
    //       if (val.length != 0) {
    //         data = val;
    //         let count = 0;
    //         let removeItemIndex = 0;
    //         for (let index in data) {
    //           if (data[index].ID == id) {
    //             count = 1;
    //             removeItemIndex = Number(index)
    //           }
    //         }
    //         if (count == 1) {
    //           //remove product from Favourites list
    //           data.splice(removeItemIndex, 1)
    //           this.product_listing = data;
    //           this.allProductList = data;
    //           this.storage.set('favouriteList', data);
    //           this.total_product = this.total_product - 1;
    //         }
    //       }
    //     }
    //     let count1 = 0;
    //     let count2 = 0;
    //     let count3 = 0;
    //     let count4 = 0;
    //     let removeItemIndex1 = 0;
    //     let removeItemIndex2 = 0;
    //     let removeItemIndex3 = 0;
    //     let removeItemIndex4 = 0;
    //     for (let index in this.airConditionerList) {
    //       if (this.airConditionerList[index].ID == id) {
    //         count1 = 1
    //         removeItemIndex1 = Number(index);
    //       }
    //     }
    //     if (count1 == 1) {
    //       this.airConditionerList.splice(removeItemIndex1, 1)
    //       this.storage.set('airConditionerList', this.airConditionerList);
    //     }
    //     for (let index in this.airCoolerList) {
    //       if (this.airCoolerList[index].ID == id) {
    //         count2 = 1
    //         removeItemIndex2 = Number(index);
    //       }
    //     }
    //     if (count2 == 1) {
    //       this.airCoolerList.splice(removeItemIndex2, 1)
    //       this.storage.set('airCoolerList', this.airCoolerList);
    //     }
    //     for (let index in this.airPurifierList) {
    //       if (this.airPurifierList[index].ID == id) {
    //         count3 = 1
    //         removeItemIndex3 = Number(index);
    //       }
    //     }
    //     if (count3 == 1) {
    //       this.airPurifierList.splice(removeItemIndex3, 1)
    //       this.storage.set('airPurifierList', this.airPurifierList);
    //     }
    //     for (let index in this.waterPurifierList) {
    //       if (this.waterPurifierList[index].ID == id) {
    //         count4 = 1
    //         removeItemIndex4 = Number(index);
    //       }
    //     }
    //     if (count4 == 1) {
    //       this.waterPurifierList.splice(removeItemIndex4, 1)
    //       this.storage.set('waterPurifierList', this.waterPurifierList);
    //     }
    //     if (this.currentTab == "Air Conditioners") {
    //       this.product_listing = this.airConditionerList;
    //     } else if (this.currentTab == "Air Coolers") {
    //       this.product_listing = this.airCoolerList;
    //     } else if (this.currentTab == "Air Purifiers") {
    //       this.product_listing = this.airPurifierList;
    //     } else if (this.currentTab == "Water Purifiers") {
    //       this.product_listing = this.waterPurifierList;
    //     } else if (this.currentTab == "All") {
    //       this.product_listing = this.allProductList;
    //     }
    //   });
    // }
    goToProductDetalPage(id, skuCode, ProductTitle, Image) {
        let flage = 0;
        for (let i in this.airConditionerList) {
            if (this.airConditionerList[i].ID == id && this.airConditionerList[i].SKUCode == skuCode) {
                flage = 1;
                let navigationExtras = {
                    queryParams: {
                        id: id,
                        title: "Air Conditioners",
                        SKUCode: skuCode,
                        ProductTitle: ProductTitle,
                        Image: Image
                    }
                };
                this.router.navigate(['product/' + id], navigationExtras);
            }
        }
        if (flage == 0) {
            for (let i in this.airCoolerList) {
                if (this.airCoolerList[i].ID == id && this.airCoolerList[i].SKUCode == skuCode) {
                    flage = 1;
                    let navigationExtras = {
                        queryParams: {
                            id: id,
                            title: "Air Coolers",
                            SKUCode: skuCode,
                            ProductTitle: ProductTitle,
                            Image: Image
                        }
                    };
                    this.router.navigate(['product/' + id], navigationExtras);
                }
            }
        }
        if (flage == 0) {
            for (let i in this.airPurifierList) {
                if (this.airPurifierList[i].ID == id && this.airPurifierList[i].SKUCode == skuCode) {
                    flage = 1;
                    let navigationExtras = {
                        queryParams: {
                            id: id,
                            title: "Air Purifiers",
                            SKUCode: skuCode,
                            ProductTitle: ProductTitle,
                            Image: Image
                        }
                    };
                    this.router.navigate(['product/' + id], navigationExtras);
                }
            }
        }
        if (flage == 0) {
            for (let i in this.waterPurifierList) {
                if (this.waterPurifierList[i].ID == id && this.waterPurifierList[i].SKUCode == skuCode) {
                    flage = 1;
                    let navigationExtras = {
                        queryParams: {
                            id: id,
                            title: "Water Purifiers",
                            SKUCode: skuCode,
                            ProductTitle: ProductTitle,
                            Image: Image
                        }
                    };
                    this.router.navigate(['product/' + id], navigationExtras);
                }
            }
        }
    }
    downloadPdf(item) {
        let flage = 0;
        for (let i in this.airConditionerList) {
            if (this.airConditionerList[i].ID == item.ID && this.airConditionerList[i].SKUCode == item.SKUCode) {
                flage = 1;
                this.title = "Air Conditioners";
            }
        }
        if (flage == 0) {
            for (let i in this.airCoolerList) {
                if (this.airCoolerList[i].ID == item.ID && this.airCoolerList[i].SKUCode == item.SKUCode) {
                    flage = 1;
                    this.title = "Air Coolers";
                }
            }
        }
        if (flage == 0) {
            for (let i in this.airPurifierList) {
                if (this.airPurifierList[i].ID == item.ID && this.airPurifierList[i].SKUCode == item.SKUCode) {
                    flage = 1;
                    this.title = "Air Purifiers";
                }
            }
        }
        if (flage == 0) {
            for (let i in this.waterPurifierList) {
                if (this.waterPurifierList[i].ID == item.ID && this.waterPurifierList[i].SKUCode == item.SKUCode) {
                    flage = 1;
                    this.title = "Water Purifiers";
                }
            }
        }
        let param = {};
        let tableName = "";
        if (this.title == "Air Conditioners") {
            param = { air_conditioner: item.ID };
            tableName = this.table_air_conditioner;
        }
        else if (this.title == "Air Coolers") {
            param = { air_cooler: item.ID };
            tableName = this.table_air_cooler;
        }
        else if (this.title == "Air Purifiers") {
            param = { air_purifier: item.ID };
            tableName = this.table_air_purifier;
        }
        else if (this.title == "Water Purifiers") {
            param = { water_purifier: item.ID };
            tableName = this.table_water_purifier;
        }
        this.myapp.databaseObj.executeSql(`
    SELECT * FROM ${tableName} WHERE ID = ${item.ID}
    `, [])
            .then((res) => {
            if (res.rows.length > 0) {
                let product_data = [];
                for (var i = 0; i < res.rows.length; i++) {
                    let rowKeys = [];
                    let singleRowData = [];
                    var object = {
                        brochures: ""
                    };
                    rowKeys = Object.keys(res.rows.item(i));
                    Object.keys(res.rows.item(i)).map(key => {
                        singleRowData.push(res.rows.item(i)[key]);
                    });
                    for (let index in rowKeys) {
                        if (rowKeys[index] == "brochures") {
                            object[rowKeys[index]] = singleRowData[index];
                        }
                    }
                    product_data.push(object);
                }
                if (product_data[0].brochures == "") {
                    this.downloadFuction(item, param);
                }
                else {
                    if (this.platform.is("ios")) {
                        const browser = this.inAppBrowser.create(product_data[0].brochures);
                        this.firebaseAnalytics.logEvent('downloads_products', { product: item.SKUCode, category: this.title })
                            .then((res) => console.log(res))
                            .catch((error) => console.error(error));
                    }
                    else {
                        this.loadingController.create({
                            message: 'Please wait while downloading',
                        }).then((res) => {
                            res.present();
                            if (navigator.onLine) {
                                var request = {
                                    uri: product_data[0].brochures,
                                    title: 'Blue Star',
                                    description: '',
                                    mimeType: 'application/pdf',
                                    visibleInDownloadsUi: true,
                                    notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["NotificationVisibility"].VisibleNotifyCompleted,
                                    destinationInExternalPublicDir: {
                                        dirType: 'Download',
                                        subPath: item.SKUCode + '.pdf'
                                    }
                                };
                                this.downloader.download(request)
                                    .then((location) => {
                                    res.dismiss();
                                    this.presentToast("Downloaded in device download folder");
                                    this.firebaseAnalytics.logEvent('downloads_products', { product: item.SKUCode, category: this.title })
                                        .then((res) => console.log(res))
                                        .catch((error) => console.error(error));
                                })
                                    .catch((error) => {
                                    console.error(error);
                                });
                            }
                            else {
                                res.dismiss();
                                console.log("no internat connection");
                            }
                        });
                    }
                }
            }
            else {
                this.downloadFuction(item, param);
            }
        })
            .catch(e => {
            console.log("error " + JSON.stringify(e));
        });
    }
    downloadFuction(item, param) {
        this.loadingController.create({
            message: 'Please wait while downloading',
        }).then((res) => {
            res.present();
            if (navigator.onLine) {
                this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/product_pdf/download', param).subscribe((response) => {
                    Object.keys(response).map(key => {
                        this.pdf_link = response[key].pdf_link;
                        //this.indicatorDownload(item, this.pdf_link);
                        if (this.platform.is("ios")) {
                            res.dismiss();
                            const browser = this.inAppBrowser.create(this.pdf_link);
                            this.firebaseAnalytics.logEvent('downloads_products', { product: item.SKUCode, category: this.title })
                                .then((res) => console.log(res))
                                .catch((error) => console.error(error));
                        }
                        else {
                            var request = {
                                uri: this.pdf_link,
                                title: 'Blue Star',
                                description: '',
                                mimeType: 'application/pdf',
                                visibleInDownloadsUi: true,
                                notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["NotificationVisibility"].VisibleNotifyCompleted,
                                destinationInExternalPublicDir: {
                                    dirType: 'Download',
                                    subPath: item.SKUCode + '.pdf'
                                }
                            };
                            this.downloader.download(request)
                                .then((location) => {
                                res.dismiss();
                                this.presentToast("Downloaded in device download folder");
                                this.firebaseAnalytics.logEvent('downloads_products', { product: item.SKUCode, category: this.title })
                                    .then((res) => console.log(res))
                                    .catch((error) => console.error(error));
                            })
                                .catch((error) => {
                                console.error(error);
                                res.dismiss();
                            });
                        }
                    });
                }, err => {
                    res.dismiss();
                    console.log("err.........", JSON.stringify(err));
                });
            }
            else {
                res.dismiss();
                console.log("no internat connection");
            }
        });
    }
    indicatorDownload(item, pdf_link) {
        if (this.platform.is("ios")) {
            const browser = this.inAppBrowser.create(this.pdf_link);
            this.firebaseAnalytics.logEvent('downloads_products', { product: item.SKUCode, category: this.title })
                .then((res) => console.log(res))
                .catch((error) => console.error(error));
        }
        else {
            var request = {
                uri: pdf_link,
                title: 'Blue Star',
                description: '',
                mimeType: 'application/pdf',
                visibleInDownloadsUi: true,
                notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["NotificationVisibility"].VisibleNotifyCompleted,
                destinationInExternalPublicDir: {
                    dirType: 'Download',
                    subPath: item.SKUCode + '.pdf'
                }
            };
            this.loadingController.create({
                message: 'Please wait while downloading',
            }).then((res) => {
                res.present();
                this.downloader.download(request)
                    .then((location) => {
                    res.dismiss();
                    this.presentToast("Downloaded in device download folder");
                    this.firebaseAnalytics.logEvent('downloads_products', { product: item.SKUCode, category: this.title })
                        .then((res) => console.log(res))
                        .catch((error) => console.error(error));
                })
                    .catch((error) => {
                    console.error(error);
                    res.dismiss();
                });
            });
        }
    }
    showSharePopover(id, item) {
        let flage = 0;
        for (let i in this.airConditionerList) {
            if (this.airConditionerList[i].ID == item.ID && this.airConditionerList[i].SKUCode == item.SKUCode) {
                flage = 1;
                this.title = "Air Conditioners";
            }
        }
        if (flage == 0) {
            for (let i in this.airCoolerList) {
                if (this.airCoolerList[i].ID == item.ID && this.airCoolerList[i].SKUCode == item.SKUCode) {
                    flage = 1;
                    this.title = "Air Coolers";
                }
            }
        }
        if (flage == 0) {
            for (let i in this.airPurifierList) {
                if (this.airPurifierList[i].ID == item.ID && this.airPurifierList[i].SKUCode == item.SKUCode) {
                    flage = 1;
                    this.title = "Air Purifiers";
                }
            }
        }
        if (flage == 0) {
            for (let i in this.waterPurifierList) {
                if (this.waterPurifierList[i].ID == item.ID && this.waterPurifierList[i].SKUCode == item.SKUCode) {
                    flage = 1;
                    this.title = "Water Purifiers";
                }
            }
        }
        this.getPdfLink(id, item);
    }
    presentToast(text) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: text,
                duration: 7000,
                position: 'bottom',
                cssClass: "msg-align",
            });
            toast.present();
        });
    }
    getPdfLink(productId, item) {
        let param = {};
        if (this.title == "Air Conditioners") {
            param = { air_conditioner: productId };
        }
        else if (this.title == "Air Coolers") {
            param = { air_cooler: productId };
        }
        else if (this.title == "Air Purifiers") {
            param = { air_purifier: productId };
        }
        else if (this.title == "Water Purifiers") {
            param = { water_purifier: productId };
        }
        this.loadingController.create({
            message: 'Please wait',
        }).then((res) => {
            res.present();
            if (navigator.onLine) {
                this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/product_pdf/download', param).subscribe((response) => {
                    Object.keys(response).map(key => {
                        this.pdf_link = response[key].pdf_link;
                        res.dismiss();
                        this.shareMethod(item);
                    });
                }, err => {
                    res.dismiss();
                    this.presentToast("No internet connection. Please try again later.");
                    console.log("err.........", JSON.stringify(err));
                });
            }
            else {
                res.dismiss();
                this.presentToast("No internet connection. Please try again later.");
            }
        });
    }
    toDataUrl(url, _this, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                callback(reader.result, _this);
            };
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    }
    shareMethod(item) {
        this.toDataUrl(item.Image, this, function (myBase64, _this) {
            _this.callMethod(myBase64, item);
        });
    }
    callMethod(myBase64, item) {
        let body = `Hi there, check out this product by Blue Star!\n\nProduct Category: ${this.title}\nProduct Title: ${item.ProductTitle}\nSKU Code: ${item.SKUCode}\nClick here to get the product specification: ${this.pdf_link.replace(/\s/g, "%20")}`;
        this.socialSharing.share(body, item.ProductTitle.replace(/\%/g, " pc"), myBase64, null)
            .then(sucess => {
            this.firebaseAnalytics.logEvent('share_products', { product: item.SKUCode, category: this.title })
                .then((res) => console.log(res))
                .catch((error) => console.error(error));
        })
            .catch(err => {
            console.log(err);
        });
    }
}
FavouritesPage.ɵfac = function FavouritesPage_Factory(t) { return new (t || FavouritesPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["Downloader"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__["InAppBrowser"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_10__["FirebaseAnalytics"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_categories_categories_page__WEBPACK_IMPORTED_MODULE_11__["CategoriesPage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_8__["SocialSharing"])); };
FavouritesPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: FavouritesPage, selectors: [["app-categories"]], viewQuery: function FavouritesPage_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstaticViewQuery"](_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"], true);
    } if (rf & 2) {
        var _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.content = _t.first);
    } }, decls: 28, vars: 10, consts: [["color", "primary"], ["slot", "start"], ["src", "assets/logo.png", 1, "logo"], ["slot", "end"], ["name", "home-outline", 1, "home_icon", 3, "routerLink"], [1, "div-fixed"], [2, "overflow-x", "auto", "white-space", "nowrap"], [1, "tabCss", 3, "ngClass", "click"], [1, "col-padding"], [1, "main-title"], ["class", "total-product", 4, "ngIf"], [2, "margin-top", "75px"], ["style", "border-bottom: 1px solid #09509d;", 4, "ngFor", "ngForOf"], [4, "ngIf"], [1, "total-product"], [2, "border-bottom", "1px solid #09509d"], [1, "split"], [1, "column", 2, "width", "30%"], [3, "click"], ["animation", "spinner", 2, "width", "100%", "height", "100%", 3, "src"], [1, "column", 2, "width", "70%"], [1, "centered", 3, "click"], [2, "margin-bottom", "5px"], ["size", "4", 2, "text-align", "center", "color", "#09509d", 3, "click"], ["name", "download-outline", 1, "icon-download"], ["name", "trash", 1, "icon-delete"], ["size", "4", 2, "text-align", "center", "color", "#09509d", "padding-top", "0px", 3, "click"], ["src", "assets/share.png", 1, "icon-share"], [1, "text-msg"]], template: function FavouritesPage_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-header");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-toolbar", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-buttons", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "ion-back-button");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "ion-buttons", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "ion-icon", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "ion-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_Template_div_click_10_listener() { return ctx.clickOnCategary("All"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "All Products");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_Template_div_click_12_listener() { return ctx.clickOnCategary("Air Conditioners"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "Air Conditioners");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_Template_div_click_14_listener() { return ctx.clickOnCategary("Air Coolers"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, " Air Coolers");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_Template_div_click_16_listener() { return ctx.clickOnCategary("Air Purifiers"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Air Purifiers");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FavouritesPage_Template_div_click_18_listener() { return ctx.clickOnCategary("Water Purifiers"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, "Water Purifiers");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "ion-row");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "ion-col", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "span", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "My Products");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, FavouritesPage_span_24_Template, 2, 1, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](26, FavouritesPage_div_26_Template, 25, 4, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](27, FavouritesPage_div_27_Template, 3, 0, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](9, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.allProductFlage ? "tabCssClick" : "tabCss");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.airConditionerFlage ? "tabCssClick" : "tabCss");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.airCoolerFlage ? "tabCssClick" : "tabCss");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.airPurifierFlage ? "tabCssClick" : "tabCss");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.waterPurifierFlage ? "tabCssClick" : "tabCss");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.total_product > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.product_listing);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.product_listing.length == 0);
    } }, directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonIcon"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["RouterLinkDelegate"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLink"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgClass"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonCol"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgForOf"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_13__["ImageShellComponent"]], styles: ["[_nghost-%COMP%] {\n  --page-margin: var(--app-narrow-margin);\n  --page-categories-gutter: calc(var(--page-margin) / 4);\n  --page-category-background: var(--ion-color-medium);\n  --page-category-background-rgb: var(--ion-color-medium-rgb);\n}\n\n.categories-list[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: var(--page-categories-gutter);\n  padding: calc(var(--page-categories-gutter) * 3);\n  height: 100%;\n  align-content: flex-start;\n  overflow: scroll;\n  -ms-overflow-style: none;\n  overflow: -moz-scrollbars-none;\n  scrollbar-width: none;\n}\n\n.categories-list[_ngcontent-%COMP%]::-webkit-scrollbar {\n  display: none;\n}\n\n.categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%] {\n  height: 100%;\n  text-decoration: none;\n  display: flex;\n  justify-content: flex-start;\n  align-items: flex-start;\n}\n\n.categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n  margin: auto;\n  text-transform: uppercase;\n  font-weight: 400;\n  font-size: 18px;\n  letter-spacing: 1px;\n  padding: calc((var(--page-margin) / 4) * 3) var(--page-margin);\n  color: var(--ion-color-lightest);\n  background-color: var(--page-category-background);\n  border-radius: var(--app-fair-radius);\n}\n\n.categories-list[_ngcontent-%COMP%]   .travel-category[_ngcontent-%COMP%] {\n  --page-category-background: #00AFFF;\n  --page-category-background-rgb: 0,175,255;\n}\n\n.categories-list[_ngcontent-%COMP%]   .fashion-category[_ngcontent-%COMP%] {\n  --page-category-background: #cb328f;\n  --page-category-background-rgb: 203,50,143;\n}\n\n.categories-list[_ngcontent-%COMP%]   .food-category[_ngcontent-%COMP%] {\n  --page-category-background: #ebbb00;\n  --page-category-background-rgb: 235,187,0;\n}\n\n.categories-list[_ngcontent-%COMP%]   .deals-category[_ngcontent-%COMP%] {\n  --page-category-background: #70df70;\n  --page-category-background-rgb: 112,223,112;\n}\n\n.categories-list[_ngcontent-%COMP%]   .real-estate-category[_ngcontent-%COMP%] {\n  --page-category-background: #d9453a;\n  --page-category-background-rgb: 217,69,58;\n}\n\nion-header[_ngcontent-%COMP%]   ion-toolbar[_ngcontent-%COMP%]:first-of-type {\n  text-align: center;\n}\n\n.logo[_ngcontent-%COMP%] {\n  width: 60%;\n  margin-top: 5px;\n}\n\n.home_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n\n.search_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 10px;\n}\n\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 10px;\n}\n\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 10px;\n  color: white;\n}\n\n\n\n.split[_ngcontent-%COMP%] {\n  height: 25%;\n  width: 100%;\n  z-index: 1;\n  position: relative;\n  overflow-x: hidden;\n}\n\n\n\n\n\n.column[_ngcontent-%COMP%] {\n  float: left;\n  padding: 10px;\n  height: 100%;\n  \n}\n\n.centered[_ngcontent-%COMP%] {\n  color: balck;\n}\n\n.icon-delete-col[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 90%;\n  transform: translate(-50%, -50%);\n  font-size: 25px;\n  color: #09509d;\n}\n\n.icon-share[_ngcontent-%COMP%] {\n  width: 30px;\n  margin-bottom: -7px;\n  padding-right: 3px;\n}\n\n.icon-download[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-bottom: -4px;\n  padding-right: 3px;\n}\n\n.icon-delete[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-bottom: -5px;\n  padding-right: 3px;\n}\n\n\n\n.tabCssClick[_ngcontent-%COMP%] {\n  border-right: 1px solid #09509d !important;\n  border-left: 1px solid #09509d !important;\n  padding: 10px 15px;\n  display: inline-block;\n  font-size: 14px;\n  text-align: center;\n  font-weight: 600;\n  text-decoration: underline;\n  color: #09509d;\n}\n\n.tabCss[_ngcontent-%COMP%] {\n  border-right: 1px solid #eae4e4;\n  padding: 10px 15px;\n  display: inline-block;\n  font-size: 14px;\n  text-align: center;\n  font-weight: 600;\n}\n\n.my-custom-class[_ngcontent-%COMP%] {\n  background: #e5e5e5;\n}\n\n.detail-title[_ngcontent-%COMP%] {\n  font-size: 17px;\n  font-weight: 600;\n  color: #09509d;\n  margin: 0px;\n}\n\n.text-msg[_ngcontent-%COMP%] {\n  font-size: 17px;\n  text-align: center;\n  margin-top: 50%;\n  margin-left: 15px;\n  margin-right: 15px;\n  font-weight: 600;\n}\n\n.col-padding[_ngcontent-%COMP%] {\n  padding-left: 10px !important;\n  padding-top: 10px !important;\n  border-top: 1px solid #09509d;\n  border-bottom: 1px solid #09509d;\n  padding-bottom: 10px !important;\n}\n\n.main-title[_ngcontent-%COMP%] {\n  color: #09509d;\n  font-weight: bold;\n}\n\n.total-product[_ngcontent-%COMP%] {\n  padding-right: 10px !important;\n  color: #09509d;\n  font-weight: bold;\n  float: right;\n}\n\n.div-fixed[_ngcontent-%COMP%] {\n  position: fixed;\n  z-index: 999;\n  background-color: white;\n  border-bottom: 1px solid #09509d;\n  width: 100%;\n  overflow-y: auto;\n}\n\n.image_align[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 45%;\n  z-index: 9;\n  width: 100%;\n  max-height: 30%;\n  overflow: hidden;\n}\n\n.close_icon[_ngcontent-%COMP%] {\n  font-size: 2.3em;\n  float: right;\n}\n\n.logo[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 0 auto;\n}\n\n.row-popup[_ngcontent-%COMP%] {\n  border: 1px solid #09509d;\n  margin: 20px;\n  background-color: #f3eded;\n  position: absolute;\n  top: 30%;\n  z-index: 9;\n}\n\n.col-margin[_ngcontent-%COMP%] {\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n\n.msg-popup[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvZmF2b3VyaXRlcy9zdHlsZXMvZmF2b3VyaXRlcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2Zhdm91cml0ZXMvc3R5bGVzL2Zhdm91cml0ZXMucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL3RoZW1lL21peGlucy9zY3JvbGxiYXJzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDRSx1Q0FBQTtFQUVBLHNEQUFBO0VBRUEsbURBQUE7RUFDQSwyREFBQTtBQ0xGOztBRFNBO0VBQ0Usd0RBQUE7RUFFQSxnREFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VFakJBLHdCQUFBO0VBR0EsOEJBQUE7RUFDQSxxQkFBQTtBRFNGOztBQ05FO0VBQ0UsYUFBQTtBRFFKOztBRE1JO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLDJCQUFBO0VBQ0EsdUJBQUE7QUNKTjs7QURNTTtFQUNFLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsOERBQUE7RUFDQSxnQ0FBQTtFQUNBLGlEQUFBO0VBQ0EscUNBQUE7QUNKUjs7QURXRTtFQUNFLG1DQUFBO0VBQ0EseUNBQUE7QUNUSjs7QURZRTtFQUNFLG1DQUFBO0VBQ0EsMENBQUE7QUNWSjs7QURhRTtFQUNFLG1DQUFBO0VBQ0EseUNBQUE7QUNYSjs7QURjRTtFQUNFLG1DQUFBO0VBQ0EsMkNBQUE7QUNaSjs7QURlRTtFQUNFLG1DQUFBO0VBQ0EseUNBQUE7QUNiSjs7QURpQkE7RUFDRSxrQkFBQTtBQ2RGOztBRGlCQTtFQUNFLFVBQUE7RUFDQSxlQUFBO0FDZEY7O0FEZ0JBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDYkY7O0FEZUE7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUNaRjs7QURjQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ1hGOztBRGFBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQ1ZGOztBRGNBLDZCQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFFQSxrQkFBQTtBQ1pGOztBRGtCQSwwQkFBQTs7QUFFQSw4REFBQTs7QUFDQTtFQUNFLFdBQUE7RUFFQSxhQUFBO0VBQ0EsWUFBQTtFQUFjLDhDQUFBO0FDaEJoQjs7QURvQkE7RUFNRSxZQUFBO0FDdEJGOztBRHdCQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDckJGOztBRHdCQTtFQU9FLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDM0JGOztBRDhCQTtFQU1FLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDaENGOztBRG1DQTtFQU9FLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDdENGOztBRHlDQSxtQ0FBQTs7QUFPQTtFQUNFLDBDQUFBO0VBQ0EseUNBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLGNBQUE7QUM1Q0Y7O0FEK0NBO0VBQ0UsK0JBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUM1Q0Y7O0FEZ0RBO0VBQ0UsbUJBQUE7QUM3Q0Y7O0FEZ0RBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUM3Q0Y7O0FEa0RBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUVBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ2hERjs7QURtREE7RUFDRSw2QkFBQTtFQUVBLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQ0FBQTtFQUNBLCtCQUFBO0FDakRGOztBRHFEQTtFQUVFLGNBQUE7RUFDQSxpQkFBQTtBQ25ERjs7QURzREE7RUFFRSw4QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7QUNwREY7O0FEd0RBO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLGdDQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDckRGOztBRHdEQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUFRLFVBQUE7RUFDUixXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDcERGOztBRHVEQTtFQUlFLGdCQUFBO0VBQ0EsWUFBQTtBQ3ZERjs7QUQwREE7RUFDRSxrQkFBQTtFQUVBLGNBQUE7QUN4REY7O0FEMERBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxVQUFBO0FDdkRGOztBRDBEQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7QUN2REY7O0FEMERBO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDdkRGIiwiZmlsZSI6InNyYy9hcHAvZmF2b3VyaXRlcy9zdHlsZXMvZmF2b3VyaXRlcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IFwiLi4vLi4vLi4vdGhlbWUvbWl4aW5zL3Njcm9sbGJhcnNcIjtcblxuLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1uYXJyb3ctbWFyZ2luKTtcblxuICAtLXBhZ2UtY2F0ZWdvcmllcy1ndXR0ZXI6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gNCk7XG5cbiAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xuICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tcmdiKTtcbn1cblxuLy8gTm90ZTogIEFsbCB0aGUgQ1NTIHZhcmlhYmxlcyBkZWZpbmVkIGJlbG93IGFyZSBvdmVycmlkZXMgb2YgSW9uaWMgZWxlbWVudHMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG4uY2F0ZWdvcmllcy1saXN0IHtcbiAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogdmFyKC0tcGFnZS1jYXRlZ29yaWVzLWd1dHRlcik7XG5cbiAgcGFkZGluZzogY2FsYyh2YXIoLS1wYWdlLWNhdGVnb3JpZXMtZ3V0dGVyKSAqIDMpO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGFsaWduLWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIG92ZXJmbG93OiBzY3JvbGw7XG5cbiAgQGluY2x1ZGUgaGlkZS1zY3JvbGxiYXJzKCk7XG5cbiAgLmNhdGVnb3J5LWl0ZW0ge1xuICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuXG4gICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgICAgICAgcGFkZGluZzogY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pIC8gNCkgKiAzKSB2YXIoLS1wYWdlLW1hcmdpbik7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQpO1xuICAgICAgICBib3JkZXItcmFkaXVzOiB2YXIoLS1hcHAtZmFpci1yYWRpdXMpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIFxuXG4gIC50cmF2ZWwtY2F0ZWdvcnkge1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiAjMDBBRkZGO1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMCwxNzUsMjU1O1xuICB9XG5cbiAgLmZhc2hpb24tY2F0ZWdvcnkge1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiAjY2IzMjhmO1xuICAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMjAzLDUwLDE0MztcbiAgfVxuXG4gIC5mb29kLWNhdGVnb3J5IHtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogI2ViYmIwMDtcbiAgICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDIzNSwxODcsMDtcbiAgfVxuXG4gIC5kZWFscy1jYXRlZ29yeSB7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICM3MGRmNzA7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAxMTIsMjIzLDExMjtcbiAgfVxuXG4gIC5yZWFsLWVzdGF0ZS1jYXRlZ29yeSB7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICNkOTQ1M2E7XG4gICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAyMTcsNjksNTg7XG4gIH1cbn1cblxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBle1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sb2dve1xuICB3aWR0aDogNjAlO1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG4uaG9tZV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuLnNlYXJjaF9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5oZWFydF9pY29ue1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi5jb3B5X2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi8vICoqKioqKioqKioqKioqKioqKioqKlxuLyogU3BsaXQgdGhlIHNjcmVlbiBpbiBoYWxmICovXG4uc3BsaXQge1xuICBoZWlnaHQ6IDI1JTtcbiAgd2lkdGg6IDEwMCU7XG4gIHotaW5kZXg6IDE7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLy8gdG9wOiAwO1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG4gIC8vIHBhZGRpbmctdG9wOiAyMHB4O1xuICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkICM5RTlFOUU7XG59XG5cbi8qIENvbnRyb2wgdGhlIGxlZnQgc2lkZSAqL1xuXG4vKiBDcmVhdGUgdGhyZWUgZXF1YWwgY29sdW1ucyB0aGF0IGZsb2F0cyBuZXh0IHRvIGVhY2ggb3RoZXIgKi9cbi5jb2x1bW4ge1xuICBmbG9hdDogbGVmdDtcbiAgLy8gd2lkdGg6IDMzLjMzJTtcbiAgcGFkZGluZzogMTBweDtcbiAgaGVpZ2h0OiAxMDAlOyAvKiBTaG91bGQgYmUgcmVtb3ZlZC4gT25seSBmb3IgZGVtb25zdHJhdGlvbiAqL1xuICAvLyBib3JkZXI6IDFweCBzb2xpZDtcbn1cblxuLmNlbnRlcmVkIHtcbiAgLy8gcG9zaXRpb246IGFic29sdXRlO1xuICAvLyB0b3A6IDUwJTtcbiAgLy8gbGVmdDogNTAlO1xuICAvLyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogYmFsY2s7XG59XG4uaWNvbi1kZWxldGUtY29se1xuICBwb3NpdGlvbjogYWJzb2x1dGU7IFxuICB0b3A6IDUwJTsgXG4gIGxlZnQ6IDkwJTsgXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGNvbG9yOiAjMDk1MDlkO1xufVxuXG4uaWNvbi1zaGFyZXtcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBwYWRkaW5nLXRvcDogMnB4O1xuICAvLyBjb2xvcjogIzA5NTA5ZDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbiAgLy8gd2lkdGg6IDEyJTtcblxuICB3aWR0aDogMzBweDtcbiAgbWFyZ2luLWJvdHRvbTogLTdweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1kb3dubG9hZHtcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBwYWRkaW5nLXRvcDogNXB4O1xuICAvLyBmb250LXNpemU6IDI1cHg7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICBmb250LXNpemU6IDIycHg7XG4gIG1hcmdpbi1ib3R0b206IC00cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cblxuLmljb24tZGVsZXRle1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIC8vIHBhZGRpbmctdG9wOiA1cHg7XG4gIC8vIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gY29sb3I6ICMwOTUwOWQ7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG5cbiAgZm9udC1zaXplOiAyMnB4O1xuICBtYXJnaW4tYm90dG9tOiAtNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG5cbi8qIENsZWFyIGZsb2F0cyBhZnRlciB0aGUgY29sdW1ucyAqL1xuLy8gLnJvdzphZnRlciB7XG4vLyAgIGNvbnRlbnQ6IFwiXCI7XG4vLyAgIGRpc3BsYXk6IHRhYmxlO1xuLy8gICBjbGVhcjogYm90aDtcbi8vIH1cblxuLnRhYkNzc0NsaWNre1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjMDk1MDlkICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgIzA5NTA5ZCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIHRleHQtZGVjb3JhdGlvbjp1bmRlcmxpbmU7XG4gIGNvbG9yOiAjMDk1MDlkO1xufVxuXG4udGFiQ3Nze1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjZWFlNGU0O1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cblxuLm15LWN1c3RvbS1jbGFzcyB7XG4gIGJhY2tncm91bmQ6ICNlNWU1ZTU7XG59XG5cbi5kZXRhaWwtdGl0bGUge1xuICBmb250LXNpemU6IDE3cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICBtYXJnaW46IDBweDtcbiAgLy8gbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4udGV4dC1tc2d7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiA1MCU7XG4gIC8vIG1hcmdpbi10b3A6IDI1MHB4O1xuICBtYXJnaW4tbGVmdDogMTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4uY29sLXBhZGRpbmd7XG4gIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxNXB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMDk1MDlkO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHggIWltcG9ydGFudDtcbiAgLy8gbWFyZ2luLWJvdHRvbTogNXB4O1xuICAvLyBtYXJnaW4tdG9wOiAtMTBweDtcbn1cbi5tYWluLXRpdGxle1xuICAvLyBmb250LXNpemU6IDE3cHg7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnRvdGFsLXByb2R1Y3R7XG4gIC8vIGZvbnQtc2l6ZTogMTdweDtcbiAgcGFkZGluZy1yaWdodDogMTBweCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuXG4uZGl2LWZpeGVke1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDk5OTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDk1MDlkO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteTogYXV0bztcbn1cblxuLmltYWdlX2FsaWdue1xuICBwb3NpdGlvbjphYnNvbHV0ZTtcbiAgdG9wOjQ1JTt6LWluZGV4OiA5O1xuICB3aWR0aDogMTAwJTsgXG4gIG1heC1oZWlnaHQ6MzAlOyBcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLmNsb3NlX2ljb257XG4gIC8vIGZvbnQtc2l6ZTogMi44ZW07XG4gIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLy8gcmlnaHQ6IDE0cHg7XG4gIGZvbnQtc2l6ZTogMi4zZW07XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLmxvZ297XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgLy93aWR0aDogMTAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuLnJvdy1wb3B1cHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgbWFyZ2luOiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNlZGVkO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMzAlO1xuICB6LWluZGV4OiA5O1xufVxuXG4uY29sLW1hcmdpbntcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLm1zZy1wb3B1cHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbiIsIjpob3N0IHtcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLW5hcnJvdy1tYXJnaW4pO1xuICAtLXBhZ2UtY2F0ZWdvcmllcy1ndXR0ZXI6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gNCk7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcbiAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXJnYik7XG59XG5cbi5jYXRlZ29yaWVzLWxpc3Qge1xuICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiB2YXIoLS1wYWdlLWNhdGVnb3JpZXMtZ3V0dGVyKTtcbiAgcGFkZGluZzogY2FsYyh2YXIoLS1wYWdlLWNhdGVnb3JpZXMtZ3V0dGVyKSAqIDMpO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGFsaWduLWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIG92ZXJmbG93OiBzY3JvbGw7XG4gIC1tcy1vdmVyZmxvdy1zdHlsZTogbm9uZTtcbiAgb3ZlcmZsb3c6IC1tb3otc2Nyb2xsYmFycy1ub25lO1xuICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7XG59XG4uY2F0ZWdvcmllcy1saXN0Ojotd2Via2l0LXNjcm9sbGJhciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4uY2F0ZWdvcmllcy1saXN0IC5jYXRlZ29yeS1pdGVtIC5jYXRlZ29yeS1hbmNob3Ige1xuICBoZWlnaHQ6IDEwMCU7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cbi5jYXRlZ29yaWVzLWxpc3QgLmNhdGVnb3J5LWl0ZW0gLmNhdGVnb3J5LWFuY2hvciAuY2F0ZWdvcnktdGl0bGUge1xuICBtYXJnaW46IGF1dG87XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgcGFkZGluZzogY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pIC8gNCkgKiAzKSB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQpO1xuICBib3JkZXItcmFkaXVzOiB2YXIoLS1hcHAtZmFpci1yYWRpdXMpO1xufVxuLmNhdGVnb3JpZXMtbGlzdCAudHJhdmVsLWNhdGVnb3J5IHtcbiAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICMwMEFGRkY7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMCwxNzUsMjU1O1xufVxuLmNhdGVnb3JpZXMtbGlzdCAuZmFzaGlvbi1jYXRlZ29yeSB7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiAjY2IzMjhmO1xuICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDIwMyw1MCwxNDM7XG59XG4uY2F0ZWdvcmllcy1saXN0IC5mb29kLWNhdGVnb3J5IHtcbiAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICNlYmJiMDA7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMjM1LDE4NywwO1xufVxuLmNhdGVnb3JpZXMtbGlzdCAuZGVhbHMtY2F0ZWdvcnkge1xuICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogIzcwZGY3MDtcbiAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAxMTIsMjIzLDExMjtcbn1cbi5jYXRlZ29yaWVzLWxpc3QgLnJlYWwtZXN0YXRlLWNhdGVnb3J5IHtcbiAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICNkOTQ1M2E7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMjE3LDY5LDU4O1xufVxuXG5pb24taGVhZGVyIGlvbi10b29sYmFyOmZpcnN0LW9mLXR5cGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sb2dvIHtcbiAgd2lkdGg6IDYwJTtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uaG9tZV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLnNlYXJjaF9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5oZWFydF9pY29uIHtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5jb3B5X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4vKiBTcGxpdCB0aGUgc2NyZWVuIGluIGhhbGYgKi9cbi5zcGxpdCB7XG4gIGhlaWdodDogMjUlO1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogMTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG59XG5cbi8qIENvbnRyb2wgdGhlIGxlZnQgc2lkZSAqL1xuLyogQ3JlYXRlIHRocmVlIGVxdWFsIGNvbHVtbnMgdGhhdCBmbG9hdHMgbmV4dCB0byBlYWNoIG90aGVyICovXG4uY29sdW1uIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGhlaWdodDogMTAwJTtcbiAgLyogU2hvdWxkIGJlIHJlbW92ZWQuIE9ubHkgZm9yIGRlbW9uc3RyYXRpb24gKi9cbn1cblxuLmNlbnRlcmVkIHtcbiAgY29sb3I6IGJhbGNrO1xufVxuXG4uaWNvbi1kZWxldGUtY29sIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogOTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogIzA5NTA5ZDtcbn1cblxuLmljb24tc2hhcmUge1xuICB3aWR0aDogMzBweDtcbiAgbWFyZ2luLWJvdHRvbTogLTdweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1kb3dubG9hZCB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgbWFyZ2luLWJvdHRvbTogLTRweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1kZWxldGUge1xuICBmb250LXNpemU6IDIycHg7XG4gIG1hcmdpbi1ib3R0b206IC01cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cblxuLyogQ2xlYXIgZmxvYXRzIGFmdGVyIHRoZSBjb2x1bW5zICovXG4udGFiQ3NzQ2xpY2sge1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjMDk1MDlkICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgIzA5NTA5ZCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBjb2xvcjogIzA5NTA5ZDtcbn1cblxuLnRhYkNzcyB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNlYWU0ZTQ7XG4gIHBhZGRpbmc6IDEwcHggMTVweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm15LWN1c3RvbS1jbGFzcyB7XG4gIGJhY2tncm91bmQ6ICNlNWU1ZTU7XG59XG5cbi5kZXRhaWwtdGl0bGUge1xuICBmb250LXNpemU6IDE3cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICBtYXJnaW46IDBweDtcbn1cblxuLnRleHQtbXNnIHtcbiAgZm9udC1zaXplOiAxN3B4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIG1hcmdpbi1yaWdodDogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLmNvbC1wYWRkaW5nIHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMDk1MDlkO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgcGFkZGluZy1ib3R0b206IDEwcHggIWltcG9ydGFudDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi50b3RhbC1wcm9kdWN0IHtcbiAgcGFkZGluZy1yaWdodDogMTBweCAhaW1wb3J0YW50O1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLmRpdi1maXhlZCB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogOTk5O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwOTUwOWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy15OiBhdXRvO1xufVxuXG4uaW1hZ2VfYWxpZ24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNDUlO1xuICB6LWluZGV4OiA5O1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LWhlaWdodDogMzAlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uY2xvc2VfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMi4zZW07XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLmxvZ28ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4ucm93LXBvcHVwIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgbWFyZ2luOiAyMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjNlZGVkO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMzAlO1xuICB6LWluZGV4OiA5O1xufVxuXG4uY29sLW1hcmdpbiB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5tc2ctcG9wdXAge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iLCIvLyBIaWRlIHNjcm9sbGJhcnM6IGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8zODk5NDgzNy8xMTE2OTU5XG5AbWl4aW4gaGlkZS1zY3JvbGxiYXJzKCkge1xuICAvLyBJRSAxMCtcbiAgLW1zLW92ZXJmbG93LXN0eWxlOiBub25lO1xuXG4gIC8vIEZpcmVmb3hcbiAgb3ZlcmZsb3c6IC1tb3otc2Nyb2xsYmFycy1ub25lO1xuICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7XG5cbiAgLy8gU2FmYXJpIGFuZCBDaHJvbWVcbiAgJjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbiJdfQ== */", "app-image-shell.category-cover[_ngcontent-%COMP%] {\n  --image-shell-loading-background: rgba(var(--page-category-background-rgb), .25);\n  --image-shell-spinner-color: var(--ion-color-lightest);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvZmF2b3VyaXRlcy9zdHlsZXMvZmF2b3VyaXRlcy5zaGVsbC5zY3NzIiwic3JjL2FwcC9mYXZvdXJpdGVzL3N0eWxlcy9mYXZvdXJpdGVzLnNoZWxsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnRkFBQTtFQUNBLHNEQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9mYXZvdXJpdGVzL3N0eWxlcy9mYXZvdXJpdGVzLnNoZWxsLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhcHAtaW1hZ2Utc2hlbGwuY2F0ZWdvcnktY292ZXIge1xuICAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogcmdiYSh2YXIoLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiKSwgLjI1KTtcbiAgLS1pbWFnZS1zaGVsbC1zcGlubmVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xufVxuIiwiYXBwLWltYWdlLXNoZWxsLmNhdGVnb3J5LWNvdmVyIHtcbiAgLS1pbWFnZS1zaGVsbC1sb2FkaW5nLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYiksIC4yNSk7XG4gIC0taW1hZ2Utc2hlbGwtc3Bpbm5lci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbn0iXX0= */", "@media only screen and (min-device-width: 320px) and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (device-aspect-ratio: 2/3) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 16px;\n  }\n}\n\n\n@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (-webkit-min-device-pixel-ratio: 2) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2);\n  }\n}\n\n@media only screen and (min-device-width: 375px) and (max-device-width: 812px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2);\n  }\n}\n\n@media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvZmF2b3VyaXRlcy9zdHlsZXMvZmF2b3VyaXRlcy5yZXNwb25zaXZlLnNjc3MiLCJzcmMvYXBwL2Zhdm91cml0ZXMvc3R5bGVzL2Zhdm91cml0ZXMucmVzcG9uc2l2ZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBLDRDQUFBO0FBQ0E7RUFhUTtJQUNFLGVBQUE7RUNoQlI7QUFDRjtBRHNCQSxxREFBQTtBQWNBLGtEQUFBO0FBQ0E7RUFZUTtJQUNFLGVBQUE7SUFDQSw4REFBQTtFQzVDUjtBQUNGO0FEa0RBLHFDQUFBO0FBQ0E7RUFZUTtJQUNFLGVBQUE7SUFDQSw4REFBQTtFQzNEUjtBQUNGO0FEaUVBLGlEQUFBO0FBQ0E7RUFZUTtJQUNFLGVBQUE7SUFDQSw4REFBQTtFQzFFUjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvZmF2b3VyaXRlcy9zdHlsZXMvZmF2b3VyaXRlcy5yZXNwb25zaXZlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAoTm90ZTogRG9uJ3QgY2hhbmdlIHRoZSBvcmRlciBvZiB0aGUgZGV2aWNlcyBhcyBpdCBtYXkgYnJlYWsgdGhlIGNvcnJlY3QgY3NzIHByZWNlZGVuY2UpXG5cbi8vIChzZWU6IGh0dHBzOi8vY3NzLXRyaWNrcy5jb20vc25pcHBldHMvY3NzL21lZGlhLXF1ZXJpZXMtZm9yLXN0YW5kYXJkLWRldmljZXMvI2lwaG9uZS1xdWVyaWVzKVxuLy8gKHNlZTogaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9hLzQ3NzUwMjYxLzExMTY5NTkpXG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA0IGFuZCA0UyAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG4gIGFuZCAobWluLWRldmljZS13aWR0aCA6IDMyMHB4KVxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA0ODBweClcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpXG4gIGFuZCAoZGV2aWNlLWFzcGVjdC1yYXRpbzogMi8zKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNSwgNVMsIDVDIGFuZCA1U0UgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzMjBweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNTY4cHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKVxuICBhbmQgKGRldmljZS1hc3BlY3QtcmF0aW86IDQwIC8gNzEpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBwb3J0cmFpdDpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSlcbntcblxufVxuXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNiwgNlMsIDcgYW5kIDggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNjY3cHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pICogMykgLyAyKSA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLyogLS0tLS0tLS0tLS0gaVBob25lIFggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogODEycHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvIDogMylcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdClcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxue1xuICAuY2F0ZWdvcmllcy1saXN0IHtcbiAgICAuY2F0ZWdvcnktaXRlbSB7XG4gICAgICAuY2F0ZWdvcnktYW5jaG9yIHtcbiAgICAgICAgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMikgO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2KywgNysgYW5kIDgrIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbiAgYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogNDE0cHgpXG4gIGFuZCAobWF4LWRldmljZS13aWR0aCA6IDczNnB4KVxuICBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMylcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdClcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxue1xuICAuY2F0ZWdvcmllcy1saXN0IHtcbiAgICAuY2F0ZWdvcnktaXRlbSB7XG4gICAgICAuY2F0ZWdvcnktYW5jaG9yIHtcbiAgICAgICAgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMikgO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iLCIvKiAtLS0tLS0tLS0tLSBpUGhvbmUgNCBhbmQgNFMgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDMyMHB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDQ4MHB4KSBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMikgYW5kIChkZXZpY2UtYXNwZWN0LXJhdGlvOiAyLzMpIHtcbiAgLmNhdGVnb3JpZXMtbGlzdCAuY2F0ZWdvcnktaXRlbSAuY2F0ZWdvcnktYW5jaG9yIC5jYXRlZ29yeS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG59XG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNSwgNVMsIDVDIGFuZCA1U0UgLS0tLS0tLS0tLS0gKi9cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2LCA2UywgNyBhbmQgOCAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogMzc1cHgpIGFuZCAobWF4LWRldmljZS13aWR0aDogNjY3cHgpIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSB7XG4gIC5jYXRlZ29yaWVzLWxpc3QgLmNhdGVnb3J5LWl0ZW0gLmNhdGVnb3J5LWFuY2hvciAuY2F0ZWdvcnktdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pICogMykgLyAyKTtcbiAgfVxufVxuLyogLS0tLS0tLS0tLS0gaVBob25lIFggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDgxMnB4KSBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMykge1xuICAuY2F0ZWdvcmllcy1saXN0IC5jYXRlZ29yeS1pdGVtIC5jYXRlZ29yeS1hbmNob3IgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMik7XG4gIH1cbn1cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2KywgNysgYW5kIDgrIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiA3MzZweCkgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpIHtcbiAgLmNhdGVnb3JpZXMtbGlzdCAuY2F0ZWdvcnktaXRlbSAuY2F0ZWdvcnktYW5jaG9yIC5jYXRlZ29yeS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIHBhZGRpbmc6IHZhcigtLXBhZ2UtbWFyZ2luKSBjYWxjKCh2YXIoLS1wYWdlLW1hcmdpbikgKiAzKSAvIDIpO1xuICB9XG59Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](FavouritesPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
                selector: 'app-categories',
                templateUrl: './favourites.page.html',
                styleUrls: [
                    './styles/favourites.page.scss',
                    './styles/favourites.shell.scss',
                    './styles/favourites.responsive.scss'
                ]
            }]
    }], function () { return [{ type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] }, { type: _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"] }, { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }, { type: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["Downloader"] }, { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }, { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__["InAppBrowser"] }, { type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_10__["FirebaseAnalytics"] }, { type: _categories_categories_page__WEBPACK_IMPORTED_MODULE_11__["CategoriesPage"] }, { type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_8__["SocialSharing"] }]; }, { content: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
            args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"], { static: true }]
        }] }); })();


/***/ })

}]);
//# sourceMappingURL=favourites-favourites-module-es2015.js.map