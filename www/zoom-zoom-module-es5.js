function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["zoom-zoom-module"], {
  /***/
  "./src/app/zoom/zoom.module.ts":
  /*!*************************************!*\
    !*** ./src/app/zoom/zoom.module.ts ***!
    \*************************************/

  /*! exports provided: ZoomPageModule */

  /***/
  function srcAppZoomZoomModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ZoomPageModule", function () {
      return ZoomPageModule;
    });
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");
    /* harmony import */


    var _zoom_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./zoom.page */
    "./src/app/zoom/zoom.page.ts");

    var zoomRoutes = [{
      path: '',
      component: _zoom_page__WEBPACK_IMPORTED_MODULE_6__["ZoomPage"]
    }];

    var ZoomPageModule = function ZoomPageModule() {
      _classCallCheck(this, ZoomPageModule);
    };

    ZoomPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
      type: ZoomPageModule
    });
    ZoomPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
      factory: function ZoomPageModule_Factory(t) {
        return new (t || ZoomPageModule)();
      },
      imports: [[_ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(zoomRoutes), _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](ZoomPageModule, {
        declarations: [_zoom_page__WEBPACK_IMPORTED_MODULE_6__["ZoomPage"]],
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](ZoomPageModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"],
        args: [{
          imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(zoomRoutes), _components_components_module__WEBPACK_IMPORTED_MODULE_5__["ComponentsModule"]],
          declarations: [_zoom_page__WEBPACK_IMPORTED_MODULE_6__["ZoomPage"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/zoom/zoom.page.ts":
  /*!***********************************!*\
    !*** ./src/app/zoom/zoom.page.ts ***!
    \***********************************/

  /*! exports provided: ZoomPage */

  /***/
  function srcAppZoomZoomPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ZoomPage", function () {
      return ZoomPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./../app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    var _c0 = ["slides"];

    function ZoomPage_ion_slide_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-slide");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var image_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", image_r2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
      }
    }

    var _c1 = function _c1() {
      return ["/favourites"];
    };

    var _c2 = function _c2() {
      return ["/categories"];
    };

    var ZoomPage = /*#__PURE__*/function () {
      function ZoomPage(alertController, loadingController, route, myapp, navCtrl) {
        var _this = this;

        _classCallCheck(this, ZoomPage);

        this.alertController = alertController;
        this.loadingController = loadingController;
        this.route = route;
        this.myapp = myapp;
        this.navCtrl = navCtrl;
        this.images = [];
        this.currentNumber = 1;
        this.slidesOptions = {
          zoom: {
            toggle: true // Disable zooming to prevent weird double tap zomming on slide images

          }
        };
        this.route.queryParams.subscribe(function (params) {
          console.log("params", params);
          _this.images = params.images;
          _this.currentImageIndex = params.currentImageIndex;
        });
      }

      _createClass(ZoomPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {
          this.slides.slideTo(this.currentImageIndex, 50); // setTimeout(() => {
          // this.slides.slideTo(this.currentImageIndex);
          // });
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          var _this2 = this;

          this.slides.length().then(function (number) {
            _this2.allNumber = number;
            console.log("allNumber", _this2.allNumber);
          });
        }
      }, {
        key: "getIndex",
        value: function getIndex() {
          var _this3 = this;

          this.slides.getActiveIndex().then(function (index) {
            _this3.currentNumber = index + 1;
            console.log("currentNumber", _this3.currentNumber);
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          this.navCtrl.pop();
        }
      }]);

      return ZoomPage;
    }();

    ZoomPage.ɵfac = function ZoomPage_Factory(t) {
      return new (t || ZoomPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]));
    };

    ZoomPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ZoomPage,
      selectors: [["app-categories"]],
      viewQuery: function ZoomPage_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.slides = _t.first);
        }
      },
      decls: 19,
      vars: 8,
      consts: [["color", "primary"], ["slot", "start"], ["src", "assets/logo.png", 1, "logo"], ["slot", "end"], ["name", "heart-outline", 1, "heart_icon", 3, "routerLink"], ["name", "home-outline", 1, "menu_icon", 3, "routerLink"], ["fullscreen", ""], [1, "pinch-div"], ["src", "assets/pinch.png", 1, "pinch-img"], ["pager", "false", 3, "options", "ionSlideDidChange"], ["slides", ""], [4, "ngFor", "ngForOf"], [1, "centered"], [1, "lbl-current-number"], [3, "src"]],
      template: function ZoomPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ion-buttons", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "ion-icon", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "ion-icon", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "ion-content", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "img", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "ion-slides", 9, 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ionSlideDidChange", function ZoomPage_Template_ion_slides_ionSlideDidChange_11_listener() {
            return ctx.getIndex();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ZoomPage_ion_slide_13_Template, 2, 1, "ion-slide", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h4", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](7, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx.slidesOptions);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.images);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.currentNumber);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("/", ctx.allNumber, "");
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonIcon"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["RouterLinkDelegate"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLink"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonSlides"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonSlide"]],
      styles: ["[_nghost-%COMP%] {\n  --page-margin: var(--app-narrow-margin);\n  --page-categories-gutter: calc(var(--page-margin) / 4);\n  --page-category-background: var(--ion-color-medium);\n  --page-category-background-rgb: var(--ion-color-medium-rgb);\n}\n\n.categories-list[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: var(--page-categories-gutter);\n  padding: calc(var(--page-categories-gutter) * 3);\n  height: 100%;\n  align-content: flex-start;\n  overflow: scroll;\n  -ms-overflow-style: none;\n  overflow: -moz-scrollbars-none;\n  scrollbar-width: none;\n}\n\n.categories-list[_ngcontent-%COMP%]::-webkit-scrollbar {\n  display: none;\n}\n\nion-header[_ngcontent-%COMP%]   ion-toolbar[_ngcontent-%COMP%]:first-of-type {\n  text-align: center;\n}\n\n\n\n\n\n\n\n\n\n.swiper-container[_ngcontent-%COMP%] {\n  position: unset;\n}\n\nion-slides[_ngcontent-%COMP%] {\n  height: 100%;\n}\n\n.slides-md[_ngcontent-%COMP%] {\n  --bullet-background: #09509d;\n}\n\n.centered[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 95%;\n  left: 90%;\n  transform: translate(-50%, -50%);\n  z-index: 999;\n  display: inline-flex;\n}\n\n.lbl-current-number[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-top: 12px;\n  margin-right: 2px;\n}\n\n.logo[_ngcontent-%COMP%] {\n  width: 70%;\n  margin-top: 5px;\n}\n\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 10px;\n}\n\n.pinch-div[_ngcontent-%COMP%] {\n  position: absolute;\n  z-index: 999;\n}\n\n.pinch-img[_ngcontent-%COMP%] {\n  width: 10%;\n  float: right;\n  margin-right: 10px;\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvem9vbS9zdHlsZXMvem9vbS5wYWdlLnNjc3MiLCJzcmMvYXBwL3pvb20vc3R5bGVzL3pvb20ucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL3RoZW1lL21peGlucy9zY3JvbGxiYXJzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBSUE7RUFDRSx1Q0FBQTtFQUVBLHNEQUFBO0VBRUEsbURBQUE7RUFDQSwyREFBQTtBQ0xGOztBRFNBO0VBQ0Usd0RBQUE7RUFFQSxnREFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VFakJBLHdCQUFBO0VBR0EsOEJBQUE7RUFDQSxxQkFBQTtBRFNGOztBQ05FO0VBQ0UsYUFBQTtBRFFKOztBRHVEQTtFQUNFLGtCQUFBO0FDcERGOztBRDhFQSw2QkFBQTs7QUFhQSwwQkFBQTs7QUFFQSw4REFBQTs7QUE2REEsbUNBQUE7O0FBdUZBO0VBQ0UsZUFBQTtBQzFPRjs7QUQ2T0E7RUFDRSxZQUFBO0FDMU9GOztBRDZPQTtFQUNFLDRCQUFBO0FDMU9GOztBRDZPQTtFQUNFLGtCQUFBO0VBQ0UsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQzFPSjs7QUQ2T0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQzFPSjs7QUQ2T0E7RUFFRSxVQUFBO0VBQ0EsZUFBQTtBQzNPRjs7QUQ4T0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUMzT0Y7O0FENk9BO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDMU9GOztBRDRPQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtBQ3pPRjs7QUQyT0E7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUN4T0YiLCJmaWxlIjoic3JjL2FwcC96b29tL3N0eWxlcy96b29tLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgXCIuLi8uLi8uLi90aGVtZS9taXhpbnMvc2Nyb2xsYmFyc1wiO1xuXG4vLyBDdXN0b20gdmFyaWFibGVzXG4vLyBOb3RlOiAgVGhlc2Ugb25lcyB3ZXJlIGFkZGVkIGJ5IHVzIGFuZCBoYXZlIG5vdGhpbmcgdG8gZG8gd2l0aCBJb25pYyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbjpob3N0IHtcbiAgLS1wYWdlLW1hcmdpbjogdmFyKC0tYXBwLW5hcnJvdy1tYXJnaW4pO1xuXG4gIC0tcGFnZS1jYXRlZ29yaWVzLWd1dHRlcjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyA0KTtcblxuICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1yZ2IpO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi5jYXRlZ29yaWVzLWxpc3Qge1xuICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiB2YXIoLS1wYWdlLWNhdGVnb3JpZXMtZ3V0dGVyKTtcblxuICBwYWRkaW5nOiBjYWxjKHZhcigtLXBhZ2UtY2F0ZWdvcmllcy1ndXR0ZXIpICogMyk7XG4gIGhlaWdodDogMTAwJTtcbiAgYWxpZ24tY29udGVudDogZmxleC1zdGFydDtcbiAgb3ZlcmZsb3c6IHNjcm9sbDtcblxuICBAaW5jbHVkZSBoaWRlLXNjcm9sbGJhcnMoKTtcblxuICAvLyAuY2F0ZWdvcnktaXRlbSB7XG4gIC8vICAgLmNhdGVnb3J5LWFuY2hvciB7XG4gIC8vICAgICBoZWlnaHQ6IDEwMCU7XG4gIC8vICAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gIC8vICAgICBkaXNwbGF5OiBmbGV4O1xuICAvLyAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICAvLyAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG5cbiAgLy8gICAgIC5jYXRlZ29yeS10aXRsZSB7XG4gIC8vICAgICAgIG1hcmdpbjogYXV0bztcbiAgLy8gICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgLy8gICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgLy8gICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAvLyAgICAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAvLyAgICAgICBwYWRkaW5nOiBjYWxjKCh2YXIoLS1wYWdlLW1hcmdpbikgLyA0KSAqIDMpIHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgLy8gICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gIC8vICAgICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZCk7XG4gIC8vICAgICAgIGJvcmRlci1yYWRpdXM6IHZhcigtLWFwcC1mYWlyLXJhZGl1cyk7XG4gIC8vICAgICB9XG4gIC8vICAgfVxuICAvLyB9XG5cbiAgXG5cbiAgLy8gLnRyYXZlbC1jYXRlZ29yeSB7XG4gIC8vICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICMwMEFGRkY7XG4gIC8vICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAwLDE3NSwyNTU7XG4gIC8vIH1cblxuICAvLyAuZmFzaGlvbi1jYXRlZ29yeSB7XG4gIC8vICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQ6ICNjYjMyOGY7XG4gIC8vICAgLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiOiAyMDMsNTAsMTQzO1xuICAvLyB9XG5cbiAgLy8gLmZvb2QtY2F0ZWdvcnkge1xuICAvLyAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kOiAjZWJiYjAwO1xuICAvLyAgIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogMjM1LDE4NywwO1xuICAvLyB9XG5cbiAgLy8gLmRlYWxzLWNhdGVnb3J5IHtcbiAgLy8gICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogIzcwZGY3MDtcbiAgLy8gICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDExMiwyMjMsMTEyO1xuICAvLyB9XG5cbiAgLy8gLnJlYWwtZXN0YXRlLWNhdGVnb3J5IHtcbiAgLy8gICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogI2Q5NDUzYTtcbiAgLy8gICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZC1yZ2I6IDIxNyw2OSw1ODtcbiAgLy8gfVxufVxuXG5pb24taGVhZGVyIGlvbi10b29sYmFyOmZpcnN0LW9mLXR5cGV7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLy8gLmxvZ297XG4vLyAgIHdpZHRoOiA2MCU7XG4vLyAgIG1hcmdpbi10b3A6IDVweDtcbi8vIH1cbi8vIC5ob21lX2ljb257XG4vLyAgIGZvbnQtc2l6ZTogMjVweDtcbi8vICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4vLyB9XG4vLyAuc2VhcmNoX2ljb257XG4vLyAgIGZvbnQtc2l6ZTogMjVweDtcbi8vICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuLy8gfVxuLy8gLmhlYXJ0X2ljb257XG4vLyAgIGZvbnQtc2l6ZTogMjhweDtcbi8vICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuLy8gfVxuLy8gLmNvcHlfaWNvbntcbi8vICAgZm9udC1zaXplOiAyNXB4O1xuLy8gICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4vLyAgIGNvbG9yOiB3aGl0ZTtcbi8vIH1cblxuLy8gKioqKioqKioqKioqKioqKioqKioqXG4vKiBTcGxpdCB0aGUgc2NyZWVuIGluIGhhbGYgKi9cbi8vIC5zcGxpdCB7XG4vLyAgIC8vIGhlaWdodDogMjUlO1xuLy8gICB3aWR0aDogMTAwJTtcbi8vICAgei1pbmRleDogMTtcbi8vICAgcG9zaXRpb246IHJlbGF0aXZlO1xuLy8gICAvLyB0b3A6IDA7XG4vLyAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbi8vICAgLy8gcGFkZGluZy10b3A6IDIwcHg7XG4vLyAgIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDk1MDlkO1xuLy8gICAvLyBib3JkZXItdG9wOiAxcHggc29saWQgIzlFOUU5RTtcbi8vIH1cblxuLyogQ29udHJvbCB0aGUgbGVmdCBzaWRlICovXG5cbi8qIENyZWF0ZSB0aHJlZSBlcXVhbCBjb2x1bW5zIHRoYXQgZmxvYXRzIG5leHQgdG8gZWFjaCBvdGhlciAqL1xuLy8gLmNvbHVtbiB7XG4vLyAgIGZsb2F0OiBsZWZ0O1xuLy8gICAvLyB3aWR0aDogMzMuMzMlO1xuLy8gICBwYWRkaW5nOiAxMHB4O1xuLy8gICBoZWlnaHQ6IDEwMCU7IC8qIFNob3VsZCBiZSByZW1vdmVkLiBPbmx5IGZvciBkZW1vbnN0cmF0aW9uICovXG4vLyAgIC8vIGJvcmRlcjogMXB4IHNvbGlkO1xuLy8gfVxuXG4vLyAuY2VudGVyZWQge1xuICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8vIHRvcDogNTAlO1xuICAvLyBsZWZ0OiA1MCU7XG4gIC8vIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIC8vIGNvbG9yOiBiYWxjaztcbi8vIH1cbi8vIC5pY29uLWRlbGV0ZS1jb2x7XG4vLyAgIHBvc2l0aW9uOiBhYnNvbHV0ZTsgXG4vLyAgIHRvcDogNTAlOyBcbi8vICAgbGVmdDogOTAlOyBcbi8vICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4vLyAgIGZvbnQtc2l6ZTogMjVweDtcbi8vICAgY29sb3I6ICMwOTUwOWQ7XG4vLyB9XG5cbi8vIC5pY29uLXNoYXJle1xuLy8gICAvLyBmbG9hdDogcmlnaHQ7XG4vLyAgIC8vIHBhZGRpbmctdG9wOiAycHg7XG4vLyAgIC8vIGNvbG9yOiAjMDk1MDlkO1xuLy8gICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuLy8gICAvLyB3aWR0aDogMTIlO1xuXG4vLyAgIHdpZHRoOiAzMHB4O1xuLy8gICBtYXJnaW4tYm90dG9tOiAtN3B4O1xuLy8gICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG4vLyB9XG5cbi8vIC5pY29uLWRvd25sb2Fke1xuLy8gICAvLyBmbG9hdDogcmlnaHQ7XG4vLyAgIC8vIHBhZGRpbmctdG9wOiA1cHg7XG4vLyAgIC8vIGZvbnQtc2l6ZTogMjVweDtcbi8vICAgLy8gY29sb3I6ICMwOTUwOWQ7XG4vLyAgIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4vLyAgIGZvbnQtc2l6ZTogMjJweDtcbi8vICAgbWFyZ2luLWJvdHRvbTogLTRweDtcbi8vICAgcGFkZGluZy1yaWdodDogM3B4O1xuLy8gfVxuXG4vLyAuaWNvbi1kZWxldGV7XG4vLyAgIC8vIGZsb2F0OiByaWdodDtcbi8vICAgLy8gcGFkZGluZy10b3A6IDVweDtcbi8vICAgLy8gZm9udC1zaXplOiAyNXB4O1xuLy8gICAvLyBjb2xvcjogIzA5NTA5ZDtcbi8vICAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcblxuLy8gICBmb250LXNpemU6IDIycHg7XG4vLyAgIG1hcmdpbi1ib3R0b206IC01cHg7XG4vLyAgIHBhZGRpbmctcmlnaHQ6IDNweDtcbi8vIH1cblxuLyogQ2xlYXIgZmxvYXRzIGFmdGVyIHRoZSBjb2x1bW5zICovXG4vLyAucm93OmFmdGVyIHtcbi8vICAgY29udGVudDogXCJcIjtcbi8vICAgZGlzcGxheTogdGFibGU7XG4vLyAgIGNsZWFyOiBib3RoO1xuLy8gfVxuXG4vLyAudGFiQ3NzQ2xpY2t7XG4vLyAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICMwOTUwOWQgIWltcG9ydGFudDtcbi8vICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjMDk1MDlkICFpbXBvcnRhbnQ7XG4vLyAgIHBhZGRpbmc6IDEwcHggMTVweDtcbi8vICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuLy8gICBmb250LXNpemU6IDE0cHg7XG4vLyAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vICAgZm9udC13ZWlnaHQ6IDYwMDtcbi8vICAgdGV4dC1kZWNvcmF0aW9uOnVuZGVybGluZTtcbi8vICAgY29sb3I6ICMwOTUwOWQ7XG4vLyB9XG5cbi8vIC50YWJDc3N7XG4vLyAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNlYWU0ZTQ7XG4vLyAgIHBhZGRpbmc6IDEwcHggMTVweDtcbi8vICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuLy8gICBmb250LXNpemU6IDE0cHg7XG4vLyAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vICAgZm9udC13ZWlnaHQ6IDYwMDtcbi8vIH1cblxuXG4vLyAubXktY3VzdG9tLWNsYXNzIHtcbi8vICAgYmFja2dyb3VuZDogI2U1ZTVlNTtcbi8vIH1cblxuLy8gLmRldGFpbC10aXRsZSB7XG4vLyAgIGZvbnQtc2l6ZTogMTdweDtcbi8vICAgZm9udC13ZWlnaHQ6IDYwMDtcbi8vICAgY29sb3I6ICMwOTUwOWQ7XG4vLyAgIG1hcmdpbjogMHB4O1xuLy8gICAvLyBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuLy8gICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XG4vLyB9XG5cbi8vIC50ZXh0LW1zZ3tcbi8vICAgZm9udC1zaXplOiAxN3B4O1xuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4vLyAgIG1hcmdpbi10b3A6IDUwJTtcbi8vICAgLy8gbWFyZ2luLXRvcDogMjUwcHg7XG4vLyAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xuLy8gICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4vLyAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4vLyB9XG5cbi8vIC5jb2wtcGFkZGluZ3tcbi8vICAgLy8gcGFkZGluZy1sZWZ0OiAxMHB4ICFpbXBvcnRhbnQ7XG4vLyAgIC8vIHBhZGRpbmctcmlnaHQ6IDE1cHggIWltcG9ydGFudDtcbi8vICAgcGFkZGluZy10b3A6IDEwcHggIWltcG9ydGFudDtcbi8vICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICMwOTUwOWQ7XG4vLyAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDk1MDlkO1xuLy8gICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4vLyAgIC8vIG1hcmdpbi1ib3R0b206IDVweDtcbi8vICAgLy8gbWFyZ2luLXRvcDogLTEwcHg7XG4vLyB9XG4vLyAubWFpbi10aXRsZXtcbi8vICAgLy8gZm9udC1zaXplOiAxN3B4O1xuLy8gICBjb2xvcjogIzA5NTA5ZDtcbi8vICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4vLyB9XG5cbi8vIC50b3RhbC1wcm9kdWN0e1xuLy8gICAvLyBmb250LXNpemU6IDE3cHg7XG4vLyAgIHBhZGRpbmctcmlnaHQ6IDEwcHggIWltcG9ydGFudDtcbi8vICAgY29sb3I6ICMwOTUwOWQ7XG4vLyAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuLy8gICBmbG9hdDogcmlnaHQ7XG4vLyB9XG5cblxuLy8gLmRpdi1maXhlZHtcbi8vICAgcG9zaXRpb246IGZpeGVkO1xuLy8gICB6LWluZGV4OiA5OTk7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuLy8gICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzA5NTA5ZDtcbi8vICAgd2lkdGg6IDEwMCU7XG4vLyAgIG92ZXJmbG93LXk6IGF1dG87XG4vLyB9XG5cbi5zd2lwZXItY29udGFpbmVye1xuICBwb3NpdGlvbjogdW5zZXQ7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5zbGlkZXMtbWR7XG4gIC0tYnVsbGV0LWJhY2tncm91bmQ6ICMwOTUwOWQ7XG59XG5cbi5jZW50ZXJlZCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDk1JTtcbiAgICBsZWZ0OiA5MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgei1pbmRleDogOTk5O1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuXG4ubGJsLWN1cnJlbnQtbnVtYmVye1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBtYXJnaW4tdG9wOiAxMnB4O1xuICAgIG1hcmdpbi1yaWdodDogMnB4O1xufVxuXG4ubG9nb3tcbiAgLy8gd2lkdGg6IDY1JTtcbiAgd2lkdGg6IDcwJTtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4ubWVudV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuLmhlYXJ0X2ljb257XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLnBpbmNoLWRpdntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5OTk7XG59XG4ucGluY2gtaW1ne1xuICB3aWR0aDogMTAlO1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDsgXG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4iLCI6aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1uYXJyb3ctbWFyZ2luKTtcbiAgLS1wYWdlLWNhdGVnb3JpZXMtZ3V0dGVyOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDQpO1xuICAtLXBhZ2UtY2F0ZWdvcnktYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG4gIC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1yZ2IpO1xufVxuXG4uY2F0ZWdvcmllcy1saXN0IHtcbiAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogdmFyKC0tcGFnZS1jYXRlZ29yaWVzLWd1dHRlcik7XG4gIHBhZGRpbmc6IGNhbGModmFyKC0tcGFnZS1jYXRlZ29yaWVzLWd1dHRlcikgKiAzKTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBhbGlnbi1jb250ZW50OiBmbGV4LXN0YXJ0O1xuICBvdmVyZmxvdzogc2Nyb2xsO1xuICAtbXMtb3ZlcmZsb3ctc3R5bGU6IG5vbmU7XG4gIG92ZXJmbG93OiAtbW96LXNjcm9sbGJhcnMtbm9uZTtcbiAgc2Nyb2xsYmFyLXdpZHRoOiBub25lO1xufVxuLmNhdGVnb3JpZXMtbGlzdDo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG5pb24taGVhZGVyIGlvbi10b29sYmFyOmZpcnN0LW9mLXR5cGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8qIFNwbGl0IHRoZSBzY3JlZW4gaW4gaGFsZiAqL1xuLyogQ29udHJvbCB0aGUgbGVmdCBzaWRlICovXG4vKiBDcmVhdGUgdGhyZWUgZXF1YWwgY29sdW1ucyB0aGF0IGZsb2F0cyBuZXh0IHRvIGVhY2ggb3RoZXIgKi9cbi8qIENsZWFyIGZsb2F0cyBhZnRlciB0aGUgY29sdW1ucyAqL1xuLnN3aXBlci1jb250YWluZXIge1xuICBwb3NpdGlvbjogdW5zZXQ7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5zbGlkZXMtbWQge1xuICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiAjMDk1MDlkO1xufVxuXG4uY2VudGVyZWQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogOTUlO1xuICBsZWZ0OiA5MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICB6LWluZGV4OiA5OTk7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuXG4ubGJsLWN1cnJlbnQtbnVtYmVyIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tdG9wOiAxMnB4O1xuICBtYXJnaW4tcmlnaHQ6IDJweDtcbn1cblxuLmxvZ28ge1xuICB3aWR0aDogNzAlO1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi5tZW51X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uaGVhcnRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4ucGluY2gtZGl2IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5OTk7XG59XG5cbi5waW5jaC1pbWcge1xuICB3aWR0aDogMTAlO1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn0iLCIvLyBIaWRlIHNjcm9sbGJhcnM6IGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8zODk5NDgzNy8xMTE2OTU5XG5AbWl4aW4gaGlkZS1zY3JvbGxiYXJzKCkge1xuICAvLyBJRSAxMCtcbiAgLW1zLW92ZXJmbG93LXN0eWxlOiBub25lO1xuXG4gIC8vIEZpcmVmb3hcbiAgb3ZlcmZsb3c6IC1tb3otc2Nyb2xsYmFycy1ub25lO1xuICBzY3JvbGxiYXItd2lkdGg6IG5vbmU7XG5cbiAgLy8gU2FmYXJpIGFuZCBDaHJvbWVcbiAgJjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbn1cbiJdfQ== */", "app-image-shell.category-cover[_ngcontent-%COMP%] {\n  --image-shell-loading-background: rgba(var(--page-category-background-rgb), .25);\n  --image-shell-spinner-color: var(--ion-color-lightest);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvem9vbS9zdHlsZXMvem9vbS5zaGVsbC5zY3NzIiwic3JjL2FwcC96b29tL3N0eWxlcy96b29tLnNoZWxsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnRkFBQTtFQUNBLHNEQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC96b29tL3N0eWxlcy96b29tLnNoZWxsLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJhcHAtaW1hZ2Utc2hlbGwuY2F0ZWdvcnktY292ZXIge1xuICAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogcmdiYSh2YXIoLS1wYWdlLWNhdGVnb3J5LWJhY2tncm91bmQtcmdiKSwgLjI1KTtcbiAgLS1pbWFnZS1zaGVsbC1zcGlubmVyLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xufVxuIiwiYXBwLWltYWdlLXNoZWxsLmNhdGVnb3J5LWNvdmVyIHtcbiAgLS1pbWFnZS1zaGVsbC1sb2FkaW5nLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tcGFnZS1jYXRlZ29yeS1iYWNrZ3JvdW5kLXJnYiksIC4yNSk7XG4gIC0taW1hZ2Utc2hlbGwtc3Bpbm5lci1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbn0iXX0= */", "@media only screen and (min-device-width: 320px) and (max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2) and (device-aspect-ratio: 2/3) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 16px;\n  }\n}\n\n\n@media only screen and (min-device-width: 375px) and (max-device-width: 667px) and (-webkit-min-device-pixel-ratio: 2) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2);\n  }\n}\n\n@media only screen and (min-device-width: 375px) and (max-device-width: 812px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2);\n  }\n}\n\n@media only screen and (min-device-width: 414px) and (max-device-width: 736px) and (-webkit-min-device-pixel-ratio: 3) {\n  .categories-list[_ngcontent-%COMP%]   .category-item[_ngcontent-%COMP%]   .category-anchor[_ngcontent-%COMP%]   .category-title[_ngcontent-%COMP%] {\n    font-size: 20px;\n    padding: var(--page-margin) calc((var(--page-margin) * 3) / 2);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvem9vbS9zdHlsZXMvem9vbS5yZXNwb25zaXZlLnNjc3MiLCJzcmMvYXBwL3pvb20vc3R5bGVzL3pvb20ucmVzcG9uc2l2ZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtBLDRDQUFBO0FBQ0E7RUFhUTtJQUNFLGVBQUE7RUNoQlI7QUFDRjtBRHNCQSxxREFBQTtBQWNBLGtEQUFBO0FBQ0E7RUFZUTtJQUNFLGVBQUE7SUFDQSw4REFBQTtFQzVDUjtBQUNGO0FEa0RBLHFDQUFBO0FBQ0E7RUFZUTtJQUNFLGVBQUE7SUFDQSw4REFBQTtFQzNEUjtBQUNGO0FEaUVBLGlEQUFBO0FBQ0E7RUFZUTtJQUNFLGVBQUE7SUFDQSw4REFBQTtFQzFFUjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvem9vbS9zdHlsZXMvem9vbS5yZXNwb25zaXZlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyAoTm90ZTogRG9uJ3QgY2hhbmdlIHRoZSBvcmRlciBvZiB0aGUgZGV2aWNlcyBhcyBpdCBtYXkgYnJlYWsgdGhlIGNvcnJlY3QgY3NzIHByZWNlZGVuY2UpXG5cbi8vIChzZWU6IGh0dHBzOi8vY3NzLXRyaWNrcy5jb20vc25pcHBldHMvY3NzL21lZGlhLXF1ZXJpZXMtZm9yLXN0YW5kYXJkLWRldmljZXMvI2lwaG9uZS1xdWVyaWVzKVxuLy8gKHNlZTogaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9hLzQ3NzUwMjYxLzExMTY5NTkpXG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA0IGFuZCA0UyAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG4gIGFuZCAobWluLWRldmljZS13aWR0aCA6IDMyMHB4KVxuICBhbmQgKG1heC1kZXZpY2Utd2lkdGggOiA0ODBweClcbiAgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDIpXG4gIGFuZCAoZGV2aWNlLWFzcGVjdC1yYXRpbzogMi8zKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNSwgNVMsIDVDIGFuZCA1U0UgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzMjBweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNTY4cHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKVxuICBhbmQgKGRldmljZS1hc3BlY3QtcmF0aW86IDQwIC8gNzEpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBwb3J0cmFpdDpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpXG4gIC8vIHVuY29tbWVudCBmb3Igb25seSBsYW5kc2NhcGU6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSlcbntcblxufVxuXG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNiwgNlMsIDcgYW5kIDggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNjY3cHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgcG9ydHJhaXQ6XG4gIC8vIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KVxuICAvLyB1bmNvbW1lbnQgZm9yIG9ubHkgbGFuZHNjYXBlOlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpXG57XG4gIC5jYXRlZ29yaWVzLWxpc3Qge1xuICAgIC5jYXRlZ29yeS1pdGVtIHtcbiAgICAgIC5jYXRlZ29yeS1hbmNob3Ige1xuICAgICAgICAuY2F0ZWdvcnktdGl0bGUge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pICogMykgLyAyKSA7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLyogLS0tLS0tLS0tLS0gaVBob25lIFggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuICBhbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzNzVweClcbiAgYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogODEycHgpXG4gIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvIDogMylcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdClcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxue1xuICAuY2F0ZWdvcmllcy1saXN0IHtcbiAgICAuY2F0ZWdvcnktaXRlbSB7XG4gICAgICAuY2F0ZWdvcnktYW5jaG9yIHtcbiAgICAgICAgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMikgO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2KywgNysgYW5kIDgrIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbiAgYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogNDE0cHgpXG4gIGFuZCAobWF4LWRldmljZS13aWR0aCA6IDczNnB4KVxuICBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMylcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IHBvcnRyYWl0OlxuICAvLyBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdClcbiAgLy8gdW5jb21tZW50IGZvciBvbmx5IGxhbmRzY2FwZTpcbiAgLy8gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKVxue1xuICAuY2F0ZWdvcmllcy1saXN0IHtcbiAgICAuY2F0ZWdvcnktaXRlbSB7XG4gICAgICAuY2F0ZWdvcnktYW5jaG9yIHtcbiAgICAgICAgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMikgO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iLCIvKiAtLS0tLS0tLS0tLSBpUGhvbmUgNCBhbmQgNFMgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDMyMHB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDQ4MHB4KSBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMikgYW5kIChkZXZpY2UtYXNwZWN0LXJhdGlvOiAyLzMpIHtcbiAgLmNhdGVnb3JpZXMtbGlzdCAuY2F0ZWdvcnktaXRlbSAuY2F0ZWdvcnktYW5jaG9yIC5jYXRlZ29yeS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG59XG4vKiAtLS0tLS0tLS0tLSBpUGhvbmUgNSwgNVMsIDVDIGFuZCA1U0UgLS0tLS0tLS0tLS0gKi9cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2LCA2UywgNyBhbmQgOCAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogMzc1cHgpIGFuZCAobWF4LWRldmljZS13aWR0aDogNjY3cHgpIGFuZCAoLXdlYmtpdC1taW4tZGV2aWNlLXBpeGVsLXJhdGlvOiAyKSB7XG4gIC5jYXRlZ29yaWVzLWxpc3QgLmNhdGVnb3J5LWl0ZW0gLmNhdGVnb3J5LWFuY2hvciAuY2F0ZWdvcnktdGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBwYWRkaW5nOiB2YXIoLS1wYWdlLW1hcmdpbikgY2FsYygodmFyKC0tcGFnZS1tYXJnaW4pICogMykgLyAyKTtcbiAgfVxufVxuLyogLS0tLS0tLS0tLS0gaVBob25lIFggLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDgxMnB4KSBhbmQgKC13ZWJraXQtbWluLWRldmljZS1waXhlbC1yYXRpbzogMykge1xuICAuY2F0ZWdvcmllcy1saXN0IC5jYXRlZ29yeS1pdGVtIC5jYXRlZ29yeS1hbmNob3IgLmNhdGVnb3J5LXRpdGxlIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgcGFkZGluZzogdmFyKC0tcGFnZS1tYXJnaW4pIGNhbGMoKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDMpIC8gMik7XG4gIH1cbn1cbi8qIC0tLS0tLS0tLS0tIGlQaG9uZSA2KywgNysgYW5kIDgrIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA0MTRweCkgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiA3MzZweCkgYW5kICgtd2Via2l0LW1pbi1kZXZpY2UtcGl4ZWwtcmF0aW86IDMpIHtcbiAgLmNhdGVnb3JpZXMtbGlzdCAuY2F0ZWdvcnktaXRlbSAuY2F0ZWdvcnktYW5jaG9yIC5jYXRlZ29yeS10aXRsZSB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIHBhZGRpbmc6IHZhcigtLXBhZ2UtbWFyZ2luKSBjYWxjKCh2YXIoLS1wYWdlLW1hcmdpbikgKiAzKSAvIDIpO1xuICB9XG59Il19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ZoomPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-categories',
          templateUrl: './zoom.page.html',
          styleUrls: ['./styles/zoom.page.scss', './styles/zoom.shell.scss', './styles/zoom.responsive.scss']
        }]
      }], function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]
        }];
      }, {
        slides: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['slides', {
            "static": false
          }]
        }]
      });
    })();
    /***/

  }
}]);
//# sourceMappingURL=zoom-zoom-module-es5.js.map