function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app-shell-app-shell-module"], {
  /***/
  "./src/app/showcase/app-shell/app-shell.module.ts":
  /*!********************************************************!*\
    !*** ./src/app/showcase/app-shell/app-shell.module.ts ***!
    \********************************************************/

  /*! exports provided: AppShellModule */

  /***/
  function srcAppShowcaseAppShellAppShellModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppShellModule", function () {
      return AppShellModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../components/components.module */
    "./src/app/components/components.module.ts");
    /* harmony import */


    var _app_shell_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./app-shell.page */
    "./src/app/showcase/app-shell/app-shell.page.ts");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _aspect_ratio_aspect_ratio_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./aspect-ratio/aspect-ratio.page */
    "./src/app/showcase/app-shell/aspect-ratio/aspect-ratio.page.ts");
    /* harmony import */


    var _image_shell_image_shell_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./image-shell/image-shell.page */
    "./src/app/showcase/app-shell/image-shell/image-shell.page.ts");
    /* harmony import */


    var _text_shell_text_shell_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./text-shell/text-shell.page */
    "./src/app/showcase/app-shell/text-shell/text-shell.page.ts");
    /* harmony import */


    var _simple_data_binding_simple_data_binding_page__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./simple-data-binding/simple-data-binding.page */
    "./src/app/showcase/app-shell/simple-data-binding/simple-data-binding.page.ts");
    /* harmony import */


    var _data_store_basic_data_store_basic_page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./data-store-basic/data-store-basic.page */
    "./src/app/showcase/app-shell/data-store-basic/data-store-basic.page.ts");
    /* harmony import */


    var _data_store_list_data_store_list_page__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./data-store-list/data-store-list.page */
    "./src/app/showcase/app-shell/data-store-list/data-store-list.page.ts");
    /* harmony import */


    var _data_store_subset_data_store_subset_page__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./data-store-subset/data-store-subset.page */
    "./src/app/showcase/app-shell/data-store-subset/data-store-subset.page.ts");
    /* harmony import */


    var _data_store_combined_data_store_combined_page__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./data-store-combined/data-store-combined.page */
    "./src/app/showcase/app-shell/data-store-combined/data-store-combined.page.ts");
    /* harmony import */


    var _data_store_multiple_data_store_multiple_page__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./data-store-multiple/data-store-multiple.page */
    "./src/app/showcase/app-shell/data-store-multiple/data-store-multiple.page.ts");
    /* harmony import */


    var _data_store_pagination_data_store_pagination_page__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ./data-store-pagination/data-store-pagination.page */
    "./src/app/showcase/app-shell/data-store-pagination/data-store-pagination.page.ts");
    /* harmony import */


    var _data_store_stacked_data_store_stacked_page__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ./data-store-stacked/data-store-stacked.page */
    "./src/app/showcase/app-shell/data-store-stacked/data-store-stacked.page.ts");
    /* harmony import */


    var _data_store_dependant_data_store_dependant_page__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./data-store-dependant/data-store-dependant.page */
    "./src/app/showcase/app-shell/data-store-dependant/data-store-dependant.page.ts");
    /* harmony import */


    var _product_listing_product_listing_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ./../../product/listing/product-listing.module */
    "./src/app/product/listing/product-listing.module.ts");

    var routes = [{
      path: '',
      component: _app_shell_page__WEBPACK_IMPORTED_MODULE_7__["AppShellPage"]
    }, {
      path: 'aspect-ratio',
      component: _aspect_ratio_aspect_ratio_page__WEBPACK_IMPORTED_MODULE_9__["AspectRatioPage"]
    }, {
      path: 'image-shell',
      component: _image_shell_image_shell_page__WEBPACK_IMPORTED_MODULE_10__["ImageShellPage"]
    }, {
      path: 'text-shell',
      component: _text_shell_text_shell_page__WEBPACK_IMPORTED_MODULE_11__["TextShellPage"]
    }, {
      path: 'simple-data-binding',
      component: _simple_data_binding_simple_data_binding_page__WEBPACK_IMPORTED_MODULE_12__["SimpleDataBindingPage"]
    }, {
      path: 'data-store-basic',
      component: _data_store_basic_data_store_basic_page__WEBPACK_IMPORTED_MODULE_13__["DataStoreBasicPage"]
    }, {
      path: 'data-store-list',
      component: _data_store_list_data_store_list_page__WEBPACK_IMPORTED_MODULE_14__["DataStoreListPage"]
    }, {
      path: 'data-store-subset',
      component: _data_store_subset_data_store_subset_page__WEBPACK_IMPORTED_MODULE_15__["DataStoreSubsetPage"]
    }, {
      path: 'data-store-combined',
      component: _data_store_combined_data_store_combined_page__WEBPACK_IMPORTED_MODULE_16__["DataStoreCombinedPage"]
    }, {
      path: 'data-store-multiple',
      component: _data_store_multiple_data_store_multiple_page__WEBPACK_IMPORTED_MODULE_17__["DataStoreMultiplePage"]
    }, {
      path: 'data-store-pagination',
      component: _data_store_pagination_data_store_pagination_page__WEBPACK_IMPORTED_MODULE_18__["DataStorePaginationPage"]
    }, {
      path: 'data-store-stacked',
      component: _data_store_stacked_data_store_stacked_page__WEBPACK_IMPORTED_MODULE_19__["DataStoreStackedPage"]
    }, {
      path: 'data-store-dependant',
      component: _data_store_dependant_data_store_dependant_page__WEBPACK_IMPORTED_MODULE_20__["DataStoreDependantPage"]
    }];

    var AppShellModule = function AppShellModule() {
      _classCallCheck(this, AppShellModule);
    };

    AppShellModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: AppShellModule
    });
    AppShellModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function AppShellModule_Factory(t) {
        return new (t || AppShellModule)();
      },
      providers: [_showcase_service__WEBPACK_IMPORTED_MODULE_8__["ShowcaseService"]],
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _product_listing_product_listing_module__WEBPACK_IMPORTED_MODULE_21__["ProductListingPageModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppShellModule, {
        declarations: [_app_shell_page__WEBPACK_IMPORTED_MODULE_7__["AppShellPage"], _aspect_ratio_aspect_ratio_page__WEBPACK_IMPORTED_MODULE_9__["AspectRatioPage"], _image_shell_image_shell_page__WEBPACK_IMPORTED_MODULE_10__["ImageShellPage"], _text_shell_text_shell_page__WEBPACK_IMPORTED_MODULE_11__["TextShellPage"], _simple_data_binding_simple_data_binding_page__WEBPACK_IMPORTED_MODULE_12__["SimpleDataBindingPage"], _data_store_basic_data_store_basic_page__WEBPACK_IMPORTED_MODULE_13__["DataStoreBasicPage"], _data_store_list_data_store_list_page__WEBPACK_IMPORTED_MODULE_14__["DataStoreListPage"], _data_store_subset_data_store_subset_page__WEBPACK_IMPORTED_MODULE_15__["DataStoreSubsetPage"], _data_store_combined_data_store_combined_page__WEBPACK_IMPORTED_MODULE_16__["DataStoreCombinedPage"], _data_store_multiple_data_store_multiple_page__WEBPACK_IMPORTED_MODULE_17__["DataStoreMultiplePage"], _data_store_pagination_data_store_pagination_page__WEBPACK_IMPORTED_MODULE_18__["DataStorePaginationPage"], _data_store_stacked_data_store_stacked_page__WEBPACK_IMPORTED_MODULE_19__["DataStoreStackedPage"], _data_store_dependant_data_store_dependant_page__WEBPACK_IMPORTED_MODULE_20__["DataStoreDependantPage"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _product_listing_product_listing_module__WEBPACK_IMPORTED_MODULE_21__["ProductListingPageModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppShellModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _product_listing_product_listing_module__WEBPACK_IMPORTED_MODULE_21__["ProductListingPageModule"]],
          declarations: [_app_shell_page__WEBPACK_IMPORTED_MODULE_7__["AppShellPage"], _aspect_ratio_aspect_ratio_page__WEBPACK_IMPORTED_MODULE_9__["AspectRatioPage"], _image_shell_image_shell_page__WEBPACK_IMPORTED_MODULE_10__["ImageShellPage"], _text_shell_text_shell_page__WEBPACK_IMPORTED_MODULE_11__["TextShellPage"], _simple_data_binding_simple_data_binding_page__WEBPACK_IMPORTED_MODULE_12__["SimpleDataBindingPage"], _data_store_basic_data_store_basic_page__WEBPACK_IMPORTED_MODULE_13__["DataStoreBasicPage"], _data_store_list_data_store_list_page__WEBPACK_IMPORTED_MODULE_14__["DataStoreListPage"], _data_store_subset_data_store_subset_page__WEBPACK_IMPORTED_MODULE_15__["DataStoreSubsetPage"], _data_store_combined_data_store_combined_page__WEBPACK_IMPORTED_MODULE_16__["DataStoreCombinedPage"], _data_store_multiple_data_store_multiple_page__WEBPACK_IMPORTED_MODULE_17__["DataStoreMultiplePage"], _data_store_pagination_data_store_pagination_page__WEBPACK_IMPORTED_MODULE_18__["DataStorePaginationPage"], _data_store_stacked_data_store_stacked_page__WEBPACK_IMPORTED_MODULE_19__["DataStoreStackedPage"], _data_store_dependant_data_store_dependant_page__WEBPACK_IMPORTED_MODULE_20__["DataStoreDependantPage"]],
          providers: [_showcase_service__WEBPACK_IMPORTED_MODULE_8__["ShowcaseService"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/aspect-ratio/aspect-ratio.page.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/showcase/app-shell/aspect-ratio/aspect-ratio.page.ts ***!
    \**********************************************************************/

  /*! exports provided: AspectRatioPage */

  /***/
  function srcAppShowcaseAppShellAspectRatioAspectRatioPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AspectRatioPage", function () {
      return AspectRatioPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");

    var _c0 = function _c0() {
      return {
        w: 2,
        h: 1
      };
    };

    var AspectRatioPage = /*#__PURE__*/function () {
      function AspectRatioPage() {
        _classCallCheck(this, AspectRatioPage);
      }

      _createClass(AspectRatioPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AspectRatioPage;
    }();

    AspectRatioPage.ɵfac = function AspectRatioPage_Factory(t) {
      return new (t || AspectRatioPage)();
    };

    AspectRatioPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AspectRatioPage,
      selectors: [["app-aspect-ratio-page"]],
      decls: 41,
      vars: 2,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], ["href", "http://stackoverflow.com/a/10441480/1116959", "target", "_blank"], [2, "width", "80%", "margin", "0px auto"], [2, "background-color", "#00AFFF", 3, "ratio"], [2, "margin", "10px", "display", "block"]],
      template: function AspectRatioPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Aspect Ratio ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " The ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "<app-aspect-ratio>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " is a simple component that ensures a block will maintain the specified aspect ratio. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " It uses an ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "old CSS technique");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " to adjust the height of the element based on its width (using padding-bottom). ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " This is a very handy component to prevent content from jumping around while the page is loading. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " The ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "<app-aspect-ratio>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " component must be surrounded by a container element with a defined width, as this component will fill the parent width. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "<div style=\"width:80%; margin: 0px auto;\">");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "\n  <app-aspect-ratio [ratio]=\"{w:2, h:1}\">\n    <span>This container will always have a 2:1 aspect ratio</span>\n  </app-aspect-ratio>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "</div>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "app-aspect-ratio", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "span", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "This container will always have a ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "2:1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, " aspect ratio");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonContent"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_2__["AspectRatioComponent"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2FzcGVjdC1yYXRpby9hc3BlY3QtcmF0aW8ucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvYXBwLXNoZWxsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7RUFDRSxtQ0FBQTtBREVGO0FDQUU7RUFDRSx3Q0FBQTtFQUNBLGtDQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FERUo7QUNDRTtFQUNFLHNCQUFBO0FEQ0o7QUNFRTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QURBSjtBQ0dFO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FEREo7QUNJQTtFQUVFLGVBQUE7QURGRjtBQ09BO0VBQ0Usa0JBQUE7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QURKSjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSw2QkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QURIRjtBQ0tBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FERkY7QUNJQTtFQUNFLHVCQUFBO0FEREY7QUNVQTtFQUNFLGNBSlM7RUFLVCxtQkFKTztFQUtQLGtCQUFBO0FEUEY7QUNTQTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURORjtBQ1FBO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtBRExGO0FDT0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FESkY7QUNPQTtFQUNFLGFBQUE7QURKRjtBQ09FO0VBQ0UsT0FBQTtBRExKO0FDV0EscUJBQUE7QUFDQTtFQUVFLGdCQUFBO0FEVkY7QUNhQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QURWRjtBQ1lFO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUVBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0EsU0FBQTtBRFhKO0FDZUk7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FEYk47QUNtQkU7RUFDRSxhQUFBO0VBRUEsY0F2RU87RUF3RVAsaUJBQUE7RUFDQSxxQkFBQTtBRGxCSjtBQ29CRTtFQUNFLGFBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQWhGTztFQWlGUCxlQUFBO0FEbEJKO0FDNkJJO0VBQ0Usd0JBQUE7QUQzQk47QUM4QkU7RUFDRSxpQkFBQTtBRDVCSjtBQ2lDQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUQ5QkYiLCJmaWxlIjoic3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvYXNwZWN0LXJhdGlvL2FzcGVjdC1yYXRpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG4uc2hvd2Nhc2UtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xufVxuLnNob3djYXNlLWNvbnRlbnQgaW9uLWl0ZW0tZGl2aWRlciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG4uc2hvd2Nhc2UtY29udGVudCAuc2hvd2Nhc2Utc2VjdGlvbiB7XG4gIG1hcmdpbjogMjBweCAyMHB4IDYwcHg7XG59XG4uc2hvd2Nhc2UtY29udGVudCBwcmUge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJhY2tncm91bmQ6ICNDQ0M7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG4uc2hvd2Nhc2UtY29udGVudCBjb2RlIHtcbiAgY29sb3I6ICNGOTI2NzI7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5tZW51X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG59XG5cbi5jbHNvZS1pY29uIHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICBmb250LXNpemU6IDMwcHg7XG59XG5cbi5oZWFydF9pY29uIHtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmNvcHlfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5tYWluLXRpdGxlIHtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5pY29uX2Nsb3NlIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuN2VtO1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuXG4ubGlzdC1tZCB7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5ib2R5IHtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIGJhY2tncm91bmQ6ICNlY2YwZjE7XG4gIHBhZGRpbmc6IDAgMWVtIDFlbTtcbn1cblxuaDEge1xuICBtYXJnaW46IDA7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmgyIHtcbiAgbWFyZ2luOiAwIDAgMC41ZW07XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5cbmlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiAtMTtcbn1cblxuLnJvdyB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4ucm93IC5jb2wge1xuICBmbGV4OiAxO1xufVxuLyogQWNjb3JkaW9uIHN0eWxlcyAqL1xuLnRhYnMge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4udGFiIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi50YWItbGFiZWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgLyogSWNvbiAqL1xufVxuLnRhYi1sYWJlbDo6YWZ0ZXIge1xuICBjb250ZW50OiBcIuKdr1wiO1xuICB3aWR0aDogMWVtO1xuICBoZWlnaHQ6IDFlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXM7XG59XG4udGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAwO1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY2xvc2Uge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBwYWRkaW5nOiAxZW07XG4gIGZvbnQtc2l6ZTogMC43NWVtO1xuICBiYWNrZ3JvdW5kOiAjMmMzZTUwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5pbnB1dDpjaGVja2VkICsgLnRhYi1sYWJlbDo6YWZ0ZXIge1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG59XG5pbnB1dDpjaGVja2VkIH4gLnRhYi1jb250ZW50IHtcbiAgbWF4LWhlaWdodDogMTAwdmg7XG59XG5cbi5iZWRnZS1hbGlnbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDc1JTtcbn0iLCIuc2hvd2Nhc2UtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xuXG4gIGlvbi1pdGVtLWRpdmlkZXIge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgfVxuXG4gIC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xuICB9XG5cbiAgcHJlIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYmFja2dyb3VuZDogI0NDQztcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG5cbiAgY29kZSB7XG4gICAgY29sb3I6ICNGOTI2NzI7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgfVxufVxuLm1lbnVfaWNvbntcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI1cHg7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIC8vIHBhZGRpbmctdG9wOiA3cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jbHNvZS1pY29ue1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICBmb250LXNpemU6IDMwcHg7XG59XG4uaGVhcnRfaWNvbntcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNvcHlfaWNvbntcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLm1haW4tdGl0bGV7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLmljb25fY2xvc2V7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cbi5saXN0LW1ke1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuXG4vLyAqKioqKioqKioqKioqKioqKioqKioqKioqIEFjY29yZGlhblxuXG4kbWlkbmlnaHQ6ICMyYzNlNTA7XG4kY2xvdWRzOiAjZWNmMGYxO1xuLy8gR2VuZXJhbFxuYm9keSB7XG4gIGNvbG9yOiAkbWlkbmlnaHQ7XG4gIGJhY2tncm91bmQ6ICRjbG91ZHM7XG4gIHBhZGRpbmc6IDAgMWVtIDFlbTtcbn1cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaDIge1xuICBtYXJnaW46IDAgMCAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuLy8gTGF5b3V0XG4ucm93IHtcbiAgZGlzcGxheTpmbGV4O1xuICAvLyBtYXJnaW4tbGVmdDogMTBweDsgXG4gIC8vIG1hcmdpbi1yaWdodDogMTBweDtcbiAgLmNvbCB7XG4gICAgZmxleDoxO1xuICAgICY6bGFzdC1jaGlsZCB7XG4gICAgICAvLyBtYXJnaW4tbGVmdDogMWVtO1xuICAgIH1cbiAgfVxufVxuLyogQWNjb3JkaW9uIHN0eWxlcyAqL1xuLnRhYnMge1xuICAvLyBib3JkZXItcmFkaXVzOiA4cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJveC1zaGFkb3c6IDAgNHB4IDRweCAtMnB4IHJnYmEoMCwwLDAsMC41KTtcbn1cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgJi1sYWJlbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgcGFkZGluZzogMWVtO1xuICAgIC8vIGJhY2tncm91bmQ6ICRtaWRuaWdodDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgICAvKiBJY29uICovXG4gICAgJjpob3ZlciB7XG4gICAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgIH1cbiAgICAmOjphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlxcMjc2RlwiO1xuICAgICAgd2lkdGg6IDFlbTtcbiAgICAgIGhlaWdodDogMWVtO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gICAgfVxuICAgIC8vIC5hY3RpdmU6YWZ0ZXIge1xuICAgIC8vICAgY29udGVudDogXCJcXDI3OTZcIjsgLyogVW5pY29kZSBjaGFyYWN0ZXIgZm9yIFwibWludXNcIiBzaWduICgtKSAqL1xuICAgIC8vIH1cbiAgfVxuICAmLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDA7XG4gICAgLy8gcGFkZGluZzogMCAxZW07XG4gICAgY29sb3I6ICRtaWRuaWdodDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgfVxuICAmLWNsb3NlIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgcGFkZGluZzogMWVtO1xuICAgIGZvbnQtc2l6ZTogMC43NWVtO1xuICAgIGJhY2tncm91bmQ6ICRtaWRuaWdodDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgJjpob3ZlciB7XG4gICAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgIH1cbiAgfVxufVxuXG4vLyA6Y2hlY2tlZFxuaW5wdXQ6Y2hlY2tlZCB7XG4gICsgLnRhYi1sYWJlbCB7XG4gICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICAmOjphZnRlciB7XG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gICAgfVxuICB9XG4gIH4gLnRhYi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAxMDB2aDtcbiAgICAvLyBwYWRkaW5nOiAxZW07XG4gIH1cbn1cblxuLmJlZGdlLWFsaWdue1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59XG5cbi8vIC5iZWRnZS1hbGlnbi1tcnB7XG4vLyAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbi8vICAgdG9wOiA2NXB4O1xuLy8gICByaWdodDogNDVweFxuLy8gfVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AspectRatioPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-aspect-ratio-page',
          templateUrl: './aspect-ratio.page.html',
          styleUrls: ['./aspect-ratio.page.scss']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-basic/data-store-basic.page.ts":
  /*!******************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-basic/data-store-basic.page.ts ***!
    \******************************************************************************/

  /*! exports provided: DataStoreBasicPage */

  /***/
  function srcAppShowcaseAppShellDataStoreBasicDataStoreBasicPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStoreBasicPage", function () {
      return DataStoreBasicPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../showcase-shell.model */
    "./src/app/showcase/showcase-shell.model.ts");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    var _c0 = function _c0() {
      return {
        w: 2,
        h: 1
      };
    };

    var _c1 = function _c1() {
      return {
        w: 1,
        h: 1
      };
    };

    var DataStoreBasicPage = /*#__PURE__*/function () {
      function DataStoreBasicPage(showcaseService) {
        _classCallCheck(this, DataStoreBasicPage);

        this.showcaseService = showcaseService;
        this.dataStoreButtonDisabled = true;
      }

      _createClass(DataStoreBasicPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.showcaseDataStore();
        }
      }, {
        key: "showcaseDataStore",
        value: function showcaseDataStore() {
          var _this = this;

          // Prevent rage clicks on the 'Fetch more Data' button
          this.dataStoreButtonDisabled = true;
          var dataSource = this.showcaseService.getSimpleDataSource(); // Initialize the model specifying that it is a shell model

          var shellModel = new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellModel"]();
          var dataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_4__["DataStore"](shellModel); // Trigger the loading mechanism (with shell) in the dataStore

          dataStore.load(dataSource);
          dataStore.state.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["take"])(2), // DataStore will emit a mock object and the real data fetched from the source. Emit those two values and then complete.
          Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["finalize"])(function () {
            return _this.dataStoreButtonDisabled = false;
          })).subscribe(function (data) {
            _this.dataStoreData = data;
          });
        }
      }]);

      return DataStoreBasicPage;
    }();

    DataStoreBasicPage.ɵfac = function DataStoreBasicPage_Factory(t) {
      return new (t || DataStoreBasicPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_3__["ShowcaseService"]));
    };

    DataStoreBasicPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStoreBasicPage,
      selectors: [["app-data-store-basic"]],
      decls: 31,
      vars: 12,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], [2, "background-color", "#FFF", "padding", "10px", "margin", "20px"], ["size", "12"], ["animation", "spinner", 1, "add-overlay", 3, "display", "src", "alt"], [3, "ratio"], [2, "margin", "10px", "text-align", "center", "color", "#FFF"], ["size", "4"], ["animation", "spinner", 3, "src", "alt"], ["size", "8"], [2, "margin-top", "0px"], [3, "data"], ["lines", "3", 3, "data"], [3, "disabled", "click"]],
      template: function DataStoreBasicPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - Basic example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Using the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "DataStore");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " utility you can keep track and append shell values to the data stream. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " This drastically reduces the boilerplate needed to add app shell loading interactions to your app. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "ion-row", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ion-col", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "app-image-shell", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "app-aspect-ratio", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "h4", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Heading on top of a cover image");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "ion-col", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "app-aspect-ratio", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "app-image-shell", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "ion-col", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "h3", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "app-text-shell", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "app-text-shell", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "ion-button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DataStoreBasicPage_Template_ion_button_click_29_listener() {
            return ctx.showcaseDataStore();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Fetch more Data");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover")("src", ctx.dataStoreData == null ? null : ctx.dataStoreData.cover)("alt", "Sample Image Cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.dataStoreData == null ? null : ctx.dataStoreData.image)("alt", "Sample Image");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.dataStoreData == null ? null : ctx.dataStoreData.title);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.dataStoreData == null ? null : ctx.dataStoreData.description);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.dataStoreButtonDisabled);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonCol"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_6__["ImageShellComponent"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_7__["AspectRatioComponent"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_8__["TextShellComponent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonButton"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtYmFzaWMvZGF0YS1zdG9yZS1iYXNpYy5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9hcHAtc2hlbGwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLG1DQUFBO0FERUY7QUNBRTtFQUNFLHdDQUFBO0VBQ0Esa0NBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QURFSjtBQ0NFO0VBQ0Usc0JBQUE7QURDSjtBQ0VFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBREFKO0FDR0U7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QURESjtBQ0lBO0VBRUUsZUFBQTtBREZGO0FDT0E7RUFDRSxrQkFBQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBREpKO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLDZCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtBREhGO0FDS0E7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURGRjtBQ0lBO0VBQ0UsdUJBQUE7QURERjtBQ1VBO0VBQ0UsY0FKUztFQUtULG1CQUpPO0VBS1Asa0JBQUE7QURQRjtBQ1NBO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBRE5GO0FDUUE7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FETEY7QUNPQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QURKRjtBQ09BO0VBQ0UsYUFBQTtBREpGO0FDT0U7RUFDRSxPQUFBO0FETEo7QUNXQSxxQkFBQTtBQUNBO0VBRUUsZ0JBQUE7QURWRjtBQ2FBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBRFZGO0FDWUU7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBRUEsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSxTQUFBO0FEWEo7QUNlSTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QURiTjtBQ21CRTtFQUNFLGFBQUE7RUFFQSxjQXZFTztFQXdFUCxpQkFBQTtFQUNBLHFCQUFBO0FEbEJKO0FDb0JFO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBaEZPO0VBaUZQLGVBQUE7QURsQko7QUM2Qkk7RUFDRSx3QkFBQTtBRDNCTjtBQzhCRTtFQUNFLGlCQUFBO0FENUJKO0FDaUNBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBRDlCRiIsImZpbGUiOiJzcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9kYXRhLXN0b3JlLWJhc2ljL2RhdGEtc3RvcmUtYmFzaWMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogNTAwO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgLnNob3djYXNlLXNlY3Rpb24ge1xuICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgcHJlIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgY29kZSB7XG4gIGNvbG9yOiAjRjkyNjcyO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ubWVudV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uY2xzb2UtaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG4uaGVhcnRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5jb3B5X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubWFpbi10aXRsZSB7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uaWNvbl9jbG9zZSB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cblxuLmxpc3QtbWQge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuYm9keSB7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5oMiB7XG4gIG1hcmdpbjogMCAwIDAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuXG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG5cbi5yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnJvdyAuY29sIHtcbiAgZmxleDogMTtcbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4udGFiLWxhYmVsIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAxZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gIC8qIEljb24gKi9cbn1cbi50YWItbGFiZWw6OmFmdGVyIHtcbiAgY29udGVudDogXCLina9cIjtcbiAgd2lkdGg6IDFlbTtcbiAgaGVpZ2h0OiAxZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jb250ZW50IHtcbiAgbWF4LWhlaWdodDogMDtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXM7XG59XG4udGFiLWNsb3NlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXNpemU6IDAuNzVlbTtcbiAgYmFja2dyb3VuZDogIzJjM2U1MDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuaW5wdXQ6Y2hlY2tlZCArIC50YWItbGFiZWw6OmFmdGVyIHtcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xufVxuaW5wdXQ6Y2hlY2tlZCB+IC50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDEwMHZoO1xufVxuXG4uYmVkZ2UtYWxpZ24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59IiwiLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcblxuICBpb24taXRlbS1kaXZpZGVyIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIH1cblxuICAuc2hvd2Nhc2Utc2VjdGlvbiB7XG4gICAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbiAgfVxuXG4gIHByZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJhY2tncm91bmQ6ICNDQ0M7XG4gICAgcGFkZGluZzogMTBweDtcbiAgfVxuXG4gIGNvZGUge1xuICAgIGNvbG9yOiAjRjkyNjcyO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbn1cbi5tZW51X2ljb257XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogN3B4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY2xzb2UtaWNvbntcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgZm9udC1zaXplOiAzMHB4O1xufVxuLmhlYXJ0X2ljb257XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jb3B5X2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5tYWluLXRpdGxle1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5pY29uX2Nsb3Nle1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG4ubGlzdC1tZHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cblxuLy8gKioqKioqKioqKioqKioqKioqKioqKioqKiBBY2NvcmRpYW5cblxuJG1pZG5pZ2h0OiAjMmMzZTUwO1xuJGNsb3VkczogI2VjZjBmMTtcbi8vIEdlbmVyYWxcbmJvZHkge1xuICBjb2xvcjogJG1pZG5pZ2h0O1xuICBiYWNrZ3JvdW5kOiAkY2xvdWRzO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmgyIHtcbiAgbWFyZ2luOiAwIDAgLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbmlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiAtMTtcbn1cbi8vIExheW91dFxuLnJvdyB7XG4gIGRpc3BsYXk6ZmxleDtcbiAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7IFxuICAvLyBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIC5jb2wge1xuICAgIGZsZXg6MTtcbiAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDFlbTtcbiAgICB9XG4gIH1cbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgLy8gYm9yZGVyLXJhZGl1czogOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3gtc2hhZG93OiAwIDRweCA0cHggLTJweCByZ2JhKDAsMCwwLDAuNSk7XG59XG4udGFiIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICYtbGFiZWwge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICAgLyogSWNvbiAqL1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcXDI3NkZcIjtcbiAgICAgIHdpZHRoOiAxZW07XG4gICAgICBoZWlnaHQ6IDFlbTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICAgIH1cbiAgICAvLyAuYWN0aXZlOmFmdGVyIHtcbiAgICAvLyAgIGNvbnRlbnQ6IFwiXFwyNzk2XCI7IC8qIFVuaWNvZGUgY2hhcmFjdGVyIGZvciBcIm1pbnVzXCIgc2lnbiAoLSkgKi9cbiAgICAvLyB9XG4gIH1cbiAgJi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAwO1xuICAgIC8vIHBhZGRpbmc6IDAgMWVtO1xuICAgIGNvbG9yOiAkbWlkbmlnaHQ7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gIH1cbiAgJi1jbG9zZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICBmb250LXNpemU6IDAuNzVlbTtcbiAgICBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gIH1cbn1cblxuLy8gOmNoZWNrZWRcbmlucHV0OmNoZWNrZWQge1xuICArIC50YWItbGFiZWwge1xuICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgIH1cbiAgfVxuICB+IC50YWItY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMTAwdmg7XG4gICAgLy8gcGFkZGluZzogMWVtO1xuICB9XG59XG5cbi5iZWRnZS1hbGlnbntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4vLyAuYmVkZ2UtYWxpZ24tbXJwe1xuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgIHRvcDogNjVweDtcbi8vICAgcmlnaHQ6IDQ1cHhcbi8vIH1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStoreBasicPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-basic',
          templateUrl: './data-store-basic.page.html',
          styleUrls: ['./data-store-basic.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_3__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-combined/data-store-combined.page.ts":
  /*!************************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-combined/data-store-combined.page.ts ***!
    \************************************************************************************/

  /*! exports provided: DataStoreCombinedPage */

  /***/
  function srcAppShowcaseAppShellDataStoreCombinedDataStoreCombinedPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStoreCombinedPage", function () {
      return DataStoreCombinedPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../showcase-shell.model */
    "./src/app/showcase/showcase-shell.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    function DataStoreCombinedPage_ion_item_42_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-item", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-text-shell", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "span", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Status:");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "h3", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "app-text-shell", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Owner:");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "app-text-shell", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "app-text-shell", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "app-text-shell", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var task_r1 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", task_r1 == null ? null : task_r1.title);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", task_r1.completed ? "Completed" : "In Progress");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", task_r1.user == null ? null : task_r1.user.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", (task_r1 == null ? null : task_r1.user) ? "@".concat("", task_r1.user.username) : null);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", task_r1.user == null ? null : task_r1.user.email);
      }
    }

    var DataStoreCombinedPage = /*#__PURE__*/function () {
      function DataStoreCombinedPage(showcaseService) {
        _classCallCheck(this, DataStoreCombinedPage);

        this.showcaseService = showcaseService;
      }

      _createClass(DataStoreCombinedPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this2 = this;

          // We created ShowcaseCombinedTaskUserModel to combine the task with his user data.
          // They are 2 different collections (or data tables in the DB) and we need to combine them into 1 dataSource.
          var dataSource = this.showcaseService.getCombinedTasksUserDataSource();
          var shellModel = [new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseCombinedTaskUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseCombinedTaskUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseCombinedTaskUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseCombinedTaskUserModel"]()];
          this.tasksCombinedDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_2__["DataStore"](shellModel);
          this.tasksCombinedDataStore.load(dataSource);
          this.tasksCombinedDataStore.state.subscribe(function (data) {
            _this2.tasks = data;
          });
        }
      }]);

      return DataStoreCombinedPage;
    }();

    DataStoreCombinedPage.ɵfac = function DataStoreCombinedPage_Factory(t) {
      return new (t || DataStoreCombinedPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]));
    };

    DataStoreCombinedPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStoreCombinedPage,
      selectors: [["app-data-store-combined"]],
      decls: 44,
      vars: 5,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], ["href", "https://blog.angularindepth.com/learn-to-combine-rxjs-sequences-with-super-intuitive-interactive-diagrams-20fce8e6511", "target", "_blank"], [1, "tasks"], ["lines", "full", 2, "--background", "transparent"], ["class", "task", 4, "ngFor", "ngForOf"], [1, "task"], ["animation", "bouncing", 2, "--text-shell-line-height", "16px", 3, "data"], [2, "display", "flex"], [2, "margin-right", "10px"], [2, "min-width", "100px"], ["animation", "bouncing", 2, "--text-shell-line-height", "14px", 3, "data"], ["animation", "bouncing", 3, "data"]],
      template: function DataStoreCombinedPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - Combined Data Sources ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Some modern ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "no-sql");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " solutions like ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "u");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "MongoDB");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " or ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "u");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Firebase Firestore");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " do not support multi-collection queries (left join, inner join). ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " That's why you may end up using some ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "RxJs");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " operators to manage and combine queries independently. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, " These techniques are ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "i");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "intermediate / advanced");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " and require some patience to fully grasp some reactive programming concepts before you can master RxJs operators. I strongly recommend you to read this guide on ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "learning to combine RxJs sequences");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, " Below you can see how we combine two data sources (one depending on the other) into one Observable. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, " Let's do an example of Tasks and their Owners. Imagine a case where we need to have the list of tasks and the details of the 'owner' user combined in the same data source. Let's see how to do it. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "h6");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Tasks");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "ion-list", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](42, DataStoreCombinedPage_ion_item_42_Template, 19, 5, "ion-item", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](43, "slice");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind3"](43, 1, ctx.tasks, 0, 40));
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonList"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonItem"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonLabel"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_6__["TextShellComponent"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["SlicePipe"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n.tasks[_ngcontent-%COMP%] {\n  background-color: #ececec;\n  background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ffffff' fill-opacity='0.38'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\n  padding: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtY29tYmluZWQvZGF0YS1zdG9yZS1jb21iaW5lZC5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9hcHAtc2hlbGwucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvZGF0YS1zdG9yZS1jb21iaW5lZC9kYXRhLXN0b3JlLWNvbWJpbmVkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7RUFDRSxtQ0FBQTtBREVGO0FDQUU7RUFDRSx3Q0FBQTtFQUNBLGtDQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FERUo7QUNDRTtFQUNFLHNCQUFBO0FEQ0o7QUNFRTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QURBSjtBQ0dFO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FEREo7QUNJQTtFQUVFLGVBQUE7QURGRjtBQ09BO0VBQ0Usa0JBQUE7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QURKSjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSw2QkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QURIRjtBQ0tBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FERkY7QUNJQTtFQUNFLHVCQUFBO0FEREY7QUNVQTtFQUNFLGNBSlM7RUFLVCxtQkFKTztFQUtQLGtCQUFBO0FEUEY7QUNTQTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURORjtBQ1FBO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtBRExGO0FDT0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FESkY7QUNPQTtFQUNFLGFBQUE7QURKRjtBQ09FO0VBQ0UsT0FBQTtBRExKO0FDV0EscUJBQUE7QUFDQTtFQUVFLGdCQUFBO0FEVkY7QUNhQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QURWRjtBQ1lFO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUVBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0EsU0FBQTtBRFhKO0FDZUk7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FEYk47QUNtQkU7RUFDRSxhQUFBO0VBRUEsY0F2RU87RUF3RVAsaUJBQUE7RUFDQSxxQkFBQTtBRGxCSjtBQ29CRTtFQUNFLGFBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQWhGTztFQWlGUCxlQUFBO0FEbEJKO0FDNkJJO0VBQ0Usd0JBQUE7QUQzQk47QUM4QkU7RUFDRSxpQkFBQTtBRDVCSjtBQ2lDQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUQ5QkY7QUU5SUE7RUFDRSx5QkFBQTtFQUNBLHdpQkFBQTtFQUNBLGFBQUE7QUZpSkYiLCJmaWxlIjoic3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvZGF0YS1zdG9yZS1jb21iaW5lZC9kYXRhLXN0b3JlLWNvbWJpbmVkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG59XG4uc2hvd2Nhc2UtY29udGVudCBpb24taXRlbS1kaXZpZGVyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IHByZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogI0NDQztcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGNvZGUge1xuICBjb2xvcjogI0Y5MjY3MjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLm1lbnVfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmNsc29lLWljb24ge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uY29weV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmljb25fY2xvc2Uge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG5cbi5saXN0LW1kIHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmJvZHkge1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogI2VjZjBmMTtcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuXG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaDIge1xuICBtYXJnaW46IDAgMCAwLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5yb3cgLmNvbCB7XG4gIGZsZXg6IDE7XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnRhYi1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAvKiBJY29uICovXG59XG4udGFiLWxhYmVsOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwi4p2vXCI7XG4gIHdpZHRoOiAxZW07XG4gIGhlaWdodDogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDA7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jbG9zZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC1zaXplOiAwLjc1ZW07XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbmlucHV0OmNoZWNrZWQgKyAudGFiLWxhYmVsOjphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cbmlucHV0OmNoZWNrZWQgfiAudGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAxMDB2aDtcbn1cblxuLmJlZGdlLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4udGFza3Mge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWNlY2VjO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzQwJyBoZWlnaHQ9JzQwJyB2aWV3Qm94PScwIDAgNDAgNDAnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjNmZmZmZmYnIGZpbGwtb3BhY2l0eT0nMC4zOCclM0UlM0NwYXRoIGQ9J00wIDM4LjU5bDIuODMtMi44MyAxLjQxIDEuNDFMMS40MSA0MEgwdi0xLjQxek0wIDEuNGwyLjgzIDIuODMgMS40MS0xLjQxTDEuNDEgMEgwdjEuNDF6TTM4LjU5IDQwbC0yLjgzLTIuODMgMS40MS0xLjQxTDQwIDM4LjU5VjQwaC0xLjQxek00MCAxLjQxbC0yLjgzIDIuODMtMS40MS0xLjQxTDM4LjU5IDBINDB2MS40MXpNMjAgMTguNmwyLjgzLTIuODMgMS40MSAxLjQxTDIxLjQxIDIwbDIuODMgMi44My0xLjQxIDEuNDFMMjAgMjEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMTguNTkgMjBsLTIuODMtMi44MyAxLjQxLTEuNDFMMjAgMTguNTl6Jy8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcbiAgcGFkZGluZzogMjBweDtcbn0iLCIuc2hvd2Nhc2UtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xuXG4gIGlvbi1pdGVtLWRpdmlkZXIge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgfVxuXG4gIC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xuICB9XG5cbiAgcHJlIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYmFja2dyb3VuZDogI0NDQztcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG5cbiAgY29kZSB7XG4gICAgY29sb3I6ICNGOTI2NzI7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgfVxufVxuLm1lbnVfaWNvbntcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI1cHg7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIC8vIHBhZGRpbmctdG9wOiA3cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jbHNvZS1pY29ue1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICBmb250LXNpemU6IDMwcHg7XG59XG4uaGVhcnRfaWNvbntcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNvcHlfaWNvbntcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLm1haW4tdGl0bGV7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLmljb25fY2xvc2V7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cbi5saXN0LW1ke1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuXG4vLyAqKioqKioqKioqKioqKioqKioqKioqKioqIEFjY29yZGlhblxuXG4kbWlkbmlnaHQ6ICMyYzNlNTA7XG4kY2xvdWRzOiAjZWNmMGYxO1xuLy8gR2VuZXJhbFxuYm9keSB7XG4gIGNvbG9yOiAkbWlkbmlnaHQ7XG4gIGJhY2tncm91bmQ6ICRjbG91ZHM7XG4gIHBhZGRpbmc6IDAgMWVtIDFlbTtcbn1cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaDIge1xuICBtYXJnaW46IDAgMCAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuLy8gTGF5b3V0XG4ucm93IHtcbiAgZGlzcGxheTpmbGV4O1xuICAvLyBtYXJnaW4tbGVmdDogMTBweDsgXG4gIC8vIG1hcmdpbi1yaWdodDogMTBweDtcbiAgLmNvbCB7XG4gICAgZmxleDoxO1xuICAgICY6bGFzdC1jaGlsZCB7XG4gICAgICAvLyBtYXJnaW4tbGVmdDogMWVtO1xuICAgIH1cbiAgfVxufVxuLyogQWNjb3JkaW9uIHN0eWxlcyAqL1xuLnRhYnMge1xuICAvLyBib3JkZXItcmFkaXVzOiA4cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJveC1zaGFkb3c6IDAgNHB4IDRweCAtMnB4IHJnYmEoMCwwLDAsMC41KTtcbn1cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgJi1sYWJlbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgcGFkZGluZzogMWVtO1xuICAgIC8vIGJhY2tncm91bmQ6ICRtaWRuaWdodDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgICAvKiBJY29uICovXG4gICAgJjpob3ZlciB7XG4gICAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgIH1cbiAgICAmOjphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlxcMjc2RlwiO1xuICAgICAgd2lkdGg6IDFlbTtcbiAgICAgIGhlaWdodDogMWVtO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gICAgfVxuICAgIC8vIC5hY3RpdmU6YWZ0ZXIge1xuICAgIC8vICAgY29udGVudDogXCJcXDI3OTZcIjsgLyogVW5pY29kZSBjaGFyYWN0ZXIgZm9yIFwibWludXNcIiBzaWduICgtKSAqL1xuICAgIC8vIH1cbiAgfVxuICAmLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDA7XG4gICAgLy8gcGFkZGluZzogMCAxZW07XG4gICAgY29sb3I6ICRtaWRuaWdodDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgfVxuICAmLWNsb3NlIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgcGFkZGluZzogMWVtO1xuICAgIGZvbnQtc2l6ZTogMC43NWVtO1xuICAgIGJhY2tncm91bmQ6ICRtaWRuaWdodDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgJjpob3ZlciB7XG4gICAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgIH1cbiAgfVxufVxuXG4vLyA6Y2hlY2tlZFxuaW5wdXQ6Y2hlY2tlZCB7XG4gICsgLnRhYi1sYWJlbCB7XG4gICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICAmOjphZnRlciB7XG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gICAgfVxuICB9XG4gIH4gLnRhYi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAxMDB2aDtcbiAgICAvLyBwYWRkaW5nOiAxZW07XG4gIH1cbn1cblxuLmJlZGdlLWFsaWdue1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59XG5cbi8vIC5iZWRnZS1hbGlnbi1tcnB7XG4vLyAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbi8vICAgdG9wOiA2NXB4O1xuLy8gICByaWdodDogNDVweFxuLy8gfVxuIiwiQGltcG9ydCBcIi4uL2FwcC1zaGVsbC5wYWdlXCI7XG5cbi50YXNrcyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2VjZWM7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nNDAnIGhlaWdodD0nNDAnIHZpZXdCb3g9JzAgMCA0MCA0MCclM0UlM0NnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyM2ZmZmZmZicgZmlsbC1vcGFjaXR5PScwLjM4JyUzRSUzQ3BhdGggZD0nTTAgMzguNTlsMi44My0yLjgzIDEuNDEgMS40MUwxLjQxIDQwSDB2LTEuNDF6TTAgMS40bDIuODMgMi44MyAxLjQxLTEuNDFMMS40MSAwSDB2MS40MXpNMzguNTkgNDBsLTIuODMtMi44MyAxLjQxLTEuNDFMNDAgMzguNTlWNDBoLTEuNDF6TTQwIDEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMzguNTkgMEg0MHYxLjQxek0yMCAxOC42bDIuODMtMi44MyAxLjQxIDEuNDFMMjEuNDEgMjBsMi44MyAyLjgzLTEuNDEgMS40MUwyMCAyMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwxOC41OSAyMGwtMi44My0yLjgzIDEuNDEtMS40MUwyMCAxOC41OXonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xuICBwYWRkaW5nOiAyMHB4O1xufVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStoreCombinedPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-combined',
          templateUrl: './data-store-combined.page.html',
          styleUrls: ['./data-store-combined.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-dependant/data-store-dependant.page.ts":
  /*!**************************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-dependant/data-store-dependant.page.ts ***!
    \**************************************************************************************/

  /*! exports provided: DataStoreDependantPage */

  /***/
  function srcAppShowcaseAppShellDataStoreDependantDataStoreDependantPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStoreDependantPage", function () {
      return DataStoreDependantPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _showcase_shell_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../showcase-shell.model */
    "./src/app/showcase/showcase-shell.model.ts");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function DataStoreDependantPage_ion_item_33_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-item");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h2");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-text-shell", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-text-shell", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var comment_r1 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", comment_r1 == null ? null : comment_r1.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", comment_r1 == null ? null : comment_r1.body);
      }
    }

    var _c0 = function _c0() {
      return ["/showcase/app-shell/data-store-multiple"];
    };

    var _c1 = function _c1() {
      return ["/showcase/app-shell/data-store-combined"];
    };

    var DataStoreDependantPage = /*#__PURE__*/function () {
      function DataStoreDependantPage(showcaseService) {
        _classCallCheck(this, DataStoreDependantPage);

        this.showcaseService = showcaseService;
      }

      _createClass(DataStoreDependantPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this3 = this;

          var postDataSource = this.showcaseService.getDependantDataSourcePost();
          var postShellModel = new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_1__["ShowcasePostModel"]();
          this.postDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](postShellModel);
          this.postDataStore.load(postDataSource);
          this.postDataStore.state.subscribe(function (data) {
            _this3.post = data;
          });
          var commentsShellModel = [new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_1__["ShowcaseCommentModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_1__["ShowcaseCommentModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_1__["ShowcaseCommentModel"]()];
          var commentsDataSource = this.showcaseService.getDependantDataSourcePostComments(this.postDataStore.state);
          this.commentsDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](commentsShellModel); // Trigger the loading mechanism (with shell) in the dataStore

          this.commentsDataStore.load(commentsDataSource);
          this.commentsDataStore.state.subscribe(function (data) {
            _this3.comments = data;
          });
        }
      }]);

      return DataStoreDependantPage;
    }();

    DataStoreDependantPage.ɵfac = function DataStoreDependantPage_Factory(t) {
      return new (t || DataStoreDependantPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_2__["ShowcaseService"]));
    };

    DataStoreDependantPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStoreDependantPage,
      selectors: [["app-data-store-dependant"]],
      decls: 35,
      vars: 13,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], [3, "routerLink"], [1, "post"], ["animation", "bouncing", 2, "--text-shell-line-color", "#FFF", 3, "data"], ["animation", "bouncing", "lines", "3", 2, "--text-shell-line-color", "#fffdfd", 3, "data"], ["lines", "full", 2, "--background", "transparent"], [4, "ngFor", "ngForOf"], ["animation", "bouncing", 2, "--text-shell-line-height", "16px", 3, "data"], ["animation", "bouncing", 3, "data"]],
      template: function DataStoreDependantPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - Dependant Data Sources ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " This example kinda mixes the two previous examples (");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "multiple data stores");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, ", ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "one depending on the other");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "). ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " The uses cases in which to use this approach would be if you have different data sources that depend on each other, but you want the different sections of the view to 're-load' independently one from the other. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " This contrasts the use case of the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Combined Data Source");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " where you transform multiple data sources into one, causing the hole view to be 're-loaded' as it's binded to just one combined Observable. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, " A good example can be a post and its comments. The comments depend on the post but you want them to 're-load' independently from the post. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "app-text-shell", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "app-text-shell", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "h6");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Comments");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "ion-list", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](33, DataStoreDependantPage_ion_item_33_Template, 6, 2, "ion-item", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](34, "slice");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.post == null ? null : ctx.post.title);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.post == null ? null : ctx.post.body);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind3"](34, 6, ctx.comments, 0, 5));
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["RouterLinkDelegate"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_6__["TextShellComponent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonList"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonItem"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonLabel"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["SlicePipe"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n.post[_ngcontent-%COMP%] {\n  background-color: #ececec;\n  background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ffffff' fill-opacity='0.38'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\n  padding: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtZGVwZW5kYW50L2RhdGEtc3RvcmUtZGVwZW5kYW50LnBhZ2Uuc2NzcyIsIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2FwcC1zaGVsbC5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9kYXRhLXN0b3JlLWRlcGVuZGFudC9kYXRhLXN0b3JlLWRlcGVuZGFudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQWhCO0VBQ0UsbUNBQUE7QURFRjtBQ0FFO0VBQ0Usd0NBQUE7RUFDQSxrQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBREVKO0FDQ0U7RUFDRSxzQkFBQTtBRENKO0FDRUU7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FEQUo7QUNHRTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtBRERKO0FDSUE7RUFFRSxlQUFBO0FERkY7QUNPQTtFQUNFLGtCQUFBO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FESko7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FESEY7QUNLQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBREZGO0FDSUE7RUFDRSx1QkFBQTtBRERGO0FDVUE7RUFDRSxjQUpTO0VBS1QsbUJBSk87RUFLUCxrQkFBQTtBRFBGO0FDU0E7RUFDRSxTQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FETkY7QUNRQTtFQUNFLGlCQUFBO0VBQ0EsbUJBQUE7QURMRjtBQ09BO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBREpGO0FDT0E7RUFDRSxhQUFBO0FESkY7QUNPRTtFQUNFLE9BQUE7QURMSjtBQ1dBLHFCQUFBO0FBQ0E7RUFFRSxnQkFBQTtBRFZGO0FDYUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FEVkY7QUNZRTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLFlBQUE7RUFFQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSw2QkFBQTtFQUNBLFNBQUE7QURYSjtBQ2VJO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBRGJOO0FDbUJFO0VBQ0UsYUFBQTtFQUVBLGNBdkVPO0VBd0VQLGlCQUFBO0VBQ0EscUJBQUE7QURsQko7QUNvQkU7RUFDRSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFoRk87RUFpRlAsZUFBQTtBRGxCSjtBQzZCSTtFQUNFLHdCQUFBO0FEM0JOO0FDOEJFO0VBQ0UsaUJBQUE7QUQ1Qko7QUNpQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FEOUJGO0FFOUlBO0VBQ0UseUJBQUE7RUFDQSx3aUJBQUE7RUFDQSxhQUFBO0FGaUpGIiwiZmlsZSI6InNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtZGVwZW5kYW50L2RhdGEtc3RvcmUtZGVwZW5kYW50LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG59XG4uc2hvd2Nhc2UtY29udGVudCBpb24taXRlbS1kaXZpZGVyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IHByZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogI0NDQztcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGNvZGUge1xuICBjb2xvcjogI0Y5MjY3MjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLm1lbnVfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmNsc29lLWljb24ge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uY29weV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmljb25fY2xvc2Uge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG5cbi5saXN0LW1kIHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmJvZHkge1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogI2VjZjBmMTtcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuXG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaDIge1xuICBtYXJnaW46IDAgMCAwLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5yb3cgLmNvbCB7XG4gIGZsZXg6IDE7XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnRhYi1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAvKiBJY29uICovXG59XG4udGFiLWxhYmVsOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwi4p2vXCI7XG4gIHdpZHRoOiAxZW07XG4gIGhlaWdodDogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDA7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jbG9zZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC1zaXplOiAwLjc1ZW07XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbmlucHV0OmNoZWNrZWQgKyAudGFiLWxhYmVsOjphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cbmlucHV0OmNoZWNrZWQgfiAudGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAxMDB2aDtcbn1cblxuLmJlZGdlLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4ucG9zdCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2VjZWM7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nNDAnIGhlaWdodD0nNDAnIHZpZXdCb3g9JzAgMCA0MCA0MCclM0UlM0NnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyM2ZmZmZmZicgZmlsbC1vcGFjaXR5PScwLjM4JyUzRSUzQ3BhdGggZD0nTTAgMzguNTlsMi44My0yLjgzIDEuNDEgMS40MUwxLjQxIDQwSDB2LTEuNDF6TTAgMS40bDIuODMgMi44MyAxLjQxLTEuNDFMMS40MSAwSDB2MS40MXpNMzguNTkgNDBsLTIuODMtMi44MyAxLjQxLTEuNDFMNDAgMzguNTlWNDBoLTEuNDF6TTQwIDEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMzguNTkgMEg0MHYxLjQxek0yMCAxOC42bDIuODMtMi44MyAxLjQxIDEuNDFMMjEuNDEgMjBsMi44MyAyLjgzLTEuNDEgMS40MUwyMCAyMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwxOC41OSAyMGwtMi44My0yLjgzIDEuNDEtMS40MUwyMCAxOC41OXonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xuICBwYWRkaW5nOiAyMHB4O1xufSIsIi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgaW9uLWl0ZW0tZGl2aWRlciB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICB9XG5cbiAgLnNob3djYXNlLXNlY3Rpb24ge1xuICAgIG1hcmdpbjogMjBweCAyMHB4IDYwcHg7XG4gIH1cblxuICBwcmUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cblxuICBjb2RlIHtcbiAgICBjb2xvcjogI0Y5MjY3MjtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG59XG4ubWVudV9pY29ue1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbiAgLy8gcGFkZGluZy10b3A6IDdweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNsc29lLWljb257XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbn1cbi5oZWFydF9pY29ue1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY29weV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4ubWFpbi10aXRsZXtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uaWNvbl9jbG9zZXtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuN2VtO1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuLmxpc3QtbWR7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5cbi8vICoqKioqKioqKioqKioqKioqKioqKioqKiogQWNjb3JkaWFuXG5cbiRtaWRuaWdodDogIzJjM2U1MDtcbiRjbG91ZHM6ICNlY2YwZjE7XG4vLyBHZW5lcmFsXG5ib2R5IHtcbiAgY29sb3I6ICRtaWRuaWdodDtcbiAgYmFja2dyb3VuZDogJGNsb3VkcztcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuaDEge1xuICBtYXJnaW46IDA7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5oMiB7XG4gIG1hcmdpbjogMCAwIC41ZW07XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG4vLyBMYXlvdXRcbi5yb3cge1xuICBkaXNwbGF5OmZsZXg7XG4gIC8vIG1hcmdpbi1sZWZ0OiAxMHB4OyBcbiAgLy8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAuY29sIHtcbiAgICBmbGV4OjE7XG4gICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgIC8vIG1hcmdpbi1sZWZ0OiAxZW07XG4gICAgfVxuICB9XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm94LXNoYWRvdzogMCA0cHggNHB4IC0ycHggcmdiYSgwLDAsMCwwLjUpO1xufVxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAmLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgLy8gYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAgIC8qIEljb24gKi9cbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICAgICY6OmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFwyNzZGXCI7XG4gICAgICB3aWR0aDogMWVtO1xuICAgICAgaGVpZ2h0OiAxZW07XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgICB9XG4gICAgLy8gLmFjdGl2ZTphZnRlciB7XG4gICAgLy8gICBjb250ZW50OiBcIlxcMjc5NlwiOyAvKiBVbmljb2RlIGNoYXJhY3RlciBmb3IgXCJtaW51c1wiIHNpZ24gKC0pICovXG4gICAgLy8gfVxuICB9XG4gICYtY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMDtcbiAgICAvLyBwYWRkaW5nOiAwIDFlbTtcbiAgICBjb2xvcjogJG1pZG5pZ2h0O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICB9XG4gICYtY2xvc2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgZm9udC1zaXplOiAwLjc1ZW07XG4gICAgYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICB9XG59XG5cbi8vIDpjaGVja2VkXG5pbnB1dDpjaGVja2VkIHtcbiAgKyAudGFiLWxhYmVsIHtcbiAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgICY6OmFmdGVyIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICB9XG4gIH1cbiAgfiAudGFiLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDEwMHZoO1xuICAgIC8vIHBhZGRpbmc6IDFlbTtcbiAgfVxufVxuXG4uYmVkZ2UtYWxpZ257XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDc1JTtcbn1cblxuLy8gLmJlZGdlLWFsaWduLW1ycHtcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICB0b3A6IDY1cHg7XG4vLyAgIHJpZ2h0OiA0NXB4XG4vLyB9XG4iLCJAaW1wb3J0IFwiLi4vYXBwLXNoZWxsLnBhZ2VcIjtcblxuLnBvc3Qge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWNlY2VjO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzQwJyBoZWlnaHQ9JzQwJyB2aWV3Qm94PScwIDAgNDAgNDAnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjNmZmZmZmYnIGZpbGwtb3BhY2l0eT0nMC4zOCclM0UlM0NwYXRoIGQ9J00wIDM4LjU5bDIuODMtMi44MyAxLjQxIDEuNDFMMS40MSA0MEgwdi0xLjQxek0wIDEuNGwyLjgzIDIuODMgMS40MS0xLjQxTDEuNDEgMEgwdjEuNDF6TTM4LjU5IDQwbC0yLjgzLTIuODMgMS40MS0xLjQxTDQwIDM4LjU5VjQwaC0xLjQxek00MCAxLjQxbC0yLjgzIDIuODMtMS40MS0xLjQxTDM4LjU5IDBINDB2MS40MXpNMjAgMTguNmwyLjgzLTIuODMgMS40MSAxLjQxTDIxLjQxIDIwbDIuODMgMi44My0xLjQxIDEuNDFMMjAgMjEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMTguNTkgMjBsLTIuODMtMi44MyAxLjQxLTEuNDFMMjAgMTguNTl6Jy8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcbiAgcGFkZGluZzogMjBweDtcbn1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStoreDependantPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-dependant',
          templateUrl: './data-store-dependant.page.html',
          styleUrls: ['./data-store-dependant.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_2__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-list/data-store-list.page.ts":
  /*!****************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-list/data-store-list.page.ts ***!
    \****************************************************************************/

  /*! exports provided: DataStoreListPage */

  /***/
  function srcAppShowcaseAppShellDataStoreListDataStoreListPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStoreListPage", function () {
      return DataStoreListPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../showcase-shell.model */
    "./src/app/showcase/showcase-shell.model.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    var _c0 = function _c0() {
      return {
        w: 1,
        h: 1
      };
    };

    function DataStoreListPage_ion_item_26_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-item");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-thumbnail", 1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-aspect-ratio", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-image-shell", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "app-text-shell", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h3", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-text-shell", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var user_r1 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c0));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", user_r1 == null ? null : user_r1.avatar)("alt", "Sample Image");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", user_r1 == null ? null : user_r1.first_name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", user_r1 == null ? null : user_r1.last_name);
      }
    }

    var DataStoreListPage = /*#__PURE__*/function () {
      function DataStoreListPage(showcaseService) {
        _classCallCheck(this, DataStoreListPage);

        this.showcaseService = showcaseService;
      }

      _createClass(DataStoreListPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this4 = this;

          var dataSource = this.showcaseService.getListDataSource(); // Initialize the model specifying that it is a shell model

          var shellModel = [new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"]()];
          this.dataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](shellModel); // Trigger the loading mechanism (with shell) in the dataStore

          this.dataStore.load(dataSource);
          this.dataStore.state.subscribe(function (data) {
            _this4.data = data;
          });
        }
      }, {
        key: "isShell",
        get: function get() {
          return this.data && this.data.isShell ? true : false;
        }
      }]);

      return DataStoreListPage;
    }();

    DataStoreListPage.ɵfac = function DataStoreListPage_Factory(t) {
      return new (t || DataStoreListPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]));
    };

    DataStoreListPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStoreListPage,
      selectors: [["app-data-store-list"]],
      hostVars: 2,
      hostBindings: function DataStoreListPage_HostBindings(rf, ctx) {
        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("is-shell", ctx.isShell);
        }
      },
      decls: 43,
      vars: 1,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], ["lines", "full", 1, "users-list"], [4, "ngFor", "ngForOf"], ["href", "https://www.typescriptlang.org/docs/handbook/advanced-types.html#intersection-types", "target", "_blank"], [3, "ratio"], ["animation", "spinner", 3, "src", "alt"], [1, "user-name"], ["animation", "bouncing", 3, "data"], [1, "user-lastname"]],
      template: function DataStoreListPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - List example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " When you fetch data from a remote API, sometimes you request a list of items. If that\u2019s the case you would end up having an ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Observable");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " of type ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Observable<Array<YourModel>>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " The ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "DataStore");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " class works by assigning a shell property to any object. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "const myObj = new YourModel();\nObject.assign(myObj, {isShell: false});\n\nObject.assign([1, 2, 3], {isShell: false});\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, " In Javascript, everything is an object (including Arrays). That\u2019s why you can assign properties to Arrays independently of their values. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "ion-list", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](26, DataStoreListPage_ion_item_26_Template, 9, 6, "ion-item", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " In this example we keep track of the current state of the data stream in a local ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "data");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, " property. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " Note how we define its type using ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Typescript intersection typing");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, " like this ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "data: Array<YourModel> & ShellModel;");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, " to indicate it's an array that has a shell model appended (by the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "DataStore");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "). ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.data);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonList"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonItem"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonThumbnail"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_6__["AspectRatioComponent"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_7__["ImageShellComponent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonLabel"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_8__["TextShellComponent"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n.users-list[_ngcontent-%COMP%]   ion-item[_ngcontent-%COMP%] {\n  --padding-start: 0px;\n}\n.user-name[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-line-height: 17px;\n  max-width: 120px;\n}\n.user-name[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  max-width: inherit;\n}\n.user-lastname[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-line-height: 14px;\n  max-width: 120px;\n}\n.user-lastname[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  max-width: inherit;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtbGlzdC9kYXRhLXN0b3JlLWxpc3QucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvYXBwLXNoZWxsLnBhZ2Uuc2NzcyIsIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtbGlzdC9kYXRhLXN0b3JlLWxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLG1DQUFBO0FERUY7QUNBRTtFQUNFLHdDQUFBO0VBQ0Esa0NBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QURFSjtBQ0NFO0VBQ0Usc0JBQUE7QURDSjtBQ0VFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBREFKO0FDR0U7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QURESjtBQ0lBO0VBRUUsZUFBQTtBREZGO0FDT0E7RUFDRSxrQkFBQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBREpKO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLDZCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtBREhGO0FDS0E7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURGRjtBQ0lBO0VBQ0UsdUJBQUE7QURERjtBQ1VBO0VBQ0UsY0FKUztFQUtULG1CQUpPO0VBS1Asa0JBQUE7QURQRjtBQ1NBO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBRE5GO0FDUUE7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FETEY7QUNPQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QURKRjtBQ09BO0VBQ0UsYUFBQTtBREpGO0FDT0U7RUFDRSxPQUFBO0FETEo7QUNXQSxxQkFBQTtBQUNBO0VBRUUsZ0JBQUE7QURWRjtBQ2FBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBRFZGO0FDWUU7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBRUEsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSxTQUFBO0FEWEo7QUNlSTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QURiTjtBQ21CRTtFQUNFLGFBQUE7RUFFQSxjQXZFTztFQXdFUCxpQkFBQTtFQUNBLHFCQUFBO0FEbEJKO0FDb0JFO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBaEZPO0VBaUZQLGVBQUE7QURsQko7QUM2Qkk7RUFDRSx3QkFBQTtBRDNCTjtBQzhCRTtFQUNFLGlCQUFBO0FENUJKO0FDaUNBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBRDlCRjtBRTdJRTtFQUNFLG9CQUFBO0FGZ0pKO0FFNUlBO0VBQ0UsOEJBQUE7RUFDQSxnQkFBQTtBRitJRjtBRTlJRTtFQUNFLGtCQUFBO0FGZ0pKO0FFNUlBO0VBQ0UsOEJBQUE7RUFDQSxnQkFBQTtBRitJRjtBRTlJRTtFQUNFLGtCQUFBO0FGZ0pKIiwiZmlsZSI6InNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtbGlzdC9kYXRhLXN0b3JlLWxpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogNTAwO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgLnNob3djYXNlLXNlY3Rpb24ge1xuICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgcHJlIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgY29kZSB7XG4gIGNvbG9yOiAjRjkyNjcyO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ubWVudV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uY2xzb2UtaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG4uaGVhcnRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5jb3B5X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubWFpbi10aXRsZSB7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uaWNvbl9jbG9zZSB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cblxuLmxpc3QtbWQge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuYm9keSB7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5oMiB7XG4gIG1hcmdpbjogMCAwIDAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuXG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG5cbi5yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnJvdyAuY29sIHtcbiAgZmxleDogMTtcbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4udGFiLWxhYmVsIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAxZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gIC8qIEljb24gKi9cbn1cbi50YWItbGFiZWw6OmFmdGVyIHtcbiAgY29udGVudDogXCLina9cIjtcbiAgd2lkdGg6IDFlbTtcbiAgaGVpZ2h0OiAxZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jb250ZW50IHtcbiAgbWF4LWhlaWdodDogMDtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXM7XG59XG4udGFiLWNsb3NlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXNpemU6IDAuNzVlbTtcbiAgYmFja2dyb3VuZDogIzJjM2U1MDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuaW5wdXQ6Y2hlY2tlZCArIC50YWItbGFiZWw6OmFmdGVyIHtcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xufVxuaW5wdXQ6Y2hlY2tlZCB+IC50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDEwMHZoO1xufVxuXG4uYmVkZ2UtYWxpZ24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59XG5cbi51c2Vycy1saXN0IGlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwcHg7XG59XG5cbi51c2VyLW5hbWUgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTdweDtcbiAgbWF4LXdpZHRoOiAxMjBweDtcbn1cbi51c2VyLW5hbWUgPiBhcHAtdGV4dC1zaGVsbC50ZXh0LWxvYWRlZCB7XG4gIG1heC13aWR0aDogaW5oZXJpdDtcbn1cblxuLnVzZXItbGFzdG5hbWUgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbiAgbWF4LXdpZHRoOiAxMjBweDtcbn1cbi51c2VyLWxhc3RuYW1lID4gYXBwLXRleHQtc2hlbGwudGV4dC1sb2FkZWQge1xuICBtYXgtd2lkdGg6IGluaGVyaXQ7XG59IiwiLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcblxuICBpb24taXRlbS1kaXZpZGVyIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIH1cblxuICAuc2hvd2Nhc2Utc2VjdGlvbiB7XG4gICAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbiAgfVxuXG4gIHByZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJhY2tncm91bmQ6ICNDQ0M7XG4gICAgcGFkZGluZzogMTBweDtcbiAgfVxuXG4gIGNvZGUge1xuICAgIGNvbG9yOiAjRjkyNjcyO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbn1cbi5tZW51X2ljb257XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogN3B4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY2xzb2UtaWNvbntcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgZm9udC1zaXplOiAzMHB4O1xufVxuLmhlYXJ0X2ljb257XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jb3B5X2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5tYWluLXRpdGxle1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5pY29uX2Nsb3Nle1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG4ubGlzdC1tZHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cblxuLy8gKioqKioqKioqKioqKioqKioqKioqKioqKiBBY2NvcmRpYW5cblxuJG1pZG5pZ2h0OiAjMmMzZTUwO1xuJGNsb3VkczogI2VjZjBmMTtcbi8vIEdlbmVyYWxcbmJvZHkge1xuICBjb2xvcjogJG1pZG5pZ2h0O1xuICBiYWNrZ3JvdW5kOiAkY2xvdWRzO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmgyIHtcbiAgbWFyZ2luOiAwIDAgLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbmlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiAtMTtcbn1cbi8vIExheW91dFxuLnJvdyB7XG4gIGRpc3BsYXk6ZmxleDtcbiAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7IFxuICAvLyBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIC5jb2wge1xuICAgIGZsZXg6MTtcbiAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDFlbTtcbiAgICB9XG4gIH1cbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgLy8gYm9yZGVyLXJhZGl1czogOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3gtc2hhZG93OiAwIDRweCA0cHggLTJweCByZ2JhKDAsMCwwLDAuNSk7XG59XG4udGFiIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICYtbGFiZWwge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICAgLyogSWNvbiAqL1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcXDI3NkZcIjtcbiAgICAgIHdpZHRoOiAxZW07XG4gICAgICBoZWlnaHQ6IDFlbTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICAgIH1cbiAgICAvLyAuYWN0aXZlOmFmdGVyIHtcbiAgICAvLyAgIGNvbnRlbnQ6IFwiXFwyNzk2XCI7IC8qIFVuaWNvZGUgY2hhcmFjdGVyIGZvciBcIm1pbnVzXCIgc2lnbiAoLSkgKi9cbiAgICAvLyB9XG4gIH1cbiAgJi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAwO1xuICAgIC8vIHBhZGRpbmc6IDAgMWVtO1xuICAgIGNvbG9yOiAkbWlkbmlnaHQ7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gIH1cbiAgJi1jbG9zZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICBmb250LXNpemU6IDAuNzVlbTtcbiAgICBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gIH1cbn1cblxuLy8gOmNoZWNrZWRcbmlucHV0OmNoZWNrZWQge1xuICArIC50YWItbGFiZWwge1xuICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgIH1cbiAgfVxuICB+IC50YWItY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMTAwdmg7XG4gICAgLy8gcGFkZGluZzogMWVtO1xuICB9XG59XG5cbi5iZWRnZS1hbGlnbntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4vLyAuYmVkZ2UtYWxpZ24tbXJwe1xuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgIHRvcDogNjVweDtcbi8vICAgcmlnaHQ6IDQ1cHhcbi8vIH1cbiIsIkBpbXBvcnQgXCIuLi9hcHAtc2hlbGwucGFnZVwiO1xuXG4udXNlcnMtbGlzdCB7XG4gIGlvbi1pdGVtIHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDBweDtcbiAgfVxufVxuXG4udXNlci1uYW1lID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE3cHg7XG4gIG1heC13aWR0aDogMTIwcHg7XG4gICYudGV4dC1sb2FkZWQge1xuICAgIG1heC13aWR0aDogaW5oZXJpdDtcbiAgfVxufVxuXG4udXNlci1sYXN0bmFtZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xuICBtYXgtd2lkdGg6IDEyMHB4O1xuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtYXgtd2lkdGg6IGluaGVyaXQ7XG4gIH1cbn1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStoreListPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-list',
          templateUrl: './data-store-list.page.html',
          styleUrls: ['./data-store-list.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]
        }];
      }, {
        isShell: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
          args: ['class.is-shell']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-multiple/data-store-multiple.page.ts":
  /*!************************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-multiple/data-store-multiple.page.ts ***!
    \************************************************************************************/

  /*! exports provided: DataStoreMultiplePage */

  /***/
  function srcAppShowcaseAppShellDataStoreMultipleDataStoreMultiplePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStoreMultiplePage", function () {
      return DataStoreMultiplePage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _product_listing_product_listing_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../product/listing/product-listing.model */
    "./src/app/product/listing/product-listing.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    var _c0 = function _c0() {
      return {
        w: 1,
        h: 1
      };
    };

    function DataStoreMultiplePage_ion_item_24_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-item");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-thumbnail", 1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-aspect-ratio", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-image-shell", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h2", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "app-text-shell", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-text-shell", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r1 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c0));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", item_r1 == null ? null : item_r1.image)("alt", "Sample Image");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", item_r1 == null ? null : item_r1.name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", (item_r1 == null ? null : item_r1.salePrice) ? "$".concat("", item_r1.salePrice) : null);
      }
    }

    var _c1 = function _c1() {
      return ["/showcase/app-shell/data-store-combined"];
    }; // import { TravelListingModel } from '../../../travel/listing/travel-listing.model';


    var DataStoreMultiplePage = /*#__PURE__*/function () {
      function DataStoreMultiplePage(showcaseService) {
        _classCallCheck(this, DataStoreMultiplePage);

        this.showcaseService = showcaseService;
      }

      _createClass(DataStoreMultiplePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this5 = this;

          // const dataSourceA = this.showcaseService.getMultipleDataSourceA();
          var dataSourceB = this.showcaseService.getMultipleDataSourceB(); // const shellModelA: TravelListingModel = new TravelListingModel();
          // this.dataStoreA = new DataStore(shellModelA);
          // this.dataStoreA.load(dataSourceA);
          // this.dataStoreA.state.subscribe(data => {
          // this.dataA = data;
          // });

          var shellModelB = new _product_listing_product_listing_model__WEBPACK_IMPORTED_MODULE_3__["ProductListingModel"]();
          this.dataStoreB = new _shell_data_store__WEBPACK_IMPORTED_MODULE_2__["DataStore"](shellModelB);
          this.dataStoreB.load(dataSourceB);
          this.dataStoreB.state.subscribe(function (data) {
            _this5.dataB = data;
          });
        }
      }]);

      return DataStoreMultiplePage;
    }();

    DataStoreMultiplePage.ɵfac = function DataStoreMultiplePage_Factory(t) {
      return new (t || DataStoreMultiplePage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]));
    };

    DataStoreMultiplePage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStoreMultiplePage,
      selectors: [["app-data-store-multiple"]],
      decls: 26,
      vars: 7,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], [3, "routerLink"], ["lines", "full"], [4, "ngFor", "ngForOf"], [3, "ratio"], ["animation", "spinner", 3, "src", "alt"], [1, "item-name"], ["animation", "gradient", 3, "data"], [1, "item-price"]],
      template: function DataStoreMultiplePage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - Multiple DataStores example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Sometimes your view displays different types of data, each of them with a different model and data source. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, " If this is your use case, then you can use multiple data stores to fetch each data stream. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " This use case is different from the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Combined Data Sources");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " because in this case you have two data streams that will change independently one from the other updating different parts of the view without causing a full view 're-loading'. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "h6");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Data Source B");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Shop what's trending");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "ion-list", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](24, DataStoreMultiplePage_ion_item_24_Template, 9, 6, "ion-item", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](25, "slice");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind3"](25, 2, ctx.dataB == null ? null : ctx.dataB.items, 0, 5));
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["RouterLinkDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonList"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonItem"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonThumbnail"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_7__["AspectRatioComponent"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_8__["ImageShellComponent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonLabel"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_9__["TextShellComponent"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["SlicePipe"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n.item-name[_ngcontent-%COMP%] {\n  font-weight: 500;\n}\n.item-name[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-line-height: 16px;\n}\n.item-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-line-height: 14px;\n  max-width: 80px;\n}\n.item-price[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  max-width: inherit;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtbXVsdGlwbGUvZGF0YS1zdG9yZS1tdWx0aXBsZS5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9hcHAtc2hlbGwucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvZGF0YS1zdG9yZS1tdWx0aXBsZS9kYXRhLXN0b3JlLW11bHRpcGxlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7RUFDRSxtQ0FBQTtBREVGO0FDQUU7RUFDRSx3Q0FBQTtFQUNBLGtDQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FERUo7QUNDRTtFQUNFLHNCQUFBO0FEQ0o7QUNFRTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QURBSjtBQ0dFO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FEREo7QUNJQTtFQUVFLGVBQUE7QURGRjtBQ09BO0VBQ0Usa0JBQUE7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QURKSjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSw2QkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QURIRjtBQ0tBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FERkY7QUNJQTtFQUNFLHVCQUFBO0FEREY7QUNVQTtFQUNFLGNBSlM7RUFLVCxtQkFKTztFQUtQLGtCQUFBO0FEUEY7QUNTQTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURORjtBQ1FBO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtBRExGO0FDT0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FESkY7QUNPQTtFQUNFLGFBQUE7QURKRjtBQ09FO0VBQ0UsT0FBQTtBRExKO0FDV0EscUJBQUE7QUFDQTtFQUVFLGdCQUFBO0FEVkY7QUNhQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QURWRjtBQ1lFO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUVBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0EsU0FBQTtBRFhKO0FDZUk7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FEYk47QUNtQkU7RUFDRSxhQUFBO0VBRUEsY0F2RU87RUF3RVAsaUJBQUE7RUFDQSxxQkFBQTtBRGxCSjtBQ29CRTtFQUNFLGFBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQWhGTztFQWlGUCxlQUFBO0FEbEJKO0FDNkJJO0VBQ0Usd0JBQUE7QUQzQk47QUM4QkU7RUFDRSxpQkFBQTtBRDVCSjtBQ2lDQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUQ5QkY7QUU5SUE7RUFDRSxnQkFBQTtBRmlKRjtBRS9JQTtFQUNFLDhCQUFBO0FGa0pGO0FFL0lBO0VBQ0UsOEJBQUE7RUFDQSxlQUFBO0FGa0pGO0FFakpFO0VBQ0Usa0JBQUE7QUZtSkoiLCJmaWxlIjoic3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvZGF0YS1zdG9yZS1tdWx0aXBsZS9kYXRhLXN0b3JlLW11bHRpcGxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG59XG4uc2hvd2Nhc2UtY29udGVudCBpb24taXRlbS1kaXZpZGVyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IHByZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogI0NDQztcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGNvZGUge1xuICBjb2xvcjogI0Y5MjY3MjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLm1lbnVfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmNsc29lLWljb24ge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uY29weV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmljb25fY2xvc2Uge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG5cbi5saXN0LW1kIHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmJvZHkge1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogI2VjZjBmMTtcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuXG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaDIge1xuICBtYXJnaW46IDAgMCAwLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5yb3cgLmNvbCB7XG4gIGZsZXg6IDE7XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnRhYi1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAvKiBJY29uICovXG59XG4udGFiLWxhYmVsOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwi4p2vXCI7XG4gIHdpZHRoOiAxZW07XG4gIGhlaWdodDogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDA7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jbG9zZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC1zaXplOiAwLjc1ZW07XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbmlucHV0OmNoZWNrZWQgKyAudGFiLWxhYmVsOjphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cbmlucHV0OmNoZWNrZWQgfiAudGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAxMDB2aDtcbn1cblxuLmJlZGdlLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4uaXRlbS1uYW1lIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLml0ZW0tbmFtZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNnB4O1xufVxuXG4uaXRlbS1wcmljZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xuICBtYXgtd2lkdGg6IDgwcHg7XG59XG4uaXRlbS1wcmljZSA+IGFwcC10ZXh0LXNoZWxsLnRleHQtbG9hZGVkIHtcbiAgbWF4LXdpZHRoOiBpbmhlcml0O1xufSIsIi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgaW9uLWl0ZW0tZGl2aWRlciB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICB9XG5cbiAgLnNob3djYXNlLXNlY3Rpb24ge1xuICAgIG1hcmdpbjogMjBweCAyMHB4IDYwcHg7XG4gIH1cblxuICBwcmUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cblxuICBjb2RlIHtcbiAgICBjb2xvcjogI0Y5MjY3MjtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG59XG4ubWVudV9pY29ue1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbiAgLy8gcGFkZGluZy10b3A6IDdweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNsc29lLWljb257XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbn1cbi5oZWFydF9pY29ue1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY29weV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4ubWFpbi10aXRsZXtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uaWNvbl9jbG9zZXtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuN2VtO1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuLmxpc3QtbWR7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5cbi8vICoqKioqKioqKioqKioqKioqKioqKioqKiogQWNjb3JkaWFuXG5cbiRtaWRuaWdodDogIzJjM2U1MDtcbiRjbG91ZHM6ICNlY2YwZjE7XG4vLyBHZW5lcmFsXG5ib2R5IHtcbiAgY29sb3I6ICRtaWRuaWdodDtcbiAgYmFja2dyb3VuZDogJGNsb3VkcztcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuaDEge1xuICBtYXJnaW46IDA7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5oMiB7XG4gIG1hcmdpbjogMCAwIC41ZW07XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG4vLyBMYXlvdXRcbi5yb3cge1xuICBkaXNwbGF5OmZsZXg7XG4gIC8vIG1hcmdpbi1sZWZ0OiAxMHB4OyBcbiAgLy8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAuY29sIHtcbiAgICBmbGV4OjE7XG4gICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgIC8vIG1hcmdpbi1sZWZ0OiAxZW07XG4gICAgfVxuICB9XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm94LXNoYWRvdzogMCA0cHggNHB4IC0ycHggcmdiYSgwLDAsMCwwLjUpO1xufVxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAmLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgLy8gYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAgIC8qIEljb24gKi9cbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICAgICY6OmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFwyNzZGXCI7XG4gICAgICB3aWR0aDogMWVtO1xuICAgICAgaGVpZ2h0OiAxZW07XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgICB9XG4gICAgLy8gLmFjdGl2ZTphZnRlciB7XG4gICAgLy8gICBjb250ZW50OiBcIlxcMjc5NlwiOyAvKiBVbmljb2RlIGNoYXJhY3RlciBmb3IgXCJtaW51c1wiIHNpZ24gKC0pICovXG4gICAgLy8gfVxuICB9XG4gICYtY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMDtcbiAgICAvLyBwYWRkaW5nOiAwIDFlbTtcbiAgICBjb2xvcjogJG1pZG5pZ2h0O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICB9XG4gICYtY2xvc2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgZm9udC1zaXplOiAwLjc1ZW07XG4gICAgYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICB9XG59XG5cbi8vIDpjaGVja2VkXG5pbnB1dDpjaGVja2VkIHtcbiAgKyAudGFiLWxhYmVsIHtcbiAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgICY6OmFmdGVyIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICB9XG4gIH1cbiAgfiAudGFiLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDEwMHZoO1xuICAgIC8vIHBhZGRpbmc6IDFlbTtcbiAgfVxufVxuXG4uYmVkZ2UtYWxpZ257XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDc1JTtcbn1cblxuLy8gLmJlZGdlLWFsaWduLW1ycHtcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICB0b3A6IDY1cHg7XG4vLyAgIHJpZ2h0OiA0NXB4XG4vLyB9XG4iLCJAaW1wb3J0IFwiLi4vYXBwLXNoZWxsLnBhZ2VcIjtcblxuLml0ZW0tbmFtZSB7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG4uaXRlbS1uYW1lID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE2cHg7XG59XG5cbi5pdGVtLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE0cHg7XG4gIG1heC13aWR0aDogODBweDtcbiAgJi50ZXh0LWxvYWRlZCB7XG4gICAgbWF4LXdpZHRoOiBpbmhlcml0O1xuICB9XG59XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStoreMultiplePage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-multiple',
          templateUrl: './data-store-multiple.page.html',
          styleUrls: ['./data-store-multiple.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-pagination/data-store-pagination.page.ts":
  /*!****************************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-pagination/data-store-pagination.page.ts ***!
    \****************************************************************************************/

  /*! exports provided: DataStorePaginationPage */

  /***/
  function srcAppShowcaseAppShellDataStorePaginationDataStorePaginationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStorePaginationPage", function () {
      return DataStorePaginationPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../showcase-shell.model */
    "./src/app/showcase/showcase-shell.model.ts");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    function DataStorePaginationPage_span_23_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "small");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "(No more pages available)");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var _c0 = function _c0() {
      return {
        w: 1,
        h: 1
      };
    };

    function DataStorePaginationPage_ion_row_27_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-row", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-col", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-aspect-ratio", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-image-shell", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-col", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "app-text-shell", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-text-shell", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var user_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c0));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", user_r2 == null ? null : user_r2.avatar)("alt", "Sample Image");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", user_r2 == null ? null : user_r2.first_name);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", user_r2 == null ? null : user_r2.last_name);
      }
    }

    var DataStorePaginationPage = /*#__PURE__*/function () {
      function DataStorePaginationPage(showcaseService) {
        _classCallCheck(this, DataStorePaginationPage);

        this.showcaseService = showcaseService;
        this.loadMorePages = true;
        this.currentPage = 0;
        this.triggerNewPageLoading = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.newPageTriggerObservable = this.triggerNewPageLoading.asObservable();
      }

      _createClass(DataStorePaginationPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this6 = this;

          var dataSource = this.showcaseService.getPaginationDataSource(1);

          if (!this.remoteApiDataStore) {
            // Initialize the model specifying that it is a shell model
            var shellModel = [new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseShellUserModel"]()];
            this.remoteApiDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_5__["DataStore"](shellModel); // Trigger the loading mechanism (with shell) in the dataStore

            this.remoteApiDataStore.load(dataSource);
          }

          var newDataObservable = this.newPageTriggerObservable.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (pageNumber) {
            var pageDataSource = _this6.showcaseService.getPaginationDataSource(pageNumber);

            var newDataShell = [new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseShellUserModel"]()];

            var dataSourceWithShellObservable = _shell_data_store__WEBPACK_IMPORTED_MODULE_5__["DataStore"].AppendShell(pageDataSource, newDataShell, 400);

            return dataSourceWithShellObservable;
          }));
          Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["merge"])(this.remoteApiDataStore.state, newDataObservable).subscribe(function (result) {
            console.log('result', result); // When successfully load next page, update currentPage pointer

            if (!result.isShell && result.length > 0) {
              _this6.currentPage++;
            }

            if (_this6.loadMorePages) {
              _this6.pagedUsers = result;
            }

            if (_this6.currentPage === 4) {
              _this6.loadMorePages = false;
            }
          });
        }
      }, {
        key: "getNextPageUsers",
        value: function getNextPageUsers() {
          this.triggerNewPageLoading.next(this.currentPage + 1);
        }
      }]);

      return DataStorePaginationPage;
    }();

    DataStorePaginationPage.ɵfac = function DataStorePaginationPage_Factory(t) {
      return new (t || DataStorePaginationPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_4__["ShowcaseService"]));
    };

    DataStorePaginationPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStorePaginationPage,
      selectors: [["app-data-store-pagination"]],
      decls: 30,
      vars: 4,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], ["style", "margin-inline-start: 10px;", 4, "ngIf"], ["style", "background-color: #FFF; padding: 10px;", 4, "ngFor", "ngForOf"], [3, "disabled", "click"], [2, "margin-inline-start", "10px"], [2, "background-color", "#FFF", "padding", "10px"], ["size", "4"], [3, "ratio"], ["animation", "spinner", 3, "src", "alt"], ["size", "8"], [2, "margin-top", "0px"], [3, "data"], ["lines", "3", 3, "data"]],
      template: function DataStorePaginationPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - Pagination example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " This example is useful for use cases that require the data stream to be updated based on user actions. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "i");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Pagination");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " and ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "i");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "filtering");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " are the clear use cases that come to my mind. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " For these use cases you get the initial data stream and then, upon user interaction, you request an update for that stream. For example requesting your remote API for the next page of results or a filtered set of results. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " This example shows you how to merge the initial data stream with future data streams that may arise from user interaction. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, DataStorePaginationPage_span_23_Template, 3, 0, "span", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "small");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Each time you click the button we fetch users from the next page.");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, DataStorePaginationPage_ion_row_27_Template, 9, 6, "ion-row", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "ion-button", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DataStorePaginationPage_Template_ion_button_click_28_listener() {
            return ctx.getNextPageUsers();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Fetch next page");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Current page: ", ctx.currentPage, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loadMorePages);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.pagedUsers);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.loadMorePages || ctx.pagedUsers.isShell);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonCol"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_8__["AspectRatioComponent"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_9__["ImageShellComponent"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_10__["TextShellComponent"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtcGFnaW5hdGlvbi9kYXRhLXN0b3JlLXBhZ2luYXRpb24ucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvYXBwLXNoZWxsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7RUFDRSxtQ0FBQTtBREVGO0FDQUU7RUFDRSx3Q0FBQTtFQUNBLGtDQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FERUo7QUNDRTtFQUNFLHNCQUFBO0FEQ0o7QUNFRTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QURBSjtBQ0dFO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FEREo7QUNJQTtFQUVFLGVBQUE7QURGRjtBQ09BO0VBQ0Usa0JBQUE7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QURKSjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSw2QkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QURIRjtBQ0tBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FERkY7QUNJQTtFQUNFLHVCQUFBO0FEREY7QUNVQTtFQUNFLGNBSlM7RUFLVCxtQkFKTztFQUtQLGtCQUFBO0FEUEY7QUNTQTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURORjtBQ1FBO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtBRExGO0FDT0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FESkY7QUNPQTtFQUNFLGFBQUE7QURKRjtBQ09FO0VBQ0UsT0FBQTtBRExKO0FDV0EscUJBQUE7QUFDQTtFQUVFLGdCQUFBO0FEVkY7QUNhQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QURWRjtBQ1lFO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUVBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0EsU0FBQTtBRFhKO0FDZUk7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FEYk47QUNtQkU7RUFDRSxhQUFBO0VBRUEsY0F2RU87RUF3RVAsaUJBQUE7RUFDQSxxQkFBQTtBRGxCSjtBQ29CRTtFQUNFLGFBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQWhGTztFQWlGUCxlQUFBO0FEbEJKO0FDNkJJO0VBQ0Usd0JBQUE7QUQzQk47QUM4QkU7RUFDRSxpQkFBQTtBRDVCSjtBQ2lDQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUQ5QkYiLCJmaWxlIjoic3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvZGF0YS1zdG9yZS1wYWdpbmF0aW9uL2RhdGEtc3RvcmUtcGFnaW5hdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG4uc2hvd2Nhc2UtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xufVxuLnNob3djYXNlLWNvbnRlbnQgaW9uLWl0ZW0tZGl2aWRlciB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG4uc2hvd2Nhc2UtY29udGVudCAuc2hvd2Nhc2Utc2VjdGlvbiB7XG4gIG1hcmdpbjogMjBweCAyMHB4IDYwcHg7XG59XG4uc2hvd2Nhc2UtY29udGVudCBwcmUge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJhY2tncm91bmQ6ICNDQ0M7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG4uc2hvd2Nhc2UtY29udGVudCBjb2RlIHtcbiAgY29sb3I6ICNGOTI2NzI7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5tZW51X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG59XG5cbi5jbHNvZS1pY29uIHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICBmb250LXNpemU6IDMwcHg7XG59XG5cbi5oZWFydF9pY29uIHtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmNvcHlfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5tYWluLXRpdGxlIHtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5pY29uX2Nsb3NlIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuN2VtO1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuXG4ubGlzdC1tZCB7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5ib2R5IHtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIGJhY2tncm91bmQ6ICNlY2YwZjE7XG4gIHBhZGRpbmc6IDAgMWVtIDFlbTtcbn1cblxuaDEge1xuICBtYXJnaW46IDA7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmgyIHtcbiAgbWFyZ2luOiAwIDAgMC41ZW07XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5cbmlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiAtMTtcbn1cblxuLnJvdyB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4ucm93IC5jb2wge1xuICBmbGV4OiAxO1xufVxuLyogQWNjb3JkaW9uIHN0eWxlcyAqL1xuLnRhYnMge1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4udGFiIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi50YWItbGFiZWwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgLyogSWNvbiAqL1xufVxuLnRhYi1sYWJlbDo6YWZ0ZXIge1xuICBjb250ZW50OiBcIuKdr1wiO1xuICB3aWR0aDogMWVtO1xuICBoZWlnaHQ6IDFlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXM7XG59XG4udGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAwO1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY2xvc2Uge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBwYWRkaW5nOiAxZW07XG4gIGZvbnQtc2l6ZTogMC43NWVtO1xuICBiYWNrZ3JvdW5kOiAjMmMzZTUwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG5pbnB1dDpjaGVja2VkICsgLnRhYi1sYWJlbDo6YWZ0ZXIge1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG59XG5pbnB1dDpjaGVja2VkIH4gLnRhYi1jb250ZW50IHtcbiAgbWF4LWhlaWdodDogMTAwdmg7XG59XG5cbi5iZWRnZS1hbGlnbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDc1JTtcbn0iLCIuc2hvd2Nhc2UtY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xuXG4gIGlvbi1pdGVtLWRpdmlkZXIge1xuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXNlY29uZGFyeSk7XG4gICAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgfVxuXG4gIC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xuICB9XG5cbiAgcHJlIHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYmFja2dyb3VuZDogI0NDQztcbiAgICBwYWRkaW5nOiAxMHB4O1xuICB9XG5cbiAgY29kZSB7XG4gICAgY29sb3I6ICNGOTI2NzI7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgfVxufVxuLm1lbnVfaWNvbntcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDI1cHg7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIC8vIHBhZGRpbmctdG9wOiA3cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jbHNvZS1pY29ue1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgICBmb250LXNpemU6IDMwcHg7XG59XG4uaGVhcnRfaWNvbntcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNvcHlfaWNvbntcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLm1haW4tdGl0bGV7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLmljb25fY2xvc2V7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cbi5saXN0LW1ke1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuXG4vLyAqKioqKioqKioqKioqKioqKioqKioqKioqIEFjY29yZGlhblxuXG4kbWlkbmlnaHQ6ICMyYzNlNTA7XG4kY2xvdWRzOiAjZWNmMGYxO1xuLy8gR2VuZXJhbFxuYm9keSB7XG4gIGNvbG9yOiAkbWlkbmlnaHQ7XG4gIGJhY2tncm91bmQ6ICRjbG91ZHM7XG4gIHBhZGRpbmc6IDAgMWVtIDFlbTtcbn1cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaDIge1xuICBtYXJnaW46IDAgMCAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuLy8gTGF5b3V0XG4ucm93IHtcbiAgZGlzcGxheTpmbGV4O1xuICAvLyBtYXJnaW4tbGVmdDogMTBweDsgXG4gIC8vIG1hcmdpbi1yaWdodDogMTBweDtcbiAgLmNvbCB7XG4gICAgZmxleDoxO1xuICAgICY6bGFzdC1jaGlsZCB7XG4gICAgICAvLyBtYXJnaW4tbGVmdDogMWVtO1xuICAgIH1cbiAgfVxufVxuLyogQWNjb3JkaW9uIHN0eWxlcyAqL1xuLnRhYnMge1xuICAvLyBib3JkZXItcmFkaXVzOiA4cHg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJveC1zaGFkb3c6IDAgNHB4IDRweCAtMnB4IHJnYmEoMCwwLDAsMC41KTtcbn1cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgJi1sYWJlbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgcGFkZGluZzogMWVtO1xuICAgIC8vIGJhY2tncm91bmQ6ICRtaWRuaWdodDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgZ3JleTtcbiAgICAvKiBJY29uICovXG4gICAgJjpob3ZlciB7XG4gICAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgIH1cbiAgICAmOjphZnRlciB7XG4gICAgICBjb250ZW50OiBcIlxcMjc2RlwiO1xuICAgICAgd2lkdGg6IDFlbTtcbiAgICAgIGhlaWdodDogMWVtO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gICAgfVxuICAgIC8vIC5hY3RpdmU6YWZ0ZXIge1xuICAgIC8vICAgY29udGVudDogXCJcXDI3OTZcIjsgLyogVW5pY29kZSBjaGFyYWN0ZXIgZm9yIFwibWludXNcIiBzaWduICgtKSAqL1xuICAgIC8vIH1cbiAgfVxuICAmLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDA7XG4gICAgLy8gcGFkZGluZzogMCAxZW07XG4gICAgY29sb3I6ICRtaWRuaWdodDtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgfVxuICAmLWNsb3NlIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gICAgcGFkZGluZzogMWVtO1xuICAgIGZvbnQtc2l6ZTogMC43NWVtO1xuICAgIGJhY2tncm91bmQ6ICRtaWRuaWdodDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgJjpob3ZlciB7XG4gICAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgIH1cbiAgfVxufVxuXG4vLyA6Y2hlY2tlZFxuaW5wdXQ6Y2hlY2tlZCB7XG4gICsgLnRhYi1sYWJlbCB7XG4gICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICAmOjphZnRlciB7XG4gICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gICAgfVxuICB9XG4gIH4gLnRhYi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAxMDB2aDtcbiAgICAvLyBwYWRkaW5nOiAxZW07XG4gIH1cbn1cblxuLmJlZGdlLWFsaWdue1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59XG5cbi8vIC5iZWRnZS1hbGlnbi1tcnB7XG4vLyAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbi8vICAgdG9wOiA2NXB4O1xuLy8gICByaWdodDogNDVweFxuLy8gfVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStorePaginationPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-pagination',
          templateUrl: './data-store-pagination.page.html',
          styleUrls: ['./data-store-pagination.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_4__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-stacked/data-store-stacked.page.ts":
  /*!**********************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-stacked/data-store-stacked.page.ts ***!
    \**********************************************************************************/

  /*! exports provided: DataStoreStackedPage */

  /***/
  function srcAppShowcaseAppShellDataStoreStackedDataStoreStackedPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStoreStackedPage", function () {
      return DataStoreStackedPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../showcase-shell.model */
    "./src/app/showcase/showcase-shell.model.ts");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    var _c0 = function _c0() {
      return {
        w: 1,
        h: 1
      };
    };

    function DataStoreStackedPage_ng_template_37_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-row", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-col", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-aspect-ratio", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "app-image-shell", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](4, "async");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ion-col", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h3", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "app-text-shell", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](8, "async");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "app-text-shell", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "async");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var user_r4 = ctx.user;
        var tmp_1_0 = null;
        var currVal_1 = (tmp_1_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](4, 5, user_r4)) == null ? null : tmp_1_0.avatar;
        var tmp_3_0 = null;
        var currVal_3 = (tmp_3_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](8, 7, user_r4)) == null ? null : tmp_3_0.first_name;
        var tmp_4_0 = null;
        var currVal_4 = (tmp_4_0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 9, user_r4)) == null ? null : tmp_4_0.last_name;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c0));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", currVal_1)("alt", "Sample Image");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", currVal_3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", currVal_4);
      }
    }

    function DataStoreStackedPage_div_49_ng_container_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
      }
    }

    var _c1 = function _c1(a0) {
      return {
        user: a0
      };
    };

    function DataStoreStackedPage_div_49_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, DataStoreStackedPage_div_49_ng_container_1_Template, 1, 0, "ng-container", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var user_r5 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r0)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](2, _c1, user_r5));
      }
    }

    function DataStoreStackedPage_div_50_ng_container_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
      }
    }

    function DataStoreStackedPage_div_50_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, DataStoreStackedPage_div_50_ng_container_1_Template, 1, 0, "ng-container", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var user_r7 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r0)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](2, _c1, user_r7));
      }
    }

    var DataStoreStackedPage = /*#__PURE__*/function () {
      function DataStoreStackedPage(showcaseService) {
        _classCallCheck(this, DataStoreStackedPage);

        this.showcaseService = showcaseService; // View model

        this.stackedUsers = [];
        this.shellUsers = []; // Emulate a tream of events that trigger the loading of new items

        this.triggerNewItemsLoading = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
        this.newItemsTriggerObservable = this.triggerNewItemsLoading.asObservable();
      }

      _createClass(DataStoreStackedPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this7 = this;

          var openDataStream = this.showcaseService.getOpenDataStream();

          if (!this.openDataStore) {
            // Initialize the model specifying that it is a shell model
            var shellModel = [new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"]()];
            this.openDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_4__["DataStore"](shellModel); // Trigger the loading mechanism (with shell) in the dataStore

            this.openDataStore.load(openDataStream);
          } // Each time the user triggers the loading of new items, ask the service to get those new items


          var newDataObservable = this.newItemsTriggerObservable.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function () {
            var newValuesObservable = _this7.showcaseService.getStackedValuesDataSource();

            var newDataShell = [new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"](), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"]()];

            var newValuesWithShellObservable = _shell_data_store__WEBPACK_IMPORTED_MODULE_4__["DataStore"].AppendShell(newValuesObservable, newDataShell, 400);

            return newValuesWithShellObservable;
          }));
          Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["merge"])(this.openDataStore.state, newDataObservable).subscribe(function (data) {
            console.log('data', data);

            if (data.isShell) {
              var shellsAsObservables = _toConsumableArray(data).map(function (val) {
                // Transform plain shell values into async Observables (to comply with the layout markup)
                return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(val);
              }); // When loading new data, override the shellUsers property


              _this7.shellUsers = shellsAsObservables;
            } else {
              var _this7$stackedUsers;

              // Clear shellUsers property
              _this7.shellUsers = [];

              var dataWithShell = _toConsumableArray(data).map(function (val) {
                // Transform plain values into async Observables (to comply with the layout markup)
                return _shell_data_store__WEBPACK_IMPORTED_MODULE_4__["DataStore"].AppendShell(Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(val), new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_2__["ShowcaseShellUserModel"](), 400);
              }); // Concat data to existing stackedUsers property


              (_this7$stackedUsers = _this7.stackedUsers).push.apply(_this7$stackedUsers, _toConsumableArray(dataWithShell));
            }
          });
        }
      }, {
        key: "loadStackedResults",
        value: function loadStackedResults() {
          this.triggerNewItemsLoading.next();
        }
      }, {
        key: "pushValuesToOpenStream",
        value: function pushValuesToOpenStream() {
          this.showcaseService.pushValuesToOpenStream();
        }
      }]);

      return DataStoreStackedPage;
    }();

    DataStoreStackedPage.ɵfac = function DataStoreStackedPage_Factory(t) {
      return new (t || DataStoreStackedPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_3__["ShowcaseService"]));
    };

    DataStoreStackedPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStoreStackedPage,
      selectors: [["app-data-store-stacked"]],
      decls: 51,
      vars: 2,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], ["userTemplate", ""], [1, "stacked-items-demo"], [1, "showcase-section", "sticky-section"], ["size", "12"], [3, "click"], [4, "ngFor", "ngForOf"], [2, "background-color", "#FFF", "padding", "10px"], ["size", "4"], [3, "ratio"], ["animation", "spinner", 3, "src", "alt"], ["size", "8"], [2, "margin-top", "0px"], [3, "data"], ["lines", "3", 3, "data"], [4, "ngTemplateOutlet", "ngTemplateOutletContext"]],
      template: function DataStoreStackedPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - Stacked shells example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Once you have values coming from the initial data stream, the shell animation would be off. If its an ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "open stream");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, ", then each time a new value arrives, it will be added to the view state without the shell animation. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " This example is handy If you want to change the default functionality and apply independent shell animations to each value coming from the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "open data stream");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " It's also a good example if you want to implement an ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "inifini scroll");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " feature. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " For this to work we will be transforming plain values into Observables, thus the need to update our markup and specify the binded model as async. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "<h3>\n  <app-text-shell [data]=\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "(user | async)?");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, ".first_name\"></app-text-shell>\n</h3>\n<p>\n  <app-text-shell [data]=\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "(user | async)?");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, ".last_name\" lines=\"3\"></app-text-shell>\n</p>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, " Also as we are dealing with a mix of shell models and real values that get stacked one after the other, we need to have two seperate lists. One to show the shell values and another to contain the stacked values. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](37, DataStoreStackedPage_ng_template_37_Template, 12, 12, "ng-template", null, 5, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "section", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "ion-col", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "ion-button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DataStoreStackedPage_Template_ion_button_click_43_listener() {
            return ctx.pushValuesToOpenStream();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Push values to open stream");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "ion-col", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "ion-button", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DataStoreStackedPage_Template_ion_button_click_46_listener() {
            return ctx.loadStackedResults();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Load New Values");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](49, DataStoreStackedPage_div_49_Template, 2, 4, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](50, DataStoreStackedPage_div_50_Template, 2, 4, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](49);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.stackedUsers);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.shellUsers);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonCol"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonButton"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_8__["AspectRatioComponent"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_9__["ImageShellComponent"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_10__["TextShellComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgTemplateOutlet"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["AsyncPipe"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n.stacked-items-demo[_ngcontent-%COMP%] {\n  background-color: #ececec;\n  background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ffffff' fill-opacity='0.38'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\n  padding: 0px 0px 20px;\n}\n.sticky-section[_ngcontent-%COMP%] {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0px;\n  background: #ececec;\n  padding: 20px;\n  margin: 0px 0px 60px !important;\n  z-index: 10;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtc3RhY2tlZC9kYXRhLXN0b3JlLXN0YWNrZWQucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvYXBwLXNoZWxsLnBhZ2Uuc2NzcyIsIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtc3RhY2tlZC9kYXRhLXN0b3JlLXN0YWNrZWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLG1DQUFBO0FERUY7QUNBRTtFQUNFLHdDQUFBO0VBQ0Esa0NBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QURFSjtBQ0NFO0VBQ0Usc0JBQUE7QURDSjtBQ0VFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBREFKO0FDR0U7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QURESjtBQ0lBO0VBRUUsZUFBQTtBREZGO0FDT0E7RUFDRSxrQkFBQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBREpKO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLDZCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtBREhGO0FDS0E7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURGRjtBQ0lBO0VBQ0UsdUJBQUE7QURERjtBQ1VBO0VBQ0UsY0FKUztFQUtULG1CQUpPO0VBS1Asa0JBQUE7QURQRjtBQ1NBO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBRE5GO0FDUUE7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FETEY7QUNPQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QURKRjtBQ09BO0VBQ0UsYUFBQTtBREpGO0FDT0U7RUFDRSxPQUFBO0FETEo7QUNXQSxxQkFBQTtBQUNBO0VBRUUsZ0JBQUE7QURWRjtBQ2FBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBRFZGO0FDWUU7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBRUEsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSxTQUFBO0FEWEo7QUNlSTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QURiTjtBQ21CRTtFQUNFLGFBQUE7RUFFQSxjQXZFTztFQXdFUCxpQkFBQTtFQUNBLHFCQUFBO0FEbEJKO0FDb0JFO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBaEZPO0VBaUZQLGVBQUE7QURsQko7QUM2Qkk7RUFDRSx3QkFBQTtBRDNCTjtBQzhCRTtFQUNFLGlCQUFBO0FENUJKO0FDaUNBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBRDlCRjtBRTlJQTtFQUNFLHlCQUFBO0VBQ0Esd2lCQUFBO0VBQ0EscUJBQUE7QUZpSkY7QUU5SUE7RUFDRSx3QkFBQTtFQUFBLGdCQUFBO0VBQ0EsUUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLCtCQUFBO0VBQ0EsV0FBQTtBRmlKRiIsImZpbGUiOiJzcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9kYXRhLXN0b3JlLXN0YWNrZWQvZGF0YS1zdG9yZS1zdGFja2VkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG59XG4uc2hvd2Nhc2UtY29udGVudCBpb24taXRlbS1kaXZpZGVyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IHByZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogI0NDQztcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGNvZGUge1xuICBjb2xvcjogI0Y5MjY3MjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLm1lbnVfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmNsc29lLWljb24ge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uY29weV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmljb25fY2xvc2Uge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG5cbi5saXN0LW1kIHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmJvZHkge1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogI2VjZjBmMTtcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuXG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaDIge1xuICBtYXJnaW46IDAgMCAwLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5yb3cgLmNvbCB7XG4gIGZsZXg6IDE7XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnRhYi1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAvKiBJY29uICovXG59XG4udGFiLWxhYmVsOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwi4p2vXCI7XG4gIHdpZHRoOiAxZW07XG4gIGhlaWdodDogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDA7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jbG9zZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC1zaXplOiAwLjc1ZW07XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbmlucHV0OmNoZWNrZWQgKyAudGFiLWxhYmVsOjphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cbmlucHV0OmNoZWNrZWQgfiAudGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAxMDB2aDtcbn1cblxuLmJlZGdlLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4uc3RhY2tlZC1pdGVtcy1kZW1vIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZWNlYztcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc0MCcgaGVpZ2h0PSc0MCcgdmlld0JveD0nMCAwIDQwIDQwJyUzRSUzQ2cgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ2cgZmlsbD0nJTIzZmZmZmZmJyBmaWxsLW9wYWNpdHk9JzAuMzgnJTNFJTNDcGF0aCBkPSdNMCAzOC41OWwyLjgzLTIuODMgMS40MSAxLjQxTDEuNDEgNDBIMHYtMS40MXpNMCAxLjRsMi44MyAyLjgzIDEuNDEtMS40MUwxLjQxIDBIMHYxLjQxek0zOC41OSA0MGwtMi44My0yLjgzIDEuNDEtMS40MUw0MCAzOC41OVY0MGgtMS40MXpNNDAgMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwzOC41OSAwSDQwdjEuNDF6TTIwIDE4LjZsMi44My0yLjgzIDEuNDEgMS40MUwyMS40MSAyMGwyLjgzIDIuODMtMS40MSAxLjQxTDIwIDIxLjQxbC0yLjgzIDIuODMtMS40MS0xLjQxTDE4LjU5IDIwbC0yLjgzLTIuODMgMS40MS0xLjQxTDIwIDE4LjU5eicvJTNFJTNDL2clM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XG4gIHBhZGRpbmc6IDBweCAwcHggMjBweDtcbn1cblxuLnN0aWNreS1zZWN0aW9uIHtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwcHg7XG4gIGJhY2tncm91bmQ6ICNlY2VjZWM7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMHB4IDBweCA2MHB4ICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDEwO1xufSIsIi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgaW9uLWl0ZW0tZGl2aWRlciB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICB9XG5cbiAgLnNob3djYXNlLXNlY3Rpb24ge1xuICAgIG1hcmdpbjogMjBweCAyMHB4IDYwcHg7XG4gIH1cblxuICBwcmUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cblxuICBjb2RlIHtcbiAgICBjb2xvcjogI0Y5MjY3MjtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG59XG4ubWVudV9pY29ue1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbiAgLy8gcGFkZGluZy10b3A6IDdweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNsc29lLWljb257XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbn1cbi5oZWFydF9pY29ue1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY29weV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4ubWFpbi10aXRsZXtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uaWNvbl9jbG9zZXtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuN2VtO1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuLmxpc3QtbWR7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5cbi8vICoqKioqKioqKioqKioqKioqKioqKioqKiogQWNjb3JkaWFuXG5cbiRtaWRuaWdodDogIzJjM2U1MDtcbiRjbG91ZHM6ICNlY2YwZjE7XG4vLyBHZW5lcmFsXG5ib2R5IHtcbiAgY29sb3I6ICRtaWRuaWdodDtcbiAgYmFja2dyb3VuZDogJGNsb3VkcztcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuaDEge1xuICBtYXJnaW46IDA7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5oMiB7XG4gIG1hcmdpbjogMCAwIC41ZW07XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG4vLyBMYXlvdXRcbi5yb3cge1xuICBkaXNwbGF5OmZsZXg7XG4gIC8vIG1hcmdpbi1sZWZ0OiAxMHB4OyBcbiAgLy8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAuY29sIHtcbiAgICBmbGV4OjE7XG4gICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgIC8vIG1hcmdpbi1sZWZ0OiAxZW07XG4gICAgfVxuICB9XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm94LXNoYWRvdzogMCA0cHggNHB4IC0ycHggcmdiYSgwLDAsMCwwLjUpO1xufVxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAmLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgLy8gYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAgIC8qIEljb24gKi9cbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICAgICY6OmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFwyNzZGXCI7XG4gICAgICB3aWR0aDogMWVtO1xuICAgICAgaGVpZ2h0OiAxZW07XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgICB9XG4gICAgLy8gLmFjdGl2ZTphZnRlciB7XG4gICAgLy8gICBjb250ZW50OiBcIlxcMjc5NlwiOyAvKiBVbmljb2RlIGNoYXJhY3RlciBmb3IgXCJtaW51c1wiIHNpZ24gKC0pICovXG4gICAgLy8gfVxuICB9XG4gICYtY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMDtcbiAgICAvLyBwYWRkaW5nOiAwIDFlbTtcbiAgICBjb2xvcjogJG1pZG5pZ2h0O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICB9XG4gICYtY2xvc2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgZm9udC1zaXplOiAwLjc1ZW07XG4gICAgYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICB9XG59XG5cbi8vIDpjaGVja2VkXG5pbnB1dDpjaGVja2VkIHtcbiAgKyAudGFiLWxhYmVsIHtcbiAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgICY6OmFmdGVyIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICB9XG4gIH1cbiAgfiAudGFiLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDEwMHZoO1xuICAgIC8vIHBhZGRpbmc6IDFlbTtcbiAgfVxufVxuXG4uYmVkZ2UtYWxpZ257XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDc1JTtcbn1cblxuLy8gLmJlZGdlLWFsaWduLW1ycHtcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICB0b3A6IDY1cHg7XG4vLyAgIHJpZ2h0OiA0NXB4XG4vLyB9XG4iLCJAaW1wb3J0IFwiLi4vYXBwLXNoZWxsLnBhZ2VcIjtcblxuLnN0YWNrZWQtaXRlbXMtZGVtbyB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2VjZWM7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nNDAnIGhlaWdodD0nNDAnIHZpZXdCb3g9JzAgMCA0MCA0MCclM0UlM0NnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyM2ZmZmZmZicgZmlsbC1vcGFjaXR5PScwLjM4JyUzRSUzQ3BhdGggZD0nTTAgMzguNTlsMi44My0yLjgzIDEuNDEgMS40MUwxLjQxIDQwSDB2LTEuNDF6TTAgMS40bDIuODMgMi44MyAxLjQxLTEuNDFMMS40MSAwSDB2MS40MXpNMzguNTkgNDBsLTIuODMtMi44MyAxLjQxLTEuNDFMNDAgMzguNTlWNDBoLTEuNDF6TTQwIDEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMzguNTkgMEg0MHYxLjQxek0yMCAxOC42bDIuODMtMi44MyAxLjQxIDEuNDFMMjEuNDEgMjBsMi44MyAyLjgzLTEuNDEgMS40MUwyMCAyMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwxOC41OSAyMGwtMi44My0yLjgzIDEuNDEtMS40MUwyMCAxOC41OXonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xuICBwYWRkaW5nOiAwcHggMHB4IDIwcHg7XG59XG5cbi5zdGlja3ktc2VjdGlvbiB7XG4gIHBvc2l0aW9uOiBzdGlja3k7XG4gIHRvcDogMHB4O1xuICBiYWNrZ3JvdW5kOiAjZWNlY2VjO1xuICBwYWRkaW5nOiAyMHB4O1xuICBtYXJnaW46IDBweCAwcHggNjBweCAhaW1wb3J0YW50O1xuICB6LWluZGV4OiAxMDtcbn1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStoreStackedPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-stacked',
          templateUrl: './data-store-stacked.page.html',
          styleUrls: ['./data-store-stacked.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_3__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/data-store-subset/data-store-subset.page.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/showcase/app-shell/data-store-subset/data-store-subset.page.ts ***!
    \********************************************************************************/

  /*! exports provided: DataStoreSubsetPage */

  /***/
  function srcAppShowcaseAppShellDataStoreSubsetDataStoreSubsetPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStoreSubsetPage", function () {
      return DataStoreSubsetPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../showcase-shell.model */
    "./src/app/showcase/showcase-shell.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    var DataStoreSubsetPage = /*#__PURE__*/function () {
      function DataStoreSubsetPage(showcaseService) {
        _classCallCheck(this, DataStoreSubsetPage);

        this.showcaseService = showcaseService;
      }

      _createClass(DataStoreSubsetPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this8 = this;

          var dataSource = this.showcaseService.getUserSubsetData(2);
          var shellModel = new _showcase_shell_model__WEBPACK_IMPORTED_MODULE_3__["ShowcaseCompanyModel"]();
          this.companyDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_2__["DataStore"](shellModel);
          this.companyDataStore.load(dataSource);
          this.companyDataStore.state.subscribe(function (data) {
            _this8.company = data;
          });
        }
      }]);

      return DataStoreSubsetPage;
    }();

    DataStoreSubsetPage.ɵfac = function DataStoreSubsetPage_Factory(t) {
      return new (t || DataStoreSubsetPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]));
    };

    DataStoreSubsetPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: DataStoreSubsetPage,
      selectors: [["app-data-store-subset"]],
      decls: 38,
      vars: 3,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], ["href", "https://dev.to/napoleon039/how-to-use-the-spread-and-rest-operator-4jbb", "target", "_blank"], [1, "company"], ["animation", "bouncing", 2, "--text-shell-line-color", "#FFF", 3, "data"]],
      template: function DataStoreSubsetPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " DataStore - Subset example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Sometimes the data coming from the remote API does not match 100% the model you defined for your view. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "export class YourModel extends ShellModel {\n  cover: string;\n  image: string;\n  title: string;\n  description: string;\n}\n\n// Data coming from the remote API with the following format\nconst jsonResponse = {\n  name: 'extra-data',\n  fields: {\n    cover: 'some-image';\n    image: 'some-other-image';\n    title: 'a-title';\n    description: 'a-description';\n  }\n};\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, " Hopefully, we can use some ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "RxJs operators");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " to mold the data and fit our needs. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " If that\u2019s not enough, you can use the handy ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "spread");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, " and ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "rest");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, " operators");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " to mold your data even deeper. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "public getListDataSource(): Observable<YourModel> {\n  const dataObservable = this.http.get('https://reqres.in/api/users');\n\n  return dataObservable.pipe(\n    map((jsonResponse) => {\n      const filteredData: YourModel = {\n        ...data.fields\n      };\n      return filteredData;\n    })\n  );\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Company");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "app-text-shell", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "app-text-shell", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "app-text-shell", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.company.name);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.company.catchPhrase);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.company.bs);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_5__["TextShellComponent"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n.company[_ngcontent-%COMP%] {\n  background-color: #ececec;\n  background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ffffff' fill-opacity='0.38'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\n  padding: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtc3Vic2V0L2RhdGEtc3RvcmUtc3Vic2V0LnBhZ2Uuc2NzcyIsIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2FwcC1zaGVsbC5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9kYXRhLXN0b3JlLXN1YnNldC9kYXRhLXN0b3JlLXN1YnNldC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQWhCO0VBQ0UsbUNBQUE7QURFRjtBQ0FFO0VBQ0Usd0NBQUE7RUFDQSxrQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBREVKO0FDQ0U7RUFDRSxzQkFBQTtBRENKO0FDRUU7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FEQUo7QUNHRTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtBRERKO0FDSUE7RUFFRSxlQUFBO0FERkY7QUNPQTtFQUNFLGtCQUFBO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FESko7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FESEY7QUNLQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBREZGO0FDSUE7RUFDRSx1QkFBQTtBRERGO0FDVUE7RUFDRSxjQUpTO0VBS1QsbUJBSk87RUFLUCxrQkFBQTtBRFBGO0FDU0E7RUFDRSxTQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FETkY7QUNRQTtFQUNFLGlCQUFBO0VBQ0EsbUJBQUE7QURMRjtBQ09BO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBREpGO0FDT0E7RUFDRSxhQUFBO0FESkY7QUNPRTtFQUNFLE9BQUE7QURMSjtBQ1dBLHFCQUFBO0FBQ0E7RUFFRSxnQkFBQTtBRFZGO0FDYUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FEVkY7QUNZRTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLFlBQUE7RUFFQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSw2QkFBQTtFQUNBLFNBQUE7QURYSjtBQ2VJO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBRGJOO0FDbUJFO0VBQ0UsYUFBQTtFQUVBLGNBdkVPO0VBd0VQLGlCQUFBO0VBQ0EscUJBQUE7QURsQko7QUNvQkU7RUFDRSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFoRk87RUFpRlAsZUFBQTtBRGxCSjtBQzZCSTtFQUNFLHdCQUFBO0FEM0JOO0FDOEJFO0VBQ0UsaUJBQUE7QUQ1Qko7QUNpQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FEOUJGO0FFOUlBO0VBQ0UseUJBQUE7RUFDQSx3aUJBQUE7RUFDQSxhQUFBO0FGaUpGIiwiZmlsZSI6InNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2RhdGEtc3RvcmUtc3Vic2V0L2RhdGEtc3RvcmUtc3Vic2V0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG59XG4uc2hvd2Nhc2UtY29udGVudCBpb24taXRlbS1kaXZpZGVyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IHByZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogI0NDQztcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGNvZGUge1xuICBjb2xvcjogI0Y5MjY3MjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLm1lbnVfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmNsc29lLWljb24ge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uY29weV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmljb25fY2xvc2Uge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG5cbi5saXN0LW1kIHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmJvZHkge1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogI2VjZjBmMTtcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuXG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaDIge1xuICBtYXJnaW46IDAgMCAwLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5yb3cgLmNvbCB7XG4gIGZsZXg6IDE7XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnRhYi1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAvKiBJY29uICovXG59XG4udGFiLWxhYmVsOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwi4p2vXCI7XG4gIHdpZHRoOiAxZW07XG4gIGhlaWdodDogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDA7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jbG9zZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC1zaXplOiAwLjc1ZW07XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbmlucHV0OmNoZWNrZWQgKyAudGFiLWxhYmVsOjphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cbmlucHV0OmNoZWNrZWQgfiAudGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAxMDB2aDtcbn1cblxuLmJlZGdlLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4uY29tcGFueSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlY2VjZWM7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcImRhdGE6aW1hZ2Uvc3ZnK3htbCwlM0NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nNDAnIGhlaWdodD0nNDAnIHZpZXdCb3g9JzAgMCA0MCA0MCclM0UlM0NnIGZpbGwtcnVsZT0nZXZlbm9kZCclM0UlM0NnIGZpbGw9JyUyM2ZmZmZmZicgZmlsbC1vcGFjaXR5PScwLjM4JyUzRSUzQ3BhdGggZD0nTTAgMzguNTlsMi44My0yLjgzIDEuNDEgMS40MUwxLjQxIDQwSDB2LTEuNDF6TTAgMS40bDIuODMgMi44MyAxLjQxLTEuNDFMMS40MSAwSDB2MS40MXpNMzguNTkgNDBsLTIuODMtMi44MyAxLjQxLTEuNDFMNDAgMzguNTlWNDBoLTEuNDF6TTQwIDEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMzguNTkgMEg0MHYxLjQxek0yMCAxOC42bDIuODMtMi44MyAxLjQxIDEuNDFMMjEuNDEgMjBsMi44MyAyLjgzLTEuNDEgMS40MUwyMCAyMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwxOC41OSAyMGwtMi44My0yLjgzIDEuNDEtMS40MUwyMCAxOC41OXonLyUzRSUzQy9nJTNFJTNDL2clM0UlM0Mvc3ZnJTNFXCIpO1xuICBwYWRkaW5nOiAyMHB4O1xufSIsIi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgaW9uLWl0ZW0tZGl2aWRlciB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICB9XG5cbiAgLnNob3djYXNlLXNlY3Rpb24ge1xuICAgIG1hcmdpbjogMjBweCAyMHB4IDYwcHg7XG4gIH1cblxuICBwcmUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cblxuICBjb2RlIHtcbiAgICBjb2xvcjogI0Y5MjY3MjtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG59XG4ubWVudV9pY29ue1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbiAgLy8gcGFkZGluZy10b3A6IDdweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNsc29lLWljb257XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbn1cbi5oZWFydF9pY29ue1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY29weV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4ubWFpbi10aXRsZXtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uaWNvbl9jbG9zZXtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuN2VtO1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuLmxpc3QtbWR7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5cbi8vICoqKioqKioqKioqKioqKioqKioqKioqKiogQWNjb3JkaWFuXG5cbiRtaWRuaWdodDogIzJjM2U1MDtcbiRjbG91ZHM6ICNlY2YwZjE7XG4vLyBHZW5lcmFsXG5ib2R5IHtcbiAgY29sb3I6ICRtaWRuaWdodDtcbiAgYmFja2dyb3VuZDogJGNsb3VkcztcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuaDEge1xuICBtYXJnaW46IDA7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5oMiB7XG4gIG1hcmdpbjogMCAwIC41ZW07XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG4vLyBMYXlvdXRcbi5yb3cge1xuICBkaXNwbGF5OmZsZXg7XG4gIC8vIG1hcmdpbi1sZWZ0OiAxMHB4OyBcbiAgLy8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAuY29sIHtcbiAgICBmbGV4OjE7XG4gICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgIC8vIG1hcmdpbi1sZWZ0OiAxZW07XG4gICAgfVxuICB9XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm94LXNoYWRvdzogMCA0cHggNHB4IC0ycHggcmdiYSgwLDAsMCwwLjUpO1xufVxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAmLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgLy8gYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAgIC8qIEljb24gKi9cbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICAgICY6OmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFwyNzZGXCI7XG4gICAgICB3aWR0aDogMWVtO1xuICAgICAgaGVpZ2h0OiAxZW07XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgICB9XG4gICAgLy8gLmFjdGl2ZTphZnRlciB7XG4gICAgLy8gICBjb250ZW50OiBcIlxcMjc5NlwiOyAvKiBVbmljb2RlIGNoYXJhY3RlciBmb3IgXCJtaW51c1wiIHNpZ24gKC0pICovXG4gICAgLy8gfVxuICB9XG4gICYtY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMDtcbiAgICAvLyBwYWRkaW5nOiAwIDFlbTtcbiAgICBjb2xvcjogJG1pZG5pZ2h0O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICB9XG4gICYtY2xvc2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgZm9udC1zaXplOiAwLjc1ZW07XG4gICAgYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICB9XG59XG5cbi8vIDpjaGVja2VkXG5pbnB1dDpjaGVja2VkIHtcbiAgKyAudGFiLWxhYmVsIHtcbiAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgICY6OmFmdGVyIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICB9XG4gIH1cbiAgfiAudGFiLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDEwMHZoO1xuICAgIC8vIHBhZGRpbmc6IDFlbTtcbiAgfVxufVxuXG4uYmVkZ2UtYWxpZ257XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDc1JTtcbn1cblxuLy8gLmJlZGdlLWFsaWduLW1ycHtcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICB0b3A6IDY1cHg7XG4vLyAgIHJpZ2h0OiA0NXB4XG4vLyB9XG4iLCJAaW1wb3J0IFwiLi4vYXBwLXNoZWxsLnBhZ2VcIjtcblxuLmNvbXBhbnkge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWNlY2VjO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzQwJyBoZWlnaHQ9JzQwJyB2aWV3Qm94PScwIDAgNDAgNDAnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjNmZmZmZmYnIGZpbGwtb3BhY2l0eT0nMC4zOCclM0UlM0NwYXRoIGQ9J00wIDM4LjU5bDIuODMtMi44MyAxLjQxIDEuNDFMMS40MSA0MEgwdi0xLjQxek0wIDEuNGwyLjgzIDIuODMgMS40MS0xLjQxTDEuNDEgMEgwdjEuNDF6TTM4LjU5IDQwbC0yLjgzLTIuODMgMS40MS0xLjQxTDQwIDM4LjU5VjQwaC0xLjQxek00MCAxLjQxbC0yLjgzIDIuODMtMS40MS0xLjQxTDM4LjU5IDBINDB2MS40MXpNMjAgMTguNmwyLjgzLTIuODMgMS40MSAxLjQxTDIxLjQxIDIwbDIuODMgMi44My0xLjQxIDEuNDFMMjAgMjEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMTguNTkgMjBsLTIuODMtMi44MyAxLjQxLTEuNDFMMjAgMTguNTl6Jy8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcbiAgcGFkZGluZzogMjBweDtcbn1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](DataStoreSubsetPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-data-store-subset',
          templateUrl: './data-store-subset.page.html',
          styleUrls: ['./data-store-subset.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_1__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/image-shell/image-shell.page.ts":
  /*!********************************************************************!*\
    !*** ./src/app/showcase/app-shell/image-shell/image-shell.page.ts ***!
    \********************************************************************/

  /*! exports provided: ImageShellPage */

  /***/
  function srcAppShowcaseAppShellImageShellImageShellPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ImageShellPage", function () {
      return ImageShellPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");

    var _c0 = function _c0() {
      return {
        w: 1,
        h: 1
      };
    };

    var _c1 = function _c1() {
      return {
        w: 3,
        h: 2
      };
    };

    var _c2 = function _c2() {
      return {
        w: 2,
        h: 1
      };
    };

    var ImageShellPage = /*#__PURE__*/function () {
      function ImageShellPage() {
        _classCallCheck(this, ImageShellPage);
      }

      _createClass(ImageShellPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ImageShellPage;
    }();

    ImageShellPage.ɵfac = function ImageShellPage_Factory(t) {
      return new (t || ImageShellPage)();
    };

    ImageShellPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ImageShellPage,
      selectors: [["app-image-shell-page"]],
      decls: 302,
      vars: 44,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], [2, "width", "30%", "margin", "0px auto"], [3, "ratio"], [3, "src", "alt"], ["animation", "spinner", 3, "src", "alt"], ["animation", "gradient", 3, "src", "alt"], ["animation", "spinner", 1, "add-overlay", 3, "src", "alt"], [2, "--image-shell-loading-background", "rgba(233, 30, 99, .25)", 3, "src", "alt"], ["animation", "spinner", 1, "add-overlay", 2, "--image-shell-overlay-background", "rgba(103, 58, 183, .40)", 3, "src", "alt"], ["animation", "spinner", 2, "--image-shell-spinner-size", "40px", "--image-shell-spinner-color", "#000000", 3, "src", "alt"], ["animation", "gradient", 2, "--image-shell-loading-background", "rgba(255, 3, 109, 0.6)", "--image-shell-animation-color", "rgba(156, 4, 68, 0.7)", 3, "src", "alt"], [2, "width", "80%", "margin", "0px auto"], ["animation", "spinner", 2, "--image-shell-border-radius", "10px", 3, "src", "alt"], ["animation", "gradient", 2, "--image-shell-border-radius", "10px", 3, "src", "alt"], ["animation", "spinner", 1, "add-overlay", 3, "display", "src"], [2, "margin", "10px"], ["animation", "spinner", 3, "display", "src"], ["animation", "spinner", 2, "background-position", "bottom right", 3, "display", "src"], [2, "width", "60%", "margin", "0px auto"]],
      template: function ImageShellPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Image Shell ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " This component enables to preload an image with an elegant shell. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Set different animation options");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " You can choose between ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "no animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " (default), ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "spinner animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, ", and ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "gradient background");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " animation ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "No animation (default)");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "<app-image-shell [src]=\"\" [alt]=\"\"></app-image-shell>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "app-image-shell", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Spinner animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "<app-image-shell ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "animation=\"spinner\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, " [src]=\"\" [alt]=\"\"></app-image-shell>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "app-image-shell", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Gradient animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "<app-image-shell ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "animation=\"gradient\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, " [src]=\"\" [alt]=\"\"></app-image-shell>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "app-image-shell", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, " You can add/remove the following ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "Classes");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, " to adjust the shell element behavior: ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Overlay");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, " This class adds a background overlay after the image has loaded. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "<app-image-shell animation=\"spinner\" ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "class=\"add-overlay\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, " [src]=\"''./assets/sample-images/travel/Travel2-64.47.png''\" [alt]=\"\"></app-image-shell>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "app-image-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, " You can also override these ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "CSS 4 variables");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, " to adjust the shell element style ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Background");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "app-image-shell {\n  --image-shell-loading-background: rgba(233, 30, 99, .25);\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](81, "app-image-shell", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "Overlay");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "app-image-shell {\n  --image-shell-overlay-background: rgba(103, 58, 183, .40);\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "app-image-shell", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Spinner");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "app-image-shell {\n  --image-shell-spinner-size: 40px;\n  --image-shell-spinner-color: #000000;\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "app-image-shell", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Background Animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, " These properties are only applied when using ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "animation=\"gradient\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "app-image-shell {\n  --image-shell-loading-background: rgba(255, 3, 109, 0.6);\n  --image-shell-animation-color: rgba(156, 4, 68, 0.7);\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "app-image-shell", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Border Radius");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "app-image-shell {\n  --image-shell-border-radius: 10px;\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "ion-col");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](118, "app-image-shell", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "ion-col");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](122, "app-image-shell", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Cover Image Display");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, " In addition to the default ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "<app-image-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, ", you can specify a ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, " display if you want to display your image as a background-image. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, " This is different from the default display, because (although it's not mandatory) cover display is designed to contain content (for example some text) ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "<app-image-shell [display]=\"'cover'\" animation=\"spinner\" class=\"add-overlay\" [src]=\"\">");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](139, "\n  <app-aspect-ratio [ratio]=\"{w:3, h:2}\">\n    Some Text\n  </app-aspect-ratio>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "</app-image-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](142, "\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "app-image-shell", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h4", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Some Text on top of a background-image container");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Usage");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](151, " Let me explain you the differences between ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](153, "default");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](154, " and ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](156, "cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, " display and when you should use each one. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](159, "Default Mode Usage");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](161, " You should use the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, "default display");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, " when you are 100% confident on the aspect ratio of an image. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](166, " If the image ratio does not match the specified aspect ratio, the image will be stretched. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, "Expected behavior:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](170, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](171, " The image ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, "WILL");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, " be stretched. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](175, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](176, " We define an aspect ratio of ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, "2:1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, " but the image has an aspect ratio of ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, "1:1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](185, "app-image-shell", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, "Expected behavior:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](189, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](190, " The image ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](192, "WON'T");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](193, " be stretched. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](194, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](195, " We define an aspect ratio of ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](197, "1:1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, " that matches the image ratio of ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](199, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](200, "1:1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](203, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](204, "app-image-shell", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](205, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](206, "Cover Mode Usage");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](207, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](208, " You should use the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](209, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](210, "cover display");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](211, " when you want to fill a container with an image and you don't care if the image is shown completly or a portion of it. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](212, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](213, " If the image ratio does not match the specified aspect ratio, the image will be cropped to fit the available space without stretching the image. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](214, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](215, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](216, "Expected behavior:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](217, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](218, " The image will be cropped to fill the available space. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](219, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](220, " We define an aspect ratio of ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](221, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](222, "2:1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](223, " but the image has an aspect ratio of ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](225, "1:1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](226, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](228, "app-image-shell", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](229, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](230, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](231, " You can also change the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](232, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](233, "background-position");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](234, " property to adjust how the image is placed inside the container. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](236, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](237, "Notice:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](238, " This only applies to ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](239, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](240, "cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](241, " display. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](242, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](243, "app-image-shell[display=\"cover\"] {\n  background-position: bottom right;\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](244, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](245, "app-image-shell", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](246, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](247, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](248, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](249, "Maintain Aspect Ratio");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](250, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](251, " We use the handy ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](252, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](253, "<app-aspect-ratio>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](254, " to ensure the image shell mantains a specified aspect ratio. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](255, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](256, " By doing so, we prevent content from jumping around the page while assets (images) are loading. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](257, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](258, "Default display");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](259, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](260, " Just surround the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](261, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](262, "<app-image-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](263, " with an ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](264, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](265, "<app-aspect-ratio>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](266, " element ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](267, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](269, "<app-aspect-ratio [ratio]=\"{w:3, h:2}\">");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](270, "\n  <app-image-shell animation=\"spinner\" [src]=\"\" [alt]=\"\"></app-image-shell>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](271, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](272, "</app-aspect-ratio>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](273, "\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](274, "div", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](275, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](276, "app-image-shell", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](277, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](278, "Cover display");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](279, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](280, " When using the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](281, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](282, "cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](283, " display we need to include the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](284, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](285, "<app-aspect-ratio>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](286, " inside the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](287, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](288, "<app-image-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](289, " element instead of surrounding it. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](290, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](291, "<app-image-shell [display]=\"'cover'\" animation=\"spinner\" class=\"add-overlay\" [src]=\"\">\n  ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](292, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](293, "<app-aspect-ratio [ratio]=\"{w:3, h:2}\">");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](294, "\n    Some Text\n  ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](295, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](296, "</app-aspect-ratio>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](297, "\n</app-image-shell>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](298, "app-image-shell", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](299, "app-aspect-ratio", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](300, "h4", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](301, "Some Text on top of a background-image container");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](27, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](28, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](29, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](30, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", "./assets/sample-images/travel/Travel2-64.47.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](31, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](32, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", "./assets/sample-images/travel/Travel2-64.47.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](33, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](34, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](35, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](36, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](37, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](38, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", "./assets/sample-images/getting-started/category2-1.1.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](39, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", "./assets/sample-images/getting-started/category2-1.1.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover")("src", "./assets/sample-images/getting-started/category2-1.1.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](40, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover")("src", "./assets/sample-images/getting-started/category2-1.1.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](41, _c2));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](42, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](43, _c1));
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonContent"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_2__["AspectRatioComponent"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_3__["ImageShellComponent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonCol"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2ltYWdlLXNoZWxsL2ltYWdlLXNoZWxsLnBhZ2Uuc2NzcyIsIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2FwcC1zaGVsbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQWhCO0VBQ0UsbUNBQUE7QURFRjtBQ0FFO0VBQ0Usd0NBQUE7RUFDQSxrQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBREVKO0FDQ0U7RUFDRSxzQkFBQTtBRENKO0FDRUU7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FEQUo7QUNHRTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtBRERKO0FDSUE7RUFFRSxlQUFBO0FERkY7QUNPQTtFQUNFLGtCQUFBO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FESko7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FESEY7QUNLQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBREZGO0FDSUE7RUFDRSx1QkFBQTtBRERGO0FDVUE7RUFDRSxjQUpTO0VBS1QsbUJBSk87RUFLUCxrQkFBQTtBRFBGO0FDU0E7RUFDRSxTQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FETkY7QUNRQTtFQUNFLGlCQUFBO0VBQ0EsbUJBQUE7QURMRjtBQ09BO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBREpGO0FDT0E7RUFDRSxhQUFBO0FESkY7QUNPRTtFQUNFLE9BQUE7QURMSjtBQ1dBLHFCQUFBO0FBQ0E7RUFFRSxnQkFBQTtBRFZGO0FDYUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FEVkY7QUNZRTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLFlBQUE7RUFFQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSw2QkFBQTtFQUNBLFNBQUE7QURYSjtBQ2VJO0VBQ0UsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBRGJOO0FDbUJFO0VBQ0UsYUFBQTtFQUVBLGNBdkVPO0VBd0VQLGlCQUFBO0VBQ0EscUJBQUE7QURsQko7QUNvQkU7RUFDRSxhQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFoRk87RUFpRlAsZUFBQTtBRGxCSjtBQzZCSTtFQUNFLHdCQUFBO0FEM0JOO0FDOEJFO0VBQ0UsaUJBQUE7QUQ1Qko7QUNpQ0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0FEOUJGIiwiZmlsZSI6InNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2ltYWdlLXNoZWxsL2ltYWdlLXNoZWxsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG59XG4uc2hvd2Nhc2UtY29udGVudCBpb24taXRlbS1kaXZpZGVyIHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0ZXN0KTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IC5zaG93Y2FzZS1zZWN0aW9uIHtcbiAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IHByZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYmFja2dyb3VuZDogI0NDQztcbiAgcGFkZGluZzogMTBweDtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGNvZGUge1xuICBjb2xvcjogI0Y5MjY3MjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuLm1lbnVfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbn1cblxuLmNsc29lLWljb24ge1xuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uY29weV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmljb25fY2xvc2Uge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG5cbi5saXN0LW1kIHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbmJvZHkge1xuICBjb2xvcjogIzJjM2U1MDtcbiAgYmFja2dyb3VuZDogI2VjZjBmMTtcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuXG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaDIge1xuICBtYXJnaW46IDAgMCAwLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cblxuaW5wdXQge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG9wYWNpdHk6IDA7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbn1cbi5yb3cgLmNvbCB7XG4gIGZsZXg6IDE7XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi50YWIge1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IGJsYWNrO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLnRhYi1sYWJlbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAvKiBJY29uICovXG59XG4udGFiLWxhYmVsOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwi4p2vXCI7XG4gIHdpZHRoOiAxZW07XG4gIGhlaWdodDogMWVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRyYW5zaXRpb246IGFsbCAwLjM1cztcbn1cbi50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDA7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jbG9zZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XG4gIHBhZGRpbmc6IDFlbTtcbiAgZm9udC1zaXplOiAwLjc1ZW07XG4gIGJhY2tncm91bmQ6ICMyYzNlNTA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbmlucHV0OmNoZWNrZWQgKyAudGFiLWxhYmVsOjphZnRlciB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cbmlucHV0OmNoZWNrZWQgfiAudGFiLWNvbnRlbnQge1xuICBtYXgtaGVpZ2h0OiAxMDB2aDtcbn1cblxuLmJlZGdlLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufSIsIi5zaG93Y2FzZS1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgaW9uLWl0ZW0tZGl2aWRlciB7XG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICB9XG5cbiAgLnNob3djYXNlLXNlY3Rpb24ge1xuICAgIG1hcmdpbjogMjBweCAyMHB4IDYwcHg7XG4gIH1cblxuICBwcmUge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gIH1cblxuICBjb2RlIHtcbiAgICBjb2xvcjogI0Y5MjY3MjtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICB9XG59XG4ubWVudV9pY29ue1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbiAgLy8gcGFkZGluZy10b3A6IDdweDtcbiAgLy8gY29sb3I6IHdoaXRlO1xufVxuLmNsc29lLWljb257XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbn1cbi5oZWFydF9pY29ue1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY29weV9pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4ubWFpbi10aXRsZXtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG4uaWNvbl9jbG9zZXtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuN2VtO1xuICBjb2xvcjogIzAwMDAwMDtcbiAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuLmxpc3QtbWR7XG4gIHBhZGRpbmc6IDBweCAhaW1wb3J0YW50O1xufVxuXG5cbi8vICoqKioqKioqKioqKioqKioqKioqKioqKiogQWNjb3JkaWFuXG5cbiRtaWRuaWdodDogIzJjM2U1MDtcbiRjbG91ZHM6ICNlY2YwZjE7XG4vLyBHZW5lcmFsXG5ib2R5IHtcbiAgY29sb3I6ICRtaWRuaWdodDtcbiAgYmFja2dyb3VuZDogJGNsb3VkcztcbiAgcGFkZGluZzogMCAxZW0gMWVtO1xufVxuaDEge1xuICBtYXJnaW46IDA7XG4gIGxpbmUtaGVpZ2h0OiAyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5oMiB7XG4gIG1hcmdpbjogMCAwIC41ZW07XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG4vLyBMYXlvdXRcbi5yb3cge1xuICBkaXNwbGF5OmZsZXg7XG4gIC8vIG1hcmdpbi1sZWZ0OiAxMHB4OyBcbiAgLy8gbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAuY29sIHtcbiAgICBmbGV4OjE7XG4gICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgIC8vIG1hcmdpbi1sZWZ0OiAxZW07XG4gICAgfVxuICB9XG59XG4vKiBBY2NvcmRpb24gc3R5bGVzICovXG4udGFicyB7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm94LXNoYWRvdzogMCA0cHggNHB4IC0ycHggcmdiYSgwLDAsMCwwLjUpO1xufVxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAmLWxhYmVsIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgLy8gYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBncmV5O1xuICAgIC8qIEljb24gKi9cbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICAgICY6OmFmdGVyIHtcbiAgICAgIGNvbnRlbnQ6IFwiXFwyNzZGXCI7XG4gICAgICB3aWR0aDogMWVtO1xuICAgICAgaGVpZ2h0OiAxZW07XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB0cmFuc2l0aW9uOiBhbGwgLjM1cztcbiAgICB9XG4gICAgLy8gLmFjdGl2ZTphZnRlciB7XG4gICAgLy8gICBjb250ZW50OiBcIlxcMjc5NlwiOyAvKiBVbmljb2RlIGNoYXJhY3RlciBmb3IgXCJtaW51c1wiIHNpZ24gKC0pICovXG4gICAgLy8gfVxuICB9XG4gICYtY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMDtcbiAgICAvLyBwYWRkaW5nOiAwIDFlbTtcbiAgICBjb2xvcjogJG1pZG5pZ2h0O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICB9XG4gICYtY2xvc2Uge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgICBwYWRkaW5nOiAxZW07XG4gICAgZm9udC1zaXplOiAwLjc1ZW07XG4gICAgYmFja2dyb3VuZDogJG1pZG5pZ2h0O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAmOmhvdmVyIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgfVxuICB9XG59XG5cbi8vIDpjaGVja2VkXG5pbnB1dDpjaGVja2VkIHtcbiAgKyAudGFiLWxhYmVsIHtcbiAgICAvLyBiYWNrZ3JvdW5kOiBkYXJrZW4oJG1pZG5pZ2h0LCAxMCUpO1xuICAgICY6OmFmdGVyIHtcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbiAgICB9XG4gIH1cbiAgfiAudGFiLWNvbnRlbnQge1xuICAgIG1heC1oZWlnaHQ6IDEwMHZoO1xuICAgIC8vIHBhZGRpbmc6IDFlbTtcbiAgfVxufVxuXG4uYmVkZ2UtYWxpZ257XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLWxlZnQ6IDc1JTtcbn1cblxuLy8gLmJlZGdlLWFsaWduLW1ycHtcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICB0b3A6IDY1cHg7XG4vLyAgIHJpZ2h0OiA0NXB4XG4vLyB9XG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ImageShellPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-image-shell-page',
          templateUrl: './image-shell.page.html',
          styleUrls: ['./image-shell.page.scss']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/simple-data-binding/simple-data-binding.page.ts":
  /*!************************************************************************************!*\
    !*** ./src/app/showcase/app-shell/simple-data-binding/simple-data-binding.page.ts ***!
    \************************************************************************************/

  /*! exports provided: SimpleDataBindingPage */

  /***/
  function srcAppShowcaseAppShellSimpleDataBindingSimpleDataBindingPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SimpleDataBindingPage", function () {
      return SimpleDataBindingPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _showcase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../showcase.service */
    "./src/app/showcase/showcase.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");

    var _c0 = function _c0() {
      return {
        w: 2,
        h: 1
      };
    };

    var _c1 = function _c1() {
      return {
        w: 1,
        h: 1
      };
    };

    var SimpleDataBindingPage = /*#__PURE__*/function () {
      function SimpleDataBindingPage(showcaseService) {
        _classCallCheck(this, SimpleDataBindingPage);

        this.showcaseService = showcaseService; // Aux properties for the Simple Fetch (HttpClient) showcase

        this.simpleFetchButtonDisabled = true;
        this.simpleFetchCountdown = 0;
      }

      _createClass(SimpleDataBindingPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.showcaseShellSimpleFetch(10);
        }
      }, {
        key: "showcaseShellSimpleFetch",
        value: function showcaseShellSimpleFetch(countdown) {
          var _this9 = this;

          // Assign an (empty / null) value to the shell object to restore it's 'loading' state
          this.simpleFetchData = null; // Prevent rage clicks on the 'Fetch more Data' button

          this.simpleFetchButtonDisabled = true; // Start a countdown and an interval before executing the fetch function

          this.simpleFetchCountdown = countdown;
          this.simpleFetchInterval = Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["interval"])(1000); // Start a countdown to showcase the shell elements animations for a few seconds before the data get's fetched into the shell object

          var timer$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["timer"])(countdown * 1000); // When timer emits after X seconds, complete source

          this.simpleFetchInterval.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["takeUntil"])(timer$), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["finalize"])(function () {
            return _this9.simpleFetchButtonDisabled = false;
          })).subscribe({
            next: function next() {
              _this9.simpleFetchCountdown--;
            },
            complete: function complete() {
              _this9.simpleFetchCountdown = 0; // Once the countdown ends, fetch data using the HttpClient

              _this9.showcaseService.getSimpleDataSource().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["take"])(1) // Force Observable to complete
              ).subscribe(function (val) {
                console.log('Fetching shell data using the HttpClient', val);
                _this9.simpleFetchData = val;
              });
            }
          });
        }
      }]);

      return SimpleDataBindingPage;
    }();

    SimpleDataBindingPage.ɵfac = function SimpleDataBindingPage_Factory(t) {
      return new (t || SimpleDataBindingPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_showcase_service__WEBPACK_IMPORTED_MODULE_3__["ShowcaseService"]));
    };

    SimpleDataBindingPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: SimpleDataBindingPage,
      selectors: [["app-simple-data-binding"]],
      decls: 36,
      vars: 13,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section"], [2, "background-color", "#FFF", "padding", "10px", "margin", "20px"], ["size", "12"], ["animation", "gradient", 1, "add-overlay", 3, "display", "src", "alt"], [3, "ratio"], [2, "margin", "10px", "text-align", "center", "color", "#FFF"], ["size", "4"], ["animation", "gradient", 3, "src", "alt"], ["size", "8"], [2, "margin-top", "0px"], ["animation", "gradient", 3, "data"], ["animation", "gradient", "lines", "3", 3, "data"], [3, "disabled", "click"]],
      template: function SimpleDataBindingPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Simple Data Binding ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Back to basics ...");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, " We can tweak our data stream with some RxJs operators and achieve an app shell loading interaction ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, " Let's set a timeout and fetch data using the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "HttpClient");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, " Data coming in ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "ion-row", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "ion-col", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "app-image-shell", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "app-aspect-ratio", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "h4", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Heading on top of a cover image");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "ion-col", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "app-aspect-ratio", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "app-image-shell", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "ion-col", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "h3", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "app-text-shell", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "app-text-shell", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "ion-button", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SimpleDataBindingPage_Template_ion_button_click_34_listener() {
            return ctx.showcaseShellSimpleFetch(3);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Fetch more Data");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.simpleFetchCountdown, " seconds");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover")("src", ctx.simpleFetchData == null ? null : ctx.simpleFetchData.cover)("alt", "Sample Image Cover");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.simpleFetchData == null ? null : ctx.simpleFetchData.image)("alt", "Sample Image");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.simpleFetchData == null ? null : ctx.simpleFetchData.title);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.simpleFetchData == null ? null : ctx.simpleFetchData.description);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx.simpleFetchButtonDisabled);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCol"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_5__["ImageShellComponent"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_6__["AspectRatioComponent"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_7__["TextShellComponent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButton"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL3NpbXBsZS1kYXRhLWJpbmRpbmcvc2ltcGxlLWRhdGEtYmluZGluZy5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9hcHAtc2hlbGwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLG1DQUFBO0FERUY7QUNBRTtFQUNFLHdDQUFBO0VBQ0Esa0NBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QURFSjtBQ0NFO0VBQ0Usc0JBQUE7QURDSjtBQ0VFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBREFKO0FDR0U7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QURESjtBQ0lBO0VBRUUsZUFBQTtBREZGO0FDT0E7RUFDRSxrQkFBQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBREpKO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLDZCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtBREhGO0FDS0E7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURGRjtBQ0lBO0VBQ0UsdUJBQUE7QURERjtBQ1VBO0VBQ0UsY0FKUztFQUtULG1CQUpPO0VBS1Asa0JBQUE7QURQRjtBQ1NBO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBRE5GO0FDUUE7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FETEY7QUNPQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QURKRjtBQ09BO0VBQ0UsYUFBQTtBREpGO0FDT0U7RUFDRSxPQUFBO0FETEo7QUNXQSxxQkFBQTtBQUNBO0VBRUUsZ0JBQUE7QURWRjtBQ2FBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBRFZGO0FDWUU7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBRUEsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSxTQUFBO0FEWEo7QUNlSTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QURiTjtBQ21CRTtFQUNFLGFBQUE7RUFFQSxjQXZFTztFQXdFUCxpQkFBQTtFQUNBLHFCQUFBO0FEbEJKO0FDb0JFO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBaEZPO0VBaUZQLGVBQUE7QURsQko7QUM2Qkk7RUFDRSx3QkFBQTtBRDNCTjtBQzhCRTtFQUNFLGlCQUFBO0FENUJKO0FDaUNBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBRDlCRiIsImZpbGUiOiJzcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9zaW1wbGUtZGF0YS1iaW5kaW5nL3NpbXBsZS1kYXRhLWJpbmRpbmcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogNTAwO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgLnNob3djYXNlLXNlY3Rpb24ge1xuICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgcHJlIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgY29kZSB7XG4gIGNvbG9yOiAjRjkyNjcyO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ubWVudV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uY2xzb2UtaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG4uaGVhcnRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5jb3B5X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubWFpbi10aXRsZSB7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uaWNvbl9jbG9zZSB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cblxuLmxpc3QtbWQge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuYm9keSB7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5oMiB7XG4gIG1hcmdpbjogMCAwIDAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuXG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG5cbi5yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnJvdyAuY29sIHtcbiAgZmxleDogMTtcbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4udGFiLWxhYmVsIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAxZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gIC8qIEljb24gKi9cbn1cbi50YWItbGFiZWw6OmFmdGVyIHtcbiAgY29udGVudDogXCLina9cIjtcbiAgd2lkdGg6IDFlbTtcbiAgaGVpZ2h0OiAxZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jb250ZW50IHtcbiAgbWF4LWhlaWdodDogMDtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXM7XG59XG4udGFiLWNsb3NlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXNpemU6IDAuNzVlbTtcbiAgYmFja2dyb3VuZDogIzJjM2U1MDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuaW5wdXQ6Y2hlY2tlZCArIC50YWItbGFiZWw6OmFmdGVyIHtcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xufVxuaW5wdXQ6Y2hlY2tlZCB+IC50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDEwMHZoO1xufVxuXG4uYmVkZ2UtYWxpZ24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59IiwiLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcblxuICBpb24taXRlbS1kaXZpZGVyIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIH1cblxuICAuc2hvd2Nhc2Utc2VjdGlvbiB7XG4gICAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbiAgfVxuXG4gIHByZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJhY2tncm91bmQ6ICNDQ0M7XG4gICAgcGFkZGluZzogMTBweDtcbiAgfVxuXG4gIGNvZGUge1xuICAgIGNvbG9yOiAjRjkyNjcyO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbn1cbi5tZW51X2ljb257XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogN3B4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY2xzb2UtaWNvbntcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgZm9udC1zaXplOiAzMHB4O1xufVxuLmhlYXJ0X2ljb257XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jb3B5X2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5tYWluLXRpdGxle1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5pY29uX2Nsb3Nle1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG4ubGlzdC1tZHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cblxuLy8gKioqKioqKioqKioqKioqKioqKioqKioqKiBBY2NvcmRpYW5cblxuJG1pZG5pZ2h0OiAjMmMzZTUwO1xuJGNsb3VkczogI2VjZjBmMTtcbi8vIEdlbmVyYWxcbmJvZHkge1xuICBjb2xvcjogJG1pZG5pZ2h0O1xuICBiYWNrZ3JvdW5kOiAkY2xvdWRzO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmgyIHtcbiAgbWFyZ2luOiAwIDAgLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbmlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiAtMTtcbn1cbi8vIExheW91dFxuLnJvdyB7XG4gIGRpc3BsYXk6ZmxleDtcbiAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7IFxuICAvLyBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIC5jb2wge1xuICAgIGZsZXg6MTtcbiAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDFlbTtcbiAgICB9XG4gIH1cbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgLy8gYm9yZGVyLXJhZGl1czogOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3gtc2hhZG93OiAwIDRweCA0cHggLTJweCByZ2JhKDAsMCwwLDAuNSk7XG59XG4udGFiIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICYtbGFiZWwge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICAgLyogSWNvbiAqL1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcXDI3NkZcIjtcbiAgICAgIHdpZHRoOiAxZW07XG4gICAgICBoZWlnaHQ6IDFlbTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICAgIH1cbiAgICAvLyAuYWN0aXZlOmFmdGVyIHtcbiAgICAvLyAgIGNvbnRlbnQ6IFwiXFwyNzk2XCI7IC8qIFVuaWNvZGUgY2hhcmFjdGVyIGZvciBcIm1pbnVzXCIgc2lnbiAoLSkgKi9cbiAgICAvLyB9XG4gIH1cbiAgJi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAwO1xuICAgIC8vIHBhZGRpbmc6IDAgMWVtO1xuICAgIGNvbG9yOiAkbWlkbmlnaHQ7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gIH1cbiAgJi1jbG9zZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICBmb250LXNpemU6IDAuNzVlbTtcbiAgICBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gIH1cbn1cblxuLy8gOmNoZWNrZWRcbmlucHV0OmNoZWNrZWQge1xuICArIC50YWItbGFiZWwge1xuICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgIH1cbiAgfVxuICB+IC50YWItY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMTAwdmg7XG4gICAgLy8gcGFkZGluZzogMWVtO1xuICB9XG59XG5cbi5iZWRnZS1hbGlnbntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4vLyAuYmVkZ2UtYWxpZ24tbXJwe1xuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgIHRvcDogNjVweDtcbi8vICAgcmlnaHQ6IDQ1cHhcbi8vIH1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SimpleDataBindingPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-simple-data-binding',
          templateUrl: './simple-data-binding.page.html',
          styleUrls: ['./simple-data-binding.page.scss']
        }]
      }], function () {
        return [{
          type: _showcase_service__WEBPACK_IMPORTED_MODULE_3__["ShowcaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/text-shell/text-shell.page.ts":
  /*!******************************************************************!*\
    !*** ./src/app/showcase/app-shell/text-shell/text-shell.page.ts ***!
    \******************************************************************/

  /*! exports provided: TextShellPage */

  /***/
  function srcAppShowcaseAppShellTextShellTextShellPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TextShellPage", function () {
      return TextShellPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../shell/text-shell/text-shell.component */
    "./src/app/shell/text-shell/text-shell.component.ts");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");
    /* harmony import */


    var _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../../shell/aspect-ratio/aspect-ratio.component */
    "./src/app/shell/aspect-ratio/aspect-ratio.component.ts");

    var _c0 = function _c0() {
      return {
        w: 2,
        h: 1
      };
    };

    var TextShellPage = /*#__PURE__*/function () {
      function TextShellPage() {
        _classCallCheck(this, TextShellPage);

        this.sampleTextShellData = '';
      }

      _createClass(TextShellPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return TextShellPage;
    }();

    TextShellPage.ɵfac = function TextShellPage_Factory(t) {
      return new (t || TextShellPage)();
    };

    TextShellPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: TextShellPage,
      selectors: [["app-text-shell-page"]],
      decls: 310,
      vars: 26,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "showcase/app-shell"], [1, "showcase-content"], [1, "showcase-section", 2, "margin-bottom", "40px"], [1, "loading-state-demo"], [1, "showcase-section", "sticky-section"], ["placeholder", "When this text is empty the text-shell will show the loading state.", "clearOnEdit", "true", 2, "--background", "#FFF", 3, "ngModel", "ngModelChange"], [1, "showcase-section"], [2, "background-color", "#FFF", "padding", "20px"], [3, "data"], ["lines", "3", 3, "data"], [2, "background-color", "#FFF", "padding", "20px", "color", "#FFF", "font-weight", "500"], ["animation", "spinner", 1, "add-overlay", 3, "display", "src"], [3, "ratio"], [2, "margin", "20px 40px"], ["animation", "bouncing", 3, "data"], ["animation", "bouncing", "lines", "3", 3, "data"], ["animation", "gradient", 3, "data"], ["animation", "gradient", "lines", "3", 3, "data"], ["lines", "6", 2, "--text-shell-line-color", "#DDD", 3, "data"], ["animation", "bouncing", "lines", "2", 2, "--text-shell-line-color", "#DDD", 3, "data"], [2, "padding", "5px", "background", "#FFF"], ["animation", "gradient", "lines", "2", 2, "--text-shell-line-color", "transparent", "--text-shell-background", "#FFF", 3, "data"], [2, "--text-shell-line-color", "rgba(233, 30, 99, .25)", 3, "data"], [2, "padding", "5px", "background", "#000000"], ["lines", "3", 2, "--text-shell-line-color", "#FFFFFF", "--text-shell-background", "#000000", 3, "data"], ["lines", "3", "animation", "gradient", 2, "--text-shell-line-color", "transparent", "--text-shell-background", "#000000", "--text-shell-animation-background", "rgba(255, 3, 109, 0.6)", "--text-shell-animation-color", "rgba(156, 4, 68, 0.7)", 3, "data"], [2, "--text-shell-line-height", "40px", 3, "data"], ["lines", "4", 2, "--text-shell-line-gutter", "10px", "--text-shell-line-color", "#CCC", 3, "data"], [1, "sample-text-max-width"], [1, "sample-text-min-width", 2, "display", "inline-block"]],
      template: function TextShellPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ion-title");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " Text Shell ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-content", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "section", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " This component is useful when you want to show a loading indicator while fetching text data from the server. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, " The ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "<app-text-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, " basically works by wrapping the text node with a loading indicator while you are fetching data. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " While there are empty values the component adds some 'loading' styles and animations. Whereas while there are non empty values, the loading state is removed. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, " Try toggling the loading state of the text shells below by changing the value of this textarea. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "section", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "ion-textarea", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function TextShellPage_Template_ion_textarea_ngModelChange_21_listener($event) {
            return ctx.sampleTextShellData = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, " You can reset the 'loading' state by clearing the textarea value. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "No animation (default)");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " Single line example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, " Multi-line example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "app-text-shell", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, " On top of another element (with transparent background) ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "app-image-shell", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "app-aspect-ratio", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "app-text-shell", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Bouncing animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, " Single line example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "app-text-shell", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, " Multi-line example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "app-text-shell", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, " On top of another element (with transparent background) ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "app-image-shell", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "app-aspect-ratio", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](61, "app-text-shell", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, " As we don\u2019t use masks, this approach works well with use cases that require transparent backgrounds. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "Gradient animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, " Single line example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "app-text-shell", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, " Multi-line example ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "app-text-shell", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, " On top of another element (with transparent background) ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "app-image-shell", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "app-aspect-ratio", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "p", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "app-text-shell", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Side effect:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, " This solution doesn\u2019t play well if you require the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "<app-text-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, " to have a transparent background as the masks need a solid color to work properly. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Prepend and Append text to the data binded");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, " There are cases where we need to add some text before or after the value binded to the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "<app-text-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, ". For example if we are loading a price value and we want to put the '$' before the price. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "  <app-text-shell [data]=\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "(sampleTextShellData) ? '$'.concat(' ', sampleTextShellData) : null");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "\"></app-text-shell>\n  ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, "<app-text-shell [data]=\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "(sampleTextShellData) ? sampleTextShellData.concat(' years old') : null");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "\"></app-text-shell>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Other use cases");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, " The ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "<app-text-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, " element can be used alone or wrapped with a text tag (");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, ", ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, ", ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, ", ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, ", etc). ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, " In case you wrap it with a heading element, we included basic styles (");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](137, "src/theme/shell-defaults.scss");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, ") to match the different heading styles (mainly line-height associated to each heading). ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, ":root {\n  h1 > app-text-shell {\n    --text-shell-line-height: 32px;\n    --text-shell-line-color: #CCC;\n  }\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](142, " By matching the line-height of the text-shell with the line-height of the wrapper element, we avoid page height bumps after the data gets loaded into the shells. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "<h1>\n  <app-text-shell [data]=\"\"></app-text-shell>\n</h1>\n\n<h2>\n  <app-text-shell [data]=\"\"></app-text-shell>\n</h2>\n\n<h3>\n  <app-text-shell [data]=\"\"></app-text-shell>\n</h3>\n\n<h4>\n  <app-text-shell [data]=\"\"></app-text-shell>\n</h4>\n\n<h5>\n  <app-text-shell [data]=\"\"></app-text-shell>\n</h5>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h1");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](146, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "h2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](148, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](150, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "h4");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](152, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](154, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, " You can add/remove the following ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](159, "Attributes");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160, " to adjust the shell element behavior ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, "Multi-line support");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, " You can set up-to 6 lines ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](166, "<p>\n  <app-text-shell ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, "lines=\"6\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, " [data]=\"\"></app-text-shell>\n</p>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](171, "app-text-shell", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, "Set different animation options");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](175, " You can choose between ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, "no animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](178, " (default), ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, "bouncing animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, ", and ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183, "gradient background with line masks on top");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "<p>\n  <app-text-shell ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "animation=\"bouncing\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](188, " lines=\"2\" [data]=\"\"></app-text-shell>\n</p>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](190, "app-text-shell", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](192, "<p>\n  <app-text-shell ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](194, "animation=\"gradient\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](195, " lines=\"2\" [data]=\"\"></app-text-shell>\n</p>\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](197, "app-text-shell {\n  --text-shell-background: #FFF;\n  --text-shell-line-color: transparent;\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](198, "p", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](199, "app-text-shell", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, " In this mode, the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](203, "--text-shell-background");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](204, " property refers to the masks colors. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](205, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](207, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](208, " When using a ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](209, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](210, "gradient");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](211, " animation, make sure you set the line-color to transparent so the background animation that's beneath the masks can be seen. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](212, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](213, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](214, " You can also override these ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](215, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](216, "CSS 4 variables");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](217, " to adjust the shell element style ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](218, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](219, "Line color");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](221, "app-text-shell {\n  --text-shell-line-color: rgba(233, 30, 99, .25);\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](222, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](223, "app-text-shell", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](225, "Background");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](226, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](227, " Background by default is set to ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](228, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](229, "transparent");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](230, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](231, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](232, "app-text-shell {\n  --text-shell-line-color: #FFFFFF;\n  --text-shell-background: #000000;\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](233, "p", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](234, "app-text-shell", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](236, "Background Animation");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](237, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](238, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](239, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](240, " These properties are only applied when using ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](241, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](242, "animation=\"gradient\"");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](243, ". ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](244, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](245, "app-text-shell {\n  --text-shell-line-color: transparent;\n  --text-shell-background: #000000;\n  --text-shell-animation-background: rgba(255, 3, 109, 0.6);\n  --text-shell-animation-color: rgba(156, 4, 68, 0.7);\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](246, "p", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](247, "app-text-shell", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](248, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](249, "Line height");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](250, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](251, "app-text-shell {\n  --text-shell-line-height: 40px;\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](252, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](253, "app-text-shell", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](254, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](255, "Line gutter");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](256, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](257, "app-text-shell {\n  --text-shell-line-gutter: 10px;\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](258, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](259, "app-text-shell", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](260, "section", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](261, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](262, "Max-width");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](263, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](264, " By default, the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](265, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](266, "<app-text-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](267, " element will fill it's container width. You can adjust this by setting a temporary ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](269, "max-width");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](270, " while the shell loading state is present. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](271, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](272, ".sample-text-max-width > app-text-shell {\n  ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](273, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](274, "max-width: 50%;");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](275, "\n\n  &.text-loaded {\n    ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](276, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](277, "max-width: unset;");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](278, "\n  }\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](279, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](280, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](281, "h5");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](282, "Min-width");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](283, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](284, " When the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](285, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](286, "<app-text-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](287, " parent container doesn't have a width defined, it may happen that the ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](288, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](289, "<app-text-shell>");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](290, " inherits an undefined width and thus it's not shown. ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](291, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](292, "In these cases setting a temporary ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](293, "code");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](294, "min-width");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](295, " fixes the issue.");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](296, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](297, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](298, "Note:");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](299, " If the parent doesn't have a defined width, it will fail to set a min-width based on a percentage of it's parents width. You will have to use absolute units (px, ch, etc). ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](300, "pre");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](301, ".sample-text-min-width > app-text-shell {\n  ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](302, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](303, "min-width: 80px;");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](304, "\n\n  &.text-loaded {\n    ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](305, "b");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](306, "min-width: 0px;");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](307, "\n  }\n}\n");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](308, "span", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](309, "app-text-shell", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover")("src", "./assets/sample-images/getting-started/category5-1.1.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](23, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover")("src", "./assets/sample-images/getting-started/category5-1.1.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](24, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("display", "cover")("src", "./assets/sample-images/getting-started/category5-1.1.png");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ratio", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](25, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData ? "$".concat(" ", ctx.sampleTextShellData) : null);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData ? ctx.sampleTextShellData.concat(" years old") : null);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](167);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.sampleTextShellData);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonTitle"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonTextarea"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["TextValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgModel"], _shell_text_shell_text_shell_component__WEBPACK_IMPORTED_MODULE_3__["TextShellComponent"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_4__["ImageShellComponent"], _shell_aspect_ratio_aspect_ratio_component__WEBPACK_IMPORTED_MODULE_5__["AspectRatioComponent"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n.loading-state-demo[_ngcontent-%COMP%] {\n  background-color: #ececec;\n  background-image: url(\"data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='40' height='40' viewBox='0 0 40 40'%3E%3Cg fill-rule='evenodd'%3E%3Cg fill='%23ffffff' fill-opacity='0.38'%3E%3Cpath d='M0 38.59l2.83-2.83 1.41 1.41L1.41 40H0v-1.41zM0 1.4l2.83 2.83 1.41-1.41L1.41 0H0v1.41zM38.59 40l-2.83-2.83 1.41-1.41L40 38.59V40h-1.41zM40 1.41l-2.83 2.83-1.41-1.41L38.59 0H40v1.41zM20 18.6l2.83-2.83 1.41 1.41L21.41 20l2.83 2.83-1.41 1.41L20 21.41l-2.83 2.83-1.41-1.41L18.59 20l-2.83-2.83 1.41-1.41L20 18.59z'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E\");\n  padding: 0px 0px 20px;\n}\n.sticky-section[_ngcontent-%COMP%] {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0px;\n  background: #ececec;\n  padding: 20px;\n  margin: 0px 0px 60px !important;\n  z-index: 10;\n}\n.sample-text-max-width[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  max-width: 50%;\n}\n.sample-text-max-width[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  max-width: unset;\n}\n.sample-text-min-width[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  min-width: 80px;\n}\n.sample-text-min-width[_ngcontent-%COMP%]    > app-text-shell.text-loaded[_ngcontent-%COMP%] {\n  min-width: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL3RleHQtc2hlbGwvdGV4dC1zaGVsbC5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9hcHAtc2hlbGwucGFnZS5zY3NzIiwiL2hvbWUvb2VtL2Rldi93b3Jrc3BhY2UvQmx1ZVN0YXJJa3N1bGEvc3JjL2FwcC9zaG93Y2FzZS9hcHAtc2hlbGwvdGV4dC1zaGVsbC90ZXh0LXNoZWxsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnQkFBZ0I7QUNBaEI7RUFDRSxtQ0FBQTtBREVGO0FDQUU7RUFDRSx3Q0FBQTtFQUNBLGtDQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FERUo7QUNDRTtFQUNFLHNCQUFBO0FEQ0o7QUNFRTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7QURBSjtBQ0dFO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0FEREo7QUNJQTtFQUVFLGVBQUE7QURGRjtBQ09BO0VBQ0Usa0JBQUE7RUFDRSxtQkFBQTtFQUNBLGVBQUE7QURKSjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBREhGO0FDTUE7RUFDRSw2QkFBQTtFQUNBLDRCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QURIRjtBQ0tBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FERkY7QUNJQTtFQUNFLHVCQUFBO0FEREY7QUNVQTtFQUNFLGNBSlM7RUFLVCxtQkFKTztFQUtQLGtCQUFBO0FEUEY7QUNTQTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURORjtBQ1FBO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtBRExGO0FDT0E7RUFDRSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FESkY7QUNPQTtFQUNFLGFBQUE7QURKRjtBQ09FO0VBQ0UsT0FBQTtBRExKO0FDV0EscUJBQUE7QUFDQTtFQUVFLGdCQUFBO0FEVkY7QUNhQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QURWRjtBQ1lFO0VBQ0UsYUFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUVBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLDZCQUFBO0VBQ0EsU0FBQTtBRFhKO0FDZUk7RUFDRSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0FEYk47QUNtQkU7RUFDRSxhQUFBO0VBRUEsY0F2RU87RUF3RVAsaUJBQUE7RUFDQSxxQkFBQTtBRGxCSjtBQ29CRTtFQUNFLGFBQUE7RUFDQSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQWhGTztFQWlGUCxlQUFBO0FEbEJKO0FDNkJJO0VBQ0Usd0JBQUE7QUQzQk47QUM4QkU7RUFDRSxpQkFBQTtBRDVCSjtBQ2lDQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUQ5QkY7QUU5SUE7RUFDRSx5QkFBQTtFQUNBLHdpQkFBQTtFQUNBLHFCQUFBO0FGaUpGO0FFOUlBO0VBQ0Usd0JBQUE7RUFBQSxnQkFBQTtFQUNBLFFBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSwrQkFBQTtFQUNBLFdBQUE7QUZpSkY7QUU5SUE7RUFDRSxjQUFBO0FGaUpGO0FFL0lFO0VBQ0UsZ0JBQUE7QUZpSko7QUU3SUE7RUFDRSxlQUFBO0FGZ0pGO0FFOUlFO0VBQ0UsY0FBQTtBRmdKSiIsImZpbGUiOiJzcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC90ZXh0LXNoZWxsL3RleHQtc2hlbGwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogNTAwO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgLnNob3djYXNlLXNlY3Rpb24ge1xuICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgcHJlIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgY29kZSB7XG4gIGNvbG9yOiAjRjkyNjcyO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ubWVudV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uY2xzb2UtaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG4uaGVhcnRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5jb3B5X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubWFpbi10aXRsZSB7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uaWNvbl9jbG9zZSB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cblxuLmxpc3QtbWQge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuYm9keSB7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5oMiB7XG4gIG1hcmdpbjogMCAwIDAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuXG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG5cbi5yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnJvdyAuY29sIHtcbiAgZmxleDogMTtcbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4udGFiLWxhYmVsIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAxZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gIC8qIEljb24gKi9cbn1cbi50YWItbGFiZWw6OmFmdGVyIHtcbiAgY29udGVudDogXCLina9cIjtcbiAgd2lkdGg6IDFlbTtcbiAgaGVpZ2h0OiAxZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jb250ZW50IHtcbiAgbWF4LWhlaWdodDogMDtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXM7XG59XG4udGFiLWNsb3NlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXNpemU6IDAuNzVlbTtcbiAgYmFja2dyb3VuZDogIzJjM2U1MDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuaW5wdXQ6Y2hlY2tlZCArIC50YWItbGFiZWw6OmFmdGVyIHtcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xufVxuaW5wdXQ6Y2hlY2tlZCB+IC50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDEwMHZoO1xufVxuXG4uYmVkZ2UtYWxpZ24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59XG5cbi5sb2FkaW5nLXN0YXRlLWRlbW8ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWNlY2VjO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNDc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzQwJyBoZWlnaHQ9JzQwJyB2aWV3Qm94PScwIDAgNDAgNDAnJTNFJTNDZyBmaWxsLXJ1bGU9J2V2ZW5vZGQnJTNFJTNDZyBmaWxsPSclMjNmZmZmZmYnIGZpbGwtb3BhY2l0eT0nMC4zOCclM0UlM0NwYXRoIGQ9J00wIDM4LjU5bDIuODMtMi44MyAxLjQxIDEuNDFMMS40MSA0MEgwdi0xLjQxek0wIDEuNGwyLjgzIDIuODMgMS40MS0xLjQxTDEuNDEgMEgwdjEuNDF6TTM4LjU5IDQwbC0yLjgzLTIuODMgMS40MS0xLjQxTDQwIDM4LjU5VjQwaC0xLjQxek00MCAxLjQxbC0yLjgzIDIuODMtMS40MS0xLjQxTDM4LjU5IDBINDB2MS40MXpNMjAgMTguNmwyLjgzLTIuODMgMS40MSAxLjQxTDIxLjQxIDIwbDIuODMgMi44My0xLjQxIDEuNDFMMjAgMjEuNDFsLTIuODMgMi44My0xLjQxLTEuNDFMMTguNTkgMjBsLTIuODMtMi44MyAxLjQxLTEuNDFMMjAgMTguNTl6Jy8lM0UlM0MvZyUzRSUzQy9nJTNFJTNDL3N2ZyUzRVwiKTtcbiAgcGFkZGluZzogMHB4IDBweCAyMHB4O1xufVxuXG4uc3RpY2t5LXNlY3Rpb24ge1xuICBwb3NpdGlvbjogc3RpY2t5O1xuICB0b3A6IDBweDtcbiAgYmFja2dyb3VuZDogI2VjZWNlYztcbiAgcGFkZGluZzogMjBweDtcbiAgbWFyZ2luOiAwcHggMHB4IDYwcHggIWltcG9ydGFudDtcbiAgei1pbmRleDogMTA7XG59XG5cbi5zYW1wbGUtdGV4dC1tYXgtd2lkdGggPiBhcHAtdGV4dC1zaGVsbCB7XG4gIG1heC13aWR0aDogNTAlO1xufVxuLnNhbXBsZS10ZXh0LW1heC13aWR0aCA+IGFwcC10ZXh0LXNoZWxsLnRleHQtbG9hZGVkIHtcbiAgbWF4LXdpZHRoOiB1bnNldDtcbn1cblxuLnNhbXBsZS10ZXh0LW1pbi13aWR0aCA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgbWluLXdpZHRoOiA4MHB4O1xufVxuLnNhbXBsZS10ZXh0LW1pbi13aWR0aCA+IGFwcC10ZXh0LXNoZWxsLnRleHQtbG9hZGVkIHtcbiAgbWluLXdpZHRoOiAwcHg7XG59IiwiLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcblxuICBpb24taXRlbS1kaXZpZGVyIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIH1cblxuICAuc2hvd2Nhc2Utc2VjdGlvbiB7XG4gICAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbiAgfVxuXG4gIHByZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJhY2tncm91bmQ6ICNDQ0M7XG4gICAgcGFkZGluZzogMTBweDtcbiAgfVxuXG4gIGNvZGUge1xuICAgIGNvbG9yOiAjRjkyNjcyO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbn1cbi5tZW51X2ljb257XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogN3B4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY2xzb2UtaWNvbntcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgZm9udC1zaXplOiAzMHB4O1xufVxuLmhlYXJ0X2ljb257XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jb3B5X2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5tYWluLXRpdGxle1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5pY29uX2Nsb3Nle1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG4ubGlzdC1tZHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cblxuLy8gKioqKioqKioqKioqKioqKioqKioqKioqKiBBY2NvcmRpYW5cblxuJG1pZG5pZ2h0OiAjMmMzZTUwO1xuJGNsb3VkczogI2VjZjBmMTtcbi8vIEdlbmVyYWxcbmJvZHkge1xuICBjb2xvcjogJG1pZG5pZ2h0O1xuICBiYWNrZ3JvdW5kOiAkY2xvdWRzO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmgyIHtcbiAgbWFyZ2luOiAwIDAgLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbmlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiAtMTtcbn1cbi8vIExheW91dFxuLnJvdyB7XG4gIGRpc3BsYXk6ZmxleDtcbiAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7IFxuICAvLyBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIC5jb2wge1xuICAgIGZsZXg6MTtcbiAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDFlbTtcbiAgICB9XG4gIH1cbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgLy8gYm9yZGVyLXJhZGl1czogOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3gtc2hhZG93OiAwIDRweCA0cHggLTJweCByZ2JhKDAsMCwwLDAuNSk7XG59XG4udGFiIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICYtbGFiZWwge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICAgLyogSWNvbiAqL1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcXDI3NkZcIjtcbiAgICAgIHdpZHRoOiAxZW07XG4gICAgICBoZWlnaHQ6IDFlbTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICAgIH1cbiAgICAvLyAuYWN0aXZlOmFmdGVyIHtcbiAgICAvLyAgIGNvbnRlbnQ6IFwiXFwyNzk2XCI7IC8qIFVuaWNvZGUgY2hhcmFjdGVyIGZvciBcIm1pbnVzXCIgc2lnbiAoLSkgKi9cbiAgICAvLyB9XG4gIH1cbiAgJi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAwO1xuICAgIC8vIHBhZGRpbmc6IDAgMWVtO1xuICAgIGNvbG9yOiAkbWlkbmlnaHQ7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gIH1cbiAgJi1jbG9zZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICBmb250LXNpemU6IDAuNzVlbTtcbiAgICBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gIH1cbn1cblxuLy8gOmNoZWNrZWRcbmlucHV0OmNoZWNrZWQge1xuICArIC50YWItbGFiZWwge1xuICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgIH1cbiAgfVxuICB+IC50YWItY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMTAwdmg7XG4gICAgLy8gcGFkZGluZzogMWVtO1xuICB9XG59XG5cbi5iZWRnZS1hbGlnbntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4vLyAuYmVkZ2UtYWxpZ24tbXJwe1xuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgIHRvcDogNjVweDtcbi8vICAgcmlnaHQ6IDQ1cHhcbi8vIH1cbiIsIkBpbXBvcnQgXCIuLi9hcHAtc2hlbGwucGFnZVwiO1xuXG4ubG9hZGluZy1zdGF0ZS1kZW1vIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VjZWNlYztcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiZGF0YTppbWFnZS9zdmcreG1sLCUzQ3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSc0MCcgaGVpZ2h0PSc0MCcgdmlld0JveD0nMCAwIDQwIDQwJyUzRSUzQ2cgZmlsbC1ydWxlPSdldmVub2RkJyUzRSUzQ2cgZmlsbD0nJTIzZmZmZmZmJyBmaWxsLW9wYWNpdHk9JzAuMzgnJTNFJTNDcGF0aCBkPSdNMCAzOC41OWwyLjgzLTIuODMgMS40MSAxLjQxTDEuNDEgNDBIMHYtMS40MXpNMCAxLjRsMi44MyAyLjgzIDEuNDEtMS40MUwxLjQxIDBIMHYxLjQxek0zOC41OSA0MGwtMi44My0yLjgzIDEuNDEtMS40MUw0MCAzOC41OVY0MGgtMS40MXpNNDAgMS40MWwtMi44MyAyLjgzLTEuNDEtMS40MUwzOC41OSAwSDQwdjEuNDF6TTIwIDE4LjZsMi44My0yLjgzIDEuNDEgMS40MUwyMS40MSAyMGwyLjgzIDIuODMtMS40MSAxLjQxTDIwIDIxLjQxbC0yLjgzIDIuODMtMS40MS0xLjQxTDE4LjU5IDIwbC0yLjgzLTIuODMgMS40MS0xLjQxTDIwIDE4LjU5eicvJTNFJTNDL2clM0UlM0MvZyUzRSUzQy9zdmclM0VcIik7XG4gIHBhZGRpbmc6IDBweCAwcHggMjBweDtcbn1cblxuLnN0aWNreS1zZWN0aW9uIHtcbiAgcG9zaXRpb246IHN0aWNreTtcbiAgdG9wOiAwcHg7XG4gIGJhY2tncm91bmQ6ICNlY2VjZWM7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIG1hcmdpbjogMHB4IDBweCA2MHB4ICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDEwO1xufVxuXG4uc2FtcGxlLXRleHQtbWF4LXdpZHRoID4gYXBwLXRleHQtc2hlbGwge1xuICBtYXgtd2lkdGg6IDUwJTtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtYXgtd2lkdGg6IHVuc2V0O1xuICB9XG59XG5cbi5zYW1wbGUtdGV4dC1taW4td2lkdGggPiBhcHAtdGV4dC1zaGVsbCB7XG4gIG1pbi13aWR0aDogODBweDtcblxuICAmLnRleHQtbG9hZGVkIHtcbiAgICBtaW4td2lkdGg6IDBweDtcbiAgfVxufVxuIl19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TextShellPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-text-shell-page',
          templateUrl: './text-shell.page.html',
          styleUrls: ['./text-shell.page.scss']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=app-shell-app-shell-module-es5.js.map