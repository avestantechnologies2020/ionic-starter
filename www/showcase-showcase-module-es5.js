function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["showcase-showcase-module"], {
  /***/
  "./src/app/showcase/showcase.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/showcase/showcase.module.ts ***!
    \*********************************************/

  /*! exports provided: ShowcasePageModule */

  /***/
  function srcAppShowcaseShowcaseModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShowcasePageModule", function () {
      return ShowcasePageModule;
    });
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");

    var showcaseRoutes = [{
      path: 'app-shell',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | app-shell-app-shell-module */
        [__webpack_require__.e("default~app-shell-app-shell-module~product-listing-product-listing-module"), __webpack_require__.e("common"), __webpack_require__.e("app-shell-app-shell-module")]).then(__webpack_require__.bind(null,
        /*! ./app-shell/app-shell.module */
        "./src/app/showcase/app-shell/app-shell.module.ts")).then(function (m) {
          return m.AppShellModule;
        });
      }
    }, {
      path: 'custom-components',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | custom-components-custom-components-module */
        [__webpack_require__.e("common"), __webpack_require__.e("custom-components-custom-components-module")]).then(__webpack_require__.bind(null,
        /*! ./custom-components/custom-components.module */
        "./src/app/showcase/custom-components/custom-components.module.ts")).then(function (m) {
          return m.CustomComponentsModule;
        });
      }
    }, {
      path: 'route-resolvers-ux',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | route-resolvers-ux-route-resolvers-ux-module */
        [__webpack_require__.e("common"), __webpack_require__.e("route-resolvers-ux-route-resolvers-ux-module")]).then(__webpack_require__.bind(null,
        /*! ./route-resolvers-ux/route-resolvers-ux.module */
        "./src/app/showcase/route-resolvers-ux/route-resolvers-ux.module.ts")).then(function (m) {
          return m.RouteResolversUXModule;
        });
      }
    }];

    var ShowcasePageModule = function ShowcasePageModule() {
      _classCallCheck(this, ShowcasePageModule);
    };

    ShowcasePageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
      type: ShowcasePageModule
    });
    ShowcasePageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
      factory: function ShowcasePageModule_Factory(t) {
        return new (t || ShowcasePageModule)();
      },
      imports: [[_ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(showcaseRoutes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](ShowcasePageModule, {
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](ShowcasePageModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"],
        args: [{
          imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_0__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(showcaseRoutes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"]],
          declarations: []
        }]
      }], null, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=showcase-showcase-module-es5.js.map