function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~app-shell-app-shell-module~product-listing-product-listing-module"], {
  /***/
  "./src/app/product/details/product-details.model.ts":
  /*!**********************************************************!*\
    !*** ./src/app/product/details/product-details.model.ts ***!
    \**********************************************************/

  /*! exports provided: ProductDetailsModel */

  /***/
  function srcAppProductDetailsProductDetailsModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductDetailsModel", function () {
      return ProductDetailsModel;
    });
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ../../shell/data-store */
    "./src/app/shell/data-store.ts");

    var ProductDetailsModel = /*#__PURE__*/function (_shell_data_store__WE) {
      _inherits(ProductDetailsModel, _shell_data_store__WE);

      var _super = _createSuper(ProductDetailsModel);

      function ProductDetailsModel() {
        var _this2;

        _classCallCheck(this, ProductDetailsModel);

        _this2 = _super.call(this);
        _this2.showcaseImages = [{
          type: '',
          source: ''
        }, {
          type: '',
          source: ''
        }, {
          type: '',
          source: ''
        }];
        _this2.colorVariants = [{
          name: '',
          value: '',
          "default": false
        }, {
          name: '',
          value: '',
          "default": false
        }, {
          name: '',
          value: '',
          "default": false
        }];
        _this2.sizeVariants = [{
          name: '',
          value: '',
          "default": false
        }, {
          name: '',
          value: '',
          "default": false
        }, {
          name: '',
          value: '',
          "default": false
        }];
        _this2.relatedProducts = [{
          id: null
        }, {
          id: null
        }];
        return _this2;
      }

      return ProductDetailsModel;
    }(_shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"]);
    /***/

  },

  /***/
  "./src/app/product/listing/product-listing.model.ts":
  /*!**********************************************************!*\
    !*** ./src/app/product/listing/product-listing.model.ts ***!
    \**********************************************************/

  /*! exports provided: ProductItemModel, ProductListingModel */

  /***/
  function srcAppProductListingProductListingModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductItemModel", function () {
      return ProductItemModel;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductListingModel", function () {
      return ProductListingModel;
    });
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ../../shell/data-store */
    "./src/app/shell/data-store.ts");

    var ProductItemModel = function ProductItemModel() {
      _classCallCheck(this, ProductItemModel);
    };

    var ProductListingModel = /*#__PURE__*/function (_shell_data_store__WE2) {
      _inherits(ProductListingModel, _shell_data_store__WE2);

      var _super2 = _createSuper(ProductListingModel);

      function ProductListingModel() {
        var _this3;

        _classCallCheck(this, ProductListingModel);

        _this3 = _super2.call(this);
        _this3.items = [new ProductItemModel(), new ProductItemModel(), new ProductItemModel(), new ProductItemModel()];
        return _this3;
      }

      return ProductListingModel;
    }(_shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"]);
    /***/

  },

  /***/
  "./src/app/product/listing/product-listing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/product/listing/product-listing.module.ts ***!
    \***********************************************************/

  /*! exports provided: ProductListingPageModule */

  /***/
  function srcAppProductListingProductListingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductListingPageModule", function () {
      return ProductListingPageModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../components/components.module */
    "./src/app/components/components.module.ts");
    /* harmony import */


    var _product_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../product.service */
    "./src/app/product/product.service.ts");
    /* harmony import */


    var _product_listing_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./product-listing.page */
    "./src/app/product/listing/product-listing.page.ts");
    /* harmony import */


    var _product_listing_resolver__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./product-listing.resolver */
    "./src/app/product/listing/product-listing.resolver.ts");

    var routes = [{
      path: '',
      component: _product_listing_page__WEBPACK_IMPORTED_MODULE_8__["ProductListingPage"],
      resolve: {
        data: _product_listing_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductListingResolver"]
      }
    }];

    var ProductListingPageModule = function ProductListingPageModule() {
      _classCallCheck(this, ProductListingPageModule);
    };

    ProductListingPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: ProductListingPageModule
    });
    ProductListingPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function ProductListingPageModule_Factory(t) {
        return new (t || ProductListingPageModule)();
      },
      providers: [_product_listing_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductListingResolver"], _product_service__WEBPACK_IMPORTED_MODULE_7__["ProductService"]],
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ProductListingPageModule, {
        declarations: [_product_listing_page__WEBPACK_IMPORTED_MODULE_8__["ProductListingPage"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductListingPageModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]],
          declarations: [_product_listing_page__WEBPACK_IMPORTED_MODULE_8__["ProductListingPage"]],
          providers: [_product_listing_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductListingResolver"], _product_service__WEBPACK_IMPORTED_MODULE_7__["ProductService"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/product/listing/product-listing.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/product/listing/product-listing.page.ts ***!
    \*********************************************************/

  /*! exports provided: ProductListingPage */

  /***/
  function srcAppProductListingProductListingPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductListingPage", function () {
      return ProductListingPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _utils_resolver_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../utils/resolver-helper */
    "./src/app/utils/resolver-helper.ts");
    /* harmony import */


    var _product_listing_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./product-listing.model */
    "./src/app/product/listing/product-listing.model.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _showcase_app_shell_app_shell_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../showcase/app-shell/app-shell.page */
    "./src/app/showcase/app-shell/app-shell.page.ts");
    /* harmony import */


    var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/sqlite/ngx */
    "./node_modules/@ionic-native/sqlite/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _categories_categories_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./../../categories/categories.page */
    "./src/app/categories/categories.page.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./../../app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _share_popover_share_popover__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../share-popover/share-popover */
    "./src/app/share-popover/share-popover.ts");
    /* harmony import */


    var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @ionic-native/downloader/ngx */
    "./node_modules/@ionic-native/downloader/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @ionic-native/firebase-analytics/ngx */
    "./node_modules/@ionic-native/firebase-analytics/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _popup_compare_popup_compare__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ../../popup-compare/popup-compare */
    "./src/app/popup-compare/popup-compare.ts");
    /* harmony import */


    var _popup_compare_same_category_popup_compare_same_category__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ../../popup-compare-same-category/popup-compare-same-category */
    "./src/app/popup-compare-same-category/popup-compare-same-category.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ../../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");

    function ProductListingPage_ion_badge_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-badge", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx_r0.countNotification);
      }
    }

    function ProductListingPage_span_25_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Total Products: ", ctx_r1.total_product, "");
      }
    }

    function ProductListingPage_div_28_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "div", 25);
      }
    }

    function ProductListingPage_div_32_app_image_shell_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "app-image-shell", 43);
      }

      if (rf & 2) {
        var item_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("src", ctx_r7.defolutImageFlage ? "assets/default_product_image" : item_r6.Image)("alt", "product image");
      }
    }

    function ProductListingPage_div_32_img_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "img", 44);
      }
    }

    function ProductListingPage_div_32_div_6_div_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_32_div_6_div_1_Template_div_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r18);

          var item_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

          var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r16.getProductaddToCompare(item_r6);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Compare ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function ProductListingPage_div_32_div_6_div_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r21 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_32_div_6_div_2_Template_div_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r21);

          var item_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;

          var ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r19.getProductaddToCompare(item_r6);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "ion-icon", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Added ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function ProductListingPage_div_32_div_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ProductListingPage_div_32_div_6_div_1_Template, 2, 0, "div", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ProductListingPage_div_32_div_6_div_2_Template, 3, 0, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r6.showCompareFlage == false);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r6.showCompareFlage == true);
      }
    }

    function ProductListingPage_div_32_p_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" MRP: \u20B9", ctx_r10.formatNumber(item_r6), "");
      }
    }

    function ProductListingPage_div_32_ion_icon_20_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 51);
      }
    }

    function ProductListingPage_div_32_ion_icon_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 52);
      }
    }

    function ProductListingPage_div_32_Template(rf, ctx) {
      if (rf & 1) {
        var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "a", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, ProductListingPage_div_32_app_image_shell_4_Template, 1, 2, "app-image-shell", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, ProductListingPage_div_32_img_5_Template, 1, 0, "img", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ProductListingPage_div_32_div_6_Template, 3, 2, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_32_Template_div_click_8_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r25);

          var item_r6 = ctx.$implicit;

          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r24.clickOnProduct(item_r6.ID, ctx_r24.title, item_r6.SKUCode, item_r6.ProductTitle, item_r6.Image);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, ProductListingPage_div_32_p_13_Template, 2, 1, "p", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "ion-row", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "ion-col", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_32_Template_ion_col_click_15_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r25);

          var item_r6 = ctx.$implicit;

          var ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r26.downloadPdf(item_r6);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "ion-icon", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, "Download");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "ion-col", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_32_Template_ion_col_click_19_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r25);

          var item_r6 = ctx.$implicit;

          var ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r27.clickOnFavourites(item_r6);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, ProductListingPage_div_32_ion_icon_20_Template, 1, 0, "ion-icon", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, ProductListingPage_div_32_ion_icon_21_Template, 1, 0, "ion-icon", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "My Products");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "ion-col", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_32_Template_ion_col_click_24_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r25);

          var item_r6 = ctx.$implicit;

          var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r28.getPdfLink(item_r6.ID, item_r6);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](25, "img", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27, "Share");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r6 = ctx.$implicit;

        var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.defolutImageFlage == false);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.defolutImageFlage == true);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r3.compareFlage);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r6.SKUCode);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r6.ProductTitle);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r6.MRP != null && item_r6.MRP != "" || item_r6.Price != null && item_r6.Price != "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r6.favouritesFlage == false);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r6.favouritesFlage == true);
      }
    }

    function ProductListingPage_div_33_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "No product found");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function ProductListingPage_div_36_div_1_ion_col_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r33 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-col", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-icon", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_36_div_1_ion_col_2_Template_ion_icon_click_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r33);

          var item_r31 = ctx.$implicit;

          var ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3);

          return ctx_r32.removeProductFromCompareList(item_r31.ID);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "img", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r31 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("src", item_r31.Image, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeUrl"]);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r31.SKUCode);
      }
    }

    function ProductListingPage_div_36_div_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r35 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-row", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ProductListingPage_div_36_div_1_ion_col_2_Template, 6, 2, "ion-col", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "ion-row", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "ion-col", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_div_36_div_1_Template_ion_col_click_4_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r35);

          var ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

          return ctx_r34.onClickCompareNow();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "P", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Compare Now");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r29.popupCompareList);
      }
    }

    function ProductListingPage_div_36_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ProductListingPage_div_36_div_1_Template, 7, 1, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r5.popupCompareList.length != 0);
      }
    }

    var _c0 = function _c0() {
      return ["/categories"];
    };

    var ProductListingPage = /*#__PURE__*/function () {
      function ProductListingPage(http, route, router, loadingController, modalCtrl, platform, popoverCtrl, sqlite, downloader, storage, toastCtrl, socialSharing, firebaseAnalytics, inAppBrowser, myapp, alertController, categoriesPage) {
        var _this4 = this;

        _classCallCheck(this, ProductListingPage);

        this.http = http;
        this.route = route;
        this.router = router;
        this.loadingController = loadingController;
        this.modalCtrl = modalCtrl;
        this.platform = platform;
        this.popoverCtrl = popoverCtrl;
        this.sqlite = sqlite;
        this.downloader = downloader;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.socialSharing = socialSharing;
        this.firebaseAnalytics = firebaseAnalytics;
        this.inAppBrowser = inAppBrowser;
        this.myapp = myapp;
        this.alertController = alertController;
        this.categoriesPage = categoriesPage;
        this.data = [];
        this.title = '';
        this.product_listing = [];
        this.all_product_listing = [];
        this.page = 1;
        this.pagenationProductCount = 20;
        this.airPurifierCoverageAreaSelected = [];
        this.airPurifierCADRSelected = [];
        this.airPurifierPriceSelected = []; //airConditioner filter selected option array

        this.airConditionerSubCategorySelected = [];
        this.airConditionerCapacitySelected = [];
        this.airConditionerSeriesSelected = [];
        this.airConditionerStarRatingSelected = [];
        this.airConditionerPriseSelected = []; //waterPurifie filter selected option array

        this.waterPurifierTechnologySelected = [];
        this.waterPurifierModelNameSelected = [];
        this.waterPurifierCapacitySelected = [];
        this.waterPurifierPriseSelected = []; //airCooler filter selected option array

        this.airCoolerTypeSelected = [];
        this.airCoolerPriceSelected = [];
        this.airCoolerCapacitySelected = [];
        this.database_name = "bluestar_dealer.db";
        this.table_air_conditioner = "air_conditioner";
        this.table_air_cooler = "air_cooler";
        this.table_air_purifier = "air_purifier";
        this.table_water_purifier = "water_purifier";
        this.emptyListFlage = true;
        this.defolutImageFlage = false;
        this.airConditionerFlage = false;
        this.airCoolerFlage = false;
        this.airPurifierFlage = false;
        this.waterPurifierFlage = false;
        this.currentTab = "All";
        this.filterApply = false;
        this.flagShowFilter = false; //Compare 

        this.compareFlage = false; // addedFlage = false;

        this.compareCount = 0;
        this.compareList = [];
        this.btnCompareText = 'Compare';
        this.productDetails = [];
        this.uspList = [];
        this.images = [];
        this.item = {
          title: "",
          ID: "",
          Image: "",
          ProductTitle: "",
          SKUCode: "",
          MRP: "",
          productSpecifications: [],
          uspList: []
        };
        this.showBottomPopup = false;
        this.popupCompareList = [];
        this.notification_listing = [];
        this.openCompareFunction = false;
        this.route.queryParams.subscribe(function (params) {
          console.log("params", params);
          _this4.openCompareFunction = params.openCompareFunction;

          if (params && params.title) {
            _this4.title = params.title;

            if (_this4.filterApply == false) {
              _this4.clearFilter();

              _this4.clickOnCategary(_this4.title);

              _this4.compareFlage = false;
              _this4.compareCount = 0;
              _this4.showBottomPopup = false;
            }

            if (_this4.openCompareFunction) {
              _this4.compareFlage = true;
              _this4.compareCount = 0;
              _this4.showBottomPopup = true;

              _this4.storage.get('compareList').then(function (val) {
                _this4.popupCompareList = val;
                console.log("this.popupCompareList", _this4.popupCompareList);
              });
            }
          }
        });
      }

      _createClass(ProductListingPage, [{
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          var _this5 = this;

          console.log("Enter ionViewWillEnter");
          this.compareFlage = false;
          this.compareCount = 0;
          this.showBottomPopup = false;

          if (this.openCompareFunction) {
            this.compareFlage = true;
            this.compareCount = 0;
            this.showBottomPopup = true;
            this.storage.get('compareList').then(function (val) {
              _this5.popupCompareList = val;
              console.log("this.popupCompareList", _this5.popupCompareList);
            });
          }

          this.clearFilter();
          this.refreshId = setInterval(function () {
            _this5.getCountNotification();
          }, 2000);
        }
      }, {
        key: "ionViewDidLeave",
        value: function ionViewDidLeave() {
          //Stop refresh
          clearInterval(this.refreshId);
        }
      }, {
        key: "getCountNotification",
        value: function getCountNotification() {
          var _this6 = this;

          // this.loadingController.create({
          //   message: 'Please wait',
          // }).then((res) => {
          //   res.present();
          if (navigator.onLine) {
            this.http.get(this.categoriesPage.apiBaseUrl + '/bluestar_api/notification_list').subscribe(function (response) {
              Object.keys(response).map(function (key) {
                _this6.notification_listing = response[key].notification_listing; // console.log("notification_listing", this.notification_listing);

                var data = [];

                _this6.storage.get('notificationList').then(function (val) {
                  // console.log("local storage notificationList", val);
                  if (val == null) {
                    _this6.storage.set('notificationList', _this6.notification_listing);

                    _this6.countNotification = _this6.notification_listing.length; // console.log("countNotification", this.countNotification);
                  } else {
                    for (var i in _this6.notification_listing) {
                      data = val;
                      var count = 0;

                      for (var index in data) {
                        if (data[index].ID == _this6.notification_listing[i].ID) {
                          count = 1;
                        }
                      }

                      if (count == 0) {
                        data.push(_this6.notification_listing[i]);

                        _this6.storage.set('notificationList', data);
                      }
                    }

                    setTimeout(function () {
                      _this6.storage.get('notificationList').then(function (val) {
                        data = val;
                        var count = 0;

                        for (var _index in data) {
                          if (data[_index].status) {
                            if (data[_index].status != 'read') {
                              count++;
                            }
                          } else {
                            count++;
                          }
                        }

                        _this6.countNotification = count;

                        _this6.removeNotification();

                        console.log("countNotification****", _this6.countNotification);
                      });
                    }, 500);
                  }
                }); // res.dismiss();

              });
            }, function (err) {
              // res.dismiss();
              console.log("err.........", JSON.stringify(err));
            });
          } else {
            // res.dismiss();
            console.log("no internat connection");
          } // });
          // let data = []
          // this.storage.get('notificationList').then((val) => {
          //   data = val;
          //   let count = 0;
          //   for (let index in data) {
          //     if (data[index].status) {
          //        if(data[index].status != 'read'){
          //           count++;
          //        }
          //     }else{
          //       count++;
          //     }
          //   }
          //   this.countNotification = count;
          //   console.log("countNotification****", this.countNotification);
          // })

        }
      }, {
        key: "removeNotification",
        value: function removeNotification() {
          var _this7 = this;

          // console.log("Eneter removeNotification");
          var data = [];
          var position = [];
          var newArr = [];
          this.storage.get('notificationList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;

                for (var index in data) {
                  // console.log("index", index);
                  var count = 0;
                  var removeItemIndex = 0;

                  for (var i in _this7.notification_listing) {
                    // console.log("*****", data[index]);
                    // console.log("notification_listing", this.notification_listing[i]);
                    if (data[index].ID == _this7.notification_listing[i].ID) {
                      count = 1;
                      removeItemIndex = Number(index);
                      position.push(removeItemIndex);
                    }
                  }
                }

                for (var j in position) {
                  newArr.push(data[position[j]]);
                }

                _this7.storage.set('notificationList', newArr); //  console.log("newArr", newArr);

              } else {
                _this7.storage.set('notificationList', []);
              }
            }
          });
        }
      }, {
        key: "formatNumber",
        value: function formatNumber(item) {
          var number = item.MRP ? item.MRP : item.Price;
          return new Intl.NumberFormat('en-IN').format(number);
        }
      }, {
        key: "loadData",
        value: function loadData(event) {
          var _this8 = this;

          if (this.product_listing.length < this.data.length) {
            setTimeout(function () {
              event.target.complete();
              var len = _this8.product_listing.length;

              for (var i = len; i < _this8.data.length; i++) {
                if (i < _this8.page * _this8.pagenationProductCount) {
                  _this8.product_listing.push(_this8.data[i]);
                }
              }

              _this8.ckeckFavourateProduct();

              _this8.checkCompareData();

              _this8.page++; //event.target.disabled = true;
            }, 500);
          } else {
            event.target.complete();
          }
        }
      }, {
        key: "goToProductListPage",
        value: function goToProductListPage(text) {
          this.filterApply = false;
          var navigationExtras = {
            queryParams: {
              title: text
            }
          };
          this.router.navigate(['product'], navigationExtras);
        }
      }, {
        key: "getProductData",
        value: function getProductData(title) {
          var _this9 = this;

          var param = {};

          if (title == "Air Conditioners") {
            param = {
              air_conditioner: "Air Conditioners"
            };
          } else if (title == "Air Coolers") {
            param = {
              air_cooler: "Air Coolers"
            };
          } else if (title == "Air Purifiers") {
            param = {
              air_purifier: "Air Purifiers"
            };
          } else if (title == "Water Purifiers") {
            param = {
              water_purifier: "Water Purifiers"
            };
          }

          if (this.platform.is('cordova')) {
            this.firebaseAnalytics.logEvent('product_category_view', {
              category: this.title
            }).then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          }

          this.loadingController.create({
            message: 'Please wait'
          }).then(function (res) {
            res.present();

            if (navigator.onLine) {
              _this9.http.post(_this9.categoriesPage.apiBaseUrl + '/bluestar_api/product_listing', param).subscribe(function (response) {
                Object.keys(response).map(function (key) {
                  _this9.product_listing = [];
                  _this9.data = response[key].product_listing;
                  _this9.page = 1;

                  _this9.content.scrollToTop(0);

                  for (var i = 0; i < _this9.data.length; i++) {
                    if (i < _this9.page * _this9.pagenationProductCount) {
                      _this9.product_listing.push(_this9.data[i]);
                    }
                  } // console.log("product_listing", this.product_listing);


                  _this9.page = 2; //this.product_listing = response[key].product_listing;

                  _this9.total_product = response[key].product_listing.length; // console.log("total_product", this.total_product);

                  _this9.all_product_listing = response[key].product_listing; // this.product_listing.sort(function (a, b) {   // finally sort what remains
                  //   if (Number(a.MRP) < Number(b.MRP)) {
                  //     return -1;
                  //   }
                  //   else if (Number(a.MRP) > Number(b.MRP)) {
                  //     return 1;
                  //   }
                  //   else {
                  //     return 0;
                  //   }
                  // });

                  _this9.checkEmptyProductList();

                  _this9.ckeckFavourateProduct();

                  _this9.checkCompareData();

                  _this9.defolutImageFlage = false;
                  _this9.flagShowFilter = false;
                  res.dismiss();
                });
              }, function (err) {
                console.log("err.........", JSON.stringify(err));

                _this9.getDataFromSqlit(title);

                res.dismiss();
              });
            } else {
              _this9.getDataFromSqlit(title);

              res.dismiss();
              console.log("no internat connection");
            }
          });
        }
      }, {
        key: "doRefresh",
        value: function doRefresh(event) {
          var _this10 = this;

          setTimeout(function () {
            _this10.getProductData(_this10.title); //this.myapp.refreseProductData();


            _this10.clearFilter();

            event.target.complete();
          }, 2000);
        }
      }, {
        key: "clearFilter",
        value: function clearFilter() {
          this.flagShowFilter = false;
          this.airPurifierCoverageAreaSelected = [];
          this.airPurifierCADRSelected = [];
          this.airPurifierPriceSelected = [];
          this.airConditionerSubCategorySelected = [];
          this.airConditionerCapacitySelected = [];
          this.airConditionerSeriesSelected = [];
          this.airConditionerStarRatingSelected = [];
          this.airConditionerPriseSelected = [];
          this.waterPurifierTechnologySelected = [];
          this.waterPurifierModelNameSelected = [];
          this.waterPurifierCapacitySelected = [];
          this.waterPurifierPriseSelected = [];
          this.airCoolerTypeSelected = [];
          this.airCoolerPriceSelected = [];
          this.airCoolerCapacitySelected = [];
        }
      }, {
        key: "getDataFromSqlit",
        value: function getDataFromSqlit(title) {
          this.defolutImageFlage = true;

          if (title == "Air Conditioners") {
            this.getAirConditionerDataFromSqlite();
          } else if (title == "Air Coolers") {
            this.getAirCoolerDataFromSqlite();
          } else if (title == "Air Purifiers") {
            this.getAirPurifierDataFromSqlite();
          } else if (title == "Water Purifiers") {
            this.getWaterPurifierDataFromSqlite();
          }
        }
      }, {
        key: "getAirConditionerFilterData",
        value: function getAirConditionerFilterData(url, priceArray) {
          var _this11 = this;

          if (priceArray.length == 0) {
            this.myapp.databaseObj.executeSql("\n      SELECT * FROM ".concat(this.table_air_conditioner, " ").concat(url, " ORDER BY Price\n      "), []).then(function (res) {
              if (res.rows.length > 0) {
                _this11.product_listing = [];

                var _loop = function _loop() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    SubCategory: "",
                    ProductTitle: "",
                    Price: "",
                    Capacity: "",
                    ID: "",
                    Image: "",
                    Series: "",
                    StarRating: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this11.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop();
                }

                _this11.ckeckFavourateProduct2();
              } else {
                _this11.product_listing = [];
              }

              _this11.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          } else {
            var queryUrl = "";

            for (var i in priceArray) {
              if (i == '0') {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_air_conditioner, " ") + url + "Price BETWEEN " + priceArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_air_conditioner, " ") + url + "AND Price BETWEEN " + priceArray[i] + " ";
                }
              } else {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_air_conditioner, " ") + url + "Price BETWEEN " + priceArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_air_conditioner, " ") + url + "AND Price BETWEEN " + priceArray[i] + " ";
                }
              }
            }

            this.myapp.databaseObj.executeSql("".concat(queryUrl, " ORDER BY Price"), []).then(function (res) {
              if (res.rows.length > 0) {
                _this11.product_listing = [];

                var _loop2 = function _loop2() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    SubCategory: "",
                    ProductTitle: "",
                    Price: "",
                    Capacity: "",
                    ID: "",
                    Image: "",
                    Series: "",
                    StarRating: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this11.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop2();
                }

                _this11.ckeckFavourateProduct2();
              } else {
                _this11.product_listing = [];
              }

              _this11.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          }
        }
      }, {
        key: "getWaterPurifierFilterData",
        value: function getWaterPurifierFilterData(url, priceArray) {
          var _this12 = this;

          if (priceArray.length == 0) {
            this.myapp.databaseObj.executeSql("\n      SELECT * FROM ".concat(this.table_water_purifier, " ").concat(url, " ORDER BY Price\n      "), []).then(function (res) {
              if (res.rows.length > 0) {
                _this12.product_listing = [];

                var _loop3 = function _loop3() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    Capacity: "",
                    ProductTitle: "",
                    Price: "",
                    Technology: "",
                    ID: "",
                    Image: "",
                    ModelName: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this12.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop3();
                }

                _this12.ckeckFavourateProduct2();
              } else {
                _this12.product_listing = [];
              }

              _this12.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          } else {
            var queryUrl = "";

            for (var i in priceArray) {
              if (i == '0') {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_water_purifier, " ") + url + "Price BETWEEN " + priceArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_water_purifier, " ") + url + "AND Price BETWEEN " + priceArray[i] + " ";
                }
              } else {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_water_purifier, " ") + url + "Price BETWEEN " + priceArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_water_purifier, " ") + url + "AND Price BETWEEN " + priceArray[i] + " ";
                }
              }
            }

            this.myapp.databaseObj.executeSql("".concat(queryUrl, " ORDER BY Price"), []).then(function (res) {
              if (res.rows.length > 0) {
                _this12.product_listing = [];

                var _loop4 = function _loop4() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    Capacity: "",
                    ProductTitle: "",
                    Price: "",
                    Technology: "",
                    ID: "",
                    Image: "",
                    ModelName: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this12.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop4();
                }

                _this12.ckeckFavourateProduct2();
              } else {
                _this12.product_listing = [];
              }

              _this12.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          }
        }
      }, {
        key: "getAirCoolerFilterData",
        value: function getAirCoolerFilterData(url, categaryArray) {
          var _this13 = this;

          if (categaryArray.length == 0) {
            this.myapp.databaseObj.executeSql("\n    SELECT * FROM ".concat(this.table_air_cooler, " ").concat(url, " ORDER BY Price\n    "), []).then(function (res) {
              if (res.rows.length > 0) {
                _this13.product_listing = [];

                var _loop5 = function _loop5() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    Capacity: "",
                    ProductTitle: "",
                    Price: "",
                    Type: "",
                    ID: "",
                    Image: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this13.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop5();
                }

                _this13.ckeckFavourateProduct2();
              } else {
                _this13.product_listing = [];
              }

              _this13.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          } else {
            var queryUrl = "";

            for (var i in categaryArray) {
              if (i == '0') {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_air_cooler, " ") + url + "Price BETWEEN " + categaryArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_air_cooler, " ") + url + "AND Price BETWEEN " + categaryArray[i] + " ";
                }
              } else {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_air_cooler, " ") + url + "Price BETWEEN " + categaryArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_air_cooler, " ") + url + "AND Price BETWEEN " + categaryArray[i] + " ";
                }
              }
            }

            this.myapp.databaseObj.executeSql("".concat(queryUrl, " ORDER BY Price"), []).then(function (res) {
              if (res.rows.length > 0) {
                _this13.product_listing = [];

                var _loop6 = function _loop6() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    Capacity: "",
                    ProductTitle: "",
                    Price: "",
                    Type: "",
                    ID: "",
                    Image: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this13.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop6();
                }

                _this13.ckeckFavourateProduct2();
              } else {
                _this13.product_listing = [];
              }

              _this13.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          }
        }
      }, {
        key: "getAirPurifierFilterData",
        value: function getAirPurifierFilterData(url, priceArray) {
          var _this14 = this;

          if (priceArray.length == 0) {
            this.myapp.databaseObj.executeSql("\n      SELECT * FROM ".concat(this.table_air_purifier, " ").concat(url, " ORDER BY Price\n      "), []).then(function (res) {
              if (res.rows.length > 0) {
                _this14.product_listing = [];

                var _loop7 = function _loop7() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    CADR: "",
                    ProductTitle: "",
                    Price: "",
                    CoverageArea: "",
                    ID: "",
                    Image: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this14.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop7();
                }

                _this14.ckeckFavourateProduct2();
              } else {
                _this14.product_listing = [];
              }

              _this14.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          } else {
            var queryUrl = "";

            for (var i in priceArray) {
              if (i == '0') {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_air_purifier, " ") + url + "Price BETWEEN " + priceArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "SELECT * FROM ".concat(this.table_air_purifier, " ") + url + "AND Price BETWEEN " + priceArray[i] + " ";
                }
              } else {
                if (url == "WHERE ") {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_air_purifier, " ") + url + "Price BETWEEN " + priceArray[i] + " ";
                } else {
                  queryUrl = queryUrl + "UNION " + "SELECT * FROM ".concat(this.table_air_purifier, " ") + url + "AND Price BETWEEN " + priceArray[i] + " ";
                }
              }
            }

            this.myapp.databaseObj.executeSql("".concat(queryUrl, " ORDER BY Price"), []).then(function (res) {
              if (res.rows.length > 0) {
                _this14.product_listing = [];

                var _loop8 = function _loop8() {
                  var rowKeys = [];
                  var singleRowData = [];
                  object = {
                    SKUCode: "",
                    JsonData: "",
                    CADR: "",
                    ProductTitle: "",
                    Price: "",
                    CoverageArea: "",
                    ID: "",
                    Image: ""
                  };
                  rowKeys = Object.keys(res.rows.item(i));
                  Object.keys(res.rows.item(i)).map(function (key) {
                    singleRowData.push(res.rows.item(i)[key]);
                  });

                  for (var index in rowKeys) {
                    object[rowKeys[index]] = singleRowData[index];
                  }

                  _this14.product_listing.push(object);
                };

                for (var i = 0; i < res.rows.length; i++) {
                  var object;

                  _loop8();
                }

                _this14.ckeckFavourateProduct2();
              } else {
                _this14.product_listing = [];
              }

              _this14.checkEmptyProductList();
            })["catch"](function (e) {
              console.log("error " + JSON.stringify(e));
            });
          }
        }
      }, {
        key: "getAirPurifierDataFromSqlite",
        value: function getAirPurifierDataFromSqlite() {
          var _this15 = this;

          this.myapp.databaseObj.executeSql("\n    SELECT * FROM ".concat(this.table_air_purifier, " ORDER BY Price\n    "), []).then(function (res) {
            if (res.rows.length > 0) {
              _this15.product_listing = [];

              var _loop9 = function _loop9() {
                var rowKeys = [];
                var singleRowData = [];
                object = {
                  SKUCode: "",
                  JsonData: "",
                  CADR: "",
                  ProductTitle: "",
                  Price: "",
                  CoverageArea: "",
                  ID: "",
                  Image: ""
                };
                rowKeys = Object.keys(res.rows.item(i));
                Object.keys(res.rows.item(i)).map(function (key) {
                  singleRowData.push(res.rows.item(i)[key]);
                });

                for (var index in rowKeys) {
                  object[rowKeys[index]] = singleRowData[index];
                }

                _this15.product_listing.push(object);
              };

              for (var i = 0; i < res.rows.length; i++) {
                var object;

                _loop9();
              }

              _this15.sqlitePagenation();

              _this15.ckeckFavourateProduct();

              _this15.checkEmptyProductList();
            }
          })["catch"](function (e) {
            console.log("error " + JSON.stringify(e));
          });
        }
      }, {
        key: "getAirConditionerDataFromSqlite",
        value: function getAirConditionerDataFromSqlite() {
          var _this16 = this;

          //ID, ProductTitle, SKUCode, Image, SubCategory, JsonData, Capacity, Series, StarRating, Price
          this.myapp.databaseObj.executeSql("\n    SELECT * FROM ".concat(this.table_air_conditioner, " ORDER BY Price\n    "), []).then(function (res) {
            if (res.rows.length > 0) {
              _this16.product_listing = [];

              var _loop10 = function _loop10() {
                var rowKeys = [];
                var singleRowData = [];
                object = {
                  SKUCode: "",
                  JsonData: "",
                  SubCategory: "",
                  ProductTitle: "",
                  Price: "",
                  Capacity: "",
                  ID: "",
                  Image: "",
                  Series: "",
                  StarRating: ""
                };
                rowKeys = Object.keys(res.rows.item(i));
                Object.keys(res.rows.item(i)).map(function (key) {
                  singleRowData.push(res.rows.item(i)[key]);
                });

                for (var index in rowKeys) {
                  object[rowKeys[index]] = singleRowData[index];
                }

                _this16.product_listing.push(object);
              };

              for (var i = 0; i < res.rows.length; i++) {
                var object;

                _loop10();
              }

              _this16.sqlitePagenation();

              _this16.ckeckFavourateProduct();

              _this16.checkEmptyProductList();
            }
          })["catch"](function (e) {
            console.log("error " + JSON.stringify(e));
          });
        }
      }, {
        key: "getAirCoolerDataFromSqlite",
        value: function getAirCoolerDataFromSqlite() {
          var _this17 = this;

          //(ID, ProductTitle, SKUCode, Image, JsonData, Capacity, Type, Price)
          this.myapp.databaseObj.executeSql("\n    SELECT * FROM ".concat(this.table_air_cooler, " ORDER BY Price\n    "), []).then(function (res) {
            if (res.rows.length > 0) {
              _this17.product_listing = [];

              var _loop11 = function _loop11() {
                var rowKeys = [];
                var singleRowData = [];
                object = {
                  SKUCode: "",
                  JsonData: "",
                  Capacity: "",
                  ProductTitle: "",
                  Price: "",
                  Type: "",
                  ID: "",
                  Image: ""
                };
                rowKeys = Object.keys(res.rows.item(i));
                Object.keys(res.rows.item(i)).map(function (key) {
                  singleRowData.push(res.rows.item(i)[key]);
                });

                for (var index in rowKeys) {
                  object[rowKeys[index]] = singleRowData[index];
                }

                _this17.product_listing.push(object);
              };

              for (var i = 0; i < res.rows.length; i++) {
                var object;

                _loop11();
              }

              _this17.sqlitePagenation();

              _this17.ckeckFavourateProduct();

              _this17.checkEmptyProductList();
            }
          })["catch"](function (e) {
            console.log("error " + JSON.stringify(e));
          });
        }
      }, {
        key: "getWaterPurifierDataFromSqlite",
        value: function getWaterPurifierDataFromSqlite() {
          var _this18 = this;

          //(ID, ProductTitle, SKUCode, Image, JsonData, Capacity, Technology, Price, ModelName)
          this.myapp.databaseObj.executeSql("\n    SELECT * FROM ".concat(this.table_water_purifier, " ORDER BY Price\n    "), []).then(function (res) {
            if (res.rows.length > 0) {
              _this18.product_listing = [];

              var _loop12 = function _loop12() {
                var rowKeys = [];
                var singleRowData = [];
                object = {
                  SKUCode: "",
                  JsonData: "",
                  Capacity: "",
                  ProductTitle: "",
                  Price: "",
                  Technology: "",
                  ID: "",
                  Image: "",
                  ModelName: ""
                };
                rowKeys = Object.keys(res.rows.item(i));
                Object.keys(res.rows.item(i)).map(function (key) {
                  singleRowData.push(res.rows.item(i)[key]);
                });

                for (var index in rowKeys) {
                  object[rowKeys[index]] = singleRowData[index];
                }

                _this18.product_listing.push(object);
              };

              for (var i = 0; i < res.rows.length; i++) {
                var object;

                _loop12();
              }

              _this18.sqlitePagenation();

              _this18.ckeckFavourateProduct();

              _this18.checkEmptyProductList();
            }
          })["catch"](function (e) {
            console.log("error " + JSON.stringify(e));
          });
        }
      }, {
        key: "sqlitePagenation",
        value: function sqlitePagenation() {
          this.all_product_listing = this.product_listing;
          this.total_product = this.product_listing.length;
          this.data = this.product_listing;
          this.product_listing = [];
          this.page = 1;
          this.content.scrollToTop(0);

          for (var i = 0; i < this.data.length; i++) {
            if (i < this.page * this.pagenationProductCount) {
              this.product_listing.push(this.data[i]);
            }
          }

          this.page = 2;
        }
      }, {
        key: "ckeckFavourateProduct",
        value: function ckeckFavourateProduct() {
          var _this19 = this;

          var data = [];
          this.storage.get('favouriteList').then(function (val) {
            if (val != null) {
              data = val;
              var count = 0;

              for (var index in _this19.product_listing) {
                for (var item in data) {
                  if (_this19.product_listing[index].ID == data[item].ID) {
                    count = 1;
                  }
                }

                if (count == 0) {
                  _this19.product_listing[index].favouritesFlage = false;
                } else {
                  _this19.product_listing[index].favouritesFlage = true;
                }

                count = 0;
              }
            } else {
              for (var _index2 in _this19.product_listing) {
                _this19.product_listing[_index2].favouritesFlage = false;
              }
            }
          });
        }
      }, {
        key: "ckeckFavourateProduct2",
        value: function ckeckFavourateProduct2() {
          var _this20 = this;

          this.data = this.product_listing;
          this.total_product = this.product_listing.length;
          this.page = 1;
          this.content.scrollToTop(0);
          this.product_listing = [];

          for (var i = 0; i < this.data.length; i++) {
            if (i < this.page * this.pagenationProductCount) {
              this.product_listing.push(this.data[i]);
            }
          }

          this.page = 2;
          var data = [];
          this.storage.get('favouriteList').then(function (val) {
            if (val != null) {
              data = val;
              var count = 0;

              for (var index in _this20.product_listing) {
                for (var item in data) {
                  if (_this20.product_listing[index].ID == data[item].ID) {
                    count = 1;
                  }
                }

                if (count == 0) {
                  _this20.product_listing[index].favouritesFlage = false;
                } else {
                  _this20.product_listing[index].favouritesFlage = true;
                }

                count = 0;
              }
            } else {
              for (var _index3 in _this20.product_listing) {
                _this20.product_listing[_index3].favouritesFlage = false;
              }
            }
          });
        }
      }, {
        key: "loadingFilter",
        value: function loadingFilter() {
          this.loadingController.create({
            message: 'This Loader Will Auto Hide in 2 Seconds',
            duration: 1000
          }).then(function (res) {
            res.present();
            res.onDidDismiss().then(function (dis) {
              console.log('Loading dismissed! after 2 Seconds', dis);
            });
          });
        }
      }, {
        key: "openFilterModal",
        value: function openFilterModal() {
          var _this21 = this;

          this.loadingController.create({
            message: 'Please wait',
            duration: 1000
          }).then(function (res) {
            res.present();
            res.onDidDismiss().then(function (dis) {
              _this21.openModal();
            });
          });
        }
      }, {
        key: "openModal",
        value: function openModal() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this22 = this;

            var modalPage, _modalPage, _modalPage2, _modalPage3;

            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    console.log("Enter openModal");

                    if (!(this.title == "Air Conditioners")) {
                      _context.next = 11;
                      break;
                    }

                    _context.next = 4;
                    return this.modalCtrl.create({
                      component: _showcase_app_shell_app_shell_page__WEBPACK_IMPORTED_MODULE_8__["AppShellPage"],
                      componentProps: {
                        title: this.title,
                        airConditionerSubCategorySelected: this.airConditionerSubCategorySelected,
                        airConditionerCapacitySelected: this.airConditionerCapacitySelected,
                        airConditionerSeriesSelected: this.airConditionerSeriesSelected,
                        airConditionerStarRatingSelected: this.airConditionerStarRatingSelected,
                        airConditionerPriseSelected: this.airConditionerPriseSelected
                      }
                    });

                  case 4:
                    modalPage = _context.sent;
                    modalPage.onDidDismiss().then(function (data) {
                      var feilterData = data['data']; // Here's your selected user!

                      _this22.filterApply = true;

                      if (feilterData.close == "false") {
                        var query = "WHERE ";
                        _this22.airConditionerSubCategorySelected = feilterData.airConditionerSubCategorySelected;
                        _this22.airConditionerCapacitySelected = feilterData.airConditionerCapacitySelected;
                        _this22.airConditionerSeriesSelected = feilterData.airConditionerSeriesSelected;
                        _this22.airConditionerStarRatingSelected = feilterData.airConditionerStarRatingSelected;
                        _this22.airConditionerPriseSelected = feilterData.airConditionerPriseSelected; // start flagShowFilter

                        if (_this22.airConditionerSubCategorySelected.length != 0 || _this22.airConditionerCapacitySelected.length != 0 || _this22.airConditionerSeriesSelected.length != 0 || _this22.airConditionerStarRatingSelected.length != 0 || _this22.airConditionerPriseSelected.length != 0) {
                          _this22.flagShowFilter = true;
                          console.log("if", _this22.flagShowFilter);
                          _this22.compareFlage = false;
                          _this22.compareCount = 0;
                          _this22.showBottomPopup = false;
                        } else {
                          _this22.flagShowFilter = false;
                          console.log("else", _this22.flagShowFilter);
                        }

                        for (var i in _this22.airConditionerSubCategorySelected) {
                          _this22.firebaseAnalytics.logEvent('air_conditioner_filter', {
                            sub_category: _this22.airConditionerSubCategorySelected[i]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i in _this22.airConditionerCapacitySelected) {
                          _this22.firebaseAnalytics.logEvent('air_conditioner_filter', {
                            capacity: _this22.airConditionerCapacitySelected[_i]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i2 in _this22.airConditionerSeriesSelected) {
                          _this22.firebaseAnalytics.logEvent('air_conditioner_filter', {
                            series: _this22.airConditionerSeriesSelected[_i2]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i3 in _this22.airConditionerStarRatingSelected) {
                          _this22.firebaseAnalytics.logEvent('air_conditioner_filter', {
                            star_rating: _this22.airConditionerStarRatingSelected[_i3]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i4 in _this22.airConditionerPriseSelected) {
                          console.log("this.airConditionerPriseSelected[i]", _this22.airConditionerPriseSelected[_i4]);

                          _this22.firebaseAnalytics.logEvent('air_conditioner_filter', {
                            MRP: _this22.airConditionerPriseSelected[_i4]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        var filterCount = 0;

                        if (feilterData.SubCategory != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "SubCategory IN " + "(" + feilterData.SubCategory + ") ";
                            filterCount++;
                          } else {
                            query = query + "SubCategory IN " + "(" + feilterData.SubCategory + ") ";
                            filterCount++;
                          }
                        }

                        if (feilterData.Capacity != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "Capacity IN " + "(" + feilterData.Capacity + ") ";
                            filterCount++;
                          } else {
                            query = query + "Capacity IN " + "(" + feilterData.Capacity + ") ";
                            filterCount++;
                          }
                        }

                        if (feilterData.Series != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "Series IN " + "(" + feilterData.Series + ") ";
                            filterCount++;
                          } else {
                            query = query + "Series IN " + "(" + feilterData.Series + ") ";
                            filterCount++;
                          }
                        }

                        if (feilterData.StarRating != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "StarRating IN " + "(" + feilterData.StarRating + ") ";
                            filterCount++;
                          } else {
                            query = query + "StarRating IN " + "(" + feilterData.StarRating + ") ";
                            filterCount++;
                          }
                        }

                        var PriceValue = [];

                        if (_this22.airConditionerPriseSelected.length != 0) {
                          for (var _i5 in _this22.airConditionerPriseSelected) {
                            PriceValue.push(_this22.airConditionerPriseSelected[_i5].replace("-", " AND "));
                          }
                        }

                        if (query != "WHERE " || PriceValue.length != 0) {
                          _this22.getAirConditionerFilterData(query, PriceValue);
                        } else {
                          console.log("Enter openModal onDidDismiss 3");
                          _this22.product_listing = _this22.all_product_listing;

                          _this22.sqlitePagenation();

                          _this22.checkEmptyProductList();
                        }
                      } else {
                        _this22.airConditionerSubCategorySelected = feilterData.airConditionerSubCategorySelected;
                        _this22.airConditionerCapacitySelected = feilterData.airConditionerCapacitySelected;
                        _this22.airConditionerSeriesSelected = feilterData.airConditionerSeriesSelected;
                        _this22.airConditionerStarRatingSelected = feilterData.airConditionerStarRatingSelected;
                        _this22.airConditionerPriseSelected = feilterData.airConditionerPriseSelected;
                        _this22.flagShowFilter = false;
                        console.log("flag Show Filter", _this22.flagShowFilter);
                      }
                    });
                    _context.next = 8;
                    return modalPage.present();

                  case 8:
                    return _context.abrupt("return", _context.sent);

                  case 11:
                    if (!(this.title == "Air Coolers")) {
                      _context.next = 21;
                      break;
                    }

                    _context.next = 14;
                    return this.modalCtrl.create({
                      component: _showcase_app_shell_app_shell_page__WEBPACK_IMPORTED_MODULE_8__["AppShellPage"],
                      componentProps: {
                        title: this.title,
                        airCoolerTypeSelected: this.airCoolerTypeSelected,
                        airCoolerPriceSelected: this.airCoolerPriceSelected,
                        airCoolerCapacitySelected: this.airCoolerCapacitySelected
                      }
                    });

                  case 14:
                    _modalPage = _context.sent;

                    _modalPage.onDidDismiss().then(function (data) {
                      _this22.filterApply = true;
                      var feilterData = data['data']; // Here's your selected user!

                      if (feilterData.close == "false") {
                        var query = "WHERE ";
                        _this22.airCoolerTypeSelected = feilterData.airCoolerTypeSelected;
                        _this22.airCoolerPriceSelected = feilterData.airCoolerPriceSelected;
                        _this22.airCoolerCapacitySelected = feilterData.airCoolerCapacitySelected; // start flagShowFilter

                        if (_this22.airCoolerTypeSelected.length != 0 || _this22.airCoolerPriceSelected.length != 0 || _this22.airCoolerCapacitySelected.length != 0) {
                          _this22.flagShowFilter = true;
                          console.log("if", _this22.flagShowFilter);
                          _this22.compareFlage = false;
                          _this22.compareCount = 0;
                          _this22.showBottomPopup = false;
                        } else {
                          _this22.flagShowFilter = false;
                          console.log("else", _this22.flagShowFilter);
                        } // end flagShowFilter


                        console.log("Type", _this22.airCoolerTypeSelected);
                        console.log("Price", _this22.airCoolerPriceSelected);
                        console.log("Capacity", _this22.airCoolerCapacitySelected);

                        for (var i in _this22.airCoolerTypeSelected) {
                          _this22.firebaseAnalytics.logEvent('air_cooler_filter', {
                            type: _this22.airCoolerTypeSelected[i]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i6 in _this22.airCoolerPriceSelected) {
                          _this22.firebaseAnalytics.logEvent('air_cooler_filter', {
                            MRP: _this22.airCoolerPriceSelected[_i6]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i7 in _this22.airCoolerCapacitySelected) {
                          _this22.firebaseAnalytics.logEvent('air_cooler_filter', {
                            capacity: _this22.airCoolerCapacitySelected[_i7]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        var filterCount = 0; //Capacity BETWEEN 25 AND 50

                        if (feilterData.Type != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "Type IN " + "(" + feilterData.Type + ") ";
                            filterCount++;
                          } else {
                            query = query + "Type IN " + "(" + feilterData.Type + ") ";
                            filterCount++;
                          }
                        }

                        var CapacityValue = [];
                        var Capacity = "";

                        if (_this22.airCoolerCapacitySelected.length != 0) {
                          for (var _i8 in _this22.airCoolerCapacitySelected) {
                            var firstValue = _this22.airCoolerCapacitySelected[_i8].split('-')[0];

                            var secondValue = _this22.airCoolerCapacitySelected[_i8].split('-')[1];

                            var size = Number(secondValue) - Number(firstValue);

                            for (var _i9 = 0; _i9 < size; _i9++) {
                              CapacityValue.push(Number(firstValue) + _i9);
                            }
                          }

                          for (var item in CapacityValue) {
                            if (Capacity.length == 0) {
                              Capacity = Capacity + CapacityValue[item];
                            } else {
                              Capacity = Capacity + ',' + CapacityValue[item];
                            }
                          }
                        }

                        if (Capacity != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "Capacity IN " + "(" + Capacity + ") ";
                            filterCount++;
                          } else {
                            query = query + "Capacity IN " + "(" + Capacity + ") ";
                            filterCount++;
                          }
                        }

                        var PriceValue = [];

                        if (_this22.airCoolerPriceSelected.length != 0) {
                          for (var _i10 in _this22.airCoolerPriceSelected) {
                            PriceValue.push(_this22.airCoolerPriceSelected[_i10].replace("-", " AND "));
                          }
                        }

                        if (query != "WHERE " || PriceValue.length != 0) {
                          _this22.getAirCoolerFilterData(query, PriceValue);
                        } else {
                          _this22.product_listing = _this22.all_product_listing;

                          _this22.sqlitePagenation();

                          _this22.checkEmptyProductList();
                        }
                      } else {
                        _this22.airCoolerTypeSelected = feilterData.airCoolerTypeSelected;
                        _this22.airCoolerPriceSelected = feilterData.airCoolerPriceSelected;
                        _this22.airCoolerCapacitySelected = feilterData.airCoolerCapacitySelected;
                        _this22.flagShowFilter = false;
                        console.log("flag Show Filter", _this22.flagShowFilter);
                      }
                    });

                    _context.next = 18;
                    return _modalPage.present();

                  case 18:
                    return _context.abrupt("return", _context.sent);

                  case 21:
                    if (!(this.title == "Air Purifiers")) {
                      _context.next = 31;
                      break;
                    }

                    _context.next = 24;
                    return this.modalCtrl.create({
                      component: _showcase_app_shell_app_shell_page__WEBPACK_IMPORTED_MODULE_8__["AppShellPage"],
                      componentProps: {
                        title: this.title,
                        airPurifierCoverageAreaSelected: this.airPurifierCoverageAreaSelected,
                        airPurifierCADRSelected: this.airPurifierCADRSelected,
                        airPurifierPriceSelected: this.airPurifierPriceSelected
                      }
                    });

                  case 24:
                    _modalPage2 = _context.sent;

                    _modalPage2.onDidDismiss().then(function (data) {
                      _this22.filterApply = true;
                      var feilterData = data['data']; // Here's your selected user!

                      if (feilterData.close == "false") {
                        var query = "WHERE ";
                        _this22.airPurifierCoverageAreaSelected = feilterData.airPurifierCoverageAreaSelected;
                        _this22.airPurifierCADRSelected = feilterData.airPurifierCADRSelected;
                        _this22.airPurifierPriceSelected = feilterData.airPurifierPriceSelected; // start flagShowFilter

                        if (_this22.airPurifierCoverageAreaSelected.length != 0 || _this22.airPurifierCADRSelected.length != 0 || _this22.airPurifierPriceSelected.length != 0) {
                          _this22.flagShowFilter = true;
                          console.log("if", _this22.flagShowFilter);
                          _this22.compareFlage = false;
                          _this22.compareCount = 0;
                          _this22.showBottomPopup = false;
                        } else {
                          _this22.flagShowFilter = false;
                          console.log("else", _this22.flagShowFilter);
                        } // end flagShowFilter


                        for (var i in _this22.airPurifierPriceSelected) {
                          _this22.firebaseAnalytics.logEvent('air_purifier_filter', {
                            MRP: _this22.airPurifierPriceSelected[i]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i11 in _this22.airPurifierCoverageAreaSelected) {
                          _this22.firebaseAnalytics.logEvent('air_purifier_filter', {
                            coverage_area: _this22.airPurifierCoverageAreaSelected[_i11]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i12 in _this22.airPurifierCADRSelected) {
                          _this22.firebaseAnalytics.logEvent('air_purifier_filter', {
                            CADR: _this22.airPurifierCADRSelected[_i12]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        var filterCount = 0;

                        if (feilterData.CADR != "") {
                          filterCount++;
                          query = query + "CADR IN " + "(" + feilterData.CADR + ") ";
                        }

                        if (feilterData.CoverageArea != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "CoverageArea IN " + "(" + feilterData.CoverageArea + ") ";
                            filterCount++;
                          } else {
                            query = query + "CoverageArea IN " + "(" + feilterData.CoverageArea + ") ";
                            filterCount++;
                          }
                        }

                        var PriceValue = [];

                        if (_this22.airPurifierPriceSelected.length != 0) {
                          for (var _i13 in _this22.airPurifierPriceSelected) {
                            PriceValue.push(_this22.airPurifierPriceSelected[_i13].replace("-", " AND "));
                          }
                        }

                        if (query != "WHERE " || PriceValue.length != 0) {
                          _this22.getAirPurifierFilterData(query, PriceValue);
                        } else {
                          _this22.product_listing = _this22.all_product_listing;

                          _this22.sqlitePagenation();

                          _this22.checkEmptyProductList();
                        }
                      } else {
                        _this22.airPurifierCoverageAreaSelected = feilterData.airPurifierCoverageAreaSelected;
                        _this22.airPurifierCADRSelected = feilterData.airPurifierCADRSelected;
                        _this22.airPurifierPriceSelected = feilterData.airPurifierPriceSelected;
                        _this22.flagShowFilter = false;
                        console.log("flag Show Filter", _this22.flagShowFilter);
                      }
                    });

                    _context.next = 28;
                    return _modalPage2.present();

                  case 28:
                    return _context.abrupt("return", _context.sent);

                  case 31:
                    if (!(this.title == "Water Purifiers")) {
                      _context.next = 39;
                      break;
                    }

                    _context.next = 34;
                    return this.modalCtrl.create({
                      component: _showcase_app_shell_app_shell_page__WEBPACK_IMPORTED_MODULE_8__["AppShellPage"],
                      componentProps: {
                        title: this.title,
                        waterPurifierTechnologySelected: this.waterPurifierTechnologySelected,
                        waterPurifierModelNameSelected: this.waterPurifierModelNameSelected,
                        waterPurifierCapacitySelected: this.waterPurifierCapacitySelected,
                        waterPurifierPriseSelected: this.waterPurifierPriseSelected
                      }
                    });

                  case 34:
                    _modalPage3 = _context.sent;

                    _modalPage3.onDidDismiss().then(function (data) {
                      _this22.filterApply = true;
                      var feilterData = data['data']; // Here's your selected user!

                      if (feilterData.close == "false") {
                        var query = "WHERE ";
                        _this22.waterPurifierTechnologySelected = feilterData.waterPurifierTechnologySelected, _this22.waterPurifierModelNameSelected = feilterData.waterPurifierModelNameSelected, _this22.waterPurifierCapacitySelected = feilterData.waterPurifierCapacitySelected, _this22.waterPurifierPriseSelected = feilterData.waterPurifierPriseSelected; // start flagShowFilter

                        if (_this22.waterPurifierTechnologySelected.length != 0 || _this22.waterPurifierModelNameSelected.length != 0 || _this22.waterPurifierCapacitySelected.length != 0 || _this22.waterPurifierPriseSelected.length != 0) {
                          _this22.flagShowFilter = true;
                          console.log("if", _this22.flagShowFilter);
                          _this22.compareFlage = false;
                          _this22.compareCount = 0;
                          _this22.showBottomPopup = false;
                        } else {
                          _this22.flagShowFilter = false;
                          console.log("else", _this22.flagShowFilter);
                        } // end flagShowFilter


                        for (var i in _this22.waterPurifierTechnologySelected) {
                          _this22.firebaseAnalytics.logEvent('water_purifier_filter', {
                            technology: _this22.waterPurifierTechnologySelected[i]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i14 in _this22.waterPurifierPriseSelected) {
                          _this22.firebaseAnalytics.logEvent('water_purifier_filter', {
                            MRP: _this22.waterPurifierPriseSelected[_i14]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i15 in _this22.waterPurifierCapacitySelected) {
                          _this22.firebaseAnalytics.logEvent('water_purifier_filter', {
                            capacity: _this22.waterPurifierCapacitySelected[_i15]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        for (var _i16 in _this22.waterPurifierModelNameSelected) {
                          _this22.firebaseAnalytics.logEvent('water_purifier_filter', {
                            model_name: _this22.waterPurifierModelNameSelected[_i16]
                          }).then(function (res) {
                            return console.log(res);
                          })["catch"](function (error) {
                            return console.error(error);
                          });
                        }

                        var filterCount = 0;

                        if (feilterData.Technology != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "Technology IN " + "(" + feilterData.Technology + ") ";
                            filterCount++;
                          } else {
                            query = query + "Technology IN " + "(" + feilterData.Technology + ") ";
                            filterCount++;
                          }
                        }

                        if (feilterData.ModelName != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "ModelName IN " + "(" + feilterData.ModelName + ") ";
                            filterCount++;
                          } else {
                            query = query + "ModelName IN " + "(" + feilterData.ModelName + ") ";
                            filterCount++;
                          }
                        }

                        if (feilterData.Capacity != "") {
                          if (filterCount > 0) {
                            query = query + "AND " + "Capacity IN " + "(" + feilterData.Capacity + ") ";
                            filterCount++;
                          } else {
                            query = query + "Capacity IN " + "(" + feilterData.Capacity + ") ";
                            filterCount++;
                          }
                        }

                        var PriceValue = [];

                        if (_this22.waterPurifierPriseSelected.length != 0) {
                          for (var _i17 in _this22.waterPurifierPriseSelected) {
                            PriceValue.push(_this22.waterPurifierPriseSelected[_i17].replace("-", " AND "));
                          }
                        }

                        if (query != "WHERE " || PriceValue.length != 0) {
                          _this22.getWaterPurifierFilterData(query, PriceValue);
                        } else {
                          _this22.product_listing = _this22.all_product_listing;

                          _this22.sqlitePagenation();

                          _this22.checkEmptyProductList();
                        }
                      } else {
                        _this22.waterPurifierTechnologySelected = feilterData.waterPurifierTechnologySelected;
                        _this22.waterPurifierModelNameSelected = feilterData.waterPurifierModelNameSelected;
                        _this22.waterPurifierCapacitySelected = feilterData.waterPurifierCapacitySelected;
                        _this22.waterPurifierPriseSelected = feilterData.waterPurifierPriseSelected;
                        _this22.flagShowFilter = false;
                        console.log("flag Show Filter", _this22.flagShowFilter);
                      }
                    });

                    _context.next = 38;
                    return _modalPage3.present();

                  case 38:
                    return _context.abrupt("return", _context.sent);

                  case 39:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "clickOnFavourites",
        value: function clickOnFavourites(item) {
          var _this23 = this;

          if (this.title == "Air Conditioners") {
            this.storeDataInAirConditioner(item);
          } else if (this.title == "Air Coolers") {
            this.storeDataInAirCooler(item);
          } else if (this.title == "Air Purifiers") {
            this.storeDataInAirPurifier(item);
          } else if (this.title == "Water Purifiers") {
            this.storeDataInWaterConditioner(item);
          }

          var data = [];
          this.storage.get('favouriteList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this23.storage.set('favouriteList', data);

                  _this23.mackFavouritesFlageChange(item);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this23.storage.set('favouriteList', data);

                  _this23.mackFavouritesFlageChange(item);
                }
              } else {
                data.push(item);

                _this23.storage.set('favouriteList', data);

                _this23.mackFavouritesFlageChange(item);
              }
            } else {
              data.push(item);

              _this23.storage.set('favouriteList', data);

              _this23.mackFavouritesFlageChange(item);
            }
          });
        }
      }, {
        key: "storeDataInAirConditioner",
        value: function storeDataInAirConditioner(item) {
          var _this24 = this;

          var data = [];
          this.storage.get('airConditionerList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this24.storage.set('airConditionerList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this24.storage.set('airConditionerList', data);
                }
              } else {
                data.push(item);

                _this24.storage.set('airConditionerList', data);
              }
            } else {
              data.push(item);

              _this24.storage.set('airConditionerList', data);
            }
          });
        }
      }, {
        key: "storeDataInAirCooler",
        value: function storeDataInAirCooler(item) {
          var _this25 = this;

          var data = [];
          this.storage.get('airCoolerList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this25.storage.set('airCoolerList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this25.storage.set('airCoolerList', data);
                }
              } else {
                data.push(item);

                _this25.storage.set('airCoolerList', data);
              }
            } else {
              data.push(item);

              _this25.storage.set('airCoolerList', data);
            }
          });
        }
      }, {
        key: "storeDataInAirPurifier",
        value: function storeDataInAirPurifier(item) {
          var _this26 = this;

          var data = [];
          this.storage.get('airPurifierList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this26.storage.set('airPurifierList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this26.storage.set('airPurifierList', data);
                }
              } else {
                data.push(item);

                _this26.storage.set('airPurifierList', data);
              }
            } else {
              data.push(item);

              _this26.storage.set('airPurifierList', data);
            }
          });
        }
      }, {
        key: "storeDataInWaterConditioner",
        value: function storeDataInWaterConditioner(item) {
          var _this27 = this;

          var data = [];
          this.storage.get('waterPurifierList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this27.storage.set('waterPurifierList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this27.storage.set('waterPurifierList', data);
                }
              } else {
                data.push(item);

                _this27.storage.set('waterPurifierList', data);
              }
            } else {
              data.push(item);

              _this27.storage.set('waterPurifierList', data);
            }
          });
        }
      }, {
        key: "mackFavouritesFlageChange",
        value: function mackFavouritesFlageChange(item) {
          for (var index in this.product_listing) {
            if (this.product_listing[index].ID == item.ID) {
              this.product_listing[index].favouritesFlage = !this.product_listing[index].favouritesFlage;
            }
          }
        }
      }, {
        key: "clickOnProduct",
        value: function clickOnProduct(id, text, SKUCode, ProductTitle, Image) {
          this.filterApply = true;
          var navigationExtras = {
            queryParams: {
              id: id,
              title: text,
              SKUCode: SKUCode,
              ProductTitle: ProductTitle,
              Image: Image
            }
          };
          this.router.navigate(['product/' + id], navigationExtras);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this28 = this;

          // On init, the route subscription is the active subscription
          this.subscriptions = this.route.data.subscribe(function (resolvedRouteData) {
            // Route subscription resolved, now the active subscription is the Observable extracted from the resolved route data
            _this28.subscriptions = _utils_resolver_helper__WEBPACK_IMPORTED_MODULE_3__["ResolverHelper"].extractData(resolvedRouteData.data, _product_listing_model__WEBPACK_IMPORTED_MODULE_4__["ProductListingModel"]).subscribe(function (state) {
              _this28.listing = state;
            }, function (error) {});
          }, function (error) {});
        } // NOTE: Ionic only calls ngOnDestroy if the page was popped (ex: when navigating back)
        // Since ngOnDestroy might not fire when you navigate from the current page, use ionViewWillLeave to cleanup Subscriptions

      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.subscriptions.unsubscribe();
        }
      }, {
        key: "onClickSearch",
        value: function onClickSearch(text) {
          this.filterApply = true;
          var navigationExtras = {
            queryParams: {
              title: text
            }
          };
          this.router.navigate(['search/'], navigationExtras);
        }
      }, {
        key: "onClickFavouritesIcon",
        value: function onClickFavouritesIcon() {
          this.filterApply = true;
          this.router.navigate(['favourites/']);
        }
      }, {
        key: "checkEmptyProductList",
        value: function checkEmptyProductList() {
          if (this.product_listing.length == 0) {
            this.emptyListFlage = false;
          } else {
            this.emptyListFlage = true;
          }
        }
      }, {
        key: "clickOnCategary",
        value: function clickOnCategary(categary) {
          this.filterApply = false;

          if (categary == "Air Conditioners") {
            this.currentTab = "Air Conditioners";
            this.airConditionerFlage = true;
            this.airCoolerFlage = false;
            this.airPurifierFlage = false;
            this.waterPurifierFlage = false;
          } else if (categary == "Air Coolers") {
            this.currentTab = "Air Coolers";
            this.airConditionerFlage = false;
            this.airCoolerFlage = true;
            this.airPurifierFlage = false;
            this.waterPurifierFlage = false;
          } else if (categary == "Air Purifiers") {
            this.currentTab = "Air Purifiers";
            this.airConditionerFlage = false;
            this.airCoolerFlage = false;
            this.airPurifierFlage = true;
            this.waterPurifierFlage = false;
          } else if (categary == "Water Purifiers") {
            this.currentTab = "Water Purifiers";
            this.airConditionerFlage = false;
            this.airCoolerFlage = false;
            this.airPurifierFlage = false;
            this.waterPurifierFlage = true;
          }

          this.title = this.currentTab;
          this.getProductData(this.title);
          this.clearFilter();
        }
      }, {
        key: "downloadPdf",
        value: function downloadPdf(item) {
          var _this29 = this;

          var param = {};
          var tableName = "";

          if (this.title == "Air Conditioners") {
            param = {
              air_conditioner: item.ID
            };
            tableName = this.table_air_conditioner;
          } else if (this.title == "Air Coolers") {
            param = {
              air_cooler: item.ID
            };
            tableName = this.table_air_cooler;
          } else if (this.title == "Air Purifiers") {
            param = {
              air_purifier: item.ID
            };
            tableName = this.table_air_purifier;
          } else if (this.title == "Water Purifiers") {
            param = {
              water_purifier: item.ID
            };
            tableName = this.table_water_purifier;
          }

          this.myapp.databaseObj.executeSql("\n    SELECT * FROM ".concat(tableName, " WHERE ID = ").concat(item.ID, "\n    "), []).then(function (res) {
            if (res.rows.length > 0) {
              var product_data = [];

              var _loop13 = function _loop13() {
                var rowKeys = [];
                var singleRowData = [];
                object = {
                  brochures: ""
                };
                rowKeys = Object.keys(res.rows.item(i));
                Object.keys(res.rows.item(i)).map(function (key) {
                  singleRowData.push(res.rows.item(i)[key]);
                });

                for (var index in rowKeys) {
                  if (rowKeys[index] == "brochures") {
                    object[rowKeys[index]] = singleRowData[index];
                  }
                }

                product_data.push(object);
              };

              for (var i = 0; i < res.rows.length; i++) {
                var object;

                _loop13();
              }

              if (product_data[0].brochures == "") {
                _this29.downloadFuction(item, param);
              } else {
                if (_this29.platform.is("ios")) {
                  var browser = _this29.inAppBrowser.create(product_data[0].brochures);

                  _this29.firebaseAnalytics.logEvent('downloads_products', {
                    product: item.SKUCode,
                    category: _this29.title
                  }).then(function (res) {
                    return console.log(res);
                  })["catch"](function (error) {
                    return console.error(error);
                  });
                } else {
                  _this29.loadingController.create({
                    message: 'Please wait while downloading'
                  }).then(function (res) {
                    res.present();

                    if (navigator.onLine) {
                      var request = {
                        uri: product_data[0].brochures,
                        title: 'Blue Star',
                        description: '',
                        mimeType: 'application/pdf',
                        visibleInDownloadsUi: true,
                        notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_13__["NotificationVisibility"].VisibleNotifyCompleted,
                        destinationInExternalPublicDir: {
                          dirType: 'Download',
                          subPath: item.SKUCode + '.pdf'
                        }
                      };

                      _this29.downloader.download(request).then(function (location) {
                        res.dismiss();

                        _this29.presentToast("Downloaded in device download folder");

                        _this29.firebaseAnalytics.logEvent('downloads_products', {
                          product: item.SKUCode,
                          category: _this29.title
                        }).then(function (res) {
                          return console.log(res);
                        })["catch"](function (error) {
                          return console.error(error);
                        });
                      })["catch"](function (error) {
                        console.error(error);
                      });
                    } else {
                      res.dismiss();
                      console.log("no internat connection");
                    }
                  });
                }
              }
            } else {
              _this29.downloadFuction(item, param);
            }
          })["catch"](function (e) {
            console.log("error " + JSON.stringify(e));
          });
        }
      }, {
        key: "downloadFuction",
        value: function downloadFuction(item, param) {
          var _this30 = this;

          this.loadingController.create({
            message: 'Please wait while downloading'
          }).then(function (res) {
            res.present();

            if (navigator.onLine) {
              _this30.http.post(_this30.categoriesPage.apiBaseUrl + '/bluestar_api/product_pdf/download', param).subscribe(function (response) {
                Object.keys(response).map(function (key) {
                  _this30.pdf_link = response[key].pdf_link; //this.indicatorDownload(item, this.pdf_link);

                  if (_this30.platform.is("ios")) {
                    res.dismiss();

                    var browser = _this30.inAppBrowser.create(_this30.pdf_link);

                    _this30.firebaseAnalytics.logEvent('downloads_products', {
                      product: item.SKUCode,
                      category: _this30.title
                    }).then(function (res) {
                      return console.log(res);
                    })["catch"](function (error) {
                      return console.error(error);
                    });
                  } else {
                    var request = {
                      uri: _this30.pdf_link,
                      title: 'Blue Star',
                      description: '',
                      mimeType: 'application/pdf',
                      visibleInDownloadsUi: true,
                      notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_13__["NotificationVisibility"].VisibleNotifyCompleted,
                      destinationInExternalPublicDir: {
                        dirType: 'Download',
                        subPath: item.SKUCode + '.pdf'
                      }
                    };

                    _this30.downloader.download(request).then(function (location) {
                      res.dismiss();

                      _this30.presentToast("Downloaded in device download folder");

                      _this30.firebaseAnalytics.logEvent('downloads_products', {
                        product: item.SKUCode,
                        category: _this30.title
                      }).then(function (res) {
                        return console.log(res);
                      })["catch"](function (error) {
                        return console.error(error);
                      });
                    })["catch"](function (error) {
                      console.error(error);
                      res.dismiss();
                    });
                  }
                });
              }, function (err) {
                res.dismiss();
                console.log("err.........", JSON.stringify(err));
              });
            } else {
              res.dismiss();
              console.log("no internat connection");
            }
          });
        }
      }, {
        key: "indicatorDownload",
        value: function indicatorDownload(item, pdf_link) {
          var _this31 = this;

          if (this.platform.is("ios")) {
            var browser = this.inAppBrowser.create(this.pdf_link);
            this.firebaseAnalytics.logEvent('downloads_products', {
              product: item.SKUCode,
              category: this.title
            }).then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          } else {
            var request = {
              uri: pdf_link,
              title: 'Blue Star',
              description: '',
              mimeType: 'application/pdf',
              visibleInDownloadsUi: true,
              notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_13__["NotificationVisibility"].VisibleNotifyCompleted,
              destinationInExternalPublicDir: {
                dirType: 'Download',
                subPath: item.SKUCode + '.pdf'
              }
            };
            this.loadingController.create({
              message: 'Please wait while downloading'
            }).then(function (res) {
              res.present();

              _this31.downloader.download(request).then(function (location) {
                res.dismiss();

                _this31.presentToast("Downloaded in device download folder");

                _this31.firebaseAnalytics.logEvent('downloads_products', {
                  product: item.SKUCode,
                  category: _this31.title
                }).then(function (res) {
                  return console.log(res);
                })["catch"](function (error) {
                  return console.error(error);
                });
              })["catch"](function (error) {
                console.error(error);
                res.dismiss();
              });
            });
          }
        }
      }, {
        key: "showSharePopover",
        value: function showSharePopover(ev, item) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var popover;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.popoverCtrl.create({
                      component: _share_popover_share_popover__WEBPACK_IMPORTED_MODULE_12__["SharePopover"],
                      event: ev,
                      animated: true,
                      showBackdrop: true,
                      componentProps: {
                        shareDetails: item,
                        title: this.title
                      }
                    });

                  case 2:
                    popover = _context2.sent;
                    _context2.next = 5;
                    return popover.present();

                  case 5:
                    return _context2.abrupt("return", _context2.sent);

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(text) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastCtrl.create({
                      message: text,
                      duration: 7000,
                      position: 'bottom',
                      cssClass: "msg-align"
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "getPdfLink",
        value: function getPdfLink(productId, item) {
          var _this32 = this;

          var param = {};

          if (this.title == "Air Conditioners") {
            param = {
              air_conditioner: productId
            };
          } else if (this.title == "Air Coolers") {
            param = {
              air_cooler: productId
            };
          } else if (this.title == "Air Purifiers") {
            param = {
              air_purifier: productId
            };
          } else if (this.title == "Water Purifiers") {
            param = {
              water_purifier: productId
            };
          }

          this.loadingController.create({
            message: 'Please wait'
          }).then(function (res) {
            res.present();

            if (navigator.onLine) {
              _this32.http.post(_this32.categoriesPage.apiBaseUrl + '/bluestar_api/product_pdf/download', param).subscribe(function (response) {
                Object.keys(response).map(function (key) {
                  _this32.pdf_link = response[key].pdf_link;
                  res.dismiss();

                  _this32.shareMethod(item);
                });
              }, function (err) {
                res.dismiss();

                _this32.presentToast("No internet connection. Please try again later.");

                console.log("err.........", JSON.stringify(err));
              });
            } else {
              res.dismiss();

              _this32.presentToast("No internet connection. Please try again later.");
            }
          });
        }
      }, {
        key: "toDataUrl",
        value: function toDataUrl(url, _this, callback) {
          var xhr = new XMLHttpRequest();

          xhr.onload = function () {
            var reader = new FileReader();

            reader.onloadend = function () {
              callback(reader.result, _this);
            };

            reader.readAsDataURL(xhr.response);
          };

          xhr.open('GET', url);
          xhr.responseType = 'blob';
          xhr.send();
        }
      }, {
        key: "shareMethod",
        value: function shareMethod(item) {
          this.toDataUrl(item.Image, this, function (myBase64, _this) {
            _this.callMethod(myBase64, item);
          });
        }
      }, {
        key: "callMethod",
        value: function callMethod(myBase64, item) {
          var _this33 = this;

          var body = "Hi there, check out this product by Blue Star!\n\nProduct Category: ".concat(this.title, "\nProduct Title: ").concat(item.ProductTitle, "\nSKU Code: ").concat(item.SKUCode, "\nClick here to get the product specification: ").concat(this.pdf_link.replace(/\s/g, "%20"));
          this.socialSharing.share(body, item.ProductTitle.replace(/\%/g, " pc"), myBase64, null).then(function (sucess) {
            _this33.firebaseAnalytics.logEvent('share_products', {
              product: item.SKUCode,
              category: _this33.title
            }).then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          })["catch"](function (err) {
            console.log(err);
          });
        }
      }, {
        key: "goToCompare",
        value: function goToCompare() {
          var _this34 = this;

          if (this.compareCount == 0) {
            this.clearFilter();
            this.compareCount = 1;
            this.compareFlage = true;
            console.log("compareFlage", this.compareFlage); // Show Compare Flag

            var data = [];
            this.storage.get('compareList').then(function (val) {
              console.log("compareList", val);
              _this34.popupCompareList = val;

              if (val != null) {
                data = val;
                var count = 0;

                if (data.length != 0) {
                  console.log("Enter Show bottom popup", _this34.popupCompareList);
                  _this34.showBottomPopup = true; // this.openBottomPopup();
                }

                for (var index in _this34.product_listing) {
                  for (var item in data) {
                    if (_this34.product_listing[index].ID == data[item].ID) {
                      count = 1;
                    }
                  }

                  if (count == 0) {
                    _this34.product_listing[index].showCompareFlage = false;
                  } else {
                    _this34.product_listing[index].showCompareFlage = true;
                  }

                  count = 0;
                }
              } else {
                for (var _index4 in _this34.product_listing) {
                  _this34.product_listing[_index4].showCompareFlage = false;
                }
              }
            });
            console.log("this.product_listing", this.product_listing); // Show Compare Flag End
          } else {
            this.compareCount = 0;
            this.compareFlage = false;
            this.showBottomPopup = false;
            console.log("compareFlage", this.compareFlage);
          }
        }
      }, {
        key: "getProductaddToCompare",
        value: function getProductaddToCompare(item) {
          var _this35 = this;

          console.log("Item Id", item.ID);
          console.log("Title", this.title); //  this.route.queryParams.subscribe(params => {
          // this.categoryName = params.title;
          // this.shareDetails = params;
          // if (params && params.title) {
          // this.title = params.title

          var param = {};

          if (this.title == "Air Conditioners") {
            param = {
              air_conditioner: item.ID
            };
          } else if (this.title == "Air Coolers") {
            param = {
              air_cooler: item.ID
            };
          } else if (this.title == "Air Purifiers") {
            param = {
              air_purifier: item.ID
            };
          } else if (this.title == "Water Purifiers") {
            param = {
              water_purifier: item.ID
            };
          }

          this.loadingController.create({
            message: 'Please wait'
          }).then(function (res) {
            res.present();

            if (navigator.onLine) {
              _this35.http.post(_this35.categoriesPage.apiBaseUrl + '/bluestar_api/product_details', param).subscribe(function (response) {
                Object.keys(response).map(function (key) {
                  console.log("Res", response[key].product_details); // this.product_detail = response[key].product_details;

                  _this35.setData(response[key].product_details); // this.getPdfLink();


                  res.dismiss();
                });
              }, function (err) {
                res.dismiss(); // this.presentToastInternert("No internet connection. Please try again later.")

                console.log("err.........", JSON.stringify(err));
              });
            } else {
              res.dismiss(); // this.presentToastInternert("No internet connection. Please try again later.")
            }
          }); // }
          // })
        }
      }, {
        key: "setData",
        value: function setData(value) {
          // console.log("value", value);
          this.productDetails = [];
          this.images = value.Image;

          for (var data in value) {
            if (data == "product_name") {
              // this.productName = value[data][1]
              this.item.ProductTitle = value[data][1];
            }

            if (data == "id") {
              this.item.ID = value[data][1];
            }

            if (data == "sku_model_number") {
              this.item.SKUCode = value[data][1]; // this.SKUCode = value[data][1]
              // if (this.platform.is('cordova')) {
              //   this.firebaseAnalytics.logEvent('product_view', { product: this.SKUCode })
              //   .then((res: any) => console.log(res))
              //   .catch((error: any) => console.error(error));
              // }
            }

            this.item.Image = this.images[0];

            if (data == "mrp") {
              this.item.MRP = value[data][1]; // this.MRP = value[data][1]
            }

            if (data == "mrp_product") {
              this.item.MRP = value[data][1]; // this.MRP = value[data][1]
            } // if (data == "reasons_to_buy") {
            //   this.reasonsToBuyFlage = true
            //   this.reasonsToBuyImageArray = value[data]
            // }
            // if (data == "brochures") {
            //   this.brochures = value[data]
            // }


            if (data != "Image" && data != "id" && data != "mrp" && data != "brochures" && data != "reasons_to_buy" && data != "mrp_product") {
              var textValue = "";

              if (value[data][1] == "") {
                textValue = "-";
              } else {
                textValue = value[data][1];
              }

              var object = {
                title: value[data][0],
                value: textValue
              };
              this.productDetails.push(object);
            }
          }

          this.item.productSpecifications = this.productDetails;
          if (value.usp) this.uspList = value.usp; // console.log("uspList", this.uspList);

          this.item.uspList = this.uspList; // let data = []
          // this.storage.get('favouriteList').then((val) => {
          //   if (val != null) {
          //     if (val.length != 0) {
          //       data = val;
          //       let count = 0;
          //       for (let index in data) {
          //         if (data[index].ID == this.item.ID) {
          //           count = 1;
          //         }
          //       }
          //       if (count == 0) {
          //         this.favouritesFlage = false
          //       } else {
          //         this.favouritesFlage = true
          //       }
          //     } else {
          //       this.favouritesFlage = false
          //     }
          //   } else {
          //     this.favouritesFlage = false
          //   }
          // });

          console.log("this.item", this.item);
          this.addToCompare();
        }
      }, {
        key: "addToCompare",
        value: function addToCompare() {
          var _this36 = this;

          console.log("Enter addToCompare");
          this.storage.get('compareList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                if (val.length < 3) {
                  _this36.storage.get('categaryTitle').then(function (val) {
                    if (val != null) {
                      if (val == _this36.title) {
                        _this36.openpopup(_this36.item);
                      } else {
                        // show popup not add to this categary 
                        // this.showAlertErrorLimit();
                        _this36.openpopupcomparesamecategory();
                      }
                    }
                  });
                } else {
                  _this36.openCompareModal(_this36.item);
                }
              } else {
                _this36.openpopup(_this36.item);
              }
            } else {
              _this36.openpopup(_this36.item);
            }
          });
        }
      }, {
        key: "openpopup",
        value: function openpopup(item) {
          var _this37 = this;

          var data = [];
          this.storage.get('compareList').then(function (val) {
            data = val;
            var count = 0;

            for (var index in data) {
              if (data[index].ID == item.ID) {
                count = 1;

                _this37.showAlertErrorAlreadyAdd();
              }
            }

            if (count == 0) {
              // this.alertAddComapreProduct();
              _this37.addToCompareDataInLocalStorage(_this37.item);
            }
          });
        }
      }, {
        key: "showAlertErrorLimit",
        value: function showAlertErrorLimit() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertController.create({
                      header: "Please add the product of the same category for comparison",
                      cssClass: 'variant-alert size-chooser',
                      buttons: [{
                        text: 'OK',
                        handler: function handler() {
                          // this.removeFromFavourites(id)
                          alert.dismiss();
                        }
                      }]
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "openpopupcomparesamecategory",
        value: function openpopupcomparesamecategory() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var modal;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.modalCtrl.create({
                      component: _popup_compare_same_category_popup_compare_same_category__WEBPACK_IMPORTED_MODULE_18__["popupcomparesamecategory"],
                      componentProps: {},
                      cssClass: "my-modal",
                      backdropDismiss: false
                    });

                  case 2:
                    modal = _context5.sent;
                    _context5.next = 5;
                    return modal.present();

                  case 5:
                    return _context5.abrupt("return", _context5.sent);

                  case 6:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "openCompareModal",
        value: function openCompareModal(item) {
          var _this38 = this;

          var data = [];
          this.storage.get('compareList').then(function (val) {
            data = val;
            var count = 0;

            for (var index in data) {
              if (data[index].ID == item.ID) {
                count = 1;

                _this38.showAlertErrorAlreadyAdd();
              }
            }

            if (count == 0) {
              _this38.openBottomPopup();

              _this38.showBottomPopup = true;
            }
          });
        }
      }, {
        key: "showAlertErrorAlreadyAdd",
        value: function showAlertErrorAlreadyAdd() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var alert;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.alertController.create({
                      header: "Product already added to compare list",
                      cssClass: 'variant-alert size-chooser',
                      buttons: [{
                        text: 'OK',
                        handler: function handler() {
                          // this.removeFromFavourites(id)
                          alert.dismiss();
                        }
                      }]
                    });

                  case 2:
                    alert = _context6.sent;
                    _context6.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "alertAddComapreProduct",
        value: function alertAddComapreProduct() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var _this39 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return this.alertController.create({
                      header: "Are you sure you want to add compare product?",
                      cssClass: 'variant-alert size-chooser',
                      buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: function handler() {
                          alert.dismiss();
                        }
                      }, {
                        text: 'OK',
                        handler: function handler() {
                          _this39.addToCompareDataInLocalStorage(_this39.item);

                          alert.dismiss();
                        }
                      }]
                    });

                  case 2:
                    alert = _context7.sent;
                    _context7.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }, {
        key: "openBottomPopup",
        value: function openBottomPopup() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            var _this40 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.modalCtrl.create({
                      component: _popup_compare_popup_compare__WEBPACK_IMPORTED_MODULE_17__["PopupCompare"],
                      componentProps: {
                        title: this.title
                      },
                      cssClass: "my-modal",
                      backdropDismiss: false
                    });

                  case 2:
                    modal = _context8.sent;
                    modal.onDidDismiss().then(function (modalData) {
                      _this40.checkCompareData();

                      _this40.getCompareList();
                    });
                    _context8.next = 6;
                    return modal.present();

                  case 6:
                    return _context8.abrupt("return", _context8.sent);

                  case 7:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));
        }
      }, {
        key: "addToCompareDataInLocalStorage",
        value: function addToCompareDataInLocalStorage(item) {
          var _this41 = this;

          var data = [];
          this.storage.get('compareList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;

                    _this41.showAlertErrorAlreadyAdd();
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this41.storage.set('compareList', data); // console.log("Push 1");


                  _this41.showBottomPopup = true; // this.openBottomPopup();
                  // Show Compare Flag

                  var data_new = []; // this.storage.get('compareList').then((val) => {
                  //   console.log("compareList", val);

                  _this41.popupCompareList = data; // console.log("popupCompareList", this.popupCompareList);

                  if (data != null) {
                    data_new = data;
                    var _count = 0;

                    for (var _index5 in _this41.product_listing) {
                      for (var _item in data_new) {
                        if (_this41.product_listing[_index5].ID == data_new[_item].ID) {
                          _count = 1;
                        }
                      }

                      if (_count == 0) {
                        _this41.product_listing[_index5].showCompareFlage = false;
                      } else {
                        _this41.product_listing[_index5].showCompareFlage = true;
                      }

                      _count = 0;
                    }
                  } else {
                    for (var _index6 in _this41.product_listing) {
                      _this41.product_listing[_index6].showCompareFlage = false;
                    }
                  } // })
                  // console.log("this.product_listing****3", this.product_listing);
                  // Show Compare Flag End

                }
              } else {
                data.push(item); // console.log("Data", data);

                _this41.storage.set('compareList', data); // console.log("Push 2");


                _this41.showBottomPopup = true; // this.openBottomPopup();
                // this.storage.get('compareList').then((val) => {
                //   console.log("compareList", val);
                // })
                // Show Compare Flag

                var _data_new = []; // this.storage.get('compareList').then((val) => {
                //   console.log("compareList", val);

                _this41.popupCompareList = data; // console.log("popupCompareList", this.popupCompareList);

                if (data != null) {
                  _data_new = data;
                  var _count2 = 0;

                  for (var _index7 in _this41.product_listing) {
                    for (var _item2 in _data_new) {
                      if (_this41.product_listing[_index7].ID == _data_new[_item2].ID) {
                        _count2 = 1;
                      }
                    }

                    if (_count2 == 0) {
                      _this41.product_listing[_index7].showCompareFlage = false;
                    } else {
                      _this41.product_listing[_index7].showCompareFlage = true;
                    }

                    _count2 = 0;
                  }
                } else {
                  for (var _index8 in _this41.product_listing) {
                    _this41.product_listing[_index8].showCompareFlage = false;
                  }
                } // })
                // console.log("this.product_listing****3", this.product_listing);
                // Show Compare Flag End

              }
            } else {
              data.push(item);

              _this41.storage.set('compareList', data);

              _this41.showBottomPopup = true; // Show Compare Flag

              var _data_new2 = [];

              _this41.storage.get('compareList').then(function (val) {
                _this41.popupCompareList = val;

                if (val != null) {
                  _data_new2 = val;
                  var _count3 = 0;

                  for (var _index9 in _this41.product_listing) {
                    for (var _item3 in _data_new2) {
                      if (_this41.product_listing[_index9].ID == _data_new2[_item3].ID) {
                        _count3 = 1;
                      }
                    }

                    if (_count3 == 0) {
                      _this41.product_listing[_index9].showCompareFlage = false;
                    } else {
                      _this41.product_listing[_index9].showCompareFlage = true;
                    }

                    _count3 = 0;
                  }
                } else {
                  for (var _index10 in _this41.product_listing) {
                    _this41.product_listing[_index10].showCompareFlage = false;
                  }
                }
              }); // console.log("this.product_listing****3", this.product_listing);
              // Show Compare Flag End

            }
          });
          this.storage.set('categaryTitle', this.title);
        }
      }, {
        key: "showAlertRemoveProduct",
        value: function showAlertRemoveProduct(id) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            var _this42 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    _context9.next = 2;
                    return this.alertController.create({
                      header: "Are you sure you want to remove product in compare product?",
                      cssClass: 'variant-alert size-chooser',
                      buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: function handler() {
                          alert.dismiss();
                        }
                      }, {
                        text: 'OK',
                        handler: function handler() {
                          _this42.removeProductFromCompareList(id);

                          alert.dismiss();
                        }
                      }]
                    });

                  case 2:
                    alert = _context9.sent;
                    _context9.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }, {
        key: "removeProductFromCompareList",
        value: function removeProductFromCompareList(id) {
          var _this43 = this;

          var data = [];
          this.storage.get('compareList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == id) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 1) {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);
                  _this43.popupCompareList = data;

                  _this43.storage.set('compareList', data);

                  _this43.noRecords = _this43.popupCompareList.length;

                  _this43.refreshCompareData(data);
                }
              }
            }
          });
        }
      }, {
        key: "checkCompareData",
        value: function checkCompareData() {
          var _this44 = this;

          // Show Compare Flag
          console.log("check Compare Data");
          var data = [];
          this.storage.get('compareList').then(function (val) {
            if (val != null) {
              data = val;
              var count = 0;

              for (var index in _this44.product_listing) {
                for (var item in data) {
                  if (_this44.product_listing[index].ID == data[item].ID) {
                    count = 1;
                  }
                }

                if (count == 0) {
                  _this44.product_listing[index].showCompareFlage = false;
                } else {
                  _this44.product_listing[index].showCompareFlage = true;
                }

                count = 0;
              }
            } else {
              for (var _index11 in _this44.product_listing) {
                _this44.product_listing[_index11].showCompareFlage = false;
              }
            }
          }); // console.log("this.product_listing********", this.product_listing);
          // Show Compare Flag End
        }
      }, {
        key: "refreshCompareData",
        value: function refreshCompareData(data) {
          // Show Compare Flag
          var data_new = [];
          this.popupCompareList = data;
          console.log("popupCompareList", this.popupCompareList);

          if (data != null) {
            data_new = data;
            var count = 0;

            for (var index in this.product_listing) {
              for (var item in data_new) {
                if (this.product_listing[index].ID == data_new[item].ID) {
                  count = 1;
                }
              }

              if (count == 0) {
                this.product_listing[index].showCompareFlage = false;
              } else {
                this.product_listing[index].showCompareFlage = true;
              }

              count = 0;
            }
          } else {
            for (var _index12 in this.product_listing) {
              this.product_listing[_index12].showCompareFlage = false;
            }
          }

          console.log("refresh product_listing", this.product_listing); // Show Compare Flag End
        }
      }, {
        key: "getCompareList",
        value: function getCompareList() {
          var _this45 = this;

          this.storage.get('compareList').then(function (val) {
            _this45.popupCompareList = val;
          });
        }
      }, {
        key: "onClickCompareNow",
        value: function onClickCompareNow() {
          this.showBottomPopup = false;
          var navigationExtras = {
            queryParams: {
              title: this.title
            }
          };
          this.router.navigate(['compare/'], navigationExtras);
        }
      }, {
        key: "closeBottomPopup",
        value: function closeBottomPopup() {
          this.showBottomPopup = false;
        }
      }, {
        key: "isShell",
        get: function get() {
          return this.listing && this.listing.isShell ? true : false;
        }
      }]);

      return ProductListingPage;
    }();

    ProductListingPage.ɵfac = function ProductListingPage_Factory(t) {
      return new (t || ProductListingPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["PopoverController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_9__["SQLite"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_13__["Downloader"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_15__["SocialSharing"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_14__["FirebaseAnalytics"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_16__["InAppBrowser"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_categories_categories_page__WEBPACK_IMPORTED_MODULE_10__["CategoriesPage"]));
    };

    ProductListingPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: ProductListingPage,
      selectors: [["app-product-listing"]],
      viewQuery: function ProductListingPage_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonInfiniteScroll"], true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstaticViewQuery"](_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonContent"], true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.infiniteScroll = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.content = _t.first);
        }
      },
      hostVars: 2,
      hostBindings: function ProductListingPage_HostBindings(rf, ctx) {
        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("is-shell", ctx.isShell);
        }
      },
      decls: 37,
      vars: 13,
      consts: [["color", "primary"], ["slot", "start"], ["class", "bedge-notification", 4, "ngIf"], ["src", "assets/logo.png", 1, "logo"], ["slot", "end"], ["name", "search-outline", 1, "search_icon", 3, "click"], ["name", "heart-outline", 1, "heart_icon", 3, "click"], ["name", "home-outline", 1, "home_icon", 3, "routerLink"], ["slot", "fixed", 3, "ionRefresh"], ["pullingIcon", "arrow-down", "pullingText", "Pull to refresh", "refreshingSpinner", "circles", "refreshingText", "Refreshing..."], [1, "div-fixed"], [2, "overflow-x", "auto", "white-space", "nowrap", "border-top", "1px solid #09509d"], [1, "tabCss", 3, "ngClass", "click"], [2, "border-top", "1px solid #09509d"], ["size", "6", 1, "subHeaderCSS", 2, "padding-right", "0px"], [4, "ngIf"], ["size", "6", 1, "subHeaderCSS", 3, "click"], ["name", "funnel-outline", 1, "icon_funnel"], ["class", "filter-bedge", 4, "ngIf"], [1, "divMargin", 3, "ngClass"], ["style", "border-bottom: 1px solid #09509d;", 4, "ngFor", "ngForOf"], ["threshold", "100px", 3, "ionInfinite"], ["loadingSpinner", "bubbles", "loadingText", "Loading more products..."], ["class", "div-footer", 4, "ngIf"], [1, "bedge-notification"], [1, "filter-bedge"], [2, "border-bottom", "1px solid #09509d"], [1, "split", 2, "width", "100%"], [1, "column", "image-center", 2, "position", "relative"], [1, "image-anchor"], ["class", "item-image", "animation", "spinner", 3, "src", "alt", 4, "ngIf"], ["class", "item-image", "src", "assets/default_product_image.png", 4, "ngIf"], ["class", "compare-flage-align", 4, "ngIf"], [1, "column", 2, "width", "70%"], [1, "centered", 3, "click"], [2, "margin-bottom", "5px"], ["size", "4", 2, "text-align", "center", "color", "#09509d", 3, "click"], ["name", "download-outline", 1, "icon-download"], ["size", "5", 2, "text-align", "center", "color", "#09509d", 3, "click"], ["name", "heart-outline", "class", "icon-heart-black", 4, "ngIf"], ["name", "heart", "class", "icon-heart-red", 4, "ngIf"], ["size", "3", 2, "text-align", "center", "color", "#09509d", "padding-top", "3px", 3, "click"], ["src", "assets/share.png", 1, "icon-share"], ["animation", "spinner", 1, "item-image", 3, "src", "alt"], ["src", "assets/default_product_image.png", 1, "item-image"], [1, "compare-flage-align"], ["class", "label-compare", 3, "click", 4, "ngIf"], ["class", "label-added", 3, "click", 4, "ngIf"], [1, "label-compare", 3, "click"], [1, "label-added", 3, "click"], ["name", "checkmark", 1, "icon-checkmark"], ["name", "heart-outline", 1, "icon-heart-black"], ["name", "heart", 1, "icon-heart-red"], [2, "font-size", "20px", "text-align", "center", "margin-top", "50%"], [1, "div-footer"], [1, "row-border-top"], ["size", "4", "style", "text-align: center;", 4, "ngFor", "ngForOf"], [2, "text-align", "center", 3, "click"], [1, "btn-compare"], ["size", "4", 2, "text-align", "center"], ["name", "close-circle", 1, "icon-remove", 3, "click"], [2, "height", "60px", 3, "src"], [1, "product-title"]],
      template: function ProductListingPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, ProductListingPage_ion_badge_3_Template, 2, 1, "ion-badge", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "ion-menu-button");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "img", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "ion-buttons", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "ion-icon", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_Template_ion_icon_click_7_listener() {
            return ctx.onClickSearch(ctx.title);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "ion-icon", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_Template_ion_icon_click_8_listener() {
            return ctx.onClickFavouritesIcon();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "ion-icon", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "ion-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "ion-refresher", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ionRefresh", function ProductListingPage_Template_ion_refresher_ionRefresh_11_listener($event) {
            return ctx.doRefresh($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "ion-refresher-content", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_Template_div_click_15_listener() {
            return ctx.goToProductListPage("Air Conditioners");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Air Conditioners");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_Template_div_click_17_listener() {
            return ctx.goToProductListPage("Air Coolers");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, " Air Coolers");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_Template_div_click_19_listener() {
            return ctx.goToProductListPage("Air Purifiers");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20, "Air Purifiers");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_Template_div_click_21_listener() {
            return ctx.goToProductListPage("Water Purifiers");
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, "Water Purifiers");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "ion-row", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "ion-col", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](25, ProductListingPage_span_25_Template, 2, 1, "span", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "ion-col", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductListingPage_Template_ion_col_click_26_listener() {
            return ctx.openFilterModal();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "ion-icon", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](28, ProductListingPage_div_28_Template, 1, 0, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](30, "Filter");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](31, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](32, ProductListingPage_div_32_Template, 28, 8, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](33, ProductListingPage_div_33_Template, 3, 0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "ion-infinite-scroll", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ionInfinite", function ProductListingPage_Template_ion_infinite_scroll_ionInfinite_34_listener($event) {
            return ctx.loadData($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](35, "ion-infinite-scroll-content", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](36, ProductListingPage_div_36_Template, 2, 1, "div", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.countNotification != 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](12, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.airConditionerFlage ? "tabCssClick" : "tabCss");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.airCoolerFlage ? "tabCssClick" : "tabCss");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.airPurifierFlage ? "tabCssClick" : "tabCss");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.waterPurifierFlage ? "tabCssClick" : "tabCss");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.total_product > 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.flagShowFilter == true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx.showBottomPopup ? "divMarginClick" : "divMargin");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.product_listing);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.emptyListFlage);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.showBottomPopup);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonButtons"], _angular_common__WEBPACK_IMPORTED_MODULE_19__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonMenuButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonIcon"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["RouterLinkDelegate"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLink"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonRefresher"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonRefresherContent"], _angular_common__WEBPACK_IMPORTED_MODULE_19__["NgClass"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonCol"], _angular_common__WEBPACK_IMPORTED_MODULE_19__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonInfiniteScroll"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonInfiniteScrollContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonBadge"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_20__["ImageShellComponent"]],
      styles: ["[_nghost-%COMP%] {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background);\n  --page-items-gutter: calc(var(--page-margin) / 2);\n  --page-color: #cb328f;\n}\n\n.fashion-listing-content[_ngcontent-%COMP%] {\n  --background: var(--page-background);\n  --padding-start: var(--page-margin);\n  --padding-end: var(--page-margin);\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin);\n}\n\n.items-row[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: 0;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%] {\n  margin-bottom: calc(var(--page-margin) * 1);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]:nth-child(odd) {\n  padding-right: var(--page-items-gutter);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]:nth-child(even) {\n  padding-left: var(--page-items-gutter);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%] {\n  border: 1px solid #09509d;\n  text-align: center;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%]   .image-anchor[_ngcontent-%COMP%] {\n  display: block;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: 0px;\n  padding: 5px 5px 0px;\n  text-align: center;\n  min-height: 68px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%] {\n  margin-bottom: calc(var(--page-margin) / 2);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%] {\n  margin: 0px;\n  font-size: 14px;\n  font-weight: 400;\n  text-overflow: ellipsis;\n  white-space: unset;\n  overflow: hidden;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%]   .name-anchor[_ngcontent-%COMP%] {\n  color: var(--ion-color-primary);\n  display: block;\n  text-decoration: none;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%] {\n  align-items: center;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%] {\n  padding-bottom: 5px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:first-child {\n  padding-right: calc(var(--page-margin) / 2);\n  text-align: right;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:first-child:last-child {\n  text-align: center;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:last-child {\n  padding-left: calc(var(--page-margin) / 2);\n  text-align: left;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .separator[_ngcontent-%COMP%] {\n  max-width: 0px;\n  border-right: solid 2px var(--ion-color-light-shade);\n  align-self: stretch;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .item-sale-price[_ngcontent-%COMP%] {\n  display: block;\n  font-weight: 400;\n  font-size: 14px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .item-original-price[_ngcontent-%COMP%] {\n  display: block;\n  text-decoration: line-through;\n  color: var(--ion-color-medium-shade);\n  font-size: 14px;\n}\n\nion-header[_ngcontent-%COMP%]   ion-toolbar[_ngcontent-%COMP%]:first-of-type {\n  text-align: center;\n}\n\n.logo[_ngcontent-%COMP%] {\n  width: 80%;\n  margin-top: 5px;\n}\n\n.home_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 10px;\n}\n\n.search_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 10px;\n}\n\n.icon-heart-red[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-bottom: -8px;\n  padding-right: 3px;\n}\n\n.icon-heart-black[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-bottom: -8px;\n  padding-right: 3px;\n}\n\n.icon-share[_ngcontent-%COMP%] {\n  width: 29px;\n  margin-bottom: -7px;\n  padding-right: 3px;\n}\n\n.icon-download[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-bottom: -4px;\n  padding-right: 3px;\n}\n\n.col-padding[_ngcontent-%COMP%] {\n  padding-top: 8px !important;\n  padding-bottom: 8px !important;\n  text-align: center;\n  color: #09509d;\n}\n\n.subHeaderCSS[_ngcontent-%COMP%] {\n  padding-top: 8px !important;\n  padding-bottom: 8px !important;\n  text-align: center;\n}\n\n.subHeaderCssClick[_ngcontent-%COMP%] {\n  padding-top: 8px !important;\n  padding-bottom: 8px !important;\n  text-align: center;\n  color: #09509d;\n  text-decoration: underline;\n  font-weight: 600;\n}\n\n.main-title[_ngcontent-%COMP%] {\n  color: #09509d;\n  font-weight: bold;\n}\n\n.icon-compare[_ngcontent-%COMP%] {\n  font-size: 20px;\n  margin-bottom: -6px;\n  padding-right: 3px;\n}\n\n.icon_funnel[_ngcontent-%COMP%] {\n  font-size: 20px;\n  margin-bottom: -6px;\n  padding-right: 3px;\n}\n\n.text-share[_ngcontent-%COMP%] {\n  color: #09509d;\n}\n\n.img-product[_ngcontent-%COMP%] {\n  width: 190px !important;\n  height: 127px !important;\n  -o-object-fit: contain !important;\n     object-fit: contain !important;\n}\n\n.product-text[_ngcontent-%COMP%] {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n\n\n.split[_ngcontent-%COMP%] {\n  height: 25%;\n  width: 100%;\n  z-index: 1;\n  position: relative;\n  overflow-x: hidden;\n}\n\n\n\n\n\n.column[_ngcontent-%COMP%] {\n  float: left;\n  padding: 10px;\n  height: 100%;\n  \n}\n\n.centered[_ngcontent-%COMP%] {\n  color: balck;\n}\n\n.icon-delete[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 90%;\n  transform: translate(-50%, -50%);\n  font-size: 25px;\n  color: #09509d;\n}\n\n.tabCssClick[_ngcontent-%COMP%] {\n  border-right: 1px solid #09509d !important;\n  border-left: 1px solid #09509d !important;\n  padding: 10px 15px;\n  display: inline-block;\n  font-size: 14px;\n  text-align: center;\n  font-weight: 600;\n  text-decoration: underline;\n  color: #09509d;\n}\n\n.tabCss[_ngcontent-%COMP%] {\n  border-right: 1px solid #eae4e4;\n  border-left: 1px solid #eae4e4;\n  padding: 10px 15px;\n  display: inline-block;\n  font-size: 14px;\n  text-align: center;\n  font-weight: 600;\n}\n\n.div-fixed[_ngcontent-%COMP%] {\n  position: fixed;\n  z-index: 999;\n  background-color: white;\n  border-bottom: 1px solid #09509d;\n  width: 100%;\n  overflow-y: auto;\n}\n\n.bedge-notification[_ngcontent-%COMP%] {\n  background-color: red;\n  border-radius: 50%;\n  position: absolute;\n  left: 24px;\n  top: 3px;\n  z-index: 999;\n  height: 23px;\n  width: 23px;\n}\n\n.image-center[_ngcontent-%COMP%] {\n  width: 30%;\n  display: flex;\n  align-items: center;\n  vertical-align: middle;\n  min-height: 150px;\n}\n\n.filter-bedge[_ngcontent-%COMP%] {\n  background-color: #09509d;\n  height: 10px;\n  width: 10px;\n  border-radius: 20px;\n  position: absolute;\n  top: 5px;\n  right: 55%;\n}\n\n.label-compare[_ngcontent-%COMP%] {\n  border: 1px solid black;\n  padding: 5px;\n  z-index: 999;\n  position: absolute;\n  background-color: white;\n}\n\n.label-added[_ngcontent-%COMP%] {\n  padding: 5px;\n  z-index: 999;\n  position: absolute;\n  background-color: #03A9F4;\n  color: white;\n  display: inline-flex;\n}\n\n.icon-checkmark[_ngcontent-%COMP%] {\n  font-size: 20px;\n}\n\n.compare-flage-align[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 80px;\n  left: 0;\n  right: 0;\n  margin: auto;\n}\n\n.div-footer[_ngcontent-%COMP%] {\n  position: fixed;\n  bottom: 0px;\n  z-index: 999;\n  width: 100%;\n  background-color: white;\n}\n\n.row-margin[_ngcontent-%COMP%] {\n  margin-top: 10px;\n}\n\n.icon-remove[_ngcontent-%COMP%] {\n  position: absolute;\n  z-index: 999;\n  font-size: 1.4em;\n  color: grey;\n  right: 5px;\n}\n\n.product-title[_ngcontent-%COMP%] {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n.row-border-top[_ngcontent-%COMP%] {\n  border-top: 1px solid #09509d;\n}\n\n.btn-compare[_ngcontent-%COMP%] {\n  margin-bottom: 8px;\n  margin-top: 8px;\n  font-weight: 600;\n  color: #09509d;\n  font-size: 18px;\n}\n\n.text-msg[_ngcontent-%COMP%] {\n  text-align: center;\n  margin: 15px;\n}\n\n.bottom-popup-title[_ngcontent-%COMP%] {\n  background-color: #09509d;\n  color: white;\n  font-size: 15px;\n}\n\n.bottom-popup-title-padding[_ngcontent-%COMP%] {\n  padding: 15px 10px 10px 10px;\n  font-size: 16px;\n}\n\n.divMargin[_ngcontent-%COMP%] {\n  margin-top: 75px;\n}\n\n.divMarginClick[_ngcontent-%COMP%] {\n  margin-top: 75px;\n  margin-bottom: 180px;\n}\n\n.icon-close[_ngcontent-%COMP%] {\n  font-size: 30px;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvcHJvZHVjdC9saXN0aW5nL3N0eWxlcy9wcm9kdWN0LWxpc3RpbmcucGFnZS5zY3NzIiwic3JjL2FwcC9wcm9kdWN0L2xpc3Rpbmcvc3R5bGVzL3Byb2R1Y3QtbGlzdGluZy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxxQ0FBQTtFQUNBLHdDQUFBO0VBRUEsaURBQUE7RUFDQSxxQkFBQTtBQ0ZGOztBRE1BO0VBQ0Usb0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGlDQUFBO0VBQ0EsaUNBQUE7RUFDQSxvQ0FBQTtBQ0hGOztBRE1BO0VBQ0UsNEJBQUE7QUNIRjs7QURLRTtFQUNFLDJDQUFBO0FDSEo7O0FES0k7RUFDRSx1Q0FBQTtBQ0hOOztBRE1JO0VBQ0Usc0NBQUE7QUNKTjs7QURPSTtFQUVFLHlCQUFBO0VBQ0Esa0JBQUE7QUNOTjs7QURPTTtFQUNFLGNBQUE7QUNMUjs7QURTSTtFQUNFLDhCQUFBO0VBRUEsb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDUk47O0FEU007RUFDRSwyQ0FBQTtBQ1BSOztBRFNRO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUVBLHVCQUFBO0VBRUEsa0JBQUE7RUFDQSxnQkFBQTtBQ1RWOztBRFdVO0VBQ0UsK0JBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7QUNUWjs7QURjTTtFQUNFLG1CQUFBO0FDWlI7O0FEY1E7RUFDRSxtQkFBQTtBQ1pWOztBRGFVO0VBQ0UsMkNBQUE7RUFDQSxpQkFBQTtBQ1haOztBRGFZO0VBQ0Usa0JBQUE7QUNYZDs7QURlVTtFQUNFLDBDQUFBO0VBQ0EsZ0JBQUE7QUNiWjs7QURpQlE7RUFDRSxjQUFBO0VBQ0Esb0RBQUE7RUFDQSxtQkFBQTtBQ2ZWOztBRGtCUTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUdBLGVBQUE7QUNsQlY7O0FEdUJRO0VBQ0UsY0FBQTtFQUNBLDZCQUFBO0VBQ0Esb0NBQUE7RUFDQSxlQUFBO0FDckJWOztBRDJCQTtFQUNFLGtCQUFBO0FDeEJGOztBRDBCQTtFQUVFLFVBQUE7RUFDQSxlQUFBO0FDeEJGOztBRDBCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQ3ZCRjs7QUR5QkE7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUN0QkY7O0FEd0JBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDckJGOztBRHVCQTtFQVNFLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDNUJGOztBRDhCQTtFQVNFLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDbkNGOztBRDZDQTtFQU9FLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDaERGOztBRG1EQTtFQVVFLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDekRGOztBRDREQTtFQUdFLDJCQUFBO0VBQ0EsOEJBQUE7RUFLQSxrQkFBQTtFQUNBLGNBQUE7QUMvREY7O0FEa0VBO0VBQ0UsMkJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0FDL0RGOztBRG1FQTtFQUNFLDJCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSwwQkFBQTtFQUNBLGdCQUFBO0FDaEVGOztBRG1FQTtFQUVFLGNBQUE7RUFDQSxpQkFBQTtBQ2pFRjs7QURvRUE7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ2pFRjs7QURtRUE7RUFNRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ3JFRjs7QUR5RUE7RUFJRSxjQUFBO0FDekVGOztBRDRFQTtFQUNFLHVCQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQ0FBQTtLQUFBLDhCQUFBO0FDekVGOztBRDJFQztFQUNDLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQ3hFRjs7QUQ0RUEsNkJBQUE7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUVBLGtCQUFBO0FDMUVGOztBRGlGQSwwQkFBQTs7QUFFQSw4REFBQTs7QUFDQTtFQUNFLFdBQUE7RUFFQSxhQUFBO0VBQ0EsWUFBQTtFQUFjLDhDQUFBO0FDL0VoQjs7QURtRkE7RUFNRSxZQUFBO0FDckZGOztBRHVGQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDcEZGOztBRHlGQTtFQUNFLDBDQUFBO0VBQ0EseUNBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLGNBQUE7QUN0RkY7O0FEeUZBO0VBQ0UsK0JBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ3RGRjs7QUR5RkE7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUN0RkY7O0FEeUZBO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUVBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUN2RkY7O0FEMEZBO0VBQ0UsVUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7QUN2RkY7O0FEMEZBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFFQSxRQUFBO0VBQ0EsVUFBQTtBQ3hGRjs7QUQyRkE7RUFDRSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtBQ3hGRjs7QUQyRkE7RUFFRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUN6RkY7O0FEMkZBO0VBQ0UsZUFBQTtBQ3hGRjs7QUQyRkE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7QUN4RkY7O0FENEZBO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0FDekZGOztBRDRGQTtFQUNFLGdCQUFBO0FDekZGOztBRDZGQTtFQUNFLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7QUMxRkY7O0FENkZBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0FDMUZGOztBRDZGQTtFQUNFLDZCQUFBO0FDMUZGOztBRDZGQTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUMxRkY7O0FENkZBO0VBRUUsa0JBQUE7RUFDQSxZQUFBO0FDM0ZGOztBRGlHQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUM5RkY7O0FEaUdBO0VBR0UsNEJBQUE7RUFDQSxlQUFBO0FDaEdGOztBRG1HQTtFQUNFLGdCQUFBO0FDaEdGOztBRG9HQTtFQUNFLGdCQUFBO0VBQ0Esb0JBQUE7QUNqR0Y7O0FEb0dBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0FDakdGIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdC9saXN0aW5nL3N0eWxlcy9wcm9kdWN0LWxpc3RpbmcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XG4gIC0tcGFnZS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgLS1wYWdlLWl0ZW1zLWd1dHRlcjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgLS1wYWdlLWNvbG9yOiAjY2IzMjhmO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi5mYXNoaW9uLWxpc3RpbmctY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIC0tcGFkZGluZy1lbmQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgLS1wYWRkaW5nLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAtLXBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG59XG5cbi5pdGVtcy1yb3cge1xuICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwO1xuXG4gIC5saXN0aW5nLWl0ZW0ge1xuICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMSk7XG5cbiAgICAmOm50aC1jaGlsZChvZGQpIHtcbiAgICAgIHBhZGRpbmctcmlnaHQ6IHZhcigtLXBhZ2UtaXRlbXMtZ3V0dGVyKTtcbiAgICB9XG5cbiAgICAmOm50aC1jaGlsZChldmVuKSB7XG4gICAgICBwYWRkaW5nLWxlZnQ6IHZhcigtLXBhZ2UtaXRlbXMtZ3V0dGVyKTtcbiAgICB9XG5cbiAgICAuaXRlbS1pbWFnZS13cmFwcGVyIHtcbiAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDk1MDlkO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgLmltYWdlLWFuY2hvciB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5pdGVtLWJvZHkge1xuICAgICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xuXG4gICAgICBwYWRkaW5nOiA1cHggNXB4IDBweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIG1pbi1oZWlnaHQ6IDY4cHg7ICAgLy8qKioqKm1lXG4gICAgICAubWFpbi1pbmZvIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblxuICAgICAgICAuaXRlbS1uYW1lIHtcbiAgICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcblxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICAgIC8vIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgd2hpdGUtc3BhY2U6IHVuc2V0O1xuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgICAgICAgICAubmFtZS1hbmNob3Ige1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAuc2Vjb25kYXJ5LWluZm8ge1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgICAgIC5wcmljZS1jb2wge1xuICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgJjpmaXJzdC1jaGlsZCB7XG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG5cbiAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuc2VwYXJhdG9yIHtcbiAgICAgICAgICBtYXgtd2lkdGg6IDBweDtcbiAgICAgICAgICBib3JkZXItcmlnaHQ6IHNvbGlkIDJweCB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xuICAgICAgICAgIGFsaWduLXNlbGY6IHN0cmV0Y2g7XG4gICAgICAgIH1cblxuICAgICAgICAuaXRlbS1zYWxlLXByaWNlIHtcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgIC8vIGNvbG9yOiB2YXIoLS1wYWdlLWNvbG9yKTtcbiAgICAgICAgICAvLyBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgIC8vIGNvbG9yOiBncmV5O1xuICAgICAgICAgIFxuICAgICAgICB9XG5cbiAgICAgICAgLml0ZW0tb3JpZ2luYWwtcHJpY2Uge1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlKTtcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZXtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmxvZ297XG4gIC8vIHdpZHRoOiA2NSU7XG4gIHdpZHRoOiA4MCU7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5ob21lX2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4uaGVhcnRfaWNvbntcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4uc2VhcmNoX2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLmljb24taGVhcnQtcmVke1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIC8vIHBhZGRpbmctdG9wOiA1cHg7XG4gIC8vIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gY29sb3I6ICMwOTUwOWQ7XG5cbiAgLy8gZm9udC1zaXplOiAyNXB4O1xuICAvLyBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtOHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG4uaWNvbi1oZWFydC1ibGFja3tcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogNXB4O1xuICAvLyBmb250LXNpemU6IDI1cHg7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xuXG4gIC8vIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gY29sb3I6ICMwOTUwOWQ7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogLThweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuLy8gLmljb24tc2hhcmV7XG4vLyAgIGZsb2F0OiByaWdodDtcbi8vICAgcGFkZGluZy10b3A6IDVweDtcbi8vICAgZm9udC1zaXplOiAyNXB4O1xuLy8gICBjb2xvcjogIzA5NTA5ZDtcbi8vICAgcGFkZGluZy1yaWdodDogMTBweDtcbi8vIH1cblxuLmljb24tc2hhcmV7XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgLy8gcGFkZGluZy10b3A6IDJweDtcbiAgLy8gY29sb3I6ICMwOTUwOWQ7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIC8vIHdpZHRoOiAxMiU7XG5cbiAgd2lkdGg6IDI5cHg7XG4gIG1hcmdpbi1ib3R0b206IC03cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cblxuLmljb24tZG93bmxvYWR7XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgLy8gcGFkZGluZy10b3A6IDVweDtcbiAgLy8gZm9udC1zaXplOiAyNXB4O1xuICAvLyBjb2xvcjogIzA5NTA5ZDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcblxuICAvLyBmb250LXNpemU6IDI1cHg7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xuXG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgbWFyZ2luLWJvdHRvbTogLTRweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uY29sLXBhZGRpbmd7XG4gIC8vIHBhZGRpbmctbGVmdDogMTBweCAhaW1wb3J0YW50O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxNXB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiA4cHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1ib3R0b206IDhweCAhaW1wb3J0YW50O1xuICAvLyBib3JkZXItdG9wOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwOTUwOWQ7XG4gIC8vIG1hcmdpbi1ib3R0b206IDVweDtcbiAgLy8gbWFyZ2luLXRvcDogLTEwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICMwOTUwOWQ7XG59XG5cbi5zdWJIZWFkZXJDU1N7XG4gIHBhZGRpbmctdG9wOiA4cHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1ib3R0b206IDhweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xufVxuXG4uc3ViSGVhZGVyQ3NzQ2xpY2t7XG4gIHBhZGRpbmctdG9wOiA4cHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1ib3R0b206IDhweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1haW4tdGl0bGV7XG4gIC8vIGZvbnQtc2l6ZTogMTdweDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uaWNvbi1jb21wYXJle1xuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IC02cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cbi5pY29uX2Z1bm5lbHtcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBmb250LXNpemU6IDEuNWVtO1xuICAvLyBjb2xvcjogIzA5NTA5ZDtcbiAgLy8gbWFyZ2luLXRvcDogLTNweDtcblxuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IC02cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cblxuXG4udGV4dC1zaGFyZXtcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBwYWRkaW5nLXRvcDogMTBweDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTVweDtcbiAgY29sb3I6ICMwOTUwOWQ7XG59XG5cbi5pbWctcHJvZHVjdHtcbiAgd2lkdGg6IDE5MHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTI3cHggIWltcG9ydGFudDtcbiAgb2JqZWN0LWZpdDogY29udGFpbiAhaW1wb3J0YW50O1xufVxuIC5wcm9kdWN0LXRleHR7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuIH1cblxuIC8vICoqKioqKioqKioqKioqKioqKioqKlxuLyogU3BsaXQgdGhlIHNjcmVlbiBpbiBoYWxmICovXG4uc3BsaXQge1xuICBoZWlnaHQ6IDI1JTtcbiAgd2lkdGg6IDEwMCU7XG4gIHotaW5kZXg6IDE7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLy8gdG9wOiAwO1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG4gIC8vIHBhZGRpbmctdG9wOiAyMHB4O1xuICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VhZTRlNDtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwOTUwOWQ7XG4gIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCAjOUU5RTlFO1xufVxuXG4vKiBDb250cm9sIHRoZSBsZWZ0IHNpZGUgKi9cblxuLyogQ3JlYXRlIHRocmVlIGVxdWFsIGNvbHVtbnMgdGhhdCBmbG9hdHMgbmV4dCB0byBlYWNoIG90aGVyICovXG4uY29sdW1uIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIC8vIHdpZHRoOiAzMy4zMyU7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGhlaWdodDogMTAwJTsgLyogU2hvdWxkIGJlIHJlbW92ZWQuIE9ubHkgZm9yIGRlbW9uc3RyYXRpb24gKi9cbiAgLy8gYm9yZGVyOiAxcHggc29saWQ7XG59XG5cbi5jZW50ZXJlZCB7XG4gIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLy8gdG9wOiA1MCU7XG4gIC8vIGxlZnQ6IDUwJTtcbiAgLy8gdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIC8vIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6IGJhbGNrO1xufVxuLmljb24tZGVsZXRle1xuICBwb3NpdGlvbjogYWJzb2x1dGU7IFxuICB0b3A6IDUwJTsgXG4gIGxlZnQ6IDkwJTsgXG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGNvbG9yOiAjMDk1MDlkO1xufVxuXG4vLyB0YWIgY3NzXG5cbi50YWJDc3NDbGlja3tcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzA5NTA5ZCAhaW1wb3J0YW50O1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICMwOTUwOWQgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogNjAwO1xuICB0ZXh0LWRlY29yYXRpb246dW5kZXJsaW5lO1xuICBjb2xvcjogIzA5NTA5ZDtcbn1cblxuLnRhYkNzc3tcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2VhZTRlNDtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZWFlNGU0O1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5kaXYtZml4ZWR7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogOTk5O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwOTUwOWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy15OiBhdXRvO1xufVxuXG4uYmVkZ2Utbm90aWZpY2F0aW9ue1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7IFxuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIC8vIHBhZGRpbmc6IDVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAyNHB4O1xuICB0b3A6IDNweDtcbiAgei1pbmRleDogOTk5O1xuICBoZWlnaHQ6IDIzcHg7XG4gIHdpZHRoOiAyM3B4O1xufVxuXG4uaW1hZ2UtY2VudGVye1xuICB3aWR0aDogMzAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBtaW4taGVpZ2h0OiAxNTBweDtcbn1cblxuLmZpbHRlci1iZWRnZXtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzA5NTA5ZDtcbiAgaGVpZ2h0OiAxMHB4O1xuICB3aWR0aDogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICAvLyByaWdodDogMTBweDtcbiAgdG9wOiA1cHg7XG4gIHJpZ2h0OiA1NSU7XG59XG5cbi5sYWJlbC1jb21wYXJle1xuICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbiAgcGFkZGluZzogNXB4O1xuICB6LWluZGV4OiA5OTk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5sYWJlbC1hZGRlZHtcbiAgLy8gYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIHBhZGRpbmc6IDVweDtcbiAgei1pbmRleDogOTk5O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwM0E5RjQ7O1xuICBjb2xvcjogd2hpdGU7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuLmljb24tY2hlY2ttYXJre1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5jb21wYXJlLWZsYWdlLWFsaWdue1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiA4MHB4O1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG5cbi5kaXYtZm9vdGVye1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJvdHRvbTogMHB4O1xuICB6LWluZGV4OiA5OTk7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLnJvdy1tYXJnaW57XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMDk1MDlkO1xufVxuXG4uaWNvbi1yZW1vdmV7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTk5O1xuICBmb250LXNpemU6IDEuNGVtO1xuICBjb2xvcjogZ3JleTtcbiAgcmlnaHQ6IDVweDtcbn1cblxuLnByb2R1Y3QtdGl0bGV7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG4ucm93LWJvcmRlci10b3B7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMDk1MDlkO1xufVxuXG4uYnRuLWNvbXBhcmV7XG4gIG1hcmdpbi1ib3R0b206IDhweDtcbiAgbWFyZ2luLXRvcDogOHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuXG4udGV4dC1tc2d7XG4gIC8vIGZvbnQtc2l6ZTogMTdweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDE1cHg7XG4gIC8vIG1hcmdpbi10b3A6IDIwJTtcbiAgLy8gbWFyZ2luLWxlZnQ6IDE1cHg7XG4gIC8vIG1hcmdpbi1yaWdodDogMTVweDtcbiAgLy8gZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5ib3R0b20tcG9wdXAtdGl0bGV7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwOTUwOWQ7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uYm90dG9tLXBvcHVwLXRpdGxlLXBhZGRpbmd7XG4gIC8vIHBhZGRpbmctbGVmdDogMTBweDtcbiAgLy8gcGFkZGluZy1yaWdodDogMHB4O1xuICBwYWRkaW5nOiAxNXB4IDEwcHggMTBweCAxMHB4O1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5kaXZNYXJnaW57XG4gIG1hcmdpbi10b3A6IDc1cHg7XG4gIFxufVxuXG4uZGl2TWFyZ2luQ2xpY2t7XG4gIG1hcmdpbi10b3A6IDc1cHg7XG4gIG1hcmdpbi1ib3R0b206IDE4MHB4O1xufVxuXG4uaWNvbi1jbG9zZXtcbiAgZm9udC1zaXplOiAzMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi8vIC5kaXYtaGVpZ2h0e1xuLy8gICBoZWlnaHQ6IDUwcHg7XG4vLyB9XG5cbi8vIC5kaXYtaGVpZ2h0LWNsaWNre1xuLy8gICBtaW4taGVpZ2h0OiA4MHB4O1xuLy8gfVxuXG4vLyAuYm90dG9tLXBvcHVwLXRpdGxlLWFsaWdue1xuLy8gICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4vLyAgIHBhZGRpbmc6IDEwcHggIWltcG9ydGFudDtcbi8vIH1cbi8vIC5jbG9zZV9pY29ue1xuLy8gICBmb250LXNpemU6IDEuOGVtO1xuLy8gICBmbG9hdDogcmlnaHQ7XG4vLyAgIGNvbG9yOiB3aGl0ZTtcbi8vIH1cbi8vIC5yb3ctcG9wdXB7XG4vLyAgIGJvcmRlcjogMXB4IHNvbGlkICMwOTUwOWQ7XG4vLyAgIC8vIGJvcmRlci1yYWRpdXM6IDIlO1xuLy8gICAvLyBtYXJnaW46IDEwcHg7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuLy8gICBwb3NpdGlvbjogZml4ZWQ7XG4vLyAgIC8vIHRvcDogMzAlO1xuLy8gICBib3R0b206IDBweDtcbi8vICAgei1pbmRleDogOTk5O1xuLy8gICB3aWR0aDogMTAwJVxuLy8gfVxuXG4vLyAuY29sLWJhY2tncm91bmR7XG4vLyAgIGJhY2tncm91bmQtY29sb3I6ICMwOTUwOWQ7XG4vLyB9XG4vLyAudGl0bGUtcG9wdXB7XG4vLyAgIHBhZGRpbmctbGVmdDogNXB4O1xuLy8gICBwYWRkaW5nLXRvcDogNXB4O1xuLy8gICBmb250LXdlaWdodDogNjAwO1xuLy8gICBjb2xvcjogd2hpdGVcbi8vICB9XG4gXG4vLyAgLnBvcHVwLWRlc2NyaXB0aW9ue1xuLy8gICAgcGFkZGluZzogMjBweCAxMHB4IDIwcHggMTBweCA7XG4vLyAgIH0iLCI6aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XG4gIC0tcGFnZS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG4gIC0tcGFnZS1pdGVtcy1ndXR0ZXI6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gIC0tcGFnZS1jb2xvcjogI2NiMzI4Zjtcbn1cblxuLmZhc2hpb24tbGlzdGluZy1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1wYWdlLWJhY2tncm91bmQpO1xuICAtLXBhZGRpbmctc3RhcnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgLS1wYWRkaW5nLWVuZDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAtLXBhZGRpbmctdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIC0tcGFkZGluZy1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcbn1cblxuLml0ZW1zLXJvdyB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDA7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDEpO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtOm50aC1jaGlsZChvZGQpIHtcbiAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1pdGVtcy1ndXR0ZXIpO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtOm50aC1jaGlsZChldmVuKSB7XG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFnZS1pdGVtcy1ndXR0ZXIpO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWltYWdlLXdyYXBwZXIge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDk1MDlkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0taW1hZ2Utd3JhcHBlciAuaW1hZ2UtYW5jaG9yIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcbiAgcGFkZGluZzogNXB4IDVweCAwcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWluLWhlaWdodDogNjhweDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5tYWluLWluZm8ge1xuICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLm1haW4taW5mbyAuaXRlbS1uYW1lIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiB1bnNldDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5tYWluLWluZm8gLml0ZW0tbmFtZSAubmFtZS1hbmNob3Ige1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIHtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyAucHJpY2UtY29sIHtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyAucHJpY2UtY29sOmZpcnN0LWNoaWxkIHtcbiAgcGFkZGluZy1yaWdodDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8gLnByaWNlLWNvbDpmaXJzdC1jaGlsZDpsYXN0LWNoaWxkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIC5wcmljZS1jb2w6bGFzdC1jaGlsZCB7XG4gIHBhZGRpbmctbGVmdDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyAuc2VwYXJhdG9yIHtcbiAgbWF4LXdpZHRoOiAwcHg7XG4gIGJvcmRlci1yaWdodDogc29saWQgMnB4IHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG4gIGFsaWduLXNlbGY6IHN0cmV0Y2g7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8gLml0ZW0tc2FsZS1wcmljZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogNDAwO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8gLml0ZW0tb3JpZ2luYWwtcHJpY2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlKTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG5pb24taGVhZGVyIGlvbi10b29sYmFyOmZpcnN0LW9mLXR5cGUge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5sb2dvIHtcbiAgd2lkdGg6IDgwJTtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uaG9tZV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmhlYXJ0X2ljb24ge1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLnNlYXJjaF9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5pY29uLWhlYXJ0LXJlZCB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogLThweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1oZWFydC1ibGFjayB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogLThweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1zaGFyZSB7XG4gIHdpZHRoOiAyOXB4O1xuICBtYXJnaW4tYm90dG9tOiAtN3B4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG5cbi5pY29uLWRvd25sb2FkIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBtYXJnaW4tYm90dG9tOiAtNHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG5cbi5jb2wtcGFkZGluZyB7XG4gIHBhZGRpbmctdG9wOiA4cHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1ib3R0b206IDhweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjMDk1MDlkO1xufVxuXG4uc3ViSGVhZGVyQ1NTIHtcbiAgcGFkZGluZy10b3A6IDhweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnN1YkhlYWRlckNzc0NsaWNrIHtcbiAgcGFkZGluZy10b3A6IDhweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWFpbi10aXRsZSB7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmljb24tY29tcGFyZSB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogLTZweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbl9mdW5uZWwge1xuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IC02cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cblxuLnRleHQtc2hhcmUge1xuICBjb2xvcjogIzA5NTA5ZDtcbn1cblxuLmltZy1wcm9kdWN0IHtcbiAgd2lkdGg6IDE5MHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTI3cHggIWltcG9ydGFudDtcbiAgb2JqZWN0LWZpdDogY29udGFpbiAhaW1wb3J0YW50O1xufVxuXG4ucHJvZHVjdC10ZXh0IHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5cbi8qIFNwbGl0IHRoZSBzY3JlZW4gaW4gaGFsZiAqL1xuLnNwbGl0IHtcbiAgaGVpZ2h0OiAyNSU7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAxO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbn1cblxuLyogQ29udHJvbCB0aGUgbGVmdCBzaWRlICovXG4vKiBDcmVhdGUgdGhyZWUgZXF1YWwgY29sdW1ucyB0aGF0IGZsb2F0cyBuZXh0IHRvIGVhY2ggb3RoZXIgKi9cbi5jb2x1bW4ge1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZzogMTBweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICAvKiBTaG91bGQgYmUgcmVtb3ZlZC4gT25seSBmb3IgZGVtb25zdHJhdGlvbiAqL1xufVxuXG4uY2VudGVyZWQge1xuICBjb2xvcjogYmFsY2s7XG59XG5cbi5pY29uLWRlbGV0ZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDkwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgY29sb3I6ICMwOTUwOWQ7XG59XG5cbi50YWJDc3NDbGljayB7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICMwOTUwOWQgIWltcG9ydGFudDtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjMDk1MDlkICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDEwcHggMTVweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBmb250LXNpemU6IDE0cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gIGNvbG9yOiAjMDk1MDlkO1xufVxuXG4udGFiQ3NzIHtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2VhZTRlNDtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZWFlNGU0O1xuICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5kaXYtZml4ZWQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDk5OTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDk1MDlkO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteTogYXV0bztcbn1cblxuLmJlZGdlLW5vdGlmaWNhdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDI0cHg7XG4gIHRvcDogM3B4O1xuICB6LWluZGV4OiA5OTk7XG4gIGhlaWdodDogMjNweDtcbiAgd2lkdGg6IDIzcHg7XG59XG5cbi5pbWFnZS1jZW50ZXIge1xuICB3aWR0aDogMzAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBtaW4taGVpZ2h0OiAxNTBweDtcbn1cblxuLmZpbHRlci1iZWRnZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwOTUwOWQ7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1cHg7XG4gIHJpZ2h0OiA1NSU7XG59XG5cbi5sYWJlbC1jb21wYXJlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgYmxhY2s7XG4gIHBhZGRpbmc6IDVweDtcbiAgei1pbmRleDogOTk5O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4ubGFiZWwtYWRkZWQge1xuICBwYWRkaW5nOiA1cHg7XG4gIHotaW5kZXg6IDk5OTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDNBOUY0O1xuICBjb2xvcjogd2hpdGU7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuXG4uaWNvbi1jaGVja21hcmsge1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5jb21wYXJlLWZsYWdlLWFsaWduIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogODBweDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLmRpdi1mb290ZXIge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJvdHRvbTogMHB4O1xuICB6LWluZGV4OiA5OTk7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLnJvdy1tYXJnaW4ge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uaWNvbi1yZW1vdmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDk5OTtcbiAgZm9udC1zaXplOiAxLjRlbTtcbiAgY29sb3I6IGdyZXk7XG4gIHJpZ2h0OiA1cHg7XG59XG5cbi5wcm9kdWN0LXRpdGxlIHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5cbi5yb3ctYm9yZGVyLXRvcCB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMDk1MDlkO1xufVxuXG4uYnRuLWNvbXBhcmUge1xuICBtYXJnaW4tYm90dG9tOiA4cHg7XG4gIG1hcmdpbi10b3A6IDhweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIGZvbnQtc2l6ZTogMThweDtcbn1cblxuLnRleHQtbXNnIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDE1cHg7XG59XG5cbi5ib3R0b20tcG9wdXAtdGl0bGUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDk1MDlkO1xuICBjb2xvcjogd2hpdGU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmJvdHRvbS1wb3B1cC10aXRsZS1wYWRkaW5nIHtcbiAgcGFkZGluZzogMTVweCAxMHB4IDEwcHggMTBweDtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4uZGl2TWFyZ2luIHtcbiAgbWFyZ2luLXRvcDogNzVweDtcbn1cblxuLmRpdk1hcmdpbkNsaWNrIHtcbiAgbWFyZ2luLXRvcDogNzVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTgwcHg7XG59XG5cbi5pY29uLWNsb3NlIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59Il19 */", "[_nghost-%COMP%] {\n  --shell-color: #cb328f;\n  --shell-color-rgb: 203,50,143;\n}\n\n.is-shell[_nghost-%COMP%]   a[_ngcontent-%COMP%] {\n  pointer-events: none;\n}\n\napp-image-shell.item-image[_ngcontent-%COMP%] {\n  --image-shell-loading-background: white;\n  --image-shell-spinner-color: #5289c3;\n}\n\n.item-name[_ngcontent-%COMP%]   app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .25);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .35);\n  --text-shell-line-height: 14px;\n}\n\n.item-sale-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px;\n}\n\n.item-original-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvcHJvZHVjdC9saXN0aW5nL3N0eWxlcy9wcm9kdWN0LWxpc3Rpbmcuc2hlbGwuc2NzcyIsInNyYy9hcHAvcHJvZHVjdC9saXN0aW5nL3N0eWxlcy9wcm9kdWN0LWxpc3Rpbmcuc2hlbGwuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHNCQUFBO0VBQ0EsNkJBQUE7QUNERjs7QURTRTtFQUNFLG9CQUFBO0FDTko7O0FEVUE7RUFLRSx1Q0FBQTtFQUNBLG9DQUFBO0FDWEY7O0FEY0E7RUFDRSxvRUFBQTtFQUNBLCtEQUFBO0VBQ0EsOEJBQUE7QUNYRjs7QURjQTtFQUNFLG9FQUFBO0VBQ0EsK0RBQUE7RUFDQSw4QkFBQTtBQ1hGOztBRGNBO0VBQ0Usb0VBQUE7RUFDQSwrREFBQTtFQUNBLDhCQUFBO0FDWEYiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0L2xpc3Rpbmcvc3R5bGVzL3Byb2R1Y3QtbGlzdGluZy5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tc2hlbGwtY29sb3I6ICNjYjMyOGY7XG4gIC0tc2hlbGwtY29sb3ItcmdiOiAyMDMsNTAsMTQzO1xufVxuXG4vLyBZb3UgY2FuIGFsc28gYXBwbHkgc2hlZWwgc3R5bGVzIHRvIHRoZSBlbnRpcmUgcGFnZVxuOmhvc3QoLmlzLXNoZWxsKSB7XG4gIC8vIGlvbi1jb250ZW50IHtcbiAgLy8gICBvcGFjaXR5OiAwLjg7XG4gIC8vIH1cbiAgYSB7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gIH1cbn1cblxuYXBwLWltYWdlLXNoZWxsLml0ZW0taW1hZ2Uge1xuICAvLyAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTApO1xuICAvLyAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcbiAgXG4gIC8vIC0taW1hZ2Utc2hlbGwtbG9hZGluZy1iYWNrZ3JvdW5kOiAjOGNiOWVhO1xuICAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogd2hpdGU7XG4gIC0taW1hZ2Utc2hlbGwtc3Bpbm5lci1jb2xvcjogIzUyODljMztcbn1cblxuLml0ZW0tbmFtZSBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjUpO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zNSk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTRweDtcbn1cblxuLml0ZW0tc2FsZS1wcmljZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yMCk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjMwKTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNnB4O1xufVxuXG4uaXRlbS1vcmlnaW5hbC1wcmljZSA+IGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yMCk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjMwKTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNnB4O1xufVxuIiwiOmhvc3Qge1xuICAtLXNoZWxsLWNvbG9yOiAjY2IzMjhmO1xuICAtLXNoZWxsLWNvbG9yLXJnYjogMjAzLDUwLDE0Mztcbn1cblxuOmhvc3QoLmlzLXNoZWxsKSBhIHtcbiAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG59XG5cbmFwcC1pbWFnZS1zaGVsbC5pdGVtLWltYWdlIHtcbiAgLS1pbWFnZS1zaGVsbC1sb2FkaW5nLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6ICM1Mjg5YzM7XG59XG5cbi5pdGVtLW5hbWUgYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzUpO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE0cHg7XG59XG5cbi5pdGVtLXNhbGUtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTZweDtcbn1cblxuLml0ZW0tb3JpZ2luYWwtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTZweDtcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ProductListingPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-product-listing',
          templateUrl: './product-listing.page.html',
          styleUrls: ['./styles/product-listing.page.scss', './styles/product-listing.shell.scss']
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["PopoverController"]
        }, {
          type: _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_9__["SQLite"]
        }, {
          type: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_13__["Downloader"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"]
        }, {
          type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_15__["SocialSharing"]
        }, {
          type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_14__["FirebaseAnalytics"]
        }, {
          type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_16__["InAppBrowser"]
        }, {
          type: _app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
        }, {
          type: _categories_categories_page__WEBPACK_IMPORTED_MODULE_10__["CategoriesPage"]
        }];
      }, {
        infiniteScroll: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonInfiniteScroll"]]
        }],
        content: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonContent"], {
            "static": true
          }]
        }],
        isShell: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"],
          args: ['class.is-shell']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/product/listing/product-listing.resolver.ts":
  /*!*************************************************************!*\
    !*** ./src/app/product/listing/product-listing.resolver.ts ***!
    \*************************************************************/

  /*! exports provided: ProductListingResolver */

  /***/
  function srcAppProductListingProductListingResolverTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductListingResolver", function () {
      return ProductListingResolver;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../product.service */
    "./src/app/product/product.service.ts");

    var ProductListingResolver = /*#__PURE__*/function () {
      function ProductListingResolver(productService) {
        _classCallCheck(this, ProductListingResolver);

        this.productService = productService;
      }

      _createClass(ProductListingResolver, [{
        key: "resolve",
        value: function resolve() {
          var dataSource = this.productService.getListingDataSource();
          var dataStore = this.productService.getListingStore(dataSource);
          return dataStore;
        }
      }]);

      return ProductListingResolver;
    }();

    ProductListingResolver.ɵfac = function ProductListingResolver_Factory(t) {
      return new (t || ProductListingResolver)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"]));
    };

    ProductListingResolver.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ProductListingResolver,
      factory: ProductListingResolver.ɵfac
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductListingResolver, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }], function () {
        return [{
          type: _product_service__WEBPACK_IMPORTED_MODULE_1__["ProductService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/product/product.service.ts":
  /*!********************************************!*\
    !*** ./src/app/product/product.service.ts ***!
    \********************************************/

  /*! exports provided: ProductService */

  /***/
  function srcAppProductProductServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductService", function () {
      return ProductService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _listing_product_listing_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./listing/product-listing.model */
    "./src/app/product/listing/product-listing.model.ts");
    /* harmony import */


    var _details_product_details_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./details/product-details.model */
    "./src/app/product/details/product-details.model.ts");

    var ProductService = /*#__PURE__*/function () {
      function ProductService(http) {
        _classCallCheck(this, ProductService);

        this.http = http;
      }

      _createClass(ProductService, [{
        key: "getListingDataSource",
        value: function getListingDataSource() {
          return this.http.get('./assets/sample-data/fashion/listing.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            // Note: HttpClient cannot know how to instantiate a class for the returned data
            // We need to properly cast types from json data
            var listing = new _listing_product_listing_model__WEBPACK_IMPORTED_MODULE_4__["ProductListingModel"](); // The Object.assign() method copies all enumerable own properties from one or more source objects to a target object.
            // Note: If you have non-enummerable properties, you can try a spread operator instead. listing = {...data};
            // (see: https://scotch.io/bar-talk/copying-objects-in-javascript#toc-using-spread-elements-)

            Object.assign(listing, data);
            return listing;
          }));
        }
      }, {
        key: "getListingStore",
        value: function getListingStore(dataSource) {
          // Use cache if available
          if (!this.listingDataStore) {
            // Initialize the model specifying that it is a shell model
            var shellModel = new _listing_product_listing_model__WEBPACK_IMPORTED_MODULE_4__["ProductListingModel"]();
            this.listingDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](shellModel); // Trigger the loading mechanism (with shell) in the dataStore

            this.listingDataStore.load(dataSource);
          }

          return this.listingDataStore;
        }
      }, {
        key: "getDetailsDataSource",
        value: function getDetailsDataSource() {
          return this.http.get('./assets/sample-data/fashion/details.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            // Note: HttpClient cannot know how to instantiate a class for the returned data
            // We need to properly cast types from json data
            var details = new _details_product_details_model__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsModel"](); // The Object.assign() method copies all enumerable own properties from one or more source objects to a target object.
            // Note: If you have non-enummerable properties, you can try a spread operator instead. details = {...data};
            // (see: https://scotch.io/bar-talk/copying-objects-in-javascript#toc-using-spread-elements-)

            Object.assign(details, data);
            return details;
          }));
        }
      }, {
        key: "getDetailsStore",
        value: function getDetailsStore(dataSource) {
          // Use cache if available
          if (!this.detailsDataStore) {
            // Initialize the model specifying that it is a shell model
            var shellModel = new _details_product_details_model__WEBPACK_IMPORTED_MODULE_5__["ProductDetailsModel"]();
            this.detailsDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](shellModel); // Trigger the loading mechanism (with shell) in the dataStore

            this.detailsDataStore.load(dataSource);
          }

          return this.detailsDataStore;
        }
      }]);

      return ProductService;
    }();

    ProductService.ɵfac = function ProductService_Factory(t) {
      return new (t || ProductService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
    };

    ProductService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ProductService,
      factory: ProductService.ɵfac
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/shell/data-store.ts":
  /*!*************************************!*\
    !*** ./src/app/shell/data-store.ts ***!
    \*************************************/

  /*! exports provided: ShellModel, DataStore */

  /***/
  function srcAppShellDataStoreTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShellModel", function () {
      return ShellModel;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStore", function () {
      return DataStore;
    });
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./config/app-shell.config */
    "./src/app/shell/config/app-shell.config.ts");

    var ShellModel = function ShellModel() {
      _classCallCheck(this, ShellModel);

      this.isShell = false;
    };

    var DataStore = /*#__PURE__*/function () {
      function DataStore(shellModel) {
        _classCallCheck(this, DataStore);

        this.shellModel = shellModel; // We wait on purpose 2 secs on local environment when fetching from json to simulate the backend roundtrip.
        // However, in production you should set this delay to 0 in the assets/config/app-shell.config.prod.json file.
        // tslint:disable-next-line:max-line-length

        this.networkDelay = _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings && _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay ? _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay : 0;
        this.timeline = new rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"](1);
      } // Static function with generics
      // (ref: https://stackoverflow.com/a/24293088/1116959)
      // Append a shell (T & ShellModel) to every value (T) emmited to the timeline


      _createClass(DataStore, [{
        key: "load",
        value: function load(dataSourceObservable) {
          var _this46 = this;

          var dataSourceWithShellObservable = DataStore.AppendShell(dataSourceObservable, this.shellModel, this.networkDelay);
          dataSourceWithShellObservable.subscribe(function (dataValue) {
            _this46.timeline.next(dataValue);
          });
        }
      }, {
        key: "state",
        get: function get() {
          return this.timeline.asObservable();
        }
      }], [{
        key: "AppendShell",
        value: function AppendShell(dataObservable, shellModel) {
          var networkDelay = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 400;
          var delayObservable = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(true).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["delay"])(networkDelay)); // Assign shell flag accordingly
          // (ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)

          return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["combineLatest"])([delayObservable, dataObservable]).pipe( // Dismiss unnecessary delayValue
          Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 2),
                delayValue = _ref2[0],
                dataValue = _ref2[1];

            return Object.assign(dataValue, {
              isShell: false
            });
          }), // Set the shell model as the initial value
          Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["startWith"])(Object.assign(shellModel, {
            isShell: true
          })));
        }
      }]);

      return DataStore;
    }();
    /***/

  },

  /***/
  "./src/app/showcase/app-shell/app-shell.page.ts":
  /*!******************************************************!*\
    !*** ./src/app/showcase/app-shell/app-shell.page.ts ***!
    \******************************************************/

  /*! exports provided: AppShellPage */

  /***/
  function srcAppShowcaseAppShellAppShellPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppShellPage", function () {
      return AppShellPage;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _categories_categories_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./../../categories/categories.page */
    "./src/app/categories/categories.page.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function AppShellPage_div_8_ion_badge_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r4.airConditionerSubCategorySelectedLenght);
      }
    }

    function AppShellPage_div_8_ion_list_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_8_ion_list_9_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var item_r14 = ctx.$implicit;

          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r15.clickOnAirConditionerSubCategory(item_r14.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r14 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r14.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r14.flage);
      }
    }

    function AppShellPage_div_8_ion_badge_14_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r6.airConditionerCapacitySelectedLength);
      }
    }

    function AppShellPage_div_8_ion_list_16_Template(rf, ctx) {
      if (rf & 1) {
        var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_8_ion_list_16_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

          var item_r17 = ctx.$implicit;

          var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r18.clickOnAirConditionerCapacity(item_r17.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r17 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r17.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r17.flage);
      }
    }

    function AppShellPage_div_8_ion_badge_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r8.airConditionerSeriesSelectedLength);
      }
    }

    function AppShellPage_div_8_ion_list_23_Template(rf, ctx) {
      if (rf & 1) {
        var _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_8_ion_list_23_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22);

          var item_r20 = ctx.$implicit;

          var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r21.clickOnAirConditionerSeries(item_r20.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r20 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r20.showValue);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r20.flage);
      }
    }

    function AppShellPage_div_8_ion_badge_28_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r10.airConditionerStarRatingSelectedLength);
      }
    }

    function AppShellPage_div_8_ion_list_30_Template(rf, ctx) {
      if (rf & 1) {
        var _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_8_ion_list_30_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25);

          var item_r23 = ctx.$implicit;

          var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r24.clickOnAirConditionerStarRating(item_r23.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r23 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r23.showValue);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r23.flage);
      }
    }

    function AppShellPage_div_8_ion_badge_35_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r12.airConditionerPriseSelectedLength);
      }
    }

    function AppShellPage_div_8_ion_list_37_Template(rf, ctx) {
      if (rf & 1) {
        var _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_8_ion_list_37_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r28);

          var item_r26 = ctx.$implicit;

          var ctx_r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r27.clickOnAirConditionerPrise(item_r26.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r26 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r26.showValue);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r26.flage);
      }
    }

    function AppShellPage_div_8_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Sub Category ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, AppShellPage_div_8_ion_badge_7_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, AppShellPage_div_8_ion_list_9_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Capacity (TR) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, AppShellPage_div_8_ion_badge_14_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, AppShellPage_div_8_ion_list_16_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Series ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, AppShellPage_div_8_ion_badge_21_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, AppShellPage_div_8_ion_list_23_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "label", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Star Rating ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, AppShellPage_div_8_ion_badge_28_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](30, AppShellPage_div_8_ion_list_30_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "input", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "label", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "MRP (\u20B9) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, AppShellPage_div_8_ion_badge_35_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](37, AppShellPage_div_8_ion_list_37_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.airConditionerSubCategorySelectedLenght != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.airConditionerSubCategory);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.airConditionerCapacitySelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.airConditionerCapacity);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.airConditionerSeriesSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.airConditionerSeries);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.airConditionerStarRatingSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.airConditionerStarRating);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.airConditionerPriseSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.airConditionerMRP);
      }
    }

    function AppShellPage_div_9_ion_badge_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r29.airCoolerTypeSelectedLength);
      }
    }

    function AppShellPage_div_9_ion_list_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r37 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_9_ion_list_9_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37);

          var item_r35 = ctx.$implicit;

          var ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r36.clickOnAirCoolerType(item_r35.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r35 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r35.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r35.flage);
      }
    }

    function AppShellPage_div_9_ion_badge_14_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r31.airCoolerPriceSelectedLength);
      }
    }

    function AppShellPage_div_9_ion_list_16_Template(rf, ctx) {
      if (rf & 1) {
        var _r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_9_ion_list_16_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r40);

          var item_r38 = ctx.$implicit;

          var ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r39.clickOnAirCoolerPrice(item_r38.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r38 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r38.showValue);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r38.flage);
      }
    }

    function AppShellPage_div_9_ion_badge_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r33.airCoolerCapacitySelectedLength);
      }
    }

    function AppShellPage_div_9_ion_list_23_Template(rf, ctx) {
      if (rf & 1) {
        var _r43 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_9_ion_list_23_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r43);

          var item_r41 = ctx.$implicit;

          var ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r42.clickOnAirCoolerCapacity(item_r41.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r41 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r41.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r41.flage);
      }
    }

    function AppShellPage_div_9_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Type ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, AppShellPage_div_9_ion_badge_7_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, AppShellPage_div_9_ion_list_9_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "MRP (\u20B9) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, AppShellPage_div_9_ion_badge_14_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, AppShellPage_div_9_ion_list_16_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Capacity (Ltr) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, AppShellPage_div_9_ion_badge_21_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, AppShellPage_div_9_ion_list_23_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.airCoolerTypeSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.airCoolerType);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.airCoolerPriceSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.airCoolerMRP);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.airCoolerCapacitySelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.airCoolerCapacity);
      }
    }

    function AppShellPage_div_10_ion_badge_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r44 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r44.airPurifierPriceSelectedLength);
      }
    }

    function AppShellPage_div_10_ion_list_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r52 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_10_ion_list_9_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r52);

          var item_r50 = ctx.$implicit;

          var ctx_r51 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r51.clickOnAirPurifierPrice(item_r50.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r50 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r50.showValue);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r50.flage);
      }
    }

    function AppShellPage_div_10_ion_badge_14_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r46.airPurifierCoverageAreaSelectedLength);
      }
    }

    function AppShellPage_div_10_ion_list_16_Template(rf, ctx) {
      if (rf & 1) {
        var _r55 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_10_ion_list_16_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r55);

          var item_r53 = ctx.$implicit;

          var ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r54.clickOnAirPurifierCoverageArea(item_r53.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r53 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r53.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r53.flage);
      }
    }

    function AppShellPage_div_10_ion_badge_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r48.airPurifierCADRSelectedLength);
      }
    }

    function AppShellPage_div_10_ion_list_23_Template(rf, ctx) {
      if (rf & 1) {
        var _r58 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_10_ion_list_23_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r58);

          var item_r56 = ctx.$implicit;

          var ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r57.clockOnAirPurifierCADR(item_r56.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r56 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r56.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r56.flage);
      }
    }

    function AppShellPage_div_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "MRP (\u20B9) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, AppShellPage_div_10_ion_badge_7_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, AppShellPage_div_10_ion_list_9_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Coverage Area (sqft) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, AppShellPage_div_10_ion_badge_14_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, AppShellPage_div_10_ion_list_16_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "CADR ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, AppShellPage_div_10_ion_badge_21_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, AppShellPage_div_10_ion_list_23_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.airPurifierPriceSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.airPurifierMRP);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.airPurifierCoverageAreaSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.airPurifierCoverageArea);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.airPurifierCADRSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.airPurifierCADR);
      }
    }

    function AppShellPage_div_11_ion_badge_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r59 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r59.waterPurifierTechnologySelectedLength);
      }
    }

    function AppShellPage_div_11_ion_list_9_Template(rf, ctx) {
      if (rf & 1) {
        var _r69 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_11_ion_list_9_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r69);

          var item_r67 = ctx.$implicit;

          var ctx_r68 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r68.clickOnWaterPurifierTechnology(item_r67.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r67 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r67.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r67.flage);
      }
    }

    function AppShellPage_div_11_ion_badge_14_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r61 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r61.waterPurifierPriseSelectedLength);
      }
    }

    function AppShellPage_div_11_ion_list_16_Template(rf, ctx) {
      if (rf & 1) {
        var _r72 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_11_ion_list_16_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r72);

          var item_r70 = ctx.$implicit;

          var ctx_r71 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r71.clickOnWaterPurifierPrice(item_r70.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r70 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r70.showValue);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r70.flage);
      }
    }

    function AppShellPage_div_11_ion_badge_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r63.waterPurifierCapacitySelectedLength);
      }
    }

    function AppShellPage_div_11_ion_list_23_Template(rf, ctx) {
      if (rf & 1) {
        var _r75 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_11_ion_list_23_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r75);

          var item_r73 = ctx.$implicit;

          var ctx_r74 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r74.clickOnWaterPurifierCapacity(item_r73.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r73 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r73.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r73.flage);
      }
    }

    function AppShellPage_div_11_ion_badge_28_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-badge", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r65 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r65.waterPurifierModelNameSelectedLength);
      }
    }

    function AppShellPage_div_11_ion_list_30_Template(rf, ctx) {
      if (rf & 1) {
        var _r78 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-list");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-item", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_div_11_ion_list_30_Template_ion_item_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r78);

          var item_r76 = ctx.$implicit;

          var ctx_r77 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r77.clickOnWaterPurifierModelName(item_r76.value);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-label");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "ion-checkbox", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r76 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r76.value);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("checked", item_r76.flage);
      }
    }

    function AppShellPage_div_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "input", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "label", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Technology ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, AppShellPage_div_11_ion_badge_7_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, AppShellPage_div_11_ion_list_9_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "MRP (\u20B9) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, AppShellPage_div_11_ion_badge_14_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, AppShellPage_div_11_ion_list_16_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Capacity (Ltr) ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, AppShellPage_div_11_ion_badge_21_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, AppShellPage_div_11_ion_list_23_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "label", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Model Name ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, AppShellPage_div_11_ion_badge_28_Template, 2, 1, "ion-badge", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](30, AppShellPage_div_11_ion_list_30_Template, 5, 2, "ion-list", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.waterPurifierTechnologySelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.waterPurifieTechnology);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.waterPurifierPriseSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.waterPurifieMRP);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.waterPurifierCapacitySelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.waterPurifieCapacity);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.waterPurifierModelNameSelectedLength != 0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.waterPurifieModelName);
      }
    }

    var AppShellPage = /*#__PURE__*/function () {
      function AppShellPage(modalCtrl, loadingController, route, http, categoriesPage) {
        var _this47 = this;

        _classCallCheck(this, AppShellPage);

        this.modalCtrl = modalCtrl;
        this.loadingController = loadingController;
        this.route = route;
        this.http = http;
        this.categoriesPage = categoriesPage;
        this.title = ''; //airPurifier filter selected option array

        this.airPurifierCoverageAreaSelected = [];
        this.airPurifierCADRSelected = [];
        this.airPurifierPriceSelected = []; //waterPurifie filter selected option array

        this.waterPurifierTechnologySelected = [];
        this.waterPurifierModelNameSelected = [];
        this.waterPurifierCapacitySelected = [];
        this.waterPurifierPriseSelected = []; //airCooler filter selected option array

        this.airCoolerTypeSelected = [];
        this.airCoolerPriceSelected = [];
        this.airCoolerCapacitySelected = []; //airConditioner filter selected option array

        this.airConditionerSubCategorySelected = [];
        this.airConditionerCapacitySelected = [];
        this.airConditionerSeriesSelected = [];
        this.airConditionerStarRatingSelected = [];
        this.airConditionerPriseSelected = []; //airConditioner json

        this.airConditionerSubCategory = [{
          value: "CASSETTE AC",
          flage: false
        }, {
          value: "INVCASSETTE",
          flage: false
        }, {
          value: "INVSAC",
          flage: false
        }, {
          value: "MEGASAC",
          flage: false
        }, {
          value: "PORTABLEAC",
          flage: false
        }, {
          value: "SAC",
          flage: false
        }, {
          value: "VERTICOOLAC",
          flage: false
        }, {
          value: "WINDOW AC",
          flage: false
        }];
        this.airConditionerCapacity = [{
          value: "0.75",
          flage: false
        }, {
          value: "1",
          flage: false
        }, {
          value: "1.5",
          flage: false
        }, {
          value: "1.8",
          flage: false
        }, {
          value: "2",
          flage: false
        }, {
          value: "2.5",
          flage: false
        }, {
          value: "3",
          flage: false
        }, {
          value: "4",
          flage: false
        }];
        this.airConditionerSeries = [{
          value: "A Series",
          showValue: "A",
          flage: false
        }, {
          value: "C Series",
          showValue: "C",
          flage: false
        }, {
          value: "DA Series",
          showValue: "DA",
          flage: false
        }, {
          value: "DA-AP Series",
          showValue: "DA-AP",
          flage: false
        }, {
          value: "DB Series",
          showValue: "DB",
          flage: false
        }, {
          value: "DC Series",
          showValue: "DC",
          flage: false
        }, {
          value: "E Series",
          showValue: "E",
          flage: false
        }, {
          value: "GA Series",
          showValue: "GA",
          flage: false
        }, {
          value: "GBT Series",
          showValue: "GBT",
          flage: false
        }, {
          value: "GBTI Series",
          showValue: "GBTI",
          flage: false
        }, {
          value: "I Series",
          showValue: "I",
          flage: false
        }, {
          value: "LA Series",
          showValue: "LA",
          flage: false
        }, {
          value: "LD Series",
          showValue: "LD",
          flage: false
        }, {
          value: "LDT Series",
          showValue: "LDT",
          flage: false
        }, {
          value: "M Series",
          showValue: "M",
          flage: false
        }, {
          value: "P Series",
          showValue: "P",
          flage: false
        }, {
          value: "QA Series",
          showValue: "QA",
          flage: false
        }, {
          value: "QB Series",
          showValue: "QB",
          flage: false
        }, {
          value: "R Series",
          showValue: "R",
          flage: false
        }, {
          value: "Y Series",
          showValue: "Y",
          flage: false
        }, {
          value: "YA Series",
          showValue: "YA",
          flage: false
        }, {
          value: "YB Series",
          showValue: "YB",
          flage: false
        }, {
          value: "YC Series",
          showValue: "YC",
          flage: false
        }, {
          value: "YDF Series",
          showValue: "YDF",
          flage: false
        }, {
          value: "Z Series",
          showValue: "Z",
          flage: false
        }];
        this.airConditionerStarRating = [{
          value: "1 star",
          showValue: "1",
          flage: false
        }, {
          value: "2 star",
          showValue: "2",
          flage: false
        }, {
          value: "3 star",
          showValue: "3",
          flage: false
        }, {
          value: "4 star",
          showValue: "4",
          flage: false
        }, {
          value: "5 star",
          showValue: "5",
          flage: false
        }, {
          value: "Non star",
          showValue: "NA",
          flage: false
        }];
        this.airConditionerMRP = [{
          value: "20000-40000",
          showValue: "20,000-40,000",
          flage: false
        }, {
          value: "40001-50000",
          showValue: "40,001-50,000",
          flage: false
        }, {
          value: "50001-60000",
          showValue: "50,001-60,000",
          flage: false
        }, {
          value: "60001-70000",
          showValue: "60,001-70,000",
          flage: false
        }, {
          value: "70001-99999999",
          showValue: "70,001 and above",
          flage: false
        }]; //airCooler json

        this.airCoolerType = [{
          value: "Personal",
          flage: false
        }, {
          value: "Tower",
          flage: false
        }, {
          value: "Window",
          flage: false
        }, {
          value: "Desert",
          flage: false
        }];
        this.airCoolerMRP = [{
          value: "7000-10000",
          showValue: "7,000-10,000",
          flage: false
        }, {
          value: "10001-15000",
          showValue: "10,001-15,000",
          flage: false
        }, {
          value: "15001-20000",
          showValue: "15,001-20,000",
          flage: false
        }];
        this.airCoolerCapacity = [{
          value: "25-50",
          flage: false
        }, {
          value: "51-70",
          flage: false
        }, {
          value: "71-90",
          flage: false
        }]; //airPurifier json

        this.airPurifierMRP = [{
          value: "7000-15000",
          showValue: "7,000-15,000",
          flage: false
        }, {
          value: "15001-20000",
          showValue: "15,001-20,000",
          flage: false
        }, {
          value: "20001-99999999",
          showValue: "20,001 and above",
          flage: false
        }];
        this.airPurifierCoverageArea = [{
          value: "915",
          flage: false
        }, {
          value: "545",
          flage: false
        }, {
          value: "444",
          flage: false
        }, {
          value: "230",
          flage: false
        }];
        this.airPurifierCADR = [{
          value: "800",
          flage: false
        }, {
          value: "650",
          flage: false
        }, {
          value: "300",
          flage: false
        }, {
          value: "250",
          flage: false
        }]; //waterPurifie json

        this.waterPurifieTechnology = [{
          value: "RO+UV",
          flage: false
        }, {
          value: "RO+UV+UF",
          flage: false
        }, {
          value: "UV LED",
          flage: false
        }, {
          value: "UV",
          flage: false
        }, {
          value: "RO+UF",
          flage: false
        }];
        this.waterPurifieMRP = [{
          value: "7000-20000",
          showValue: "7,000-20,000",
          flage: false
        }, {
          value: "20001-30000",
          showValue: "20,001-30,000",
          flage: false
        }, {
          value: "30001-99999999",
          showValue: "30,001 and above",
          flage: false
        }];
        this.waterPurifieCapacity = [{
          value: "0",
          flage: false
        }, {
          value: "6",
          flage: false
        }, {
          value: "7",
          flage: false
        }, {
          value: "8",
          flage: false
        }, {
          value: "8.2",
          flage: false
        }, {
          value: "9.2",
          flage: false
        }];
        this.waterPurifieModelName = [{
          value: "Adora",
          flage: false
        }, {
          value: "Aristo",
          flage: false
        }, {
          value: "Eleanor",
          flage: false
        }, {
          value: "Eternia",
          flage: false
        }, {
          value: "Genia",
          flage: false
        }, {
          value: "Iconia",
          flage: false
        }, {
          value: "Imperia",
          flage: false
        }, {
          value: "Magnus",
          flage: false
        }, {
          value: "Opulus",
          flage: false
        }, {
          value: "Pristina",
          flage: false
        }, {
          value: "Stella",
          flage: false
        }];
        this.route.queryParams.subscribe(function (params) {
          _this47.title = params.title;

          if (params.title == "Air Conditioners") {
            _this47.getFilterAirConditioner();
          } else if (params.title == "Air Coolers") {
            _this47.getFilterAirCooler();
          } else if (params.title == "Air Purifiers") {
            _this47.getFilterAirPurifier();
          } else if (params.title == "Water Purifiers") {
            _this47.getFilterWaterPurifier();
          }
        });
      }

      _createClass(AppShellPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {//this.assignFilterPreveas();
        }
      }, {
        key: "assignFilterPreveas",
        value: function assignFilterPreveas() {
          if (this.title == "Air Conditioners") {
            if (this.airConditionerSubCategorySelected.length != 0) {
              this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length;

              for (var item in this.airConditionerSubCategorySelected) {
                for (var i in this.airConditionerSubCategory) {
                  if (this.airConditionerSubCategorySelected[item] == this.airConditionerSubCategory[i].value) this.airConditionerSubCategory[i].flage = true;
                }
              }
            }

            if (this.airConditionerCapacitySelected.length != 0) {
              this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length;

              for (var _item4 in this.airConditionerCapacitySelected) {
                for (var _i18 in this.airConditionerCapacity) {
                  if (this.airConditionerCapacitySelected[_item4] == this.airConditionerCapacity[_i18].value) this.airConditionerCapacity[_i18].flage = true;
                }
              }
            }

            if (this.airConditionerSeriesSelected.length != 0) {
              this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;

              for (var _item5 in this.airConditionerSeriesSelected) {
                for (var _i19 in this.airConditionerSeries) {
                  if (this.airConditionerSeriesSelected[_item5] == this.airConditionerSeries[_i19].value) this.airConditionerSeries[_i19].flage = true;
                }
              }
            }

            if (this.airConditionerStarRatingSelected.length != 0) {
              this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;

              for (var _item6 in this.airConditionerStarRatingSelected) {
                for (var _i20 in this.airConditionerStarRating) {
                  if (this.airConditionerStarRatingSelected[_item6] == this.airConditionerStarRating[_i20].value) this.airConditionerStarRating[_i20].flage = true;
                }
              }
            }

            if (this.airConditionerPriseSelected.length != 0) {
              this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;

              for (var _item7 in this.airConditionerPriseSelected) {
                for (var _i21 in this.airConditionerMRP) {
                  if (this.airConditionerPriseSelected[_item7] == this.airConditionerMRP[_i21].value) this.airConditionerMRP[_i21].flage = true;
                }
              }
            }
          } else if (this.title == "Air Coolers") {
            // console.log("airCoolerTypeSelected", this.airCoolerTypeSelected);
            if (this.airCoolerTypeSelected.length != 0) {
              this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length; // console.log("***********",this.airCoolerTypeSelectedLength);

              for (var _item8 in this.airCoolerTypeSelected) {
                for (var _i22 in this.airCoolerType) {
                  if (this.airCoolerTypeSelected[_item8] == this.airCoolerType[_i22].value) this.airCoolerType[_i22].flage = true;
                }
              }
            }

            if (this.airCoolerPriceSelected.length != 0) {
              this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length; // console.log("***********",this.airCoolerPriceSelectedLength);

              for (var _item9 in this.airCoolerPriceSelected) {
                for (var _i23 in this.airCoolerMRP) {
                  if (this.airCoolerPriceSelected[_item9] == this.airCoolerMRP[_i23].value) this.airCoolerMRP[_i23].flage = true;
                }
              }
            }

            if (this.airCoolerCapacitySelected.length != 0) {
              this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length; // console.log("***********",this.airCoolerCapacitySelectedLength);

              for (var _item10 in this.airCoolerCapacitySelected) {
                for (var _i24 in this.airCoolerCapacity) {
                  if (this.airCoolerCapacitySelected[_item10] == this.airCoolerCapacity[_i24].value) this.airCoolerCapacity[_i24].flage = true;
                }
              }
            }
          } else if (this.title == "Air Purifiers") {
            if (this.airPurifierCoverageAreaSelected.length != 0) {
              this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;

              for (var _item11 in this.airPurifierCoverageAreaSelected) {
                for (var _i25 in this.airPurifierCoverageArea) {
                  if (this.airPurifierCoverageAreaSelected[_item11] == this.airPurifierCoverageArea[_i25].value) this.airPurifierCoverageArea[_i25].flage = true;
                }
              }
            }

            if (this.airPurifierCADRSelected.length != 0) {
              this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;

              for (var _item12 in this.airPurifierCADRSelected) {
                for (var _i26 in this.airPurifierCADR) {
                  if (this.airPurifierCADRSelected[_item12] == this.airPurifierCADR[_i26].value) this.airPurifierCADR[_i26].flage = true;
                }
              }
            }

            if (this.airPurifierPriceSelected.length != 0) {
              this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;

              for (var _item13 in this.airPurifierPriceSelected) {
                for (var _i27 in this.airPurifierMRP) {
                  if (this.airPurifierPriceSelected[_item13] == this.airPurifierMRP[_i27].value) this.airPurifierMRP[_i27].flage = true;
                }
              }
            }
          } else if (this.title == "Water Purifiers") {
            if (this.waterPurifierTechnologySelected.length != 0) {
              this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;

              for (var _item14 in this.waterPurifierTechnologySelected) {
                for (var _i28 in this.waterPurifieTechnology) {
                  if (this.waterPurifierTechnologySelected[_item14] == this.waterPurifieTechnology[_i28].value) this.waterPurifieTechnology[_i28].flage = true;
                }
              }
            }

            if (this.waterPurifierModelNameSelected.length != 0) {
              this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;

              for (var _item15 in this.waterPurifierModelNameSelected) {
                for (var _i29 in this.waterPurifieModelName) {
                  if (this.waterPurifierModelNameSelected[_item15] == this.waterPurifieModelName[_i29].value) this.waterPurifieModelName[_i29].flage = true;
                }
              }
            }

            if (this.waterPurifierCapacitySelected.length != 0) {
              this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;

              for (var _item16 in this.waterPurifierCapacitySelected) {
                for (var _i30 in this.waterPurifieCapacity) {
                  if (this.waterPurifierCapacitySelected[_item16] == this.waterPurifieCapacity[_i30].value) this.waterPurifieCapacity[_i30].flage = true;
                }
              }
            }

            if (this.waterPurifierPriseSelected.length != 0) {
              this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;

              for (var _item17 in this.waterPurifierPriseSelected) {
                for (var _i31 in this.waterPurifieMRP) {
                  if (this.waterPurifierPriseSelected[_item17] == this.waterPurifieMRP[_i31].value) this.waterPurifieMRP[_i31].flage = true;
                }
              }
            }
          }
        }
      }, {
        key: "clearFilter",
        value: function clearFilter() {
          if (this.title == "Air Conditioners") {
            this.airConditionerSubCategorySelected = [];
            this.airConditionerCapacitySelected = [];
            this.airConditionerSeriesSelected = [];
            this.airConditionerStarRatingSelected = [];
            this.airConditionerPriseSelected = [];
            this.airConditionerSubCategorySelectedLenght = 0;
            this.airConditionerCapacitySelectedLength = 0;
            this.airConditionerSeriesSelectedLength = 0;
            this.airConditionerStarRatingSelectedLength = 0;
            this.airConditionerPriseSelectedLength = 0;

            for (var i in this.airConditionerSubCategory) {
              this.airConditionerSubCategory[i].flage = false;
            }

            for (var _i32 in this.airConditionerCapacity) {
              this.airConditionerCapacity[_i32].flage = false;
            }

            for (var _i33 in this.airConditionerSeries) {
              this.airConditionerSeries[_i33].flage = false;
            }

            for (var _i34 in this.airConditionerStarRating) {
              this.airConditionerStarRating[_i34].flage = false;
            }

            for (var _i35 in this.airConditionerMRP) {
              this.airConditionerMRP[_i35].flage = false;
            }
          } else if (this.title == "Air Coolers") {
            this.airCoolerTypeSelected = [];
            this.airCoolerPriceSelected = [];
            this.airCoolerCapacitySelected = [];
            this.airCoolerTypeSelectedLength = 0;
            this.airCoolerPriceSelectedLength = 0;
            this.airCoolerCapacitySelectedLength = 0;

            for (var _i36 in this.airCoolerType) {
              this.airCoolerType[_i36].flage = false;
            }

            for (var _i37 in this.airCoolerMRP) {
              this.airCoolerMRP[_i37].flage = false;
            }

            for (var _i38 in this.airCoolerCapacity) {
              this.airCoolerCapacity[_i38].flage = false;
            } // console.log("airCoolerTypeSelected", this.airCoolerTypeSelected);

          } else if (this.title == "Air Purifiers") {
            this.airPurifierCoverageAreaSelected = [];
            this.airPurifierCADRSelected = [];
            this.airPurifierPriceSelected = [];
            this.airPurifierPriceSelectedLength = 0;
            this.airPurifierCoverageAreaSelectedLength = 0;
            this.airPurifierCADRSelectedLength = 0;

            for (var _i39 in this.airPurifierCoverageArea) {
              this.airPurifierCoverageArea[_i39].flage = false;
            }

            for (var _i40 in this.airPurifierCADR) {
              this.airPurifierCADR[_i40].flage = false;
            }

            for (var _i41 in this.airPurifierMRP) {
              this.airPurifierMRP[_i41].flage = false;
            }
          } else if (this.title == "Water Purifiers") {
            this.waterPurifierTechnologySelected = [];
            this.waterPurifierModelNameSelected = [];
            this.waterPurifierCapacitySelected = [];
            this.waterPurifierPriseSelected = [];
            this.waterPurifierTechnologySelectedLength = 0;
            this.waterPurifierPriseSelectedLength = 0;
            this.waterPurifierCapacitySelectedLength = 0;
            this.waterPurifierModelNameSelectedLength = 0;

            for (var _i42 in this.waterPurifieTechnology) {
              this.waterPurifieTechnology[_i42].flage = false;
            }

            for (var _i43 in this.waterPurifieModelName) {
              this.waterPurifieModelName[_i43].flage = false;
            }

            for (var _i44 in this.waterPurifieCapacity) {
              this.waterPurifieCapacity[_i44].flage = false;
            }

            for (var _i45 in this.waterPurifieMRP) {
              this.waterPurifieMRP[_i45].flage = false;
            }
          }
        }
      }, {
        key: "closeModal",
        value: function closeModal() {
          if (this.title == "Air Conditioners") {
            var data = {
              close: "true",
              airConditionerSubCategorySelected: this.airConditionerSubCategorySelected,
              airConditionerCapacitySelected: this.airConditionerCapacitySelected,
              airConditionerSeriesSelected: this.airConditionerSeriesSelected,
              airConditionerStarRatingSelected: this.airConditionerStarRatingSelected,
              airConditionerPriseSelected: this.airConditionerPriseSelected
            };
            this.modalCtrl.dismiss(data);
          } else if (this.title == "Air Coolers") {
            var _data = {
              close: "true",
              airCoolerTypeSelected: this.airCoolerTypeSelected,
              airCoolerPriceSelected: this.airCoolerPriceSelected,
              airCoolerCapacitySelected: this.airCoolerCapacitySelected
            };
            this.modalCtrl.dismiss(_data);
          } else if (this.title == "Air Purifiers") {
            var _data2 = {
              close: "true",
              airPurifierCoverageAreaSelected: this.airPurifierCoverageAreaSelected,
              airPurifierCADRSelected: this.airPurifierCADRSelected,
              airPurifierPriceSelected: this.airPurifierPriceSelected
            };
            this.modalCtrl.dismiss(_data2);
          } else if (this.title == "Water Purifiers") {
            var _data3 = {
              close: "true",
              waterPurifierTechnologySelected: this.waterPurifierTechnologySelected,
              waterPurifierModelNameSelected: this.waterPurifierModelNameSelected,
              waterPurifierCapacitySelected: this.waterPurifierCapacitySelected,
              waterPurifierPriseSelected: this.waterPurifierPriseSelected
            };
            this.modalCtrl.dismiss(_data3);
          }
        }
      }, {
        key: "onClickApply",
        value: function onClickApply() {
          if (this.title == "Air Conditioners") {
            var data = {
              SubCategory: "",
              Capacity: "",
              Series: "",
              StarRating: "",
              Price: "",
              close: "false",
              airConditionerSubCategorySelected: this.airConditionerSubCategorySelected,
              airConditionerCapacitySelected: this.airConditionerCapacitySelected,
              airConditionerSeriesSelected: this.airConditionerSeriesSelected,
              airConditionerStarRatingSelected: this.airConditionerStarRatingSelected,
              airConditionerPriseSelected: this.airConditionerPriseSelected
            };

            if (this.airConditionerPriseSelected.length == 0) {
              data.Price = "";
            } else {
              var stringValue = "";

              for (var index in this.airConditionerPriseSelected) {
                if (stringValue.length == 0) {
                  stringValue = "'" + this.airConditionerPriseSelected[index] + "'";
                } else {
                  stringValue = stringValue + "," + "'" + this.airConditionerPriseSelected[index] + "'";
                }
              }

              data.Price = stringValue;
            }

            if (this.airConditionerStarRatingSelected.length == 0) {
              data.StarRating = "";
            } else {
              var _stringValue = "";

              for (var _index13 in this.airConditionerStarRatingSelected) {
                if (_stringValue.length == 0) {
                  _stringValue = "'" + this.airConditionerStarRatingSelected[_index13] + "'";
                } else {
                  _stringValue = _stringValue + "," + "'" + this.airConditionerStarRatingSelected[_index13] + "'";
                }
              }

              data.StarRating = _stringValue;
            }

            if (this.airConditionerSeriesSelected.length == 0) {
              data.Series = "";
            } else {
              var _stringValue2 = "";

              for (var _index14 in this.airConditionerSeriesSelected) {
                if (_stringValue2.length == 0) {
                  _stringValue2 = "'" + this.airConditionerSeriesSelected[_index14] + "'";
                } else {
                  _stringValue2 = _stringValue2 + "," + "'" + this.airConditionerSeriesSelected[_index14] + "'";
                }
              }

              data.Series = _stringValue2;
            }

            if (this.airConditionerSubCategorySelected.length == 0) {
              data.SubCategory = "";
            } else {
              var _stringValue3 = "";

              for (var _index15 in this.airConditionerSubCategorySelected) {
                if (_stringValue3.length == 0) {
                  _stringValue3 = "'" + this.airConditionerSubCategorySelected[_index15] + "'";
                } else {
                  _stringValue3 = _stringValue3 + "," + "'" + this.airConditionerSubCategorySelected[_index15] + "'";
                }
              }

              data.SubCategory = _stringValue3;
            }

            if (this.airConditionerCapacitySelected.length == 0) {
              data.Capacity = "";
            } else {
              var _stringValue4 = "";

              for (var _index16 in this.airConditionerCapacitySelected) {
                if (_stringValue4.length == 0) {
                  _stringValue4 = "'" + this.airConditionerCapacitySelected[_index16] + "'";
                } else {
                  _stringValue4 = _stringValue4 + "," + "'" + this.airConditionerCapacitySelected[_index16] + "'";
                }
              }

              data.Capacity = _stringValue4;
            }

            this.modalCtrl.dismiss(data);
          } else if (this.title == "Air Coolers") {
            var _data4 = {
              Type: "",
              Price: "",
              Capacity: "",
              close: "false",
              airCoolerTypeSelected: this.airCoolerTypeSelected,
              airCoolerPriceSelected: this.airCoolerPriceSelected,
              airCoolerCapacitySelected: this.airCoolerCapacitySelected
            };

            if (this.airCoolerCapacitySelected.length == 0) {
              _data4.Capacity = "";
            } else {
              var _stringValue5 = "";

              for (var _index17 in this.airCoolerCapacitySelected) {
                if (_stringValue5.length == 0) {
                  _stringValue5 = "'" + this.airCoolerCapacitySelected[_index17] + "'";
                } else {
                  _stringValue5 = _stringValue5 + "," + "'" + this.airCoolerCapacitySelected[_index17] + "'";
                }
              }

              _data4.Capacity = _stringValue5;
            }

            if (this.airCoolerPriceSelected.length == 0) {
              _data4.Price = "";
            } else {
              var _stringValue6 = "";

              for (var _index18 in this.airCoolerPriceSelected) {
                if (_stringValue6.length == 0) {
                  _stringValue6 = "'" + this.airCoolerPriceSelected[_index18] + "'";
                } else {
                  _stringValue6 = _stringValue6 + "," + "'" + this.airCoolerPriceSelected[_index18] + "'";
                }
              }

              _data4.Price = _stringValue6;
            }

            if (this.airCoolerTypeSelected.length == 0) {
              _data4.Type = "";
            } else {
              var _stringValue7 = "";

              for (var _index19 in this.airCoolerTypeSelected) {
                if (_stringValue7.length == 0) {
                  _stringValue7 = "'" + this.airCoolerTypeSelected[_index19] + "'";
                } else {
                  _stringValue7 = _stringValue7 + "," + "'" + this.airCoolerTypeSelected[_index19] + "'";
                }
              }

              _data4.Type = _stringValue7;
            }

            this.modalCtrl.dismiss(_data4);
          } else if (this.title == "Air Purifiers") {
            var _data5 = {
              CADR: "",
              CoverageArea: "",
              Price: "",
              close: "false",
              airPurifierCADRSelected: this.airPurifierCADRSelected,
              airPurifierCoverageAreaSelected: this.airPurifierCoverageAreaSelected,
              airPurifierPriceSelected: this.airPurifierPriceSelected
            };

            if (this.airPurifierCADRSelected.length == 0) {
              _data5.CADR = "";
            } else {
              var _stringValue8 = "";

              for (var _index20 in this.airPurifierCADRSelected) {
                if (_stringValue8.length == 0) {
                  _stringValue8 = "'" + this.airPurifierCADRSelected[_index20] + "'";
                } else {
                  _stringValue8 = _stringValue8 + "," + "'" + this.airPurifierCADRSelected[_index20] + "'";
                }
              }

              _data5.CADR = _stringValue8;
            }

            if (this.airPurifierCoverageAreaSelected.length == 0) {
              _data5.CoverageArea = "";
            } else {
              var _stringValue9 = "";

              for (var _index21 in this.airPurifierCoverageAreaSelected) {
                if (_stringValue9.length == 0) {
                  _stringValue9 = "'" + this.airPurifierCoverageAreaSelected[_index21] + "'";
                } else {
                  _stringValue9 = _stringValue9 + "," + "'" + this.airPurifierCoverageAreaSelected[_index21] + "'";
                }
              }

              _data5.CoverageArea = _stringValue9;
            }

            if (this.airPurifierPriceSelected.length == 0) {
              _data5.Price = "";
            } else {
              var _stringValue10 = "";

              for (var _index22 in this.airPurifierPriceSelected) {
                if (_stringValue10.length == 0) {
                  _stringValue10 = "'" + this.airPurifierPriceSelected[_index22] + "'";
                } else {
                  _stringValue10 = _stringValue10 + "," + "'" + this.airPurifierPriceSelected[_index22] + "'";
                }
              }

              _data5.Price = _stringValue10;
            }

            this.modalCtrl.dismiss(_data5);
          } else if (this.title == "Water Purifiers") {
            var _data6 = {
              Technology: "",
              ModelName: "",
              Capacity: "",
              Price: "",
              close: "false",
              waterPurifierTechnologySelected: this.waterPurifierTechnologySelected,
              waterPurifierModelNameSelected: this.waterPurifierModelNameSelected,
              waterPurifierCapacitySelected: this.waterPurifierCapacitySelected,
              waterPurifierPriseSelected: this.waterPurifierPriseSelected
            };

            if (this.waterPurifierTechnologySelected.length == 0) {
              _data6.Technology = "";
            } else {
              var _stringValue11 = "";

              for (var _index23 in this.waterPurifierTechnologySelected) {
                if (_stringValue11.length == 0) {
                  _stringValue11 = "'" + this.waterPurifierTechnologySelected[_index23] + "'";
                } else {
                  _stringValue11 = _stringValue11 + "," + "'" + this.waterPurifierTechnologySelected[_index23] + "'";
                }
              }

              _data6.Technology = _stringValue11;
            }

            if (this.waterPurifierModelNameSelected.length == 0) {
              _data6.ModelName = "";
            } else {
              var _stringValue12 = "";

              for (var _index24 in this.waterPurifierModelNameSelected) {
                if (_stringValue12.length == 0) {
                  _stringValue12 = "'" + this.waterPurifierModelNameSelected[_index24] + "'";
                } else {
                  _stringValue12 = _stringValue12 + "," + "'" + this.waterPurifierModelNameSelected[_index24] + "'";
                }
              }

              _data6.ModelName = _stringValue12;
            }

            if (this.waterPurifierCapacitySelected.length == 0) {
              _data6.Capacity = "";
            } else {
              var _stringValue13 = "";

              for (var _index25 in this.waterPurifierCapacitySelected) {
                if (_stringValue13.length == 0) {
                  _stringValue13 = "'" + this.waterPurifierCapacitySelected[_index25] + "'";
                } else {
                  _stringValue13 = _stringValue13 + "," + "'" + this.waterPurifierCapacitySelected[_index25] + "'";
                }
              }

              _data6.Capacity = _stringValue13;
            }

            if (this.waterPurifierPriseSelected.length == 0) {
              _data6.Price = "";
            } else {
              var _stringValue14 = "";

              for (var _index26 in this.waterPurifierPriseSelected) {
                if (_stringValue14.length == 0) {
                  _stringValue14 = "'" + this.waterPurifierPriseSelected[_index26] + "'";
                } else {
                  _stringValue14 = _stringValue14 + "," + "'" + this.waterPurifierPriseSelected[_index26] + "'";
                }
              }

              _data6.Price = _stringValue14;
            }

            this.modalCtrl.dismiss(_data6);
          }
        }
      }, {
        key: "clockOnAirPurifierCADR",
        value: function clockOnAirPurifierCADR(value) {
          for (var i in this.airPurifierCADR) {
            if (this.airPurifierCADR[i].value == value) {
              this.airPurifierCADR[i].flage = !this.airPurifierCADR[i].flage;
            }
          }

          if (this.airPurifierCADRSelected.length == 0) {
            this.airPurifierCADRSelected.push(value);
            this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airPurifierCADRSelected) {
              if (this.airPurifierCADRSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airPurifierCADRSelected.push(value);
              this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;
            } else {
              this.airPurifierCADRSelected.splice(removeItemIndex, 1);
              this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;
            }
          }
        }
      }, {
        key: "clickOnAirPurifierCoverageArea",
        value: function clickOnAirPurifierCoverageArea(value) {
          for (var i in this.airPurifierCoverageArea) {
            if (this.airPurifierCoverageArea[i].value == value) {
              this.airPurifierCoverageArea[i].flage = !this.airPurifierCoverageArea[i].flage;
            }
          }

          if (this.airPurifierCoverageAreaSelected.length == 0) {
            this.airPurifierCoverageAreaSelected.push(value);
            this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airPurifierCoverageAreaSelected) {
              if (this.airPurifierCoverageAreaSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airPurifierCoverageAreaSelected.push(value);
              this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;
            } else {
              this.airPurifierCoverageAreaSelected.splice(removeItemIndex, 1);
              this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;
            }
          }
        }
      }, {
        key: "clickOnAirPurifierPrice",
        value: function clickOnAirPurifierPrice(value) {
          for (var i in this.airPurifierMRP) {
            if (this.airPurifierMRP[i].value == value) {
              this.airPurifierMRP[i].flage = !this.airPurifierMRP[i].flage;
            }
          }

          if (this.airPurifierPriceSelected.length == 0) {
            this.airPurifierPriceSelected.push(value);
            this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airPurifierPriceSelected) {
              if (this.airPurifierPriceSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airPurifierPriceSelected.push(value);
              this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;
            } else {
              this.airPurifierPriceSelected.splice(removeItemIndex, 1);
              this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;
            }
          }
        }
      }, {
        key: "clickOnWaterPurifierTechnology",
        value: function clickOnWaterPurifierTechnology(value) {
          for (var i in this.waterPurifieTechnology) {
            if (this.waterPurifieTechnology[i].value == value) {
              this.waterPurifieTechnology[i].flage = !this.waterPurifieTechnology[i].flage;
            }
          }

          if (this.waterPurifierTechnologySelected.length == 0) {
            this.waterPurifierTechnologySelected.push(value);
            this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.waterPurifierTechnologySelected) {
              if (this.waterPurifierTechnologySelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.waterPurifierTechnologySelected.push(value);
              this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;
            } else {
              this.waterPurifierTechnologySelected.splice(removeItemIndex, 1);
              this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;
            }
          }
        }
      }, {
        key: "clickOnWaterPurifierModelName",
        value: function clickOnWaterPurifierModelName(value) {
          for (var i in this.waterPurifieModelName) {
            if (this.waterPurifieModelName[i].value == value) {
              this.waterPurifieModelName[i].flage = !this.waterPurifieModelName[i].flage;
            }
          }

          if (this.waterPurifierModelNameSelected.length == 0) {
            this.waterPurifierModelNameSelected.push(value);
            this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.waterPurifierModelNameSelected) {
              if (this.waterPurifierModelNameSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.waterPurifierModelNameSelected.push(value);
              this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;
            } else {
              this.waterPurifierModelNameSelected.splice(removeItemIndex, 1);
              this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;
            }
          }
        }
      }, {
        key: "clickOnWaterPurifierCapacity",
        value: function clickOnWaterPurifierCapacity(value) {
          for (var i in this.waterPurifieCapacity) {
            if (this.waterPurifieCapacity[i].value == value) {
              this.waterPurifieCapacity[i].flage = !this.waterPurifieCapacity[i].flage;
            }
          }

          if (this.waterPurifierCapacitySelected.length == 0) {
            this.waterPurifierCapacitySelected.push(value);
            this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.waterPurifierCapacitySelected) {
              if (this.waterPurifierCapacitySelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.waterPurifierCapacitySelected.push(value);
              this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;
            } else {
              this.waterPurifierCapacitySelected.splice(removeItemIndex, 1);
              this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;
            }
          }
        }
      }, {
        key: "clickOnWaterPurifierPrice",
        value: function clickOnWaterPurifierPrice(value) {
          for (var i in this.waterPurifieMRP) {
            if (this.waterPurifieMRP[i].value == value) {
              this.waterPurifieMRP[i].flage = !this.waterPurifieMRP[i].flage;
            }
          }

          if (this.waterPurifierPriseSelected.length == 0) {
            this.waterPurifierPriseSelected.push(value);
            this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.waterPurifierPriseSelected) {
              if (this.waterPurifierPriseSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.waterPurifierPriseSelected.push(value);
              this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;
            } else {
              this.waterPurifierPriseSelected.splice(removeItemIndex, 1);
              this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;
            }
          }
        }
      }, {
        key: "clickOnAirCoolerType",
        value: function clickOnAirCoolerType(value) {
          for (var i in this.airCoolerType) {
            if (this.airCoolerType[i].value == value) {
              this.airCoolerType[i].flage = !this.airCoolerType[i].flage;
            }
          }

          if (this.airCoolerTypeSelected.length == 0) {
            this.airCoolerTypeSelected.push(value);
            this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length; // console.log("this.airCoolerTypeSelected 1",this.airCoolerTypeSelectedLength);
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airCoolerTypeSelected) {
              if (this.airCoolerTypeSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airCoolerTypeSelected.push(value);
              this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length; // console.log("this.airCoolerTypeSelected 2",this.airCoolerTypeSelectedLength);
            } else {
              this.airCoolerTypeSelected.splice(removeItemIndex, 1);
              this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length; // console.log("this.airCoolerTypeSelected 3",this.airCoolerTypeSelectedLength);
            }
          }
        }
      }, {
        key: "clickOnAirCoolerPrice",
        value: function clickOnAirCoolerPrice(value) {
          for (var i in this.airCoolerMRP) {
            if (this.airCoolerMRP[i].value == value) {
              this.airCoolerMRP[i].flage = !this.airCoolerMRP[i].flage;
            }
          }

          if (this.airCoolerPriceSelected.length == 0) {
            this.airCoolerPriceSelected.push(value);
            this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length; // console.log("airCoolerPriceSelectedLength",this.airCoolerPriceSelectedLength);
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airCoolerPriceSelected) {
              if (this.airCoolerPriceSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airCoolerPriceSelected.push(value);
              this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length; // console.log("airCoolerPriceSelectedLength",this.airCoolerPriceSelectedLength);
            } else {
              this.airCoolerPriceSelected.splice(removeItemIndex, 1);
              this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length; // console.log("airCoolerPriceSelectedLength",this.airCoolerPriceSelectedLength);
            }
          }
        }
      }, {
        key: "clickOnAirCoolerCapacity",
        value: function clickOnAirCoolerCapacity(value) {
          for (var i in this.airCoolerCapacity) {
            if (this.airCoolerCapacity[i].value == value) {
              this.airCoolerCapacity[i].flage = !this.airCoolerCapacity[i].flage;
            }
          }

          if (this.airCoolerCapacitySelected.length == 0) {
            this.airCoolerCapacitySelected.push(value);
            this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length; // console.log("airCoolerCapacitySelectedLength",this.airCoolerCapacitySelectedLength);
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airCoolerCapacitySelected) {
              if (this.airCoolerCapacitySelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airCoolerCapacitySelected.push(value);
              this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length; // console.log("airCoolerCapacitySelectedLength",this.airCoolerCapacitySelectedLength);
            } else {
              this.airCoolerCapacitySelected.splice(removeItemIndex, 1);
              this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length; // console.log("airCoolerCapacitySelectedLength",this.airCoolerCapacitySelectedLength);
            }
          }
        }
      }, {
        key: "clickOnAirConditionerSubCategory",
        value: function clickOnAirConditionerSubCategory(value) {
          for (var i in this.airConditionerSubCategory) {
            if (this.airConditionerSubCategory[i].value == value) {
              this.airConditionerSubCategory[i].flage = !this.airConditionerSubCategory[i].flage;
            }
          }

          if (this.airConditionerSubCategorySelected.length == 0) {
            this.airConditionerSubCategorySelected.push(value);
            this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length; // console.log("airConditionerSubCategorySelectedLenght",this.airConditionerSubCategorySelectedLenght);
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airConditionerSubCategorySelected) {
              if (this.airConditionerSubCategorySelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airConditionerSubCategorySelected.push(value);
              this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length; // console.log("airConditionerSubCategorySelectedLenght",this.airConditionerSubCategorySelectedLenght);
            } else {
              this.airConditionerSubCategorySelected.splice(removeItemIndex, 1);
              this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length; // console.log("airConditionerSubCategorySelectedLenght",this.airConditionerSubCategorySelectedLenght);
            }
          }
        }
      }, {
        key: "clickOnAirConditionerCapacity",
        value: function clickOnAirConditionerCapacity(value) {
          for (var i in this.airConditionerCapacity) {
            if (this.airConditionerCapacity[i].value == value) {
              this.airConditionerCapacity[i].flage = !this.airConditionerCapacity[i].flage;
            }
          }

          if (this.airConditionerCapacitySelected.length == 0) {
            this.airConditionerCapacitySelected.push(value);
            this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length; // console.log("airConditionerCapacitySelectedLength",this.airConditionerCapacitySelectedLength);
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airConditionerCapacitySelected) {
              if (this.airConditionerCapacitySelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airConditionerCapacitySelected.push(value);
              this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length; // console.log("airConditionerCapacitySelectedLength",this.airConditionerCapacitySelectedLength);
            } else {
              this.airConditionerCapacitySelected.splice(removeItemIndex, 1);
              this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length; // console.log("airConditionerCapacitySelectedLength",this.airConditionerCapacitySelectedLength);
            }
          }
        }
      }, {
        key: "clickOnAirConditionerSeries",
        value: function clickOnAirConditionerSeries(value) {
          for (var i in this.airConditionerSeries) {
            if (this.airConditionerSeries[i].value == value) {
              this.airConditionerSeries[i].flage = !this.airConditionerSeries[i].flage;
            }
          }

          if (this.airConditionerSeriesSelected.length == 0) {
            this.airConditionerSeriesSelected.push(value);
            this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airConditionerSeriesSelected) {
              if (this.airConditionerSeriesSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airConditionerSeriesSelected.push(value);
              this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;
            } else {
              this.airConditionerSeriesSelected.splice(removeItemIndex, 1);
              this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;
            }
          }
        }
      }, {
        key: "clickOnAirConditionerStarRating",
        value: function clickOnAirConditionerStarRating(value) {
          for (var i in this.airConditionerStarRating) {
            if (this.airConditionerStarRating[i].value == value) {
              this.airConditionerStarRating[i].flage = !this.airConditionerStarRating[i].flage;
            }
          }

          if (this.airConditionerStarRatingSelected.length == 0) {
            this.airConditionerStarRatingSelected.push(value);
            this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airConditionerStarRatingSelected) {
              if (this.airConditionerStarRatingSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airConditionerStarRatingSelected.push(value);
              this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;
            } else {
              this.airConditionerStarRatingSelected.splice(removeItemIndex, 1);
              this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;
            }
          }
        }
      }, {
        key: "clickOnAirConditionerPrise",
        value: function clickOnAirConditionerPrise(value) {
          for (var i in this.airConditionerMRP) {
            if (this.airConditionerMRP[i].value == value) {
              this.airConditionerMRP[i].flage = !this.airConditionerMRP[i].flage;
            }
          }

          if (this.airConditionerPriseSelected.length == 0) {
            this.airConditionerPriseSelected.push(value);
            this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;
          } else {
            var count = 0;
            var removeItemIndex = 0;

            for (var index in this.airConditionerPriseSelected) {
              if (this.airConditionerPriseSelected[index] == value) {
                count = 1;
                removeItemIndex = Number(index);
              }
            }

            if (count == 0) {
              this.airConditionerPriseSelected.push(value);
              this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;
            } else {
              this.airConditionerPriseSelected.splice(removeItemIndex, 1);
              this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;
            }
          }
        }
      }, {
        key: "getFilterAirConditioner",
        value: function getFilterAirConditioner() {
          var _this48 = this;

          this.route.queryParams.subscribe(function (params) {
            if (params && params.title) {
              _this48.title = params.title;
              var param = {};

              if (params.title == "Air Conditioners") {
                param = {
                  air_conditioner: "Air Conditioners"
                };
              }

              _this48.loadingController.create({
                message: 'Please wait'
              }).then(function (res) {
                res.present();

                if (navigator.onLine) {
                  _this48.http.post(_this48.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe(function (response) {
                    Object.keys(response).map(function (key) {
                      //console.log("Res******", response[key]);
                      _this48.airConditionerSubCategory = [];
                      _this48.airConditionerCapacity = [];
                      _this48.airConditionerSeries = [];
                      _this48.airConditionerStarRating = [];
                      _this48.airConditionerMRP = [];

                      for (var index in response[key]) {
                        if (index == "capacity") {
                          for (var i in response[key][index][1]) {
                            var object = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            object.value = response[key][index][1][i];
                            object.showValue = response[key][index][1][i];

                            _this48.airConditionerCapacity.push(object);
                          }
                        }

                        if (index == "mrp") {
                          for (var _i46 in response[key][index][1]) {
                            var _object = {
                              value: "",
                              showValue: "",
                              flage: false
                            };

                            if (response[key][index][1][_i46].replace(/,/g, '').includes("and above")) {
                              var array = response[key][index][1][_i46].replace(/,/g, '').split(" ");

                              _object.value = array[0] + "-" + "99999999";
                              _object.showValue = response[key][index][1][_i46];

                              _this48.airConditionerMRP.push(_object);
                            } else {
                              _object.value = response[key][index][1][_i46].replace(/,/g, '');
                              _object.showValue = response[key][index][1][_i46];

                              _this48.airConditionerMRP.push(_object);
                            }
                          }
                        }

                        if (index == "series") {
                          for (var _i47 in response[key][index][1]) {
                            var _object2 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            _object2.value = response[key][index][1][_i47];
                            _object2.showValue = response[key][index][1][_i47];

                            _this48.airConditionerSeries.push(_object2);
                          }
                        }

                        if (index == "star_rating") {
                          for (var _i48 in response[key][index][1]) {
                            var _object3 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            _object3.value = response[key][index][1][_i48] + " star";
                            _object3.showValue = response[key][index][1][_i48];

                            _this48.airConditionerStarRating.push(_object3);
                          }
                        }

                        if (index == "subcategory") {
                          for (var _i49 in response[key][index][1]) {
                            var _object4 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            _object4.value = response[key][index][1][_i49];
                            _object4.showValue = response[key][index][1][_i49];

                            _this48.airConditionerSubCategory.push(_object4);
                          }
                        }
                      }

                      _this48.assignFilterPreveas();

                      res.dismiss();
                    });
                  }, function (err) {
                    res.dismiss();
                    console.log("err.........", JSON.stringify(err));
                  });
                } else {
                  res.dismiss();
                  console.log("no internat connection");
                }
              });
            }
          });
        }
      }, {
        key: "getFilterAirCooler",
        value: function getFilterAirCooler() {
          var _this49 = this;

          this.route.queryParams.subscribe(function (params) {
            if (params && params.title) {
              _this49.title = params.title;
              var param = {};

              if (params.title == "Air Coolers") {
                param = {
                  air_cooler: "Air Coolers"
                };
              }

              _this49.loadingController.create({
                message: 'Please wait'
              }).then(function (res) {
                res.present();

                if (navigator.onLine) {
                  _this49.http.post(_this49.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe(function (response) {
                    Object.keys(response).map(function (key) {
                      //console.log("Res******", response[key]);
                      _this49.airCoolerType = [];
                      _this49.airCoolerMRP = [];
                      _this49.airCoolerCapacity = [];

                      for (var index in response[key]) {
                        if (index == "capacity") {
                          for (var i in response[key][index][1]) {
                            var object = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            object.value = response[key][index][1][i];
                            object.showValue = response[key][index][1][i];

                            _this49.airCoolerCapacity.push(object);
                          }
                        }

                        if (index == "mrp") {
                          for (var _i50 in response[key][index][1]) {
                            var _object5 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };

                            if (response[key][index][1][_i50].replace(/,/g, '').includes("and above")) {
                              var array = response[key][index][1][_i50].replace(/,/g, '').split(" ");

                              _object5.value = array[0] + "-" + "99999999";
                              _object5.showValue = response[key][index][1][_i50];

                              _this49.airCoolerMRP.push(_object5);
                            } else {
                              _object5.value = response[key][index][1][_i50].replace(/,/g, '');
                              _object5.showValue = response[key][index][1][_i50];

                              _this49.airCoolerMRP.push(_object5);
                            }
                          }
                        }

                        if (index == "type") {
                          for (var _i51 in response[key][index][1]) {
                            var _object6 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            _object6.value = response[key][index][1][_i51];
                            _object6.showValue = response[key][index][1][_i51];

                            _this49.airCoolerType.push(_object6);
                          }
                        }
                      }

                      _this49.assignFilterPreveas();

                      res.dismiss();
                    });
                  }, function (err) {
                    res.dismiss();
                    console.log("err.........", JSON.stringify(err));
                  });
                } else {
                  res.dismiss();
                  console.log("no internat connection");
                }
              });
            }
          });
        }
      }, {
        key: "getFilterAirPurifier",
        value: function getFilterAirPurifier() {
          var _this50 = this;

          this.route.queryParams.subscribe(function (params) {
            if (params && params.title) {
              _this50.title = params.title;
              var param = {};

              if (params.title == "Air Purifiers") {
                param = {
                  air_purifier: "Air Purifiers"
                };
              }

              _this50.loadingController.create({
                message: 'Please wait'
              }).then(function (res) {
                res.present();

                if (navigator.onLine) {
                  _this50.http.post(_this50.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe(function (response) {
                    Object.keys(response).map(function (key) {
                      //console.log("Res******", response[key]);
                      _this50.airPurifierMRP = [];
                      _this50.airPurifierCoverageArea = [];
                      _this50.airPurifierCADR = [];

                      for (var index in response[key]) {
                        if (index == "area_cover") {
                          for (var i in response[key][index][1]) {
                            var object = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            object.value = response[key][index][1][i];
                            object.showValue = response[key][index][1][i];

                            _this50.airPurifierCoverageArea.push(object);
                          }
                        }

                        if (index == "mrp") {
                          for (var _i52 in response[key][index][1]) {
                            var _object7 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };

                            if (response[key][index][1][_i52].replace(/,/g, '').includes("and above")) {
                              var array = response[key][index][1][_i52].replace(/,/g, '').split(" ");

                              _object7.value = array[0] + "-" + "99999999";
                              _object7.showValue = response[key][index][1][_i52];

                              _this50.airPurifierMRP.push(_object7);
                            } else {
                              _object7.value = response[key][index][1][_i52].replace(/,/g, '');
                              _object7.showValue = response[key][index][1][_i52];

                              _this50.airPurifierMRP.push(_object7);
                            }
                          }
                        }

                        if (index == "cadr") {
                          for (var _i53 in response[key][index][1]) {
                            var _object8 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            _object8.value = response[key][index][1][_i53];
                            _object8.showValue = response[key][index][1][_i53];

                            _this50.airPurifierCADR.push(_object8);
                          }
                        }
                      }

                      _this50.assignFilterPreveas();

                      res.dismiss();
                    });
                  }, function (err) {
                    res.dismiss();
                    console.log("err.........", JSON.stringify(err));
                  });
                } else {
                  res.dismiss();
                  console.log("no internat connection");
                }
              });
            }
          });
        }
      }, {
        key: "getFilterWaterPurifier",
        value: function getFilterWaterPurifier() {
          var _this51 = this;

          this.route.queryParams.subscribe(function (params) {
            if (params && params.title) {
              _this51.title = params.title;
              var param = {};

              if (params.title == "Water Purifiers") {
                param = {
                  water_purifier: "Water Purifiers"
                };
              }

              _this51.loadingController.create({
                message: 'Please wait'
              }).then(function (res) {
                res.present();

                if (navigator.onLine) {
                  _this51.http.post(_this51.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe(function (response) {
                    Object.keys(response).map(function (key) {
                      //console.log("Res******", response[key]);
                      _this51.waterPurifieTechnology = [];
                      _this51.waterPurifieMRP = [];
                      _this51.waterPurifieCapacity = [];
                      _this51.waterPurifieModelName = [];

                      for (var index in response[key]) {
                        if (index == "purification_technology") {
                          for (var i in response[key][index][1]) {
                            var object = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            object.value = response[key][index][1][i];
                            object.showValue = response[key][index][1][i];

                            _this51.waterPurifieTechnology.push(object);
                          }
                        }

                        if (index == "mrp") {
                          for (var _i54 in response[key][index][1]) {
                            var _object9 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };

                            if (response[key][index][1][_i54].replace(/,/g, '').includes("and above")) {
                              var array = response[key][index][1][_i54].replace(/,/g, '').split(" ");

                              _object9.value = array[0] + "-" + "99999999";
                              _object9.showValue = response[key][index][1][_i54];

                              _this51.waterPurifieMRP.push(_object9);
                            } else {
                              _object9.value = response[key][index][1][_i54].replace(/,/g, '');
                              _object9.showValue = response[key][index][1][_i54];

                              _this51.waterPurifieMRP.push(_object9);
                            }
                          }
                        }

                        if (index == "storage_capacity") {
                          for (var _i55 in response[key][index][1]) {
                            var _object10 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            _object10.value = response[key][index][1][_i55];
                            _object10.showValue = response[key][index][1][_i55];

                            _this51.waterPurifieCapacity.push(_object10);
                          }
                        }

                        if (index == "model_series") {
                          for (var _i56 in response[key][index][1]) {
                            var _object11 = {
                              value: "",
                              showValue: "",
                              flage: false
                            };
                            _object11.value = response[key][index][1][_i56];
                            _object11.showValue = response[key][index][1][_i56];

                            _this51.waterPurifieModelName.push(_object11);
                          }
                        }
                      }

                      _this51.assignFilterPreveas();

                      res.dismiss();
                    });
                  }, function (err) {
                    res.dismiss();
                    console.log("err.........", JSON.stringify(err));
                  });
                } else {
                  res.dismiss();
                  console.log("no internat connection");
                }
              });
            }
          });
        }
      }]);

      return AppShellPage;
    }();

    AppShellPage.ɵfac = function AppShellPage_Factory(t) {
      return new (t || AppShellPage)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_categories_categories_page__WEBPACK_IMPORTED_MODULE_4__["CategoriesPage"]));
    };

    AppShellPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AppShellPage,
      selectors: [["app-showcase-shell"]],
      decls: 21,
      vars: 4,
      consts: [["color", "lightest"], ["slot", "start"], [2, "font-size", "18px", "font-weight", "bold", "padding-left", "15px", "color", "#09509d"], ["slot", "end"], ["name", "close-outline", 1, "icon_close", 2, "font-size", "33px", 3, "click"], [1, "showcase-content"], ["class", "row", 4, "ngIf"], [1, "ion-no-border"], [1, "details-purchase-options-row", 2, "margin-top", "10px"], ["size", "6"], ["expand", "block", 1, "select-variant-btn", 2, "font-weight", "600", 3, "click"], ["expand", "block", "fill", "outline", 1, "select-variant-btn", 2, "font-weight", "600", "color", "black", 3, "click"], [1, "row"], [1, "col"], [1, "tabs"], [1, "tab"], ["type", "checkbox", "id", "chck1"], ["for", "chck1", 1, "tab-label"], ["class", "bedge-align", "color", "primary", 4, "ngIf"], [1, "tab-content"], [4, "ngFor", "ngForOf"], ["type", "checkbox", "id", "chck2"], ["for", "chck2", 1, "tab-label"], ["type", "checkbox", "id", "chck3"], ["for", "chck3", 1, "tab-label"], ["type", "checkbox", "id", "chck4"], ["for", "chck4", 1, "tab-label"], ["type", "checkbox", "id", "chck5"], ["for", "chck5", 1, "tab-label"], ["color", "primary", 1, "bedge-align"], [3, "click"], [3, "checked"]],
      template: function AppShellPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "FILTER");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ion-buttons", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ion-icon", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_Template_ion_icon_click_6_listener() {
            return ctx.closeModal();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ion-content", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, AppShellPage_div_8_Template, 38, 10, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, AppShellPage_div_9_Template, 24, 6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, AppShellPage_div_10_Template, 24, 6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, AppShellPage_div_11_Template, 31, 8, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "ion-footer", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "ion-toolbar");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "ion-row", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "ion-col", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ion-button", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_Template_ion_button_click_16_listener() {
            return ctx.onClickApply();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Apply ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "ion-col", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "ion-button", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppShellPage_Template_ion_button_click_19_listener() {
            return ctx.clearFilter();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Clear ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.title == "Air Conditioners");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.title == "Air Coolers");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.title == "Air Purifiers");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.title == "Water Purifiers");
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonIcon"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonContent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonFooter"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonCol"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonButton"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonBadge"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonList"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonItem"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonLabel"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonCheckbox"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["BooleanValueAccessor"]],
      styles: ["@charset \"UTF-8\";\n.showcase-content[_ngcontent-%COMP%] {\n  --background: var(--app-background);\n}\n.showcase-content[_ngcontent-%COMP%]   ion-item-divider[_ngcontent-%COMP%] {\n  --background: var(--ion-color-secondary);\n  --color: var(--ion-color-lightest);\n  text-transform: uppercase;\n  font-weight: 500;\n  letter-spacing: 1px;\n}\n.showcase-content[_ngcontent-%COMP%]   .showcase-section[_ngcontent-%COMP%] {\n  margin: 20px 20px 60px;\n}\n.showcase-content[_ngcontent-%COMP%]   pre[_ngcontent-%COMP%] {\n  font-size: 14px;\n  background: #CCC;\n  padding: 10px;\n}\n.showcase-content[_ngcontent-%COMP%]   code[_ngcontent-%COMP%] {\n  color: #F92672;\n  font-weight: 500;\n}\n.menu_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n}\n.clsoe-icon[_ngcontent-%COMP%] {\n  padding-left: 10px;\n  padding-right: 10px;\n  font-size: 30px;\n}\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 5px;\n}\n.copy_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n.main-title[_ngcontent-%COMP%] {\n  padding-left: 20px !important;\n  padding-top: 15px !important;\n  font-size: 18px;\n  text-align: left;\n  font-weight: 600;\n  color: #09509d;\n  text-transform: uppercase;\n}\n.icon_close[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.7em;\n  color: #000000;\n  padding-right: 6px;\n}\n.list-md[_ngcontent-%COMP%] {\n  padding: 0px !important;\n}\nbody[_ngcontent-%COMP%] {\n  color: #2c3e50;\n  background: #ecf0f1;\n  padding: 0 1em 1em;\n}\nh1[_ngcontent-%COMP%] {\n  margin: 0;\n  line-height: 2;\n  text-align: center;\n}\nh2[_ngcontent-%COMP%] {\n  margin: 0 0 0.5em;\n  font-weight: normal;\n}\ninput[_ngcontent-%COMP%] {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n.row[_ngcontent-%COMP%] {\n  display: flex;\n}\n.row[_ngcontent-%COMP%]   .col[_ngcontent-%COMP%] {\n  flex: 1;\n}\n\n.tabs[_ngcontent-%COMP%] {\n  overflow: hidden;\n}\n.tab[_ngcontent-%COMP%] {\n  width: 100%;\n  color: black;\n  overflow: hidden;\n}\n.tab-label[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  padding: 1em;\n  font-weight: 600;\n  cursor: pointer;\n  border-bottom: 1px solid grey;\n  \n}\n.tab-label[_ngcontent-%COMP%]::after {\n  content: \"\u276F\";\n  width: 1em;\n  height: 1em;\n  text-align: center;\n  transition: all 0.35s;\n}\n.tab-content[_ngcontent-%COMP%] {\n  max-height: 0;\n  color: #2c3e50;\n  background: white;\n  transition: all 0.35s;\n}\n.tab-close[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-end;\n  padding: 1em;\n  font-size: 0.75em;\n  background: #2c3e50;\n  cursor: pointer;\n}\ninput[_ngcontent-%COMP%]:checked    + .tab-label[_ngcontent-%COMP%]::after {\n  transform: rotate(90deg);\n}\ninput[_ngcontent-%COMP%]:checked    ~ .tab-content[_ngcontent-%COMP%] {\n  max-height: 100vh;\n}\n.bedge-align[_ngcontent-%COMP%] {\n  position: absolute;\n  margin-left: 75%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hvd2Nhc2UvYXBwLXNoZWxsL2FwcC1zaGVsbC5wYWdlLnNjc3MiLCIvaG9tZS9vZW0vZGV2L3dvcmtzcGFjZS9CbHVlU3Rhcklrc3VsYS9zcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9hcHAtc2hlbGwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLG1DQUFBO0FERUY7QUNBRTtFQUNFLHdDQUFBO0VBQ0Esa0NBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QURFSjtBQ0NFO0VBQ0Usc0JBQUE7QURDSjtBQ0VFO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBREFKO0FDR0U7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QURESjtBQ0lBO0VBRUUsZUFBQTtBREZGO0FDT0E7RUFDRSxrQkFBQTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtBREpKO0FDTUE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURIRjtBQ01BO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FESEY7QUNNQTtFQUNFLDZCQUFBO0VBQ0EsNEJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5QkFBQTtBREhGO0FDS0E7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QURGRjtBQ0lBO0VBQ0UsdUJBQUE7QURERjtBQ1VBO0VBQ0UsY0FKUztFQUtULG1CQUpPO0VBS1Asa0JBQUE7QURQRjtBQ1NBO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBRE5GO0FDUUE7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0FETEY7QUNPQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QURKRjtBQ09BO0VBQ0UsYUFBQTtBREpGO0FDT0U7RUFDRSxPQUFBO0FETEo7QUNXQSxxQkFBQTtBQUNBO0VBRUUsZ0JBQUE7QURWRjtBQ2FBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBRFZGO0FDWUU7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxZQUFBO0VBRUEsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSxTQUFBO0FEWEo7QUNlSTtFQUNFLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7QURiTjtBQ21CRTtFQUNFLGFBQUE7RUFFQSxjQXZFTztFQXdFUCxpQkFBQTtFQUNBLHFCQUFBO0FEbEJKO0FDb0JFO0VBQ0UsYUFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBaEZPO0VBaUZQLGVBQUE7QURsQko7QUM2Qkk7RUFDRSx3QkFBQTtBRDNCTjtBQzhCRTtFQUNFLGlCQUFBO0FENUJKO0FDaUNBO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtBRDlCRiIsImZpbGUiOiJzcmMvYXBwL3Nob3djYXNlL2FwcC1zaGVsbC9hcHAtc2hlbGwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcbn1cbi5zaG93Y2FzZS1jb250ZW50IGlvbi1pdGVtLWRpdmlkZXIge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItbGlnaHRlc3QpO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogNTAwO1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgLnNob3djYXNlLXNlY3Rpb24ge1xuICBtYXJnaW46IDIwcHggMjBweCA2MHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgcHJlIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBiYWNrZ3JvdW5kOiAjQ0NDO1xuICBwYWRkaW5nOiAxMHB4O1xufVxuLnNob3djYXNlLWNvbnRlbnQgY29kZSB7XG4gIGNvbG9yOiAjRjkyNjcyO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4ubWVudV9pY29uIHtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuXG4uY2xzb2UtaWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG4uaGVhcnRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5jb3B5X2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4ubWFpbi10aXRsZSB7XG4gIHBhZGRpbmctbGVmdDogMjBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXRvcDogMTVweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uaWNvbl9jbG9zZSB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjdlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctcmlnaHQ6IDZweDtcbn1cblxuLmxpc3QtbWQge1xuICBwYWRkaW5nOiAwcHggIWltcG9ydGFudDtcbn1cblxuYm9keSB7XG4gIGNvbG9yOiAjMmMzZTUwO1xuICBiYWNrZ3JvdW5kOiAjZWNmMGYxO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5cbmgxIHtcbiAgbWFyZ2luOiAwO1xuICBsaW5lLWhlaWdodDogMjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5oMiB7XG4gIG1hcmdpbjogMCAwIDAuNWVtO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuXG5pbnB1dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgb3BhY2l0eTogMDtcbiAgei1pbmRleDogLTE7XG59XG5cbi5yb3cge1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnJvdyAuY29sIHtcbiAgZmxleDogMTtcbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLnRhYiB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogYmxhY2s7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4udGFiLWxhYmVsIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBwYWRkaW5nOiAxZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gIC8qIEljb24gKi9cbn1cbi50YWItbGFiZWw6OmFmdGVyIHtcbiAgY29udGVudDogXCLina9cIjtcbiAgd2lkdGg6IDFlbTtcbiAgaGVpZ2h0OiAxZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMzVzO1xufVxuLnRhYi1jb250ZW50IHtcbiAgbWF4LWhlaWdodDogMDtcbiAgY29sb3I6ICMyYzNlNTA7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB0cmFuc2l0aW9uOiBhbGwgMC4zNXM7XG59XG4udGFiLWNsb3NlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcbiAgcGFkZGluZzogMWVtO1xuICBmb250LXNpemU6IDAuNzVlbTtcbiAgYmFja2dyb3VuZDogIzJjM2U1MDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuaW5wdXQ6Y2hlY2tlZCArIC50YWItbGFiZWw6OmFmdGVyIHtcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xufVxuaW5wdXQ6Y2hlY2tlZCB+IC50YWItY29udGVudCB7XG4gIG1heC1oZWlnaHQ6IDEwMHZoO1xufVxuXG4uYmVkZ2UtYWxpZ24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA3NSU7XG59IiwiLnNob3djYXNlLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWFwcC1iYWNrZ3JvdW5kKTtcblxuICBpb24taXRlbS1kaXZpZGVyIHtcbiAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1saWdodGVzdCk7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAxcHg7XG4gIH1cblxuICAuc2hvd2Nhc2Utc2VjdGlvbiB7XG4gICAgbWFyZ2luOiAyMHB4IDIwcHggNjBweDtcbiAgfVxuXG4gIHByZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGJhY2tncm91bmQ6ICNDQ0M7XG4gICAgcGFkZGluZzogMTBweDtcbiAgfVxuXG4gIGNvZGUge1xuICAgIGNvbG9yOiAjRjkyNjcyO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIH1cbn1cbi5tZW51X2ljb257XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogN3B4O1xuICAvLyBjb2xvcjogd2hpdGU7XG59XG4uY2xzb2UtaWNvbntcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gICAgZm9udC1zaXplOiAzMHB4O1xufVxuLmhlYXJ0X2ljb257XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5jb3B5X2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIGNvbG9yOiB3aGl0ZTtcbn1cbi5tYWluLXRpdGxle1xuICBwYWRkaW5nLWxlZnQ6IDIwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5pY29uX2Nsb3Nle1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS43ZW07XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nLXJpZ2h0OiA2cHg7XG59XG4ubGlzdC1tZHtcbiAgcGFkZGluZzogMHB4ICFpbXBvcnRhbnQ7XG59XG5cblxuLy8gKioqKioqKioqKioqKioqKioqKioqKioqKiBBY2NvcmRpYW5cblxuJG1pZG5pZ2h0OiAjMmMzZTUwO1xuJGNsb3VkczogI2VjZjBmMTtcbi8vIEdlbmVyYWxcbmJvZHkge1xuICBjb2xvcjogJG1pZG5pZ2h0O1xuICBiYWNrZ3JvdW5kOiAkY2xvdWRzO1xuICBwYWRkaW5nOiAwIDFlbSAxZW07XG59XG5oMSB7XG4gIG1hcmdpbjogMDtcbiAgbGluZS1oZWlnaHQ6IDI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmgyIHtcbiAgbWFyZ2luOiAwIDAgLjVlbTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbmlucHV0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBvcGFjaXR5OiAwO1xuICB6LWluZGV4OiAtMTtcbn1cbi8vIExheW91dFxuLnJvdyB7XG4gIGRpc3BsYXk6ZmxleDtcbiAgLy8gbWFyZ2luLWxlZnQ6IDEwcHg7IFxuICAvLyBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gIC5jb2wge1xuICAgIGZsZXg6MTtcbiAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgLy8gbWFyZ2luLWxlZnQ6IDFlbTtcbiAgICB9XG4gIH1cbn1cbi8qIEFjY29yZGlvbiBzdHlsZXMgKi9cbi50YWJzIHtcbiAgLy8gYm9yZGVyLXJhZGl1czogOHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3gtc2hhZG93OiAwIDRweCA0cHggLTJweCByZ2JhKDAsMCwwLDAuNSk7XG59XG4udGFiIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGNvbG9yOiBibGFjaztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICYtbGFiZWwge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGdyZXk7XG4gICAgLyogSWNvbiAqL1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgY29udGVudDogXCJcXDI3NkZcIjtcbiAgICAgIHdpZHRoOiAxZW07XG4gICAgICBoZWlnaHQ6IDFlbTtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuMzVzO1xuICAgIH1cbiAgICAvLyAuYWN0aXZlOmFmdGVyIHtcbiAgICAvLyAgIGNvbnRlbnQ6IFwiXFwyNzk2XCI7IC8qIFVuaWNvZGUgY2hhcmFjdGVyIGZvciBcIm1pbnVzXCIgc2lnbiAoLSkgKi9cbiAgICAvLyB9XG4gIH1cbiAgJi1jb250ZW50IHtcbiAgICBtYXgtaGVpZ2h0OiAwO1xuICAgIC8vIHBhZGRpbmc6IDAgMWVtO1xuICAgIGNvbG9yOiAkbWlkbmlnaHQ7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgdHJhbnNpdGlvbjogYWxsIC4zNXM7XG4gIH1cbiAgJi1jbG9zZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIHBhZGRpbmc6IDFlbTtcbiAgICBmb250LXNpemU6IDAuNzVlbTtcbiAgICBiYWNrZ3JvdW5kOiAkbWlkbmlnaHQ7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICY6aG92ZXIge1xuICAgICAgLy8gYmFja2dyb3VuZDogZGFya2VuKCRtaWRuaWdodCwgMTAlKTtcbiAgICB9XG4gIH1cbn1cblxuLy8gOmNoZWNrZWRcbmlucHV0OmNoZWNrZWQge1xuICArIC50YWItbGFiZWwge1xuICAgIC8vIGJhY2tncm91bmQ6IGRhcmtlbigkbWlkbmlnaHQsIDEwJSk7XG4gICAgJjo6YWZ0ZXIge1xuICAgICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgIH1cbiAgfVxuICB+IC50YWItY29udGVudCB7XG4gICAgbWF4LWhlaWdodDogMTAwdmg7XG4gICAgLy8gcGFkZGluZzogMWVtO1xuICB9XG59XG5cbi5iZWRnZS1hbGlnbntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tbGVmdDogNzUlO1xufVxuXG4vLyAuYmVkZ2UtYWxpZ24tbXJwe1xuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgIHRvcDogNjVweDtcbi8vICAgcmlnaHQ6IDQ1cHhcbi8vIH1cbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppShellPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-showcase-shell',
          templateUrl: './app-shell.page.html',
          styleUrls: ['./app-shell.page.scss']
        }]
      }], function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
        }, {
          type: _categories_categories_page__WEBPACK_IMPORTED_MODULE_4__["CategoriesPage"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/utils/resolver-helper.ts":
  /*!******************************************!*\
    !*** ./src/app/utils/resolver-helper.ts ***!
    \******************************************/

  /*! exports provided: ResolverHelper */

  /***/
  function srcAppUtilsResolverHelperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResolverHelper", function () {
      return ResolverHelper;
    });
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var ResolverHelper = /*#__PURE__*/function () {
      function ResolverHelper() {
        _classCallCheck(this, ResolverHelper);
      }

      _createClass(ResolverHelper, null, [{
        key: "extractData",
        // More info on function overloads here: https://www.typescriptlang.org/docs/handbook/functions.html#overloads
        value: function extractData(source, constructor) {
          if (source instanceof _shell_data_store__WEBPACK_IMPORTED_MODULE_0__["DataStore"]) {
            return source.state;
          } else if (source instanceof constructor) {
            // The right side of instanceof should be an expression evaluating to a constructor function (ie. a class), not a type
            // That's why we included an extra parameter which acts as a constructor function for type T
            // (see: https://github.com/microsoft/TypeScript/issues/5236)
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(source);
          }
        }
      }]);

      return ResolverHelper;
    }();
    /***/

  }
}]);
//# sourceMappingURL=default~app-shell-app-shell-module~product-listing-product-listing-module-es5.js.map