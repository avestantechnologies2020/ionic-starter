function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-catalogue-product-catalogue-module"], {
  /***/
  "./src/app/product-catalogue/product-catalogue.model.ts":
  /*!**************************************************************!*\
    !*** ./src/app/product-catalogue/product-catalogue.model.ts ***!
    \**************************************************************/

  /*! exports provided: ProductCatalogueItemModel, ProductCatalogueModel */

  /***/
  function srcAppProductCatalogueProductCatalogueModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductCatalogueItemModel", function () {
      return ProductCatalogueItemModel;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductCatalogueModel", function () {
      return ProductCatalogueModel;
    });
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ../shell/data-store */
    "./src/app/shell/data-store.ts");

    var ProductCatalogueItemModel = function ProductCatalogueItemModel() {
      _classCallCheck(this, ProductCatalogueItemModel);
    };

    var ProductCatalogueModel = /*#__PURE__*/function (_shell_data_store__WE) {
      _inherits(ProductCatalogueModel, _shell_data_store__WE);

      var _super = _createSuper(ProductCatalogueModel);

      function ProductCatalogueModel() {
        var _this;

        _classCallCheck(this, ProductCatalogueModel);

        _this = _super.call(this);
        _this.items = [new ProductCatalogueItemModel(), new ProductCatalogueItemModel(), new ProductCatalogueItemModel(), new ProductCatalogueItemModel()];
        return _this;
      }

      return ProductCatalogueModel;
    }(_shell_data_store__WEBPACK_IMPORTED_MODULE_0__["ShellModel"]);
    /***/

  },

  /***/
  "./src/app/product-catalogue/product-catalogue.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/product-catalogue/product-catalogue.module.ts ***!
    \***************************************************************/

  /*! exports provided: ProductCataloguetPagePageModule */

  /***/
  function srcAppProductCatalogueProductCatalogueModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductCataloguetPagePageModule", function () {
      return ProductCataloguetPagePageModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");
    /* harmony import */


    var _product_catalogue_product_catalogue_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../product-catalogue/product-catalogue.service */
    "./src/app/product-catalogue/product-catalogue.service.ts");
    /* harmony import */


    var _product_catalogue_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./product-catalogue.page */
    "./src/app/product-catalogue/product-catalogue.page.ts");
    /* harmony import */


    var _product_catalogue_resolver__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./product-catalogue.resolver */
    "./src/app/product-catalogue/product-catalogue.resolver.ts");

    var routes = [{
      path: '',
      component: _product_catalogue_page__WEBPACK_IMPORTED_MODULE_8__["ProductCataloguetPage"],
      resolve: {
        data: _product_catalogue_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductCatalogueResolver"]
      }
    }];

    var ProductCataloguetPagePageModule = function ProductCataloguetPagePageModule() {
      _classCallCheck(this, ProductCataloguetPagePageModule);
    };

    ProductCataloguetPagePageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: ProductCataloguetPagePageModule
    });
    ProductCataloguetPagePageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function ProductCataloguetPagePageModule_Factory(t) {
        return new (t || ProductCataloguetPagePageModule)();
      },
      providers: [_product_catalogue_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductCatalogueResolver"], _product_catalogue_product_catalogue_service__WEBPACK_IMPORTED_MODULE_7__["ProductCatalogueService"]],
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ProductCataloguetPagePageModule, {
        declarations: [_product_catalogue_page__WEBPACK_IMPORTED_MODULE_8__["ProductCataloguetPage"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductCataloguetPagePageModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]],
          declarations: [_product_catalogue_page__WEBPACK_IMPORTED_MODULE_8__["ProductCataloguetPage"]],
          providers: [_product_catalogue_resolver__WEBPACK_IMPORTED_MODULE_9__["ProductCatalogueResolver"], _product_catalogue_product_catalogue_service__WEBPACK_IMPORTED_MODULE_7__["ProductCatalogueService"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/product-catalogue/product-catalogue.page.ts":
  /*!*************************************************************!*\
    !*** ./src/app/product-catalogue/product-catalogue.page.ts ***!
    \*************************************************************/

  /*! exports provided: ProductCataloguetPage */

  /***/
  function srcAppProductCatalogueProductCataloguePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductCataloguetPage", function () {
      return ProductCataloguetPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./../app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/downloader/ngx */
    "./node_modules/@ionic-native/downloader/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/firebase-analytics/ngx */
    "./node_modules/@ionic-native/firebase-analytics/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _categories_categories_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./../categories/categories.page */
    "./src/app/categories/categories.page.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function ProductCataloguetPage_ion_col_14_ion_icon_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 21);
      }
    }

    function ProductCataloguetPage_ion_col_14_ion_icon_7_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 22);
      }
    }

    function ProductCataloguetPage_ion_col_14_ion_icon_8_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 23);
      }
    }

    function ProductCataloguetPage_ion_col_14_ion_icon_9_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 24);
      }
    }

    function ProductCataloguetPage_ion_col_14_Template(rf, ctx) {
      if (rf & 1) {
        var _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-col", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-row");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "ion-col", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "a", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ProductCataloguetPage_ion_col_14_ion_icon_6_Template, 1, 0, "ion-icon", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ProductCataloguetPage_ion_col_14_ion_icon_7_Template, 1, 0, "ion-icon", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, ProductCataloguetPage_ion_col_14_ion_icon_8_Template, 1, 0, "ion-icon", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, ProductCataloguetPage_ion_col_14_ion_icon_9_Template, 1, 0, "ion-icon", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "ion-row");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "ion-col", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "ion-button", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ProductCataloguetPage_ion_col_14_Template_ion_button_click_12_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r8);

          var item_r2 = ctx.$implicit;

          var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r7.downloadPDF(item_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "Download Catalogue");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", item_r2.category, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r2.category == "Air Conditioner");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r2.category == "Air Cooler");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r2.category == "Air Purifier");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r2.category == "Water Purifier");
      }
    }

    function ProductCataloguetPage_div_15_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "No product found");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    var _c0 = function _c0() {
      return ["/favourites"];
    };

    var _c1 = function _c1() {
      return ["/categories"];
    };

    var ProductCataloguetPage = /*#__PURE__*/function () {
      function ProductCataloguetPage(http, route, router, loadingController, modalCtrl, storage, myapp, downloader, toastCtrl, platform, firebaseAnalytics, inAppBrowser, categoriesPage, socialSharing) {
        _classCallCheck(this, ProductCataloguetPage);

        this.http = http;
        this.route = route;
        this.router = router;
        this.loadingController = loadingController;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.myapp = myapp;
        this.downloader = downloader;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.firebaseAnalytics = firebaseAnalytics;
        this.inAppBrowser = inAppBrowser;
        this.categoriesPage = categoriesPage;
        this.socialSharing = socialSharing;
        this.product_leaflet = [];
        this.getproductLeaflet();
      }

      _createClass(ProductCataloguetPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "getproductLeaflet",
        value: function getproductLeaflet() {
          var _this2 = this;

          this.loadingController.create({
            message: 'Please wait'
          }).then(function (res) {
            res.present();

            if (navigator.onLine) {
              _this2.http.get(_this2.categoriesPage.apiBaseUrl + '/bluestar_api/product_leaflet').subscribe(function (response) {
                Object.keys(response).map(function (key) {
                  console.log("response[key]", response[key]);
                  res.dismiss();
                  _this2.product_leaflet = response[key].product_leaflet;

                  if (_this2.product_leaflet) {
                    console.log("product_leaflet", _this2.product_leaflet);
                    _this2.noRecords = _this2.product_leaflet.length;
                    console.log("noRecords if", _this2.noRecords);
                  } else {
                    _this2.noRecords = 0;
                    console.log("noRecords else", _this2.noRecords);
                  }
                });
              }, function (err) {
                res.dismiss();

                _this2.presentToastInternert("No internet connection. Please try again later.");
              });
            } else {
              res.dismiss();

              _this2.presentToastInternert("No internet connection. Please try again later.");
            }
          });
        }
      }, {
        key: "downloadPDF",
        value: function downloadPDF(item) {
          var _this3 = this;

          console.log("pdf_link", item.pdf_link);
          console.log("category", item.category);
          var request = {
            uri: item.pdf_link,
            title: 'Blue Star',
            description: '',
            mimeType: 'application/pdf',
            visibleInDownloadsUi: true,
            notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["NotificationVisibility"].VisibleNotifyCompleted,
            destinationInExternalPublicDir: {
              dirType: 'Download',
              subPath: item.category + '.pdf'
            }
          };
          console.log("request", request);

          if (this.platform.is("ios")) {
            var browser = this.inAppBrowser.create(item.pdf_link);
            this.firebaseAnalytics.logEvent('downloads_products', {
              category: item.category
            }).then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          } else {
            this.loadingController.create({
              message: 'Please wait while downloading'
            }).then(function (res) {
              res.present();

              _this3.downloader.download(request).then(function (location) {
                res.dismiss();

                _this3.presentToast("Downloaded in device download folder");

                _this3.firebaseAnalytics.logEvent('downloads_products', {
                  category: item.category
                }).then(function (res) {
                  return console.log(res);
                })["catch"](function (error) {
                  return console.error(error);
                });
              })["catch"](function (error) {
                res.dismiss();
                console.error(error);
              });
            });
          }
        }
      }, {
        key: "presentToastInternert",
        value: function presentToastInternert(msg) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var toast;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.toastCtrl.create({
                      message: msg,
                      duration: 8000,
                      position: 'bottom',
                      cssClass: "msg-align"
                    });

                  case 2:
                    toast = _context.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(text) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var toast;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.toastCtrl.create({
                      message: text,
                      duration: 7000,
                      position: 'bottom',
                      cssClass: "msg-align"
                    });

                  case 2:
                    toast = _context2.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }]);

      return ProductCataloguetPage;
    }();

    ProductCataloguetPage.ɵfac = function ProductCataloguetPage_Factory(t) {
      return new (t || ProductCataloguetPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["Downloader"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_10__["FirebaseAnalytics"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__["InAppBrowser"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_categories_categories_page__WEBPACK_IMPORTED_MODULE_11__["CategoriesPage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_8__["SocialSharing"]));
    };

    ProductCataloguetPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: ProductCataloguetPage,
      selectors: [["app-product-catalogue"]],
      decls: 16,
      vars: 6,
      consts: [["color", "primary"], ["slot", "start"], ["src", "assets/logo.png", 1, "logo"], ["slot", "end"], ["name", "heart-outline", 1, "heart_icon", 3, "routerLink"], ["name", "home-outline", 1, "home_icon", 3, "routerLink"], [1, "fashion-listing-content"], [2, "text-align", "center", "font-weight", "600", "margin-bottom", "10px", "font-size", "20px"], [1, "items-row"], ["size", "6", "class", "listing-item", 4, "ngFor", "ngForOf"], [4, "ngIf"], ["size", "6", 1, "listing-item"], [1, "item-image-wrapper"], [2, "text-align", "center", "padding-top", "15px", "padding-bottom", "15px", "font-size", "18px"], [1, "image-anchor", 2, "text-align", "center", "font-size", "5rem"], ["name", "snow-outline", 4, "ngIf"], ["name", "aperture-outline", 4, "ngIf"], ["name", "flower-outline", 4, "ngIf"], ["name", "water-outline", 4, "ngIf"], [2, "padding", "5px"], ["expand", "block", 2, "font-weight", "600", "text-transform", "capitalize", "font-size", "12px", 3, "click"], ["name", "snow-outline"], ["name", "aperture-outline"], ["name", "flower-outline"], ["name", "water-outline"], [2, "font-size", "20px", "text-align", "center", "margin-top", "50%"]],
      template: function ProductCataloguetPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "ion-back-button");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "img", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "ion-buttons", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "ion-icon", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "ion-icon", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "ion-content", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "ion-col", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, " Product Catalogue ");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "ion-row", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](14, ProductCataloguetPage_ion_col_14_Template, 14, 5, "ion-col", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, ProductCataloguetPage_div_15_Template, 3, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](4, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](5, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.product_leaflet);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.noRecords == 0);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonIcon"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["RouterLinkDelegate"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLink"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonCol"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgIf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonButton"]],
      styles: ["[_nghost-%COMP%] {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background);\n  --page-items-gutter: calc(var(--page-margin) / 2);\n  --page-color: #cb328f;\n}\n\n.fashion-listing-content[_ngcontent-%COMP%] {\n  --background: var(--page-background);\n  --padding-start: 8px;\n  --padding-end: 8px;\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin);\n}\n\n.items-row[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: 0;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%] {\n  margin-bottom: calc(var(--page-margin) * 1);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]:nth-child(odd) {\n  padding-right: 4px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]:nth-child(even) {\n  padding-left: 4px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%] {\n  border: 1px solid #09509d;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%]   .image-anchor[_ngcontent-%COMP%] {\n  display: block;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: 0px;\n  padding: 5px 5px 0px;\n  text-align: center;\n  min-height: 68px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%] {\n  margin-bottom: calc(var(--page-margin) / 2);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%] {\n  margin: 0px;\n  font-size: 13px;\n  font-weight: 400;\n  text-overflow: ellipsis;\n  white-space: unset;\n  overflow: hidden;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%]   .name-anchor[_ngcontent-%COMP%] {\n  color: var(--ion-color-primary);\n  display: block;\n  text-decoration: none;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%] {\n  align-items: center;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%] {\n  padding-bottom: 5px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:first-child {\n  padding-right: calc(var(--page-margin) / 2);\n  text-align: right;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:first-child:last-child {\n  text-align: center;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:last-child {\n  padding-left: calc(var(--page-margin) / 2);\n  text-align: left;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .separator[_ngcontent-%COMP%] {\n  max-width: 0px;\n  border-right: solid 2px var(--ion-color-light-shade);\n  align-self: stretch;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .item-sale-price[_ngcontent-%COMP%] {\n  display: block;\n  font-weight: 400;\n  font-size: 14px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .item-original-price[_ngcontent-%COMP%] {\n  display: block;\n  text-decoration: line-through;\n  color: var(--ion-color-medium-shade);\n  font-size: 14px;\n}\n\nion-header[_ngcontent-%COMP%]   ion-toolbar[_ngcontent-%COMP%]:first-of-type {\n  text-align: center;\n}\n\n.logo[_ngcontent-%COMP%] {\n  width: 70%;\n  margin-top: 5px;\n}\n\n.home_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 10px;\n}\n\n.icon-heart-red[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-bottom: -8px;\n  padding-right: 3px;\n}\n\n.icon-heart-black[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-bottom: -8px;\n  padding-right: 3px;\n}\n\n.icon-share[_ngcontent-%COMP%] {\n  width: 29px;\n  margin-bottom: -7px;\n  padding-right: 3px;\n}\n\n.icon-download[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-bottom: -4px;\n  padding-right: 3px;\n}\n\n.col-padding[_ngcontent-%COMP%] {\n  padding-left: 15px !important;\n  padding-right: 15px !important;\n  padding-bottom: 7px !important;\n}\n\n.main-title[_ngcontent-%COMP%] {\n  color: #09509d;\n  font-weight: bold;\n}\n\n.icon_funnel[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.5em;\n  color: #09509d;\n}\n\n.product-text[_ngcontent-%COMP%] {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n\n\n.split[_ngcontent-%COMP%] {\n  height: 25%;\n  width: 100%;\n  z-index: 1;\n  position: relative;\n  overflow-x: hidden;\n}\n\n\n\n\n\n.column[_ngcontent-%COMP%] {\n  float: left;\n  padding: 10px;\n  height: 100%;\n  \n}\n\n.centered[_ngcontent-%COMP%] {\n  color: balck;\n}\n\n.icon-delete[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 90%;\n  transform: translate(-50%, -50%);\n  font-size: 25px;\n  color: #09509d;\n}\n\n.div-fixed[_ngcontent-%COMP%] {\n  position: fixed;\n  z-index: 999;\n  background-color: white;\n  width: 100%;\n  overflow-y: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvcHJvZHVjdC1jYXRhbG9ndWUvc3R5bGVzL3Byb2R1Y3QtY2F0YWxvZ3VlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcHJvZHVjdC1jYXRhbG9ndWUvc3R5bGVzL3Byb2R1Y3QtY2F0YWxvZ3VlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNFLHFDQUFBO0VBQ0Esd0NBQUE7RUFFQSxpREFBQTtFQUNBLHFCQUFBO0FDRkY7O0FETUE7RUFDRSxvQ0FBQTtFQUdDLG9CQUFBO0VBQ0Qsa0JBQUE7RUFDQSxpQ0FBQTtFQUNBLG9DQUFBO0FDTEY7O0FEUUE7RUFDRSw0QkFBQTtBQ0xGOztBRE9FO0VBQ0UsMkNBQUE7QUNMSjs7QURPSTtFQUVFLGtCQUFBO0FDTk47O0FEU0k7RUFFRSxpQkFBQTtBQ1JOOztBRFdJO0VBRUUseUJBQUE7QUNWTjs7QURXTTtFQUNFLGNBQUE7QUNUUjs7QURhSTtFQUNFLDhCQUFBO0VBRUEsb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDWk47O0FEYU07RUFDRSwyQ0FBQTtBQ1hSOztBRGFRO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUVBLHVCQUFBO0VBRUEsa0JBQUE7RUFDQSxnQkFBQTtBQ2JWOztBRGVVO0VBQ0UsK0JBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7QUNiWjs7QURrQk07RUFDRSxtQkFBQTtBQ2hCUjs7QURrQlE7RUFDRSxtQkFBQTtBQ2hCVjs7QURpQlU7RUFDRSwyQ0FBQTtFQUNBLGlCQUFBO0FDZlo7O0FEaUJZO0VBQ0Usa0JBQUE7QUNmZDs7QURtQlU7RUFDRSwwQ0FBQTtFQUNBLGdCQUFBO0FDakJaOztBRHFCUTtFQUNFLGNBQUE7RUFDQSxvREFBQTtFQUNBLG1CQUFBO0FDbkJWOztBRHNCUTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUdBLGVBQUE7QUN0QlY7O0FEMkJRO0VBQ0UsY0FBQTtFQUNBLDZCQUFBO0VBQ0Esb0NBQUE7RUFDQSxlQUFBO0FDekJWOztBRCtCQTtFQUNFLGtCQUFBO0FDNUJGOztBRDhCQTtFQUVFLFVBQUE7RUFDQSxlQUFBO0FDNUJGOztBRDhCQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtBQzNCRjs7QUQ2QkE7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUMxQkY7O0FEaUNBO0VBTUUsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNuQ0Y7O0FEcUNBO0VBT0UsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUN4Q0Y7O0FEaURBO0VBTUUsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUNuREY7O0FEcURBO0VBTUUsZUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUN2REY7O0FEeURBO0VBQ0UsNkJBQUE7RUFDQSw4QkFBQTtFQUNBLDhCQUFBO0FDdERGOztBRDZEQTtFQUVFLGNBQUE7RUFDQSxpQkFBQTtBQzNERjs7QUQ2REE7RUFDRSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FDMURGOztBRDhEQTtFQUNFLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQzNERjs7QURnRUEsNkJBQUE7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUVBLGtCQUFBO0FDOURGOztBRHFFQSwwQkFBQTs7QUFFQSw4REFBQTs7QUFDQTtFQUNFLFdBQUE7RUFFQSxhQUFBO0VBQ0EsWUFBQTtFQUFjLDhDQUFBO0FDbkVoQjs7QUR1RUE7RUFNRSxZQUFBO0FDekVGOztBRDJFQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FDeEVGOztBRDJFQTtFQUNFLGVBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUN4RUYiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0LWNhdGFsb2d1ZS9zdHlsZXMvcHJvZHVjdC1jYXRhbG9ndWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XG4gIC0tcGFnZS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgLS1wYWdlLWl0ZW1zLWd1dHRlcjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgLS1wYWdlLWNvbG9yOiAjY2IzMjhmO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi5mYXNoaW9uLWxpc3RpbmctY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcbiAgLy8gLS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIC8vIC0tcGFkZGluZy1lbmQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgIC0tcGFkZGluZy1zdGFydDogOHB4O1xuICAtLXBhZGRpbmctZW5kOiA4cHg7O1xuICAtLXBhZGRpbmctdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIC0tcGFkZGluZy1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcbn1cblxuLml0ZW1zLXJvdyB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDA7XG5cbiAgLmxpc3RpbmctaXRlbSB7XG4gICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAxKTtcblxuICAgICY6bnRoLWNoaWxkKG9kZCkge1xuICAgICAgLy8gcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1pdGVtcy1ndXR0ZXIpO1xuICAgICAgcGFkZGluZy1yaWdodDogNHB4O1xuICAgIH1cblxuICAgICY6bnRoLWNoaWxkKGV2ZW4pIHtcbiAgICAgIC8vIHBhZGRpbmctbGVmdDogdmFyKC0tcGFnZS1pdGVtcy1ndXR0ZXIpO1xuICAgICAgcGFkZGluZy1sZWZ0OiA0cHg7XG4gICAgfVxuXG4gICAgLml0ZW0taW1hZ2Utd3JhcHBlciB7XG4gICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgICAgIC5pbWFnZS1hbmNob3Ige1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuaXRlbS1ib2R5IHtcbiAgICAgIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDBweDtcblxuICAgICAgcGFkZGluZzogNXB4IDVweCAwcHg7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBtaW4taGVpZ2h0OiA2OHB4O1xuICAgICAgLm1haW4taW5mbyB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG5cbiAgICAgICAgLml0ZW0tbmFtZSB7XG4gICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG5cbiAgICAgICAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICAgICAgICAvLyB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICAgIHdoaXRlLXNwYWNlOiB1bnNldDtcbiAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuXG4gICAgICAgICAgLm5hbWUtYW5jaG9yIHtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgLnNlY29uZGFyeS1pbmZvIHtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgICAgICAucHJpY2UtY29sIHtcbiAgICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgICAgICY6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuXG4gICAgICAgICAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgJjpsYXN0LWNoaWxkIHtcbiAgICAgICAgICAgIHBhZGRpbmctbGVmdDogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLnNlcGFyYXRvciB7XG4gICAgICAgICAgbWF4LXdpZHRoOiAwcHg7XG4gICAgICAgICAgYm9yZGVyLXJpZ2h0OiBzb2xpZCAycHggdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcbiAgICAgICAgICBhbGlnbi1zZWxmOiBzdHJldGNoO1xuICAgICAgICB9XG5cbiAgICAgICAgLml0ZW0tc2FsZS1wcmljZSB7XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICAgICAgICAvLyBjb2xvcjogdmFyKC0tcGFnZS1jb2xvcik7XG4gICAgICAgICAgLy8gZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAvLyBjb2xvcjogZ3JleTtcbiAgICAgICAgICAvLyBtaW4taGVpZ2h0OiAzMHB4O1xuICAgICAgICB9XG5cbiAgICAgICAgLml0ZW0tb3JpZ2luYWwtcHJpY2Uge1xuICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xuICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlKTtcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZXtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmxvZ297XG4gIC8vIHdpZHRoOiA1NSU7XG4gIHdpZHRoOiA3MCU7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5ob21lX2ljb257XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4uaGVhcnRfaWNvbntcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG4vLyAuY29weV9pY29ue1xuLy8gICBmb250LXNpemU6IDI1cHg7XG4vLyAgIG1hcmdpbi1yaWdodDogNXB4O1xuLy8gICAvLyBjb2xvcjogd2hpdGU7XG4vLyB9XG4uaWNvbi1oZWFydC1yZWR7XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbiAgLy8gcGFkZGluZy10b3A6IDVweDtcbiAgLy8gZm9udC1zaXplOiAyNXB4O1xuICAvLyBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtOHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG4uaWNvbi1oZWFydC1ibGFja3tcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogNXB4O1xuICAvLyBmb250LXNpemU6IDI1cHg7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xuXG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogLThweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuLy8gLmljb24tc2hhcmV7XG4vLyAgIGZsb2F0OiByaWdodDtcbi8vICAgcGFkZGluZy10b3A6IDVweDtcbi8vICAgZm9udC1zaXplOiAyNXB4O1xuLy8gICBjb2xvcjogIzA5NTA5ZDtcbi8vICAgcGFkZGluZy1yaWdodDogMTBweDtcbi8vIH1cbi5pY29uLXNoYXJle1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIC8vIHBhZGRpbmctdG9wOiAycHg7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyB3aWR0aDogMTIlXG4gIHdpZHRoOiAyOXB4O1xuICBtYXJnaW4tYm90dG9tOiAtN3B4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG4uaWNvbi1kb3dubG9hZHtcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBwYWRkaW5nLXRvcDogNXB4O1xuICAvLyBmb250LXNpemU6IDI1cHg7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICBmb250LXNpemU6IDIycHg7XG4gIG1hcmdpbi1ib3R0b206IC00cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cbi5jb2wtcGFkZGluZ3tcbiAgcGFkZGluZy1sZWZ0OiAxNXB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDE1cHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1ib3R0b206IDdweCAhaW1wb3J0YW50O1xuICAvLyBwYWRkaW5nLXRvcDogMTBweCAhaW1wb3J0YW50O1xuICAvLyBib3JkZXItdG9wOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwOTUwOWQ7XG4gIC8vIG1hcmdpbi1ib3R0b206IDVweDtcbiAgLy8gbWFyZ2luLXRvcDogLTEwcHg7XG59XG4ubWFpbi10aXRsZXtcbiAgLy8gZm9udC1zaXplOiAxN3B4O1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uaWNvbl9mdW5uZWx7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG59XG5cbi5wcm9kdWN0LXRleHR7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuIH1cblxuXG4gLy8gKioqKioqKioqKioqKioqKioqKioqXG4vKiBTcGxpdCB0aGUgc2NyZWVuIGluIGhhbGYgKi9cbi5zcGxpdCB7XG4gIGhlaWdodDogMjUlO1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogMTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAvLyB0b3A6IDA7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgLy8gcGFkZGluZy10b3A6IDIwcHg7XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWFlNGU0O1xuICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgLy8gYm9yZGVyLXRvcDogMXB4IHNvbGlkICM5RTlFOUU7XG59XG5cbi8qIENvbnRyb2wgdGhlIGxlZnQgc2lkZSAqL1xuXG4vKiBDcmVhdGUgdGhyZWUgZXF1YWwgY29sdW1ucyB0aGF0IGZsb2F0cyBuZXh0IHRvIGVhY2ggb3RoZXIgKi9cbi5jb2x1bW4ge1xuICBmbG9hdDogbGVmdDtcbiAgLy8gd2lkdGg6IDMzLjMzJTtcbiAgcGFkZGluZzogMTBweDtcbiAgaGVpZ2h0OiAxMDAlOyAvKiBTaG91bGQgYmUgcmVtb3ZlZC4gT25seSBmb3IgZGVtb25zdHJhdGlvbiAqL1xuICAvLyBib3JkZXI6IDFweCBzb2xpZDtcbn1cblxuLmNlbnRlcmVkIHtcbiAgLy8gcG9zaXRpb246IGFic29sdXRlO1xuICAvLyB0b3A6IDUwJTtcbiAgLy8gbGVmdDogNTAlO1xuICAvLyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgLy8gdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogYmFsY2s7XG59XG4uaWNvbi1kZWxldGV7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTsgXG4gIHRvcDogNTAlOyBcbiAgbGVmdDogOTAlOyBcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgY29sb3I6ICMwOTUwOWQ7XG59XG5cbi5kaXYtZml4ZWR7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogOTk5O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXk6IGF1dG87XG59IiwiOmhvc3Qge1xuICAtLXBhZ2UtbWFyZ2luOiB2YXIoLS1hcHAtZmFpci1tYXJnaW4pO1xuICAtLXBhZ2UtYmFja2dyb3VuZDogdmFyKC0tYXBwLWJhY2tncm91bmQpO1xuICAtLXBhZ2UtaXRlbXMtZ3V0dGVyOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAtLXBhZ2UtY29sb3I6ICNjYjMyOGY7XG59XG5cbi5mYXNoaW9uLWxpc3RpbmctY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XG4gIC0tcGFkZGluZy1lbmQ6IDhweDtcbiAgLS1wYWRkaW5nLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAtLXBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG59XG5cbi5pdGVtcy1yb3cge1xuICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIHtcbiAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgKiAxKTtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbTpudGgtY2hpbGQob2RkKSB7XG4gIHBhZGRpbmctcmlnaHQ6IDRweDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbTpudGgtY2hpbGQoZXZlbikge1xuICBwYWRkaW5nLWxlZnQ6IDRweDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1pbWFnZS13cmFwcGVyIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzA5NTA5ZDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1pbWFnZS13cmFwcGVyIC5pbWFnZS1hbmNob3Ige1xuICBkaXNwbGF5OiBibG9jaztcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IHtcbiAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xuICBwYWRkaW5nOiA1cHggNXB4IDBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtaW4taGVpZ2h0OiA2OHB4O1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLm1haW4taW5mbyB7XG4gIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAubWFpbi1pbmZvIC5pdGVtLW5hbWUge1xuICBtYXJnaW46IDBweDtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNDAwO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgd2hpdGUtc3BhY2U6IHVuc2V0O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLm1haW4taW5mbyAuaXRlbS1uYW1lIC5uYW1lLWFuY2hvciB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8ge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIC5wcmljZS1jb2wge1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIC5wcmljZS1jb2w6Zmlyc3QtY2hpbGQge1xuICBwYWRkaW5nLXJpZ2h0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyAucHJpY2UtY29sOmZpcnN0LWNoaWxkOmxhc3QtY2hpbGQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8gLnByaWNlLWNvbDpsYXN0LWNoaWxkIHtcbiAgcGFkZGluZy1sZWZ0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIC5zZXBhcmF0b3Ige1xuICBtYXgtd2lkdGg6IDBweDtcbiAgYm9yZGVyLXJpZ2h0OiBzb2xpZCAycHggdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcbiAgYWxpZ24tc2VsZjogc3RyZXRjaDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyAuaXRlbS1zYWxlLXByaWNlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyAuaXRlbS1vcmlnaW5hbC1wcmljZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbmlvbi1oZWFkZXIgaW9uLXRvb2xiYXI6Zmlyc3Qtb2YtdHlwZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmxvZ28ge1xuICB3aWR0aDogNzAlO1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi5ob21lX2ljb24ge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xufVxuXG4uaGVhcnRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjhweDtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4uaWNvbi1oZWFydC1yZWQge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IC04cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cblxuLmljb24taGVhcnQtYmxhY2sge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IC04cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cblxuLmljb24tc2hhcmUge1xuICB3aWR0aDogMjlweDtcbiAgbWFyZ2luLWJvdHRvbTogLTdweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1kb3dubG9hZCB7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgbWFyZ2luLWJvdHRvbTogLTRweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uY29sLXBhZGRpbmcge1xuICBwYWRkaW5nLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1yaWdodDogMTVweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWJvdHRvbTogN3B4ICFpbXBvcnRhbnQ7XG59XG5cbi5tYWluLXRpdGxlIHtcbiAgY29sb3I6ICMwOTUwOWQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uaWNvbl9mdW5uZWwge1xuICBmbG9hdDogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMS41ZW07XG4gIGNvbG9yOiAjMDk1MDlkO1xufVxuXG4ucHJvZHVjdC10ZXh0IHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5cbi8qIFNwbGl0IHRoZSBzY3JlZW4gaW4gaGFsZiAqL1xuLnNwbGl0IHtcbiAgaGVpZ2h0OiAyNSU7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAxO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbn1cblxuLyogQ29udHJvbCB0aGUgbGVmdCBzaWRlICovXG4vKiBDcmVhdGUgdGhyZWUgZXF1YWwgY29sdW1ucyB0aGF0IGZsb2F0cyBuZXh0IHRvIGVhY2ggb3RoZXIgKi9cbi5jb2x1bW4ge1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZzogMTBweDtcbiAgaGVpZ2h0OiAxMDAlO1xuICAvKiBTaG91bGQgYmUgcmVtb3ZlZC4gT25seSBmb3IgZGVtb25zdHJhdGlvbiAqL1xufVxuXG4uY2VudGVyZWQge1xuICBjb2xvcjogYmFsY2s7XG59XG5cbi5pY29uLWRlbGV0ZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDkwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgY29sb3I6ICMwOTUwOWQ7XG59XG5cbi5kaXYtZml4ZWQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDk5OTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdy15OiBhdXRvO1xufSJdfQ== */", "[_nghost-%COMP%] {\n  --shell-color: #cb328f;\n  --shell-color-rgb: 203,50,143;\n}\n\n.is-shell[_nghost-%COMP%]   a[_ngcontent-%COMP%] {\n  pointer-events: none;\n}\n\napp-image-shell.item-image[_ngcontent-%COMP%] {\n  --image-shell-loading-background: white;\n  --image-shell-spinner-color: #5289c3;\n}\n\n.item-name[_ngcontent-%COMP%]   app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .25);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .35);\n  --text-shell-line-height: 14px;\n}\n\n.item-sale-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px;\n}\n\n.item-original-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvcHJvZHVjdC1jYXRhbG9ndWUvc3R5bGVzL3Byb2R1Y3QtY2F0YWxvZ3VlLnNoZWxsLnNjc3MiLCJzcmMvYXBwL3Byb2R1Y3QtY2F0YWxvZ3VlL3N0eWxlcy9wcm9kdWN0LWNhdGFsb2d1ZS5zaGVsbC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0Usc0JBQUE7RUFDQSw2QkFBQTtBQ0RGOztBRFNFO0VBQ0Usb0JBQUE7QUNOSjs7QURVQTtFQUtFLHVDQUFBO0VBQ0Esb0NBQUE7QUNYRjs7QURjQTtFQUNFLG9FQUFBO0VBQ0EsK0RBQUE7RUFDQSw4QkFBQTtBQ1hGOztBRGNBO0VBQ0Usb0VBQUE7RUFDQSwrREFBQTtFQUNBLDhCQUFBO0FDWEY7O0FEY0E7RUFDRSxvRUFBQTtFQUNBLCtEQUFBO0VBQ0EsOEJBQUE7QUNYRiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3QtY2F0YWxvZ3VlL3N0eWxlcy9wcm9kdWN0LWNhdGFsb2d1ZS5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tc2hlbGwtY29sb3I6ICNjYjMyOGY7XG4gIC0tc2hlbGwtY29sb3ItcmdiOiAyMDMsNTAsMTQzO1xufVxuXG4vLyBZb3UgY2FuIGFsc28gYXBwbHkgc2hlZWwgc3R5bGVzIHRvIHRoZSBlbnRpcmUgcGFnZVxuOmhvc3QoLmlzLXNoZWxsKSB7XG4gIC8vIGlvbi1jb250ZW50IHtcbiAgLy8gICBvcGFjaXR5OiAwLjg7XG4gIC8vIH1cbiAgYSB7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gIH1cbn1cblxuYXBwLWltYWdlLXNoZWxsLml0ZW0taW1hZ2Uge1xuICAvLyAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTApO1xuICAvLyAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcblxuICAvLyAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogIzhjYjllYTtcbiAgLS1pbWFnZS1zaGVsbC1sb2FkaW5nLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6ICM1Mjg5YzM7XG59XG5cbi5pdGVtLW5hbWUgYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzUpO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE0cHg7XG59XG5cbi5pdGVtLXNhbGUtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTZweDtcbn1cblxuLml0ZW0tb3JpZ2luYWwtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTZweDtcbn1cbiIsIjpob3N0IHtcbiAgLS1zaGVsbC1jb2xvcjogI2NiMzI4ZjtcbiAgLS1zaGVsbC1jb2xvci1yZ2I6IDIwMyw1MCwxNDM7XG59XG5cbjpob3N0KC5pcy1zaGVsbCkgYSB7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG5hcHAtaW1hZ2Utc2hlbGwuaXRlbS1pbWFnZSB7XG4gIC0taW1hZ2Utc2hlbGwtbG9hZGluZy1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1pbWFnZS1zaGVsbC1zcGlubmVyLWNvbG9yOiAjNTI4OWMzO1xufVxuXG4uaXRlbS1uYW1lIGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yNSk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjM1KTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xufVxuXG4uaXRlbS1zYWxlLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjIwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE2cHg7XG59XG5cbi5pdGVtLW9yaWdpbmFsLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjIwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE2cHg7XG59Il19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ProductCataloguetPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-product-catalogue',
          templateUrl: './product-catalogue.page.html',
          styleUrls: ['./styles/product-catalogue.page.scss', './styles/product-catalogue.shell.scss']
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
        }, {
          type: _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]
        }, {
          type: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_7__["Downloader"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]
        }, {
          type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_10__["FirebaseAnalytics"]
        }, {
          type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_9__["InAppBrowser"]
        }, {
          type: _categories_categories_page__WEBPACK_IMPORTED_MODULE_11__["CategoriesPage"]
        }, {
          type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_8__["SocialSharing"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/product-catalogue/product-catalogue.resolver.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/product-catalogue/product-catalogue.resolver.ts ***!
    \*****************************************************************/

  /*! exports provided: ProductCatalogueResolver */

  /***/
  function srcAppProductCatalogueProductCatalogueResolverTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductCatalogueResolver", function () {
      return ProductCatalogueResolver;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _product_catalogue_product_catalogue_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../product-catalogue/product-catalogue.service */
    "./src/app/product-catalogue/product-catalogue.service.ts");

    var ProductCatalogueResolver = /*#__PURE__*/function () {
      function ProductCatalogueResolver(productCatalogueService) {
        _classCallCheck(this, ProductCatalogueResolver);

        this.productCatalogueService = productCatalogueService;
      }

      _createClass(ProductCatalogueResolver, [{
        key: "resolve",
        value: function resolve() {
          var dataSource = this.productCatalogueService.getListingDataSource();
          var dataStore = this.productCatalogueService.getListingStore(dataSource);
          return dataStore;
        }
      }]);

      return ProductCatalogueResolver;
    }();

    ProductCatalogueResolver.ɵfac = function ProductCatalogueResolver_Factory(t) {
      return new (t || ProductCatalogueResolver)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_product_catalogue_product_catalogue_service__WEBPACK_IMPORTED_MODULE_1__["ProductCatalogueService"]));
    };

    ProductCatalogueResolver.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ProductCatalogueResolver,
      factory: ProductCatalogueResolver.ɵfac
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductCatalogueResolver, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }], function () {
        return [{
          type: _product_catalogue_product_catalogue_service__WEBPACK_IMPORTED_MODULE_1__["ProductCatalogueService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/product-catalogue/product-catalogue.service.ts":
  /*!****************************************************************!*\
    !*** ./src/app/product-catalogue/product-catalogue.service.ts ***!
    \****************************************************************/

  /*! exports provided: ProductCatalogueService */

  /***/
  function srcAppProductCatalogueProductCatalogueServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductCatalogueService", function () {
      return ProductCatalogueService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var _product_catalogue_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./product-catalogue.model */
    "./src/app/product-catalogue/product-catalogue.model.ts"); // import { FashionDetailsModel } from './details/fashion-details.model';


    var ProductCatalogueService = /*#__PURE__*/function () {
      // private detailsDataStore: DataStore<FashionDetailsModel>;
      function ProductCatalogueService(http) {
        _classCallCheck(this, ProductCatalogueService);

        this.http = http;
      }

      _createClass(ProductCatalogueService, [{
        key: "getListingDataSource",
        value: function getListingDataSource() {
          return this.http.get('./assets/sample-data/fashion/listing.json').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (data) {
            // Note: HttpClient cannot know how to instantiate a class for the returned data
            // We need to properly cast types from json data
            var listing = new _product_catalogue_model__WEBPACK_IMPORTED_MODULE_4__["ProductCatalogueModel"](); // The Object.assign() method copies all enumerable own properties from one or more source objects to a target object.
            // Note: If you have non-enummerable properties, you can try a spread operator instead. listing = {...data};
            // (see: https://scotch.io/bar-talk/copying-objects-in-javascript#toc-using-spread-elements-)

            Object.assign(listing, data);
            return listing;
          }));
        }
      }, {
        key: "getListingStore",
        value: function getListingStore(dataSource) {
          // Use cache if available
          if (!this.listingDataStore) {
            // Initialize the model specifying that it is a shell model
            var shellModel = new _product_catalogue_model__WEBPACK_IMPORTED_MODULE_4__["ProductCatalogueModel"]();
            this.listingDataStore = new _shell_data_store__WEBPACK_IMPORTED_MODULE_3__["DataStore"](shellModel); // Trigger the loading mechanism (with shell) in the dataStore

            this.listingDataStore.load(dataSource);
          }

          return this.listingDataStore;
        }
      }]);

      return ProductCatalogueService;
    }();

    ProductCatalogueService.ɵfac = function ProductCatalogueService_Factory(t) {
      return new (t || ProductCatalogueService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
    };

    ProductCatalogueService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ProductCatalogueService,
      factory: ProductCatalogueService.ɵfac
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ProductCatalogueService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/shell/data-store.ts":
  /*!*************************************!*\
    !*** ./src/app/shell/data-store.ts ***!
    \*************************************/

  /*! exports provided: ShellModel, DataStore */

  /***/
  function srcAppShellDataStoreTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShellModel", function () {
      return ShellModel;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStore", function () {
      return DataStore;
    });
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./config/app-shell.config */
    "./src/app/shell/config/app-shell.config.ts");

    var ShellModel = function ShellModel() {
      _classCallCheck(this, ShellModel);

      this.isShell = false;
    };

    var DataStore = /*#__PURE__*/function () {
      function DataStore(shellModel) {
        _classCallCheck(this, DataStore);

        this.shellModel = shellModel; // We wait on purpose 2 secs on local environment when fetching from json to simulate the backend roundtrip.
        // However, in production you should set this delay to 0 in the assets/config/app-shell.config.prod.json file.
        // tslint:disable-next-line:max-line-length

        this.networkDelay = _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings && _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay ? _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay : 0;
        this.timeline = new rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"](1);
      } // Static function with generics
      // (ref: https://stackoverflow.com/a/24293088/1116959)
      // Append a shell (T & ShellModel) to every value (T) emmited to the timeline


      _createClass(DataStore, [{
        key: "load",
        value: function load(dataSourceObservable) {
          var _this4 = this;

          var dataSourceWithShellObservable = DataStore.AppendShell(dataSourceObservable, this.shellModel, this.networkDelay);
          dataSourceWithShellObservable.subscribe(function (dataValue) {
            _this4.timeline.next(dataValue);
          });
        }
      }, {
        key: "state",
        get: function get() {
          return this.timeline.asObservable();
        }
      }], [{
        key: "AppendShell",
        value: function AppendShell(dataObservable, shellModel) {
          var networkDelay = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 400;
          var delayObservable = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(true).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["delay"])(networkDelay)); // Assign shell flag accordingly
          // (ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)

          return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["combineLatest"])([delayObservable, dataObservable]).pipe( // Dismiss unnecessary delayValue
          Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 2),
                delayValue = _ref2[0],
                dataValue = _ref2[1];

            return Object.assign(dataValue, {
              isShell: false
            });
          }), // Set the shell model as the initial value
          Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["startWith"])(Object.assign(shellModel, {
            isShell: true
          })));
        }
      }]);

      return DataStore;
    }();
    /***/

  }
}]);
//# sourceMappingURL=product-catalogue-product-catalogue-module-es5.js.map