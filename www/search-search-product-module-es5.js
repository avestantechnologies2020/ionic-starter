function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["search-search-product-module"], {
  /***/
  "./src/app/search/search-product.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/search/search-product.module.ts ***!
    \*************************************************/

  /*! exports provided: SearchProductPageModule */

  /***/
  function srcAppSearchSearchProductModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SearchProductPageModule", function () {
      return SearchProductPageModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _components_components_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../components/components.module */
    "./src/app/components/components.module.ts");
    /* harmony import */


    var _search_search_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../search/search.service */
    "./src/app/search/search.service.ts");
    /* harmony import */


    var _search_product_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./search-product.page */
    "./src/app/search/search-product.page.ts");
    /* harmony import */


    var _search_product_resolver__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./search-product.resolver */
    "./src/app/search/search-product.resolver.ts");

    var routes = [{
      path: '',
      component: _search_product_page__WEBPACK_IMPORTED_MODULE_8__["SearchProductPage"],
      resolve: {
        data: _search_product_resolver__WEBPACK_IMPORTED_MODULE_9__["SearchProductResolver"]
      }
    }];

    var SearchProductPageModule = function SearchProductPageModule() {
      _classCallCheck(this, SearchProductPageModule);
    };

    SearchProductPageModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: SearchProductPageModule
    });
    SearchProductPageModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function SearchProductPageModule_Factory(t) {
        return new (t || SearchProductPageModule)();
      },
      providers: [_search_product_resolver__WEBPACK_IMPORTED_MODULE_9__["SearchProductResolver"], _search_search_service__WEBPACK_IMPORTED_MODULE_7__["SearchService"]],
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SearchProductPageModule, {
        declarations: [_search_product_page__WEBPACK_IMPORTED_MODULE_8__["SearchProductPage"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SearchProductPageModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes), _components_components_module__WEBPACK_IMPORTED_MODULE_6__["ComponentsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"]],
          declarations: [_search_product_page__WEBPACK_IMPORTED_MODULE_8__["SearchProductPage"]],
          providers: [_search_product_resolver__WEBPACK_IMPORTED_MODULE_9__["SearchProductResolver"], _search_search_service__WEBPACK_IMPORTED_MODULE_7__["SearchService"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/search/search-product.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/search/search-product.page.ts ***!
    \***********************************************/

  /*! exports provided: SearchProductPage */

  /***/
  function srcAppSearchSearchProductPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SearchProductPage", function () {
      return SearchProductPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _utils_resolver_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../utils/resolver-helper */
    "./src/app/utils/resolver-helper.ts");
    /* harmony import */


    var _search_product_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./search-product.model */
    "./src/app/search/search-product.model.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/__ivy_ngcc__/fesm2015/ionic-storage.js");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./../app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/downloader/ngx */
    "./node_modules/@ionic-native/downloader/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/social-sharing/ngx */
    "./node_modules/@ionic-native/social-sharing/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/in-app-browser/ngx */
    "./node_modules/@ionic-native/in-app-browser/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/firebase-analytics/ngx */
    "./node_modules/@ionic-native/firebase-analytics/__ivy_ngcc__/ngx/index.js");
    /* harmony import */


    var _categories_categories_page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./../categories/categories.page */
    "./src/app/categories/categories.page.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ../shell/image-shell/image-shell.component */
    "./src/app/shell/image-shell/image-shell.component.ts");

    function SearchProductPage_ion_row_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-row", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-col", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Total Products : ", ctx_r0.total_product, "");
      }
    }

    function SearchProductPage_div_15_p_12_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

        var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" MRP: \u20B9", ctx_r4.formatNumber(item_r3), "");
      }
    }

    function SearchProductPage_div_15_ion_icon_19_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 32);
      }
    }

    function SearchProductPage_div_15_ion_icon_20_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ion-icon", 33);
      }
    }

    function SearchProductPage_div_15_Template(rf, ctx) {
      if (rf & 1) {
        var _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "a", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "app-image-shell", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SearchProductPage_div_15_Template_div_click_7_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var item_r3 = ctx.$implicit;

          var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r8.clickOnProduct(item_r3.ID, ctx_r8.title, item_r3.SKUCode, item_r3.ProductTitle, item_r3.Image);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, SearchProductPage_div_15_p_12_Template, 2, 1, "p", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "ion-row", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "ion-col", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SearchProductPage_div_15_Template_ion_col_click_14_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var item_r3 = ctx.$implicit;

          var ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r10.downloadPdf(item_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "ion-icon", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Download");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "ion-col", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SearchProductPage_div_15_Template_ion_col_click_18_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var item_r3 = ctx.$implicit;

          var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r11.clickOnFavourites(item_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, SearchProductPage_div_15_ion_icon_19_Template, 1, 0, "ion-icon", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](20, SearchProductPage_div_15_ion_icon_20_Template, 1, 0, "ion-icon", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, "My Products");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "ion-col", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function SearchProductPage_div_15_Template_ion_col_click_23_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r9);

          var item_r3 = ctx.$implicit;

          var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r12.getPdfLink(item_r3.ID, item_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](24, "img", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, "Share");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r3 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("src", item_r3.Image)("alt", "product image");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r3.SKUCode);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](item_r3.ProductTitle);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r3.MRP != null && item_r3.MRP != "" || item_r3.Price != null && item_r3.Price != "");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r3.favouritesFlage == false);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", item_r3.favouritesFlage == true);
      }
    }

    function SearchProductPage_div_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "No product found");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    var _c0 = function _c0() {
      return ["/favourites"];
    };

    var _c1 = function _c1() {
      return ["/categories"];
    };

    var SearchProductPage = /*#__PURE__*/function () {
      function SearchProductPage(http, route, router, loadingController, modalCtrl, storage, myapp, downloader, toastCtrl, platform, firebaseAnalytics, inAppBrowser, categoriesPage, socialSharing) {
        var _this2 = this;

        _classCallCheck(this, SearchProductPage);

        this.http = http;
        this.route = route;
        this.router = router;
        this.loadingController = loadingController;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.myapp = myapp;
        this.downloader = downloader;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.firebaseAnalytics = firebaseAnalytics;
        this.inAppBrowser = inAppBrowser;
        this.categoriesPage = categoriesPage;
        this.socialSharing = socialSharing;
        this.title = '';
        this.product_listing = [];
        this.searchTerm = "";
        this.emptyDataFlage = false;
        this.data = [];
        this.page = 1;
        this.pagenationProductCount = 20;
        this.table_air_conditioner = "air_conditioner";
        this.table_air_cooler = "air_cooler";
        this.table_air_purifier = "air_purifier";
        this.table_water_purifier = "water_purifier";
        this.route.queryParams.subscribe(function (params) {
          if (params && params.title) {
            _this2.title = params.title;
          }
        });
      }

      _createClass(SearchProductPage, [{
        key: "formatNumber",
        value: function formatNumber(item) {
          var number = item.MRP ? item.MRP : item.Price;
          return new Intl.NumberFormat('en-IN').format(number);
        }
      }, {
        key: "onChange",
        value: function onChange(searchTerm, event) {
          if (searchTerm.length > 1) {
            this.getSearchData(searchTerm);
          } else {
            this.product_listing = [];
            this.emptyDataFlage = false;
          }
        }
      }, {
        key: "getSearchData",
        value: function getSearchData(searchText) {
          var _this3 = this;

          var param = {};

          if (this.title == "Air Conditioners") {
            param = {
              product_category: "air_conditioner",
              search_text: searchText
            };
          } else if (this.title == "Air Coolers") {
            param = {
              product_category: "air_cooler",
              search_text: searchText
            };
          } else if (this.title == "Air Purifiers") {
            param = {
              product_category: "air_purifier",
              search_text: searchText
            };
          } else if (this.title == "Water Purifiers") {
            param = {
              product_category: "water_purifier",
              search_text: searchText
            };
          }

          this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/product_search', param).subscribe(function (response) {
            Object.keys(response).map(function (key) {
              if (response[key].product_listing == undefined) {
                _this3.product_listing = [];
                _this3.total_product = _this3.product_listing.length;
                console.log("total_product If", _this3.total_product);
              } else {
                _this3.data = response[key].product_listing;
                _this3.page = 1;

                _this3.content.scrollToTop(0);

                for (var i = 0; i < _this3.data.length; i++) {
                  if (i < _this3.page * _this3.pagenationProductCount) {
                    _this3.product_listing.push(_this3.data[i]);
                  }
                }

                console.log("product_listing***", _this3.product_listing); // this.product_listing = response[key].product_listing;

                _this3.product_listing.sort(function (a, b) {
                  if (Number(a.MRP) < Number(b.MRP)) {
                    return -1;
                  } else if (Number(a.MRP) > Number(b.MRP)) {
                    return 1;
                  } else {
                    return 0;
                  }
                }); // this.total_product = this.product_listing.length;


                _this3.total_product = response[key].total_result_count;
                console.log("total_product", _this3.total_product);
              }

              if (_this3.product_listing.length == 0) {
                _this3.emptyDataFlage = true;
              } else {
                _this3.emptyDataFlage = false;
              }

              _this3.ckeckFavourateProduct();
            });
          }, function (err) {
            console.log("err.........", JSON.stringify(err));
          });
        }
      }, {
        key: "loadData",
        value: function loadData(event) {
          var _this4 = this;

          if (this.product_listing.length < this.data.length) {
            setTimeout(function () {
              event.target.complete();
              var len = _this4.product_listing.length;

              for (var i = len; i < _this4.data.length; i++) {
                if (i < _this4.page * _this4.pagenationProductCount) {
                  _this4.product_listing.push(_this4.data[i]);
                }
              }

              console.log("product_listing Load New", _this4.product_listing);

              _this4.ckeckFavourateProduct();

              _this4.page++; //event.target.disabled = true;
            }, 500);
          } else {
            event.target.complete();
          }
        }
      }, {
        key: "ckeckFavourateProduct",
        value: function ckeckFavourateProduct() {
          var _this5 = this;

          var data = [];
          this.storage.get('favouriteList').then(function (val) {
            if (val != null) {
              data = val;
              var count = 0;

              for (var index in _this5.product_listing) {
                for (var item in data) {
                  if (_this5.product_listing[index].ID == data[item].ID) {
                    count = 1;
                  }
                }

                if (count == 0) {
                  _this5.product_listing[index].favouritesFlage = false;
                } else {
                  _this5.product_listing[index].favouritesFlage = true;
                }

                count = 0;
              }
            } else {
              for (var _index in _this5.product_listing) {
                _this5.product_listing[_index].favouritesFlage = false;
              }
            }
          });
        }
      }, {
        key: "clickOnFavourites",
        value: function clickOnFavourites(item) {
          var _this6 = this;

          if (this.title == "Air Conditioners") {
            this.storeDataInAirConditioner(item);
          } else if (this.title == "Air Coolers") {
            this.storeDataInAirCooler(item);
          } else if (this.title == "Air Purifiers") {
            this.storeDataInAirPurifier(item);
          } else if (this.title == "Water Purifiers") {
            this.storeDataInWaterConditioner(item);
          }

          var data = [];
          this.storage.get('favouriteList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this6.storage.set('favouriteList', data);

                  _this6.mackFavouritesFlageChange(item);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this6.storage.set('favouriteList', data);

                  _this6.mackFavouritesFlageChange(item);
                }
              } else {
                data.push(item);

                _this6.storage.set('favouriteList', data);

                _this6.mackFavouritesFlageChange(item);
              }
            } else {
              data.push(item);

              _this6.storage.set('favouriteList', data);

              _this6.mackFavouritesFlageChange(item);
            }
          });
        }
      }, {
        key: "storeDataInAirConditioner",
        value: function storeDataInAirConditioner(item) {
          var _this7 = this;

          var data = [];
          this.storage.get('airConditionerList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this7.storage.set('airConditionerList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this7.storage.set('airConditionerList', data);
                }
              } else {
                data.push(item);

                _this7.storage.set('airConditionerList', data);
              }
            } else {
              data.push(item);

              _this7.storage.set('airConditionerList', data);
            }
          });
        }
      }, {
        key: "storeDataInAirCooler",
        value: function storeDataInAirCooler(item) {
          var _this8 = this;

          var data = [];
          this.storage.get('airCoolerList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this8.storage.set('airCoolerList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this8.storage.set('airCoolerList', data);
                }
              } else {
                data.push(item);

                _this8.storage.set('airCoolerList', data);
              }
            } else {
              data.push(item);

              _this8.storage.set('airCoolerList', data);
            }
          });
        }
      }, {
        key: "storeDataInAirPurifier",
        value: function storeDataInAirPurifier(item) {
          var _this9 = this;

          var data = [];
          this.storage.get('airPurifierList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this9.storage.set('airPurifierList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this9.storage.set('airPurifierList', data);
                }
              } else {
                data.push(item);

                _this9.storage.set('airPurifierList', data);
              }
            } else {
              data.push(item);

              _this9.storage.set('airPurifierList', data);
            }
          });
        }
      }, {
        key: "storeDataInWaterConditioner",
        value: function storeDataInWaterConditioner(item) {
          var _this10 = this;

          var data = [];
          this.storage.get('waterPurifierList').then(function (val) {
            if (val != null) {
              if (val.length != 0) {
                data = val;
                var count = 0;
                var removeItemIndex = 0;

                for (var index in data) {
                  if (data[index].ID == item.ID) {
                    count = 1;
                    removeItemIndex = Number(index);
                  }
                }

                if (count == 0) {
                  data.push(item);

                  _this10.storage.set('waterPurifierList', data);
                } else {
                  //remove product from Favourites list
                  data.splice(removeItemIndex, 1);

                  _this10.storage.set('waterPurifierList', data);
                }
              } else {
                data.push(item);

                _this10.storage.set('waterPurifierList', data);
              }
            } else {
              data.push(item);

              _this10.storage.set('waterPurifierList', data);
            }
          });
        }
      }, {
        key: "mackFavouritesFlageChange",
        value: function mackFavouritesFlageChange(item) {
          for (var index in this.product_listing) {
            if (this.product_listing[index].ID == item.ID) {
              this.product_listing[index].favouritesFlage = !this.product_listing[index].favouritesFlage;
            }
          }
        }
      }, {
        key: "clickOnProduct",
        value: function clickOnProduct(id, text) {
          var navigationExtras = {
            queryParams: {
              id: id,
              title: text
            }
          };
          this.router.navigate(['product/' + id], navigationExtras);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this11 = this;

          // On init, the route subscription is the active subscription
          this.subscriptions = this.route.data.subscribe(function (resolvedRouteData) {
            // Route subscription resolved, now the active subscription is the Observable extracted from the resolved route data
            _this11.subscriptions = _utils_resolver_helper__WEBPACK_IMPORTED_MODULE_3__["ResolverHelper"].extractData(resolvedRouteData.data, _search_product_model__WEBPACK_IMPORTED_MODULE_4__["SearchProductModel"]).subscribe(function (state) {
              _this11.listing = state;
            }, function (error) {});
          }, function (error) {});
        } // NOTE: Ionic only calls ngOnDestroy if the page was popped (ex: when navigating back)
        // Since ngOnDestroy might not fire when you navigate from the current page, use ionViewWillLeave to cleanup Subscriptions

      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          // console.log('TravelListingPage [ionViewWillLeave]');
          this.subscriptions.unsubscribe();
        }
      }, {
        key: "downloadPdf",
        value: function downloadPdf(item) {
          var _this12 = this;

          var param = {};
          var tableName = "";

          if (this.title == "Air Conditioners") {
            param = {
              air_conditioner: item.ID
            };
            tableName = this.table_air_conditioner;
          } else if (this.title == "Air Coolers") {
            param = {
              air_cooler: item.ID
            };
            tableName = this.table_air_cooler;
          } else if (this.title == "Air Purifiers") {
            param = {
              air_purifier: item.ID
            };
            tableName = this.table_air_purifier;
          } else if (this.title == "Water Purifiers") {
            param = {
              water_purifier: item.ID
            };
            tableName = this.table_water_purifier;
          }

          this.myapp.databaseObj.executeSql("\n    SELECT * FROM ".concat(tableName, " WHERE ID = ").concat(item.ID, "\n    "), []).then(function (res) {
            if (res.rows.length > 0) {
              var product_data = [];

              var _loop = function _loop() {
                var rowKeys = [];
                var singleRowData = [];
                object = {
                  brochures: ""
                };
                rowKeys = Object.keys(res.rows.item(i));
                Object.keys(res.rows.item(i)).map(function (key) {
                  singleRowData.push(res.rows.item(i)[key]);
                });

                for (var index in rowKeys) {
                  if (rowKeys[index] == "brochures") {
                    object[rowKeys[index]] = singleRowData[index];
                  }
                }

                product_data.push(object);
              };

              for (var i = 0; i < res.rows.length; i++) {
                var object;

                _loop();
              }

              if (product_data[0].brochures == "") {
                _this12.downloadFuction(item, param);
              } else {
                if (_this12.platform.is("ios")) {
                  var browser = _this12.inAppBrowser.create(product_data[0].brochures);

                  _this12.firebaseAnalytics.logEvent('downloads_products', {
                    product: item.SKUCode,
                    category: _this12.title
                  }).then(function (res) {
                    return console.log(res);
                  })["catch"](function (error) {
                    return console.error(error);
                  });
                } else {
                  _this12.loadingController.create({
                    message: 'Please wait while downloading'
                  }).then(function (res) {
                    res.present();

                    if (navigator.onLine) {
                      var request = {
                        uri: product_data[0].brochures,
                        title: 'Blue Star',
                        description: '',
                        mimeType: 'application/pdf',
                        visibleInDownloadsUi: true,
                        notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_9__["NotificationVisibility"].VisibleNotifyCompleted,
                        destinationInExternalPublicDir: {
                          dirType: 'Download',
                          subPath: item.SKUCode + '.pdf'
                        }
                      };

                      _this12.downloader.download(request).then(function (location) {
                        res.dismiss();

                        _this12.presentToast("Downloaded in device download folder");

                        _this12.firebaseAnalytics.logEvent('downloads_products', {
                          product: item.SKUCode,
                          category: _this12.title
                        }).then(function (res) {
                          return console.log(res);
                        })["catch"](function (error) {
                          return console.error(error);
                        });
                      })["catch"](function (error) {
                        console.error(error);
                      });
                    } else {
                      res.dismiss();
                      console.log("no internat connection");
                    }
                  });
                }
              }
            } else {
              _this12.downloadFuction(item, param);
            }
          })["catch"](function (e) {
            console.log("error " + JSON.stringify(e));
          });
        }
      }, {
        key: "downloadFuction",
        value: function downloadFuction(item, param) {
          var _this13 = this;

          this.loadingController.create({
            message: 'Please wait while downloading'
          }).then(function (res) {
            res.present();

            if (navigator.onLine) {
              _this13.http.post(_this13.categoriesPage.apiBaseUrl + '/bluestar_api/product_pdf/download', param).subscribe(function (response) {
                Object.keys(response).map(function (key) {
                  _this13.pdf_link = response[key].pdf_link; //this.indicatorDownload(item, this.pdf_link);

                  if (_this13.platform.is("ios")) {
                    res.dismiss();

                    var browser = _this13.inAppBrowser.create(_this13.pdf_link);

                    _this13.firebaseAnalytics.logEvent('downloads_products', {
                      product: item.SKUCode,
                      category: _this13.title
                    }).then(function (res) {
                      return console.log(res);
                    })["catch"](function (error) {
                      return console.error(error);
                    });
                  } else {
                    var request = {
                      uri: _this13.pdf_link,
                      title: 'Blue Star',
                      description: '',
                      mimeType: 'application/pdf',
                      visibleInDownloadsUi: true,
                      notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_9__["NotificationVisibility"].VisibleNotifyCompleted,
                      destinationInExternalPublicDir: {
                        dirType: 'Download',
                        subPath: item.SKUCode + '.pdf'
                      }
                    };

                    _this13.downloader.download(request).then(function (location) {
                      res.dismiss();

                      _this13.presentToast("Downloaded in device download folder");

                      _this13.firebaseAnalytics.logEvent('downloads_products', {
                        product: item.SKUCode,
                        category: _this13.title
                      }).then(function (res) {
                        return console.log(res);
                      })["catch"](function (error) {
                        return console.error(error);
                      });
                    })["catch"](function (error) {
                      console.error(error);
                      res.dismiss();
                    });
                  }
                });
              }, function (err) {
                res.dismiss();
                console.log("err.........", JSON.stringify(err));
              });
            } else {
              res.dismiss();
              console.log("no internat connection");
            }
          });
        }
      }, {
        key: "indicatorDownload",
        value: function indicatorDownload(item, pdf_link) {
          var _this14 = this;

          if (this.platform.is("ios")) {
            var browser = this.inAppBrowser.create(this.pdf_link);
            this.firebaseAnalytics.logEvent('downloads_products', {
              product: item.SKUCode,
              category: this.title
            }).then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          } else {
            var request = {
              uri: pdf_link,
              title: 'Blue Star',
              description: '',
              mimeType: 'application/pdf',
              visibleInDownloadsUi: true,
              notificationVisibility: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_9__["NotificationVisibility"].VisibleNotifyCompleted,
              destinationInExternalPublicDir: {
                dirType: 'Download',
                subPath: item.SKUCode + '.pdf'
              }
            };
            this.loadingController.create({
              message: 'Please wait while downloading'
            }).then(function (res) {
              res.present();

              _this14.downloader.download(request).then(function (location) {
                res.dismiss();

                _this14.presentToast("Downloaded in device download folder");

                _this14.firebaseAnalytics.logEvent('downloads_products', {
                  product: item.SKUCode,
                  category: _this14.title
                }).then(function (res) {
                  return console.log(res);
                })["catch"](function (error) {
                  return console.error(error);
                });
              })["catch"](function (error) {
                console.error(error);
                res.dismiss();
              });
            });
          }
        }
      }, {
        key: "presentToast",
        value: function presentToast(text) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var toast;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.toastCtrl.create({
                      message: text,
                      duration: 7000,
                      position: 'bottom',
                      cssClass: "msg-align"
                    });

                  case 2:
                    toast = _context.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getPdfLink",
        value: function getPdfLink(productId, item) {
          var _this15 = this;

          var param = {};

          if (this.title == "Air Conditioners") {
            param = {
              air_conditioner: productId
            };
          } else if (this.title == "Air Coolers") {
            param = {
              air_cooler: productId
            };
          } else if (this.title == "Air Purifiers") {
            param = {
              air_purifier: productId
            };
          } else if (this.title == "Water Purifiers") {
            param = {
              water_purifier: productId
            };
          }

          this.loadingController.create({
            message: 'Please wait'
          }).then(function (res) {
            res.present();

            if (navigator.onLine) {
              _this15.http.post(_this15.categoriesPage.apiBaseUrl + '/bluestar_api/product_pdf/download', param).subscribe(function (response) {
                Object.keys(response).map(function (key) {
                  _this15.pdf_link = response[key].pdf_link;
                  res.dismiss();

                  _this15.shareMethod(item);
                });
              }, function (err) {
                res.dismiss();

                _this15.presentToast("No internet connection. Please try again later.");

                console.log("err.........", JSON.stringify(err));
              });
            } else {
              res.dismiss();

              _this15.presentToast("No internet connection. Please try again later.");
            }
          });
        }
      }, {
        key: "toDataUrl",
        value: function toDataUrl(url, _this, callback) {
          var xhr = new XMLHttpRequest();

          xhr.onload = function () {
            var reader = new FileReader();

            reader.onloadend = function () {
              callback(reader.result, _this);
            };

            reader.readAsDataURL(xhr.response);
          };

          xhr.open('GET', url);
          xhr.responseType = 'blob';
          xhr.send();
        }
      }, {
        key: "shareMethod",
        value: function shareMethod(item) {
          this.toDataUrl(item.Image, this, function (myBase64, _this) {
            _this.callMethod(myBase64, item);
          });
        }
      }, {
        key: "callMethod",
        value: function callMethod(myBase64, item) {
          var _this16 = this;

          var body = "Hi there, check out this product by Blue Star!\n\nProduct Category: ".concat(this.title, "\nProduct Title: ").concat(item.ProductTitle, "\nSKU Code: ").concat(item.SKUCode, "\nClick here to get the product specification: ").concat(this.pdf_link.replace(/\s/g, "%20"));
          this.socialSharing.share(body, item.ProductTitle.replace(/\%/g, " pc"), myBase64, null).then(function (sucess) {
            _this16.firebaseAnalytics.logEvent('share_products', {
              product: item.SKUCode,
              category: _this16.title
            }).then(function (res) {
              return console.log(res);
            })["catch"](function (error) {
              return console.error(error);
            });
          })["catch"](function (err) {
            console.log("err....", err);
          });
        }
      }, {
        key: "isShell",
        get: function get() {
          return this.listing && this.listing.isShell ? true : false;
        }
      }]);

      return SearchProductPage;
    }();

    SearchProductPage.ɵfac = function SearchProductPage_Factory(t) {
      return new (t || SearchProductPage)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_9__["Downloader"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_12__["FirebaseAnalytics"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_11__["InAppBrowser"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_categories_categories_page__WEBPACK_IMPORTED_MODULE_13__["CategoriesPage"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_10__["SocialSharing"]));
    };

    SearchProductPage.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: SearchProductPage,
      selectors: [["app-search-product"]],
      viewQuery: function SearchProductPage_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵviewQuery"](_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonInfiniteScroll"], true);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstaticViewQuery"](_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"], true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.infiniteScroll = _t.first);
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.content = _t.first);
        }
      },
      hostVars: 2,
      hostBindings: function SearchProductPage_HostBindings(rf, ctx) {
        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("is-shell", ctx.isShell);
        }
      },
      decls: 19,
      vars: 8,
      consts: [["color", "primary"], ["slot", "start"], ["defaultHref", "app/categories"], ["src", "assets/logo.png", 1, "logo"], ["slot", "end"], ["name", "heart-outline", 1, "heart_icon", 3, "routerLink"], ["name", "home-outline", 1, "home_icon", 3, "routerLink"], [1, "div-fixed"], [3, "ngModel", "ngModelChange", "ionChange"], ["style", "border-bottom: 1px solid #09509d;", 4, "ngIf"], [2, "margin-top", "75px"], ["style", "border-bottom: 1px solid #09509d;", 4, "ngFor", "ngForOf"], [4, "ngIf"], ["threshold", "100px", 3, "ionInfinite"], ["loadingSpinner", "bubbles", "loadingText", "Loading more products..."], [2, "border-bottom", "1px solid #09509d"], [1, "col-padding"], [1, "main-title"], [1, "split"], [1, "column", 2, "width", "30%"], [1, "image-anchor"], ["animation", "spinner", 1, "item-image", 3, "src", "alt"], [1, "column", 2, "width", "70%"], [1, "centered", 3, "click"], [2, "margin-bottom", "5px"], ["size", "4", 2, "text-align", "center", "color", "#09509d", 3, "click"], ["name", "download-outline", 1, "icon-download"], ["size", "5", 2, "text-align", "center", "color", "#09509d", 3, "click"], ["name", "heart-outline", "class", "icon-heart-black", 4, "ngIf"], ["name", "heart", "class", "icon-heart-red", 4, "ngIf"], ["size", "3", 2, "text-align", "center", "color", "#09509d", "padding-top", "0px", 3, "click"], ["src", "assets/share.png", 1, "icon-share"], ["name", "heart-outline", 1, "icon-heart-black"], ["name", "heart", 1, "icon-heart-red"], [2, "font-size", "20px", "text-align", "center", "margin-top", "50%"]],
      template: function SearchProductPage_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ion-header");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "ion-toolbar", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "ion-buttons", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "ion-back-button", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "img", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "ion-buttons", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "ion-icon", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "ion-icon", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "ion-content");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "ion-row");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "ion-col");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "ion-searchbar", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function SearchProductPage_Template_ion_searchbar_ngModelChange_12_listener($event) {
            return ctx.searchTerm = $event;
          })("ionChange", function SearchProductPage_Template_ion_searchbar_ionChange_12_listener($event) {
            return ctx.onChange(ctx.searchTerm, $event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, SearchProductPage_ion_row_13_Template, 4, 1, "ion-row", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](15, SearchProductPage_div_15_Template, 27, 7, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](16, SearchProductPage_div_16_Template, 3, 0, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "ion-infinite-scroll", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ionInfinite", function SearchProductPage_Template_ion_infinite_scroll_ionInfinite_17_listener($event) {
            return ctx.loadData($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "ion-infinite-scroll-content", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](6, _c0));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](7, _c1));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.searchTerm);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.product_listing.length > 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.product_listing);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.emptyDataFlage);
        }
      },
      directives: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonHeader"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonToolbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonButtons"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonBackButton"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonBackButtonDelegate"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonIcon"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["RouterLinkDelegate"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLink"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonRow"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonCol"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonSearchbar"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["TextValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_14__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_14__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_15__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_15__["NgForOf"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonInfiniteScroll"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonInfiniteScrollContent"], _shell_image_shell_image_shell_component__WEBPACK_IMPORTED_MODULE_16__["ImageShellComponent"]],
      styles: ["[_nghost-%COMP%] {\n  --page-margin: var(--app-fair-margin);\n  --page-background: var(--app-background);\n  --page-items-gutter: calc(var(--page-margin) / 2);\n  --page-color: #cb328f;\n}\n\n.fashion-listing-content[_ngcontent-%COMP%] {\n  --background: var(--page-background);\n  --padding-start: var(--page-margin);\n  --padding-end: var(--page-margin);\n  --padding-top: var(--page-margin);\n  --padding-bottom: var(--page-margin);\n}\n\n.items-row[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: 0;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%] {\n  margin-bottom: calc(var(--page-margin) * 1);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]:nth-child(odd) {\n  padding-right: var(--page-items-gutter);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]:nth-child(even) {\n  padding-left: var(--page-items-gutter);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%] {\n  border: 1px solid #09509d;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-image-wrapper[_ngcontent-%COMP%]   .image-anchor[_ngcontent-%COMP%] {\n  display: block;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%] {\n  --ion-grid-column-padding: 0px;\n  padding: 5px 5px 0px;\n  text-align: center;\n  min-height: 68px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%] {\n  margin-bottom: calc(var(--page-margin) / 2);\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%] {\n  margin: 0px;\n  font-size: 13px;\n  font-weight: 400;\n  text-overflow: ellipsis;\n  white-space: unset;\n  overflow: hidden;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .main-info[_ngcontent-%COMP%]   .item-name[_ngcontent-%COMP%]   .name-anchor[_ngcontent-%COMP%] {\n  color: var(--ion-color-primary);\n  display: block;\n  text-decoration: none;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%] {\n  align-items: center;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%] {\n  padding-bottom: 5px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:first-child {\n  padding-right: calc(var(--page-margin) / 2);\n  text-align: right;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:first-child:last-child {\n  text-align: center;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .price-col[_ngcontent-%COMP%]:last-child {\n  padding-left: calc(var(--page-margin) / 2);\n  text-align: left;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .separator[_ngcontent-%COMP%] {\n  max-width: 0px;\n  border-right: solid 2px var(--ion-color-light-shade);\n  align-self: stretch;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .item-sale-price[_ngcontent-%COMP%] {\n  display: block;\n  font-weight: 400;\n  font-size: 14px;\n}\n\n.items-row[_ngcontent-%COMP%]   .listing-item[_ngcontent-%COMP%]   .item-body[_ngcontent-%COMP%]   .secondary-info[_ngcontent-%COMP%]   .item-original-price[_ngcontent-%COMP%] {\n  display: block;\n  text-decoration: line-through;\n  color: var(--ion-color-medium-shade);\n  font-size: 14px;\n}\n\nion-header[_ngcontent-%COMP%]   ion-toolbar[_ngcontent-%COMP%]:first-of-type {\n  text-align: center;\n}\n\n.logo[_ngcontent-%COMP%] {\n  width: 70%;\n  margin-top: 5px;\n}\n\n.home_icon[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-right: 5px;\n}\n\n.heart_icon[_ngcontent-%COMP%] {\n  font-size: 28px;\n  margin-right: 10px;\n}\n\n.icon-heart-red[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-bottom: -8px;\n  padding-right: 3px;\n}\n\n.icon-heart-black[_ngcontent-%COMP%] {\n  font-size: 25px;\n  margin-bottom: -8px;\n  padding-right: 3px;\n}\n\n.icon-share[_ngcontent-%COMP%] {\n  width: 29px;\n  margin-bottom: -7px;\n  padding-right: 3px;\n}\n\n.icon-download[_ngcontent-%COMP%] {\n  font-size: 22px;\n  margin-bottom: -4px;\n  padding-right: 3px;\n}\n\n.col-padding[_ngcontent-%COMP%] {\n  padding-left: 15px !important;\n  padding-right: 15px !important;\n  padding-bottom: 7px !important;\n}\n\n.main-title[_ngcontent-%COMP%] {\n  color: #09509d;\n  font-weight: bold;\n}\n\n.icon_funnel[_ngcontent-%COMP%] {\n  float: right;\n  font-size: 1.5em;\n  color: #09509d;\n}\n\n.product-text[_ngcontent-%COMP%] {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n\n\n\n.split[_ngcontent-%COMP%] {\n  height: 25%;\n  width: 100%;\n  z-index: 1;\n  position: relative;\n  overflow-x: hidden;\n}\n\n\n\n\n\n.column[_ngcontent-%COMP%] {\n  float: left;\n  padding: 10px;\n  height: 100%;\n  \n}\n\n.centered[_ngcontent-%COMP%] {\n  color: balck;\n}\n\n.icon-delete[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 50%;\n  left: 90%;\n  transform: translate(-50%, -50%);\n  font-size: 25px;\n  color: #09509d;\n}\n\n.div-fixed[_ngcontent-%COMP%] {\n  position: fixed;\n  z-index: 999;\n  background-color: white;\n  width: 100%;\n  overflow-y: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvc2VhcmNoL3N0eWxlcy9zZWFyY2gtcHJvZHVjdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3NlYXJjaC9zdHlsZXMvc2VhcmNoLXByb2R1Y3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UscUNBQUE7RUFDQSx3Q0FBQTtFQUVBLGlEQUFBO0VBQ0EscUJBQUE7QUNGRjs7QURNQTtFQUNFLG9DQUFBO0VBQ0EsbUNBQUE7RUFDQSxpQ0FBQTtFQUNBLGlDQUFBO0VBQ0Esb0NBQUE7QUNIRjs7QURNQTtFQUNFLDRCQUFBO0FDSEY7O0FES0U7RUFDRSwyQ0FBQTtBQ0hKOztBREtJO0VBQ0UsdUNBQUE7QUNITjs7QURNSTtFQUNFLHNDQUFBO0FDSk47O0FET0k7RUFFRSx5QkFBQTtBQ05OOztBRE9NO0VBQ0UsY0FBQTtBQ0xSOztBRFNJO0VBQ0UsOEJBQUE7RUFFQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNSTjs7QURTTTtFQUNFLDJDQUFBO0FDUFI7O0FEU1E7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBRUEsdUJBQUE7RUFFQSxrQkFBQTtFQUNBLGdCQUFBO0FDVFY7O0FEV1U7RUFDRSwrQkFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtBQ1RaOztBRGNNO0VBQ0UsbUJBQUE7QUNaUjs7QURjUTtFQUNFLG1CQUFBO0FDWlY7O0FEYVU7RUFDRSwyQ0FBQTtFQUNBLGlCQUFBO0FDWFo7O0FEYVk7RUFDRSxrQkFBQTtBQ1hkOztBRGVVO0VBQ0UsMENBQUE7RUFDQSxnQkFBQTtBQ2JaOztBRGlCUTtFQUNFLGNBQUE7RUFDQSxvREFBQTtFQUNBLG1CQUFBO0FDZlY7O0FEa0JRO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBR0EsZUFBQTtBQ2xCVjs7QUR1QlE7RUFDRSxjQUFBO0VBQ0EsNkJBQUE7RUFDQSxvQ0FBQTtFQUNBLGVBQUE7QUNyQlY7O0FEMkJBO0VBQ0Usa0JBQUE7QUN4QkY7O0FEMEJBO0VBRUUsVUFBQTtFQUNBLGVBQUE7QUN4QkY7O0FEMEJBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDdkJGOztBRHlCQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtBQ3RCRjs7QUQ2QkE7RUFNRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQy9CRjs7QURpQ0E7RUFPRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ3BDRjs7QUQ2Q0E7RUFNRSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQy9DRjs7QURpREE7RUFNRSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ25ERjs7QURxREE7RUFDRSw2QkFBQTtFQUNBLDhCQUFBO0VBQ0EsOEJBQUE7QUNsREY7O0FEeURBO0VBRUUsY0FBQTtFQUNBLGlCQUFBO0FDdkRGOztBRHlEQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUN0REY7O0FEMERBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0FDdkRGOztBRDREQSw2QkFBQTs7QUFDQTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBRUEsa0JBQUE7QUMxREY7O0FEaUVBLDBCQUFBOztBQUVBLDhEQUFBOztBQUNBO0VBQ0UsV0FBQTtFQUVBLGFBQUE7RUFDQSxZQUFBO0VBQWMsOENBQUE7QUMvRGhCOztBRG1FQTtFQU1FLFlBQUE7QUNyRUY7O0FEdUVBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNwRUY7O0FEdUVBO0VBQ0UsZUFBQTtFQUNBLFlBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ3BFRiIsImZpbGUiOiJzcmMvYXBwL3NlYXJjaC9zdHlsZXMvc2VhcmNoLXByb2R1Y3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XG4gIC0tcGFnZS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG5cbiAgLS1wYWdlLWl0ZW1zLWd1dHRlcjogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbiAgLS1wYWdlLWNvbG9yOiAjY2IzMjhmO1xufVxuXG4vLyBOb3RlOiAgQWxsIHRoZSBDU1MgdmFyaWFibGVzIGRlZmluZWQgYmVsb3cgYXJlIG92ZXJyaWRlcyBvZiBJb25pYyBlbGVtZW50cyBDU1MgQ3VzdG9tIFByb3BlcnRpZXNcbi5mYXNoaW9uLWxpc3RpbmctY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0tcGFnZS1iYWNrZ3JvdW5kKTtcbiAgLS1wYWRkaW5nLXN0YXJ0OiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIC0tcGFkZGluZy1lbmQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgLS1wYWRkaW5nLXRvcDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAtLXBhZGRpbmctYm90dG9tOiB2YXIoLS1wYWdlLW1hcmdpbik7XG59XG5cbi5pdGVtcy1yb3cge1xuICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwO1xuXG4gIC5saXN0aW5nLWl0ZW0ge1xuICAgIG1hcmdpbi1ib3R0b206IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pICogMSk7XG5cbiAgICAmOm50aC1jaGlsZChvZGQpIHtcbiAgICAgIHBhZGRpbmctcmlnaHQ6IHZhcigtLXBhZ2UtaXRlbXMtZ3V0dGVyKTtcbiAgICB9XG5cbiAgICAmOm50aC1jaGlsZChldmVuKSB7XG4gICAgICBwYWRkaW5nLWxlZnQ6IHZhcigtLXBhZ2UtaXRlbXMtZ3V0dGVyKTtcbiAgICB9XG5cbiAgICAuaXRlbS1pbWFnZS13cmFwcGVyIHtcbiAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1saWdodC1zaGFkZSk7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDk1MDlkO1xuICAgICAgLmltYWdlLWFuY2hvciB7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5pdGVtLWJvZHkge1xuICAgICAgLS1pb24tZ3JpZC1jb2x1bW4tcGFkZGluZzogMHB4O1xuXG4gICAgICBwYWRkaW5nOiA1cHggNXB4IDBweDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIG1pbi1oZWlnaHQ6IDY4cHg7XG4gICAgICAubWFpbi1pbmZvIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcblxuICAgICAgICAuaXRlbS1uYW1lIHtcbiAgICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcblxuICAgICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICAgIC8vIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgICAgd2hpdGUtc3BhY2U6IHVuc2V0O1xuICAgICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgICAgICAgICAubmFtZS1hbmNob3Ige1xuICAgICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAuc2Vjb25kYXJ5LWluZm8ge1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgICAgIC5wcmljZS1jb2wge1xuICAgICAgICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgICAgICAgJjpmaXJzdC1jaGlsZCB7XG4gICAgICAgICAgICBwYWRkaW5nLXJpZ2h0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG5cbiAgICAgICAgICAgICY6bGFzdC1jaGlsZCB7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG5cbiAgICAgICAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgICAgICAgcGFkZGluZy1sZWZ0OiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAvIDIpO1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAuc2VwYXJhdG9yIHtcbiAgICAgICAgICBtYXgtd2lkdGg6IDBweDtcbiAgICAgICAgICBib3JkZXItcmlnaHQ6IHNvbGlkIDJweCB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xuICAgICAgICAgIGFsaWduLXNlbGY6IHN0cmV0Y2g7XG4gICAgICAgIH1cblxuICAgICAgICAuaXRlbS1zYWxlLXByaWNlIHtcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgIC8vIGNvbG9yOiB2YXIoLS1wYWdlLWNvbG9yKTtcbiAgICAgICAgICAvLyBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgIC8vIGNvbG9yOiBncmV5O1xuICAgICAgICAgIC8vIG1pbi1oZWlnaHQ6IDMwcHg7XG4gICAgICAgIH1cblxuICAgICAgICAuaXRlbS1vcmlnaW5hbC1wcmljZSB7XG4gICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG4gICAgICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1tZWRpdW0tc2hhZGUpO1xuICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBle1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubG9nb3tcbiAgLy8gd2lkdGg6IDU1JTtcbiAgd2lkdGg6IDcwJTtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuLmhvbWVfaWNvbntcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cbi5oZWFydF9pY29ue1xuICBmb250LXNpemU6IDI4cHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbi8vIC5jb3B5X2ljb257XG4vLyAgIGZvbnQtc2l6ZTogMjVweDtcbi8vICAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4vLyAgIC8vIGNvbG9yOiB3aGl0ZTtcbi8vIH1cbi5pY29uLWhlYXJ0LXJlZHtcbiAgLy8gZmxvYXQ6IHJpZ2h0O1xuICAvLyBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuICAvLyBwYWRkaW5nLXRvcDogNXB4O1xuICAvLyBmb250LXNpemU6IDI1cHg7XG4gIC8vIGNvbG9yOiAjMDk1MDlkO1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1ib3R0b206IC04cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cbi5pY29uLWhlYXJ0LWJsYWNre1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIC8vIHBhZGRpbmctdG9wOiA1cHg7XG4gIC8vIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gY29sb3I6ICMwOTUwOWQ7XG5cbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtOHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG4vLyAuaWNvbi1zaGFyZXtcbi8vICAgZmxvYXQ6IHJpZ2h0O1xuLy8gICBwYWRkaW5nLXRvcDogNXB4O1xuLy8gICBmb250LXNpemU6IDI1cHg7XG4vLyAgIGNvbG9yOiAjMDk1MDlkO1xuLy8gICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuLy8gfVxuLmljb24tc2hhcmV7XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgLy8gcGFkZGluZy10b3A6IDJweDtcbiAgLy8gY29sb3I6ICMwOTUwOWQ7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIC8vIHdpZHRoOiAxMiVcbiAgd2lkdGg6IDI5cHg7XG4gIG1hcmdpbi1ib3R0b206IC03cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbn1cbi5pY29uLWRvd25sb2Fke1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIC8vIHBhZGRpbmctdG9wOiA1cHg7XG4gIC8vIGZvbnQtc2l6ZTogMjVweDtcbiAgLy8gY29sb3I6ICMwOTUwOWQ7XG4gIC8vIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMjJweDtcbiAgbWFyZ2luLWJvdHRvbTogLTRweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuLmNvbC1wYWRkaW5ne1xuICBwYWRkaW5nLWxlZnQ6IDE1cHggIWltcG9ydGFudDtcbiAgcGFkZGluZy1yaWdodDogMTVweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWJvdHRvbTogN3B4ICFpbXBvcnRhbnQ7XG4gIC8vIHBhZGRpbmctdG9wOiAxMHB4ICFpbXBvcnRhbnQ7XG4gIC8vIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMDk1MDlkO1xuICAvLyBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzA5NTA5ZDtcbiAgLy8gbWFyZ2luLWJvdHRvbTogNXB4O1xuICAvLyBtYXJnaW4tdG9wOiAtMTBweDtcbn1cbi5tYWluLXRpdGxle1xuICAvLyBmb250LXNpemU6IDE3cHg7XG4gIGNvbG9yOiAjMDk1MDlkO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5pY29uX2Z1bm5lbHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBmb250LXNpemU6IDEuNWVtO1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgLy8gcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuLnByb2R1Y3QtdGV4dHtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gfVxuXG5cbiAvLyAqKioqKioqKioqKioqKioqKioqKipcbi8qIFNwbGl0IHRoZSBzY3JlZW4gaW4gaGFsZiAqL1xuLnNwbGl0IHtcbiAgaGVpZ2h0OiAyNSU7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAxO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIC8vIHRvcDogMDtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICAvLyBwYWRkaW5nLXRvcDogMjBweDtcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWU0ZTQ7XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjMDk1MDlkO1xuICAvLyBib3JkZXItdG9wOiAxcHggc29saWQgIzlFOUU5RTtcbn1cblxuLyogQ29udHJvbCB0aGUgbGVmdCBzaWRlICovXG5cbi8qIENyZWF0ZSB0aHJlZSBlcXVhbCBjb2x1bW5zIHRoYXQgZmxvYXRzIG5leHQgdG8gZWFjaCBvdGhlciAqL1xuLmNvbHVtbiB7XG4gIGZsb2F0OiBsZWZ0O1xuICAvLyB3aWR0aDogMzMuMzMlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBoZWlnaHQ6IDEwMCU7IC8qIFNob3VsZCBiZSByZW1vdmVkLiBPbmx5IGZvciBkZW1vbnN0cmF0aW9uICovXG4gIC8vIGJvcmRlcjogMXB4IHNvbGlkO1xufVxuXG4uY2VudGVyZWQge1xuICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8vIHRvcDogNTAlO1xuICAvLyBsZWZ0OiA1MCU7XG4gIC8vIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICAvLyB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiBiYWxjaztcbn1cbi5pY29uLWRlbGV0ZXtcbiAgcG9zaXRpb246IGFic29sdXRlOyBcbiAgdG9wOiA1MCU7IFxuICBsZWZ0OiA5MCU7IFxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogIzA5NTA5ZDtcbn1cblxuLmRpdi1maXhlZHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB6LWluZGV4OiA5OTk7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteTogYXV0bztcbn0iLCI6aG9zdCB7XG4gIC0tcGFnZS1tYXJnaW46IHZhcigtLWFwcC1mYWlyLW1hcmdpbik7XG4gIC0tcGFnZS1iYWNrZ3JvdW5kOiB2YXIoLS1hcHAtYmFja2dyb3VuZCk7XG4gIC0tcGFnZS1pdGVtcy1ndXR0ZXI6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gIC0tcGFnZS1jb2xvcjogI2NiMzI4Zjtcbn1cblxuLmZhc2hpb24tbGlzdGluZy1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1wYWdlLWJhY2tncm91bmQpO1xuICAtLXBhZGRpbmctc3RhcnQ6IHZhcigtLXBhZ2UtbWFyZ2luKTtcbiAgLS1wYWRkaW5nLWVuZDogdmFyKC0tcGFnZS1tYXJnaW4pO1xuICAtLXBhZGRpbmctdG9wOiB2YXIoLS1wYWdlLW1hcmdpbik7XG4gIC0tcGFkZGluZy1ib3R0b206IHZhcigtLXBhZ2UtbWFyZ2luKTtcbn1cblxuLml0ZW1zLXJvdyB7XG4gIC0taW9uLWdyaWQtY29sdW1uLXBhZGRpbmc6IDA7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0ge1xuICBtYXJnaW4tYm90dG9tOiBjYWxjKHZhcigtLXBhZ2UtbWFyZ2luKSAqIDEpO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtOm50aC1jaGlsZChvZGQpIHtcbiAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFnZS1pdGVtcy1ndXR0ZXIpO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtOm50aC1jaGlsZChldmVuKSB7XG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFnZS1pdGVtcy1ndXR0ZXIpO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWltYWdlLXdyYXBwZXIge1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDk1MDlkO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWltYWdlLXdyYXBwZXIgLmltYWdlLWFuY2hvciB7XG4gIGRpc3BsYXk6IGJsb2NrO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkge1xuICAtLWlvbi1ncmlkLWNvbHVtbi1wYWRkaW5nOiAwcHg7XG4gIHBhZGRpbmc6IDVweCA1cHggMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1pbi1oZWlnaHQ6IDY4cHg7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAubWFpbi1pbmZvIHtcbiAgbWFyZ2luLWJvdHRvbTogY2FsYyh2YXIoLS1wYWdlLW1hcmdpbikgLyAyKTtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5tYWluLWluZm8gLml0ZW0tbmFtZSB7XG4gIG1hcmdpbjogMHB4O1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICB3aGl0ZS1zcGFjZTogdW5zZXQ7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAubWFpbi1pbmZvIC5pdGVtLW5hbWUgLm5hbWUtYW5jaG9yIHtcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyB7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8gLnByaWNlLWNvbCB7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8gLnByaWNlLWNvbDpmaXJzdC1jaGlsZCB7XG4gIHBhZGRpbmctcmlnaHQ6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIC5wcmljZS1jb2w6Zmlyc3QtY2hpbGQ6bGFzdC1jaGlsZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5pdGVtcy1yb3cgLmxpc3RpbmctaXRlbSAuaXRlbS1ib2R5IC5zZWNvbmRhcnktaW5mbyAucHJpY2UtY29sOmxhc3QtY2hpbGQge1xuICBwYWRkaW5nLWxlZnQ6IGNhbGModmFyKC0tcGFnZS1tYXJnaW4pIC8gMik7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uaXRlbXMtcm93IC5saXN0aW5nLWl0ZW0gLml0ZW0tYm9keSAuc2Vjb25kYXJ5LWluZm8gLnNlcGFyYXRvciB7XG4gIG1heC13aWR0aDogMHB4O1xuICBib3JkZXItcmlnaHQ6IHNvbGlkIDJweCB2YXIoLS1pb24tY29sb3ItbGlnaHQtc2hhZGUpO1xuICBhbGlnbi1zZWxmOiBzdHJldGNoO1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIC5pdGVtLXNhbGUtcHJpY2Uge1xuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLml0ZW1zLXJvdyAubGlzdGluZy1pdGVtIC5pdGVtLWJvZHkgLnNlY29uZGFyeS1pbmZvIC5pdGVtLW9yaWdpbmFsLXByaWNlIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLW1lZGl1bS1zaGFkZSk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuaW9uLWhlYWRlciBpb24tdG9vbGJhcjpmaXJzdC1vZi10eXBlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubG9nbyB7XG4gIHdpZHRoOiA3MCU7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLmhvbWVfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5oZWFydF9pY29uIHtcbiAgZm9udC1zaXplOiAyOHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5pY29uLWhlYXJ0LXJlZCB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogLThweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1oZWFydC1ibGFjayB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLWJvdHRvbTogLThweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xufVxuXG4uaWNvbi1zaGFyZSB7XG4gIHdpZHRoOiAyOXB4O1xuICBtYXJnaW4tYm90dG9tOiAtN3B4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG5cbi5pY29uLWRvd25sb2FkIHtcbiAgZm9udC1zaXplOiAyMnB4O1xuICBtYXJnaW4tYm90dG9tOiAtNHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG59XG5cbi5jb2wtcGFkZGluZyB7XG4gIHBhZGRpbmctbGVmdDogMTVweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLXJpZ2h0OiAxNXB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctYm90dG9tOiA3cHggIWltcG9ydGFudDtcbn1cblxuLm1haW4tdGl0bGUge1xuICBjb2xvcjogIzA5NTA5ZDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5pY29uX2Z1bm5lbCB7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udC1zaXplOiAxLjVlbTtcbiAgY29sb3I6ICMwOTUwOWQ7XG59XG5cbi5wcm9kdWN0LXRleHQge1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cblxuLyogU3BsaXQgdGhlIHNjcmVlbiBpbiBoYWxmICovXG4uc3BsaXQge1xuICBoZWlnaHQ6IDI1JTtcbiAgd2lkdGg6IDEwMCU7XG4gIHotaW5kZXg6IDE7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xufVxuXG4vKiBDb250cm9sIHRoZSBsZWZ0IHNpZGUgKi9cbi8qIENyZWF0ZSB0aHJlZSBlcXVhbCBjb2x1bW5zIHRoYXQgZmxvYXRzIG5leHQgdG8gZWFjaCBvdGhlciAqL1xuLmNvbHVtbiB7XG4gIGZsb2F0OiBsZWZ0O1xuICBwYWRkaW5nOiAxMHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIC8qIFNob3VsZCBiZSByZW1vdmVkLiBPbmx5IGZvciBkZW1vbnN0cmF0aW9uICovXG59XG5cbi5jZW50ZXJlZCB7XG4gIGNvbG9yOiBiYWxjaztcbn1cblxuLmljb24tZGVsZXRlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogOTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBjb2xvcjogIzA5NTA5ZDtcbn1cblxuLmRpdi1maXhlZCB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogOTk5O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93LXk6IGF1dG87XG59Il19 */", "[_nghost-%COMP%] {\n  --shell-color: #cb328f;\n  --shell-color-rgb: 203,50,143;\n}\n\n.is-shell[_nghost-%COMP%]   a[_ngcontent-%COMP%] {\n  pointer-events: none;\n}\n\napp-image-shell.item-image[_ngcontent-%COMP%] {\n  --image-shell-loading-background: white;\n  --image-shell-spinner-color: #5289c3;\n}\n\n.item-name[_ngcontent-%COMP%]   app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .25);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .35);\n  --text-shell-line-height: 14px;\n}\n\n.item-sale-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px;\n}\n\n.item-original-price[_ngcontent-%COMP%]    > app-text-shell[_ngcontent-%COMP%] {\n  --text-shell-animation-background: rgba(var(--shell-color-rgb), .20);\n  --text-shell-animation-color: rgba(var(--shell-color-rgb), .30);\n  --text-shell-line-height: 16px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL29lbS9kZXYvd29ya3NwYWNlL0JsdWVTdGFySWtzdWxhL3NyYy9hcHAvc2VhcmNoL3N0eWxlcy9zZWFyY2gtcHJvZHVjdC5zaGVsbC5zY3NzIiwic3JjL2FwcC9zZWFyY2gvc3R5bGVzL3NlYXJjaC1wcm9kdWN0LnNoZWxsLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxzQkFBQTtFQUNBLDZCQUFBO0FDREY7O0FEU0U7RUFDRSxvQkFBQTtBQ05KOztBRFVBO0VBS0UsdUNBQUE7RUFDQSxvQ0FBQTtBQ1hGOztBRGNBO0VBQ0Usb0VBQUE7RUFDQSwrREFBQTtFQUNBLDhCQUFBO0FDWEY7O0FEY0E7RUFDRSxvRUFBQTtFQUNBLCtEQUFBO0VBQ0EsOEJBQUE7QUNYRjs7QURjQTtFQUNFLG9FQUFBO0VBQ0EsK0RBQUE7RUFDQSw4QkFBQTtBQ1hGIiwiZmlsZSI6InNyYy9hcHAvc2VhcmNoL3N0eWxlcy9zZWFyY2gtcHJvZHVjdC5zaGVsbC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ3VzdG9tIHZhcmlhYmxlc1xuLy8gTm90ZTogIFRoZXNlIG9uZXMgd2VyZSBhZGRlZCBieSB1cyBhbmQgaGF2ZSBub3RoaW5nIHRvIGRvIHdpdGggSW9uaWMgQ1NTIEN1c3RvbSBQcm9wZXJ0aWVzXG46aG9zdCB7XG4gIC0tc2hlbGwtY29sb3I6ICNjYjMyOGY7XG4gIC0tc2hlbGwtY29sb3ItcmdiOiAyMDMsNTAsMTQzO1xufVxuXG4vLyBZb3UgY2FuIGFsc28gYXBwbHkgc2hlZWwgc3R5bGVzIHRvIHRoZSBlbnRpcmUgcGFnZVxuOmhvc3QoLmlzLXNoZWxsKSB7XG4gIC8vIGlvbi1jb250ZW50IHtcbiAgLy8gICBvcGFjaXR5OiAwLjg7XG4gIC8vIH1cbiAgYSB7XG4gICAgcG9pbnRlci1ldmVudHM6IG5vbmU7XG4gIH1cbn1cblxuYXBwLWltYWdlLXNoZWxsLml0ZW0taW1hZ2Uge1xuICAvLyAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMTApO1xuICAvLyAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcblxuICAvLyAtLWltYWdlLXNoZWxsLWxvYWRpbmctYmFja2dyb3VuZDogIzhjYjllYTtcbiAgLS1pbWFnZS1zaGVsbC1sb2FkaW5nLWJhY2tncm91bmQ6IHdoaXRlO1xuICAtLWltYWdlLXNoZWxsLXNwaW5uZXItY29sb3I6ICM1Mjg5YzM7XG59XG5cbi5pdGVtLW5hbWUgYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjI1KTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzUpO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE0cHg7XG59XG5cbi5pdGVtLXNhbGUtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTZweDtcbn1cblxuLml0ZW0tb3JpZ2luYWwtcHJpY2UgPiBhcHAtdGV4dC1zaGVsbCB7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tYmFja2dyb3VuZDogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMjApO1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWNvbG9yOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4zMCk7XG4gIC0tdGV4dC1zaGVsbC1saW5lLWhlaWdodDogMTZweDtcbn1cbiIsIjpob3N0IHtcbiAgLS1zaGVsbC1jb2xvcjogI2NiMzI4ZjtcbiAgLS1zaGVsbC1jb2xvci1yZ2I6IDIwMyw1MCwxNDM7XG59XG5cbjpob3N0KC5pcy1zaGVsbCkgYSB7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG5hcHAtaW1hZ2Utc2hlbGwuaXRlbS1pbWFnZSB7XG4gIC0taW1hZ2Utc2hlbGwtbG9hZGluZy1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgLS1pbWFnZS1zaGVsbC1zcGlubmVyLWNvbG9yOiAjNTI4OWMzO1xufVxuXG4uaXRlbS1uYW1lIGFwcC10ZXh0LXNoZWxsIHtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLXNoZWxsLWNvbG9yLXJnYiksIC4yNSk7XG4gIC0tdGV4dC1zaGVsbC1hbmltYXRpb24tY29sb3I6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjM1KTtcbiAgLS10ZXh0LXNoZWxsLWxpbmUtaGVpZ2h0OiAxNHB4O1xufVxuXG4uaXRlbS1zYWxlLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjIwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE2cHg7XG59XG5cbi5pdGVtLW9yaWdpbmFsLXByaWNlID4gYXBwLXRleHQtc2hlbGwge1xuICAtLXRleHQtc2hlbGwtYW5pbWF0aW9uLWJhY2tncm91bmQ6IHJnYmEodmFyKC0tc2hlbGwtY29sb3ItcmdiKSwgLjIwKTtcbiAgLS10ZXh0LXNoZWxsLWFuaW1hdGlvbi1jb2xvcjogcmdiYSh2YXIoLS1zaGVsbC1jb2xvci1yZ2IpLCAuMzApO1xuICAtLXRleHQtc2hlbGwtbGluZS1oZWlnaHQ6IDE2cHg7XG59Il19 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](SearchProductPage, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-search-product',
          templateUrl: './search-product.page.html',
          styleUrls: ['./styles/search-product.page.scss', './styles/search-product.shell.scss']
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
        }, {
          type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
        }, {
          type: _app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]
        }, {
          type: _ionic_native_downloader_ngx__WEBPACK_IMPORTED_MODULE_9__["Downloader"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["Platform"]
        }, {
          type: _ionic_native_firebase_analytics_ngx__WEBPACK_IMPORTED_MODULE_12__["FirebaseAnalytics"]
        }, {
          type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_11__["InAppBrowser"]
        }, {
          type: _categories_categories_page__WEBPACK_IMPORTED_MODULE_13__["CategoriesPage"]
        }, {
          type: _ionic_native_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_10__["SocialSharing"]
        }];
      }, {
        infiniteScroll: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonInfiniteScroll"]]
        }],
        content: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonContent"], {
            "static": true
          }]
        }],
        isShell: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"],
          args: ['class.is-shell']
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/search/search-product.resolver.ts":
  /*!***************************************************!*\
    !*** ./src/app/search/search-product.resolver.ts ***!
    \***************************************************/

  /*! exports provided: SearchProductResolver */

  /***/
  function srcAppSearchSearchProductResolverTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SearchProductResolver", function () {
      return SearchProductResolver;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _search_search_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../search/search.service */
    "./src/app/search/search.service.ts");

    var SearchProductResolver = /*#__PURE__*/function () {
      function SearchProductResolver(searchService) {
        _classCallCheck(this, SearchProductResolver);

        this.searchService = searchService;
      }

      _createClass(SearchProductResolver, [{
        key: "resolve",
        value: function resolve() {
          var dataSource = this.searchService.getListingDataSource();
          var dataStore = this.searchService.getListingStore(dataSource);
          return dataStore;
        }
      }]);

      return SearchProductResolver;
    }();

    SearchProductResolver.ɵfac = function SearchProductResolver_Factory(t) {
      return new (t || SearchProductResolver)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_search_search_service__WEBPACK_IMPORTED_MODULE_1__["SearchService"]));
    };

    SearchProductResolver.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: SearchProductResolver,
      factory: SearchProductResolver.ɵfac
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SearchProductResolver, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
      }], function () {
        return [{
          type: _search_search_service__WEBPACK_IMPORTED_MODULE_1__["SearchService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/shell/data-store.ts":
  /*!*************************************!*\
    !*** ./src/app/shell/data-store.ts ***!
    \*************************************/

  /*! exports provided: ShellModel, DataStore */

  /***/
  function srcAppShellDataStoreTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShellModel", function () {
      return ShellModel;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DataStore", function () {
      return DataStore;
    });
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./config/app-shell.config */
    "./src/app/shell/config/app-shell.config.ts");

    var ShellModel = function ShellModel() {
      _classCallCheck(this, ShellModel);

      this.isShell = false;
    };

    var DataStore = /*#__PURE__*/function () {
      function DataStore(shellModel) {
        _classCallCheck(this, DataStore);

        this.shellModel = shellModel; // We wait on purpose 2 secs on local environment when fetching from json to simulate the backend roundtrip.
        // However, in production you should set this delay to 0 in the assets/config/app-shell.config.prod.json file.
        // tslint:disable-next-line:max-line-length

        this.networkDelay = _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings && _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay ? _config_app_shell_config__WEBPACK_IMPORTED_MODULE_2__["AppShellConfig"].settings.networkDelay : 0;
        this.timeline = new rxjs__WEBPACK_IMPORTED_MODULE_0__["ReplaySubject"](1);
      } // Static function with generics
      // (ref: https://stackoverflow.com/a/24293088/1116959)
      // Append a shell (T & ShellModel) to every value (T) emmited to the timeline


      _createClass(DataStore, [{
        key: "load",
        value: function load(dataSourceObservable) {
          var _this17 = this;

          var dataSourceWithShellObservable = DataStore.AppendShell(dataSourceObservable, this.shellModel, this.networkDelay);
          dataSourceWithShellObservable.subscribe(function (dataValue) {
            _this17.timeline.next(dataValue);
          });
        }
      }, {
        key: "state",
        get: function get() {
          return this.timeline.asObservable();
        }
      }], [{
        key: "AppendShell",
        value: function AppendShell(dataObservable, shellModel) {
          var networkDelay = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 400;
          var delayObservable = Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["of"])(true).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["delay"])(networkDelay)); // Assign shell flag accordingly
          // (ref: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)

          return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["combineLatest"])([delayObservable, dataObservable]).pipe( // Dismiss unnecessary delayValue
          Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["map"])(function (_ref) {
            var _ref2 = _slicedToArray(_ref, 2),
                delayValue = _ref2[0],
                dataValue = _ref2[1];

            return Object.assign(dataValue, {
              isShell: false
            });
          }), // Set the shell model as the initial value
          Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["startWith"])(Object.assign(shellModel, {
            isShell: true
          })));
        }
      }]);

      return DataStore;
    }();
    /***/

  },

  /***/
  "./src/app/utils/resolver-helper.ts":
  /*!******************************************!*\
    !*** ./src/app/utils/resolver-helper.ts ***!
    \******************************************/

  /*! exports provided: ResolverHelper */

  /***/
  function srcAppUtilsResolverHelperTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResolverHelper", function () {
      return ResolverHelper;
    });
    /* harmony import */


    var _shell_data_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! ../shell/data-store */
    "./src/app/shell/data-store.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");

    var ResolverHelper = /*#__PURE__*/function () {
      function ResolverHelper() {
        _classCallCheck(this, ResolverHelper);
      }

      _createClass(ResolverHelper, null, [{
        key: "extractData",
        // More info on function overloads here: https://www.typescriptlang.org/docs/handbook/functions.html#overloads
        value: function extractData(source, constructor) {
          if (source instanceof _shell_data_store__WEBPACK_IMPORTED_MODULE_0__["DataStore"]) {
            return source.state;
          } else if (source instanceof constructor) {
            // The right side of instanceof should be an expression evaluating to a constructor function (ie. a class), not a type
            // That's why we included an extra parameter which acts as a constructor function for type T
            // (see: https://github.com/microsoft/TypeScript/issues/5236)
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["of"])(source);
          }
        }
      }]);

      return ResolverHelper;
    }();
    /***/

  }
}]);
//# sourceMappingURL=search-search-product-module-es5.js.map