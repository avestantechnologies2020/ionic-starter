import { Component, OnInit } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { CategoriesPage } from "./../../categories/categories.page";

@Component({
  selector: 'app-showcase-shell',
  templateUrl: './app-shell.page.html',
  styleUrls: ['./app-shell.page.scss']
})
export class AppShellPage implements OnInit {

  title = '';
  //airPurifier filter selected option array
  airPurifierCoverageAreaSelected = [];
  airPurifierCADRSelected = [];
  airPurifierPriceSelected = [];

  airPurifierPriceSelectedLength: any;
  airPurifierCoverageAreaSelectedLength: any;
  airPurifierCADRSelectedLength: any

  //waterPurifie filter selected option array
  waterPurifierTechnologySelected = []
  waterPurifierModelNameSelected = []
  waterPurifierCapacitySelected = []
  waterPurifierPriseSelected = []

  waterPurifierTechnologySelectedLength: any;
  waterPurifierPriseSelectedLength: any;
  waterPurifierCapacitySelectedLength: any;
  waterPurifierModelNameSelectedLength: any;

  //airCooler filter selected option array
  airCoolerTypeSelected = []
  airCoolerPriceSelected = []
  airCoolerCapacitySelected = []

  
  airCoolerTypeSelectedLength: any;
  airCoolerPriceSelectedLength: any;
  airCoolerCapacitySelectedLength: any;

  //airConditioner filter selected option array
  airConditionerSubCategorySelected = []
  airConditionerCapacitySelected = []
  airConditionerSeriesSelected = []
  airConditionerStarRatingSelected = []
  airConditionerPriseSelected = []

  airConditionerSubCategorySelectedLenght: any;
  airConditionerCapacitySelectedLength: any;
  airConditionerSeriesSelectedLength: any;
  airConditionerStarRatingSelectedLength: any;
  airConditionerPriseSelectedLength: any;

  //airConditioner json
  airConditionerSubCategory = [
    {
      value: "CASSETTE AC",
      flage: false
    },
    {
      value: "INVCASSETTE",
      flage: false
    },
    {
      value: "INVSAC",
      flage: false
    },
    {
      value: "MEGASAC",
      flage: false
    },
    {
      value: "PORTABLEAC",
      flage: false
    },
    {
      value: "SAC",
      flage: false
    },
    {
      value: "VERTICOOLAC",
      flage: false
    },
    {
      value: "WINDOW AC",
      flage: false
    }
  ]

  airConditionerCapacity = [
    {
      value: "0.75",
      flage: false
    },
    {
      value: "1",
      flage: false
    },
    {
      value: "1.5",
      flage: false
    },
    {
      value: "1.8",
      flage: false
    },
    {
      value: "2",
      flage: false
    },
    {
      value: "2.5",
      flage: false
    },
    {
      value: "3",
      flage: false
    },
    {
      value: "4",
      flage: false
    },
  ]

  airConditionerSeries = [
    {
      value: "A Series",
      showValue: "A",
      flage: false
    },
    {
      value: "C Series",
      showValue: "C",
      flage: false
    },
    {
      value: "DA Series",
      showValue: "DA",
      flage: false
    },
    {
      value: "DA-AP Series",
      showValue: "DA-AP",
      flage: false
    },
    {
      value: "DB Series",
      showValue: "DB",
      flage: false
    },
    {
      value: "DC Series",
      showValue: "DC",
      flage: false
    },
    {
      value: "E Series",
      showValue: "E",
      flage: false
    },
    {
      value: "GA Series",
      showValue: "GA",
      flage: false
    },
    {
      value: "GBT Series",
      showValue: "GBT",
      flage: false
    },
    {
      value: "GBTI Series",
      showValue: "GBTI",
      flage: false
    },
    {
      value: "I Series",
      showValue: "I",
      flage: false
    },
    {
      value: "LA Series",
      showValue: "LA",
      flage: false
    },
    {
      value: "LD Series",
      showValue: "LD",
      flage: false
    },
    {
      value: "LDT Series",
      showValue: "LDT",
      flage: false
    },
    {
      value: "M Series",
      showValue: "M",
      flage: false
    },
    {
      value: "P Series",
      showValue: "P",
      flage: false
    },
    {
      value: "QA Series",
      showValue: "QA",
      flage: false
    },
    {
      value: "QB Series",
      showValue: "QB",
      flage: false
    },
    {
      value: "R Series",
      showValue: "R",
      flage: false
    },
    {
      value: "Y Series",
      showValue: "Y",
      flage: false
    },
    {
      value: "YA Series",
      showValue: "YA",
      flage: false
    },
    {
      value: "YB Series",
      showValue: "YB",
      flage: false
    },
    {
      value: "YC Series",
      showValue: "YC",
      flage: false
    },
    {
      value: "YDF Series",
      showValue: "YDF",
      flage: false
    },
    {
      value: "Z Series",
      showValue: "Z",
      flage: false
    },
  ]

  airConditionerStarRating = [
    {
      value: "1 star",
      showValue: "1",
      flage: false
    },
    {
      value: "2 star",
      showValue: "2",
      flage: false
    },
    {
      value: "3 star",
      showValue: "3",
      flage: false
    },
    {
      value: "4 star",
      showValue: "4",
      flage: false
    },
    {
      value: "5 star",
      showValue: "5",
      flage: false
    },
    {
      value: "Non star",
      showValue: "NA",
      flage: false
    }
  ]

  airConditionerMRP = [
    {
      value: "20000-40000",
      showValue: "20,000-40,000",
      flage: false
    },
    {
      value: "40001-50000",
      showValue: "40,001-50,000",
      flage: false
    },
    {
      value: "50001-60000",
      showValue: "50,001-60,000",
      flage: false
    },
    {
      value: "60001-70000",
      showValue: "60,001-70,000",
      flage: false
    },
    {
      value: "70001-99999999",
      showValue: "70,001 and above",
      flage: false
    }
  ]

  //airCooler json
  airCoolerType = [
    {
      value: "Personal",
      flage: false
    },
    {
      value: "Tower",
      flage: false
    },
    {
      value: "Window",
      flage: false
    },
    {
      value: "Desert",
      flage: false
    }
  ]
  airCoolerMRP = [
    {
      value: "7000-10000",
      showValue: "7,000-10,000",
      flage: false
    },
    {
      value: "10001-15000",
      showValue: "10,001-15,000",
      flage: false
    },
    {
      value: "15001-20000",
      showValue: "15,001-20,000",
      flage: false
    }
  ]
  airCoolerCapacity = [
    {
      value: "25-50",
      flage: false
    },
    {
      value: "51-70",
      flage: false
    },
    {
      value: "71-90",
      flage: false
    }
  ]

  //airPurifier json
  airPurifierMRP = [
    {
      value: "7000-15000",
      showValue: "7,000-15,000",
      flage: false
    },
    {
      value: "15001-20000",
      showValue: "15,001-20,000",
      flage: false
    },
    {
      value: "20001-99999999",
      showValue: "20,001 and above",
      flage: false
    }
  ]

  airPurifierCoverageArea = [
    {
      value: "915",
      flage: false
    },
    {
      value: "545",
      flage: false
    },
    {
      value: "444",
      flage: false
    },
    {
      value: "230",
      flage: false
    }
  ]

  airPurifierCADR = [
    {
      value: "800",
      flage: false
    },
    {
      value: "650",
      flage: false
    },
    {
      value: "300",
      flage: false
    },
    {
      value: "250",
      flage: false
    }
  ]

  //waterPurifie json
  waterPurifieTechnology = [
    {
      value: "RO+UV",
      flage: false
    },
    {
      value: "RO+UV+UF",
      flage: false
    },
    {
      value: "UV LED",
      flage: false
    },
    {
      value: "UV",
      flage: false
    },
    {
      value: "RO+UF",
      flage: false
    }
  ]

  waterPurifieMRP = [
    {
      value: "7000-20000",
      showValue: "7,000-20,000",
      flage: false
    },
    {
      value: "20001-30000",
      showValue: "20,001-30,000",
      flage: false
    },
    {
      value: "30001-99999999",
      showValue: "30,001 and above",
      flage: false
    }
  ]

  waterPurifieCapacity = [
    {
      value: "0",
      flage: false
    },
    {
      value: "6",
      flage: false
    },
    {
      value: "7",
      flage: false
    },
    {
      value: "8",
      flage: false
    },
    {
      value: "8.2",
      flage: false
    },
    {
      value: "9.2",
      flage: false
    }
  ]

  waterPurifieModelName = [
    {
      value: "Adora",
      flage: false
    },
    {
      value: "Aristo",
      flage: false
    },
    {
      value: "Eleanor",
      flage: false
    },
    {
      value: "Eternia",
      flage: false
    },
    {
      value: "Genia",
      flage: false
    },
    {
      value: "Iconia",
      flage: false
    },
    {
      value: "Imperia",
      flage: false
    },
    {
      value: "Magnus",
      flage: false
    },
    {
      value: "Opulus",
      flage: false
    },
    {
      value: "Pristina",
      flage: false
    },
    {
      value: "Stella",
      flage: false
    },
  ]

  

  constructor(public modalCtrl: ModalController, public loadingController: LoadingController,
    private route: ActivatedRoute, private http: HttpClient,
    private categoriesPage: CategoriesPage) {
    this.route.queryParams.subscribe(params => {
      this.title = params.title;
      if (params.title == "Air Conditioners") {
        this.getFilterAirConditioner();
      } else if (params.title == "Air Coolers") {
        this.getFilterAirCooler();
      } else if (params.title == "Air Purifiers") {
        this.getFilterAirPurifier();
      } else if (params.title == "Water Purifiers") {
        this.getFilterWaterPurifier();
      }
    });
  }

  ngOnInit(): void {
    //this.assignFilterPreveas();
  }

  assignFilterPreveas() {
    if (this.title == "Air Conditioners") {
      if (this.airConditionerSubCategorySelected.length != 0) {
        this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length;
        for (let item in this.airConditionerSubCategorySelected) {
          for (let i in this.airConditionerSubCategory) {
            if (this.airConditionerSubCategorySelected[item] == this.airConditionerSubCategory[i].value)
              this.airConditionerSubCategory[i].flage = true;
          }
        }
      }
      if (this.airConditionerCapacitySelected.length != 0) {
        this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length;
        for (let item in this.airConditionerCapacitySelected) {
          for (let i in this.airConditionerCapacity) {
            if (this.airConditionerCapacitySelected[item] == this.airConditionerCapacity[i].value)
              this.airConditionerCapacity[i].flage = true;
          }
        }
      }
      if (this.airConditionerSeriesSelected.length != 0) {
        this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;
        for (let item in this.airConditionerSeriesSelected) {
          for (let i in this.airConditionerSeries) {
            if (this.airConditionerSeriesSelected[item] == this.airConditionerSeries[i].value)
              this.airConditionerSeries[i].flage = true;
          }
        }
      }
      if (this.airConditionerStarRatingSelected.length != 0) {
        this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;
        for (let item in this.airConditionerStarRatingSelected) {
          for (let i in this.airConditionerStarRating) {
            if (this.airConditionerStarRatingSelected[item] == this.airConditionerStarRating[i].value)
              this.airConditionerStarRating[i].flage = true;
          }
        }
      }
      if (this.airConditionerPriseSelected.length != 0) {
        this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;
        for (let item in this.airConditionerPriseSelected) {
          for (let i in this.airConditionerMRP) {
            if (this.airConditionerPriseSelected[item] == this.airConditionerMRP[i].value)
              this.airConditionerMRP[i].flage = true;
          }
        }
      }
    } else if (this.title == "Air Coolers") {
      // console.log("airCoolerTypeSelected", this.airCoolerTypeSelected);
      if (this.airCoolerTypeSelected.length != 0) {
        this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length;
        // console.log("***********",this.airCoolerTypeSelectedLength);
        for (let item in this.airCoolerTypeSelected) {
          for (let i in this.airCoolerType) {
            if (this.airCoolerTypeSelected[item] == this.airCoolerType[i].value)
              this.airCoolerType[i].flage = true;
          }
        }
      }
      if (this.airCoolerPriceSelected.length != 0) {
        this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length;
        // console.log("***********",this.airCoolerPriceSelectedLength);
        for (let item in this.airCoolerPriceSelected) {
          for (let i in this.airCoolerMRP) {
            if (this.airCoolerPriceSelected[item] == this.airCoolerMRP[i].value)
              this.airCoolerMRP[i].flage = true;
          }
        }
      }
      if (this.airCoolerCapacitySelected.length != 0) {
        this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length;
        // console.log("***********",this.airCoolerCapacitySelectedLength);
        for (let item in this.airCoolerCapacitySelected) {
          for (let i in this.airCoolerCapacity) {
            if (this.airCoolerCapacitySelected[item] == this.airCoolerCapacity[i].value)
              this.airCoolerCapacity[i].flage = true;
          }
        }
      }
    } else if (this.title == "Air Purifiers") {
      if (this.airPurifierCoverageAreaSelected.length != 0) {
        this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;
        for (let item in this.airPurifierCoverageAreaSelected) {
          for (let i in this.airPurifierCoverageArea) {
            if (this.airPurifierCoverageAreaSelected[item] == this.airPurifierCoverageArea[i].value)
              this.airPurifierCoverageArea[i].flage = true;
          }
        }
      }
      if (this.airPurifierCADRSelected.length != 0) {
        this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;
        for (let item in this.airPurifierCADRSelected) {
          for (let i in this.airPurifierCADR) {
            if (this.airPurifierCADRSelected[item] == this.airPurifierCADR[i].value)
              this.airPurifierCADR[i].flage = true;
          }
        }
      }
      if (this.airPurifierPriceSelected.length != 0) {
        this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;
        for (let item in this.airPurifierPriceSelected) {
          for (let i in this.airPurifierMRP) {
            if (this.airPurifierPriceSelected[item] == this.airPurifierMRP[i].value)
              this.airPurifierMRP[i].flage = true;
          }
        }
      }
    } else if (this.title == "Water Purifiers") {
      if (this.waterPurifierTechnologySelected.length != 0) {
        this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;
        for (let item in this.waterPurifierTechnologySelected) {
          for (let i in this.waterPurifieTechnology) {
            if (this.waterPurifierTechnologySelected[item] == this.waterPurifieTechnology[i].value)
              this.waterPurifieTechnology[i].flage = true;
          }
        }
      }
      if (this.waterPurifierModelNameSelected.length != 0) {
        this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;
        for (let item in this.waterPurifierModelNameSelected) {
          for (let i in this.waterPurifieModelName) {
            if (this.waterPurifierModelNameSelected[item] == this.waterPurifieModelName[i].value)
              this.waterPurifieModelName[i].flage = true;
          }
        }
      }
      if (this.waterPurifierCapacitySelected.length != 0) {
        this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;
        for (let item in this.waterPurifierCapacitySelected) {
          for (let i in this.waterPurifieCapacity) {
            if (this.waterPurifierCapacitySelected[item] == this.waterPurifieCapacity[i].value)
              this.waterPurifieCapacity[i].flage = true;
          }
        }
      }
      if (this.waterPurifierPriseSelected.length != 0) {
        this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;
        for (let item in this.waterPurifierPriseSelected) {
          for (let i in this.waterPurifieMRP) {
            if (this.waterPurifierPriseSelected[item] == this.waterPurifieMRP[i].value)
              this.waterPurifieMRP[i].flage = true;
          }
        }
      }
    }
  }

  clearFilter() {
    if (this.title == "Air Conditioners") {
      this.airConditionerSubCategorySelected = []
      this.airConditionerCapacitySelected = []
      this.airConditionerSeriesSelected = []
      this.airConditionerStarRatingSelected = []
      this.airConditionerPriseSelected = []

      this.airConditionerSubCategorySelectedLenght = 0;
      this.airConditionerCapacitySelectedLength = 0;
      this.airConditionerSeriesSelectedLength = 0;
      this.airConditionerStarRatingSelectedLength = 0;
      this.airConditionerPriseSelectedLength = 0;

      for (let i in this.airConditionerSubCategory) {
        this.airConditionerSubCategory[i].flage = false;
      }
      for (let i in this.airConditionerCapacity) {
        this.airConditionerCapacity[i].flage = false;
      }
      for (let i in this.airConditionerSeries) {
        this.airConditionerSeries[i].flage = false;
      }
      for (let i in this.airConditionerStarRating) {
        this.airConditionerStarRating[i].flage = false;
      }
      for (let i in this.airConditionerMRP) {
        this.airConditionerMRP[i].flage = false;
      }
    } else if (this.title == "Air Coolers") {
      this.airCoolerTypeSelected = []
      this.airCoolerPriceSelected = []
      this.airCoolerCapacitySelected = []

      this.airCoolerTypeSelectedLength = 0;
      this.airCoolerPriceSelectedLength = 0;
      this.airCoolerCapacitySelectedLength = 0;
       
      for (let i in this.airCoolerType) {
        this.airCoolerType[i].flage = false;
      }
      for (let i in this.airCoolerMRP) {
        this.airCoolerMRP[i].flage = false;
      }
      for (let i in this.airCoolerCapacity) {
        this.airCoolerCapacity[i].flage = false;
      }
      // console.log("airCoolerTypeSelected", this.airCoolerTypeSelected);
    } else if (this.title == "Air Purifiers") {
      this.airPurifierCoverageAreaSelected = [];
      this.airPurifierCADRSelected = [];
      this.airPurifierPriceSelected = [];

      this.airPurifierPriceSelectedLength = 0;
      this.airPurifierCoverageAreaSelectedLength = 0;
      this.airPurifierCADRSelectedLength = 0;

      for (let i in this.airPurifierCoverageArea) {
        this.airPurifierCoverageArea[i].flage = false;
      }
      for (let i in this.airPurifierCADR) {
        this.airPurifierCADR[i].flage = false;
      }
      for (let i in this.airPurifierMRP) {
        this.airPurifierMRP[i].flage = false;
      }
    } else if (this.title == "Water Purifiers") {
      this.waterPurifierTechnologySelected = []
      this.waterPurifierModelNameSelected = []
      this.waterPurifierCapacitySelected = []
      this.waterPurifierPriseSelected = []

      this.waterPurifierTechnologySelectedLength = 0;
      this.waterPurifierPriseSelectedLength = 0;
      this.waterPurifierCapacitySelectedLength = 0;
      this.waterPurifierModelNameSelectedLength = 0;

      for (let i in this.waterPurifieTechnology) {
        this.waterPurifieTechnology[i].flage = false;
      }
      for (let i in this.waterPurifieModelName) {
        this.waterPurifieModelName[i].flage = false;
      }
      for (let i in this.waterPurifieCapacity) {
        this.waterPurifieCapacity[i].flage = false;
      }
      for (let i in this.waterPurifieMRP) {
        this.waterPurifieMRP[i].flage = false;
      }
    }
  }

  closeModal() {
    if (this.title == "Air Conditioners") {
      let data = {
        close: "true",
        airConditionerSubCategorySelected: this.airConditionerSubCategorySelected,
        airConditionerCapacitySelected: this.airConditionerCapacitySelected,
        airConditionerSeriesSelected: this.airConditionerSeriesSelected,
        airConditionerStarRatingSelected: this.airConditionerStarRatingSelected,
        airConditionerPriseSelected: this.airConditionerPriseSelected,
      };
      this.modalCtrl.dismiss(data);
    } else if (this.title == "Air Coolers") {
      let data = {
        close: "true",
        airCoolerTypeSelected: this.airCoolerTypeSelected,
        airCoolerPriceSelected: this.airCoolerPriceSelected,
        airCoolerCapacitySelected: this.airCoolerCapacitySelected,
      };
      this.modalCtrl.dismiss(data);
    } else if (this.title == "Air Purifiers") {
      let data = {
        close: "true",
        airPurifierCoverageAreaSelected: this.airPurifierCoverageAreaSelected,
        airPurifierCADRSelected: this.airPurifierCADRSelected,
        airPurifierPriceSelected: this.airPurifierPriceSelected,
      };
      this.modalCtrl.dismiss(data);
    } else if (this.title == "Water Purifiers") {
      let data = {
        close: "true",
        waterPurifierTechnologySelected: this.waterPurifierTechnologySelected,
        waterPurifierModelNameSelected: this.waterPurifierModelNameSelected,
        waterPurifierCapacitySelected: this.waterPurifierCapacitySelected,
        waterPurifierPriseSelected: this.waterPurifierPriseSelected,
      };
      this.modalCtrl.dismiss(data);
    }
  }

  onClickApply() {
    if (this.title == "Air Conditioners") {
      let data = {
        SubCategory: "",
        Capacity: "",
        Series: "",
        StarRating: "",
        Price: "",
        close: "false",
        airConditionerSubCategorySelected: this.airConditionerSubCategorySelected,
        airConditionerCapacitySelected: this.airConditionerCapacitySelected,
        airConditionerSeriesSelected: this.airConditionerSeriesSelected,
        airConditionerStarRatingSelected: this.airConditionerStarRatingSelected,
        airConditionerPriseSelected: this.airConditionerPriseSelected
      };

      if (this.airConditionerPriseSelected.length == 0) {
        data.Price = ""
      } else {
        let stringValue = ""
        for (let index in this.airConditionerPriseSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airConditionerPriseSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airConditionerPriseSelected[index] + "'"
          }
        }
        data.Price = stringValue
      }

      if (this.airConditionerStarRatingSelected.length == 0) {
        data.StarRating = ""
      } else {
        let stringValue = ""
        for (let index in this.airConditionerStarRatingSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airConditionerStarRatingSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airConditionerStarRatingSelected[index] + "'"
          }
        }
        data.StarRating = stringValue
      }

      if (this.airConditionerSeriesSelected.length == 0) {
        data.Series = ""
      } else {
        let stringValue = ""
        for (let index in this.airConditionerSeriesSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airConditionerSeriesSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airConditionerSeriesSelected[index] + "'"
          }
        }
        data.Series = stringValue
      }

      if (this.airConditionerSubCategorySelected.length == 0) {
        data.SubCategory = ""
      } else {
        let stringValue = ""
        for (let index in this.airConditionerSubCategorySelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airConditionerSubCategorySelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airConditionerSubCategorySelected[index] + "'"
          }
        }
        data.SubCategory = stringValue
      }

      if (this.airConditionerCapacitySelected.length == 0) {
        data.Capacity = ""
      } else {
        let stringValue = ""
        for (let index in this.airConditionerCapacitySelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airConditionerCapacitySelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airConditionerCapacitySelected[index] + "'"
          }
        }
        data.Capacity = stringValue
      }
      this.modalCtrl.dismiss(data);
    } else if (this.title == "Air Coolers") {
      let data = {
        Type: "",
        Price: "",
        Capacity: "",
        close: "false",
        airCoolerTypeSelected: this.airCoolerTypeSelected,
        airCoolerPriceSelected: this.airCoolerPriceSelected,
        airCoolerCapacitySelected: this.airCoolerCapacitySelected
      };

      if (this.airCoolerCapacitySelected.length == 0) {
        data.Capacity = ""
      } else {
        let stringValue = ""
        for (let index in this.airCoolerCapacitySelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airCoolerCapacitySelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airCoolerCapacitySelected[index] + "'"
          }
        }
        data.Capacity = stringValue
      }

      if (this.airCoolerPriceSelected.length == 0) {
        data.Price = ""
      } else {
        let stringValue = ""
        for (let index in this.airCoolerPriceSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airCoolerPriceSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airCoolerPriceSelected[index] + "'"
          }
        }
        data.Price = stringValue
      }

      if (this.airCoolerTypeSelected.length == 0) {
        data.Type = ""
      } else {
        let stringValue = ""
        for (let index in this.airCoolerTypeSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airCoolerTypeSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airCoolerTypeSelected[index] + "'"
          }
        }
        data.Type = stringValue
      }
      this.modalCtrl.dismiss(data);
    } else if (this.title == "Air Purifiers") {
      let data = {
        CADR: "",
        CoverageArea: "",
        Price: "",
        close: "false",
        airPurifierCADRSelected: this.airPurifierCADRSelected,
        airPurifierCoverageAreaSelected: this.airPurifierCoverageAreaSelected,
        airPurifierPriceSelected: this.airPurifierPriceSelected
      };
      if (this.airPurifierCADRSelected.length == 0) {
        data.CADR = ""
      } else {
        let stringValue = ""
        for (let index in this.airPurifierCADRSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airPurifierCADRSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airPurifierCADRSelected[index] + "'"
          }
        }
        data.CADR = stringValue
      }

      if (this.airPurifierCoverageAreaSelected.length == 0) {
        data.CoverageArea = ""
      } else {
        let stringValue = ""
        for (let index in this.airPurifierCoverageAreaSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airPurifierCoverageAreaSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airPurifierCoverageAreaSelected[index] + "'"
          }
        }
        data.CoverageArea = stringValue
      }

      if (this.airPurifierPriceSelected.length == 0) {
        data.Price = ""
      } else {
        let stringValue = ""
        for (let index in this.airPurifierPriceSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.airPurifierPriceSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.airPurifierPriceSelected[index] + "'"
          }
        }
        data.Price = stringValue
      }
      this.modalCtrl.dismiss(data);
    } else if (this.title == "Water Purifiers") {
      let data = {
        Technology: "",
        ModelName: "",
        Capacity: "",
        Price: "",
        close: "false",
        waterPurifierTechnologySelected: this.waterPurifierTechnologySelected,
        waterPurifierModelNameSelected: this.waterPurifierModelNameSelected,
        waterPurifierCapacitySelected: this.waterPurifierCapacitySelected,
        waterPurifierPriseSelected: this.waterPurifierPriseSelected
      };

      if (this.waterPurifierTechnologySelected.length == 0) {
        data.Technology = ""
      } else {
        let stringValue = ""
        for (let index in this.waterPurifierTechnologySelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.waterPurifierTechnologySelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.waterPurifierTechnologySelected[index] + "'"
          }
        }
        data.Technology = stringValue
      }

      if (this.waterPurifierModelNameSelected.length == 0) {
        data.ModelName = ""
      } else {
        let stringValue = ""
        for (let index in this.waterPurifierModelNameSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.waterPurifierModelNameSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.waterPurifierModelNameSelected[index] + "'"
          }
        }
        data.ModelName = stringValue
      }

      if (this.waterPurifierCapacitySelected.length == 0) {
        data.Capacity = ""
      } else {
        let stringValue = ""
        for (let index in this.waterPurifierCapacitySelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.waterPurifierCapacitySelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.waterPurifierCapacitySelected[index] + "'"
          }
        }
        data.Capacity = stringValue
      }

      if (this.waterPurifierPriseSelected.length == 0) {
        data.Price = ""
      } else {
        let stringValue = ""
        for (let index in this.waterPurifierPriseSelected) {
          if (stringValue.length == 0) {
            stringValue = "'" + this.waterPurifierPriseSelected[index] + "'"
          } else {
            stringValue = stringValue + "," + "'" + this.waterPurifierPriseSelected[index] + "'"
          }
        }
        data.Price = stringValue
      }

      this.modalCtrl.dismiss(data);
    }

  }

  clockOnAirPurifierCADR(value) {
    for (let i in this.airPurifierCADR) {
      if (this.airPurifierCADR[i].value == value) {
        this.airPurifierCADR[i].flage = !this.airPurifierCADR[i].flage
      }
    }
    if (this.airPurifierCADRSelected.length == 0) {
      this.airPurifierCADRSelected.push(value);
      this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airPurifierCADRSelected) {
        if (this.airPurifierCADRSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airPurifierCADRSelected.push(value);
        this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;
      } else {
        this.airPurifierCADRSelected.splice(removeItemIndex, 1);
        this.airPurifierCADRSelectedLength = this.airPurifierCADRSelected.length;
      }
    }
  }

  clickOnAirPurifierCoverageArea(value) {
    for (let i in this.airPurifierCoverageArea) {
      if (this.airPurifierCoverageArea[i].value == value) {
        this.airPurifierCoverageArea[i].flage = !this.airPurifierCoverageArea[i].flage
      }
    }
    if (this.airPurifierCoverageAreaSelected.length == 0) {
      this.airPurifierCoverageAreaSelected.push(value);
      this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airPurifierCoverageAreaSelected) {
        if (this.airPurifierCoverageAreaSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airPurifierCoverageAreaSelected.push(value);
        this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;
      } else {
        this.airPurifierCoverageAreaSelected.splice(removeItemIndex, 1);
        this.airPurifierCoverageAreaSelectedLength = this.airPurifierCoverageAreaSelected.length;
      }
    }
  }

  clickOnAirPurifierPrice(value) {
    for (let i in this.airPurifierMRP) {
      if (this.airPurifierMRP[i].value == value) {
        this.airPurifierMRP[i].flage = !this.airPurifierMRP[i].flage
      }
    }
    if (this.airPurifierPriceSelected.length == 0) {
      this.airPurifierPriceSelected.push(value);
      this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airPurifierPriceSelected) {
        if (this.airPurifierPriceSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airPurifierPriceSelected.push(value);
        this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;
      } else {
        this.airPurifierPriceSelected.splice(removeItemIndex, 1);
        this.airPurifierPriceSelectedLength = this.airPurifierPriceSelected.length;
      }
    }
  }

  clickOnWaterPurifierTechnology(value) {
    for (let i in this.waterPurifieTechnology) {
      if (this.waterPurifieTechnology[i].value == value) {
        this.waterPurifieTechnology[i].flage = !this.waterPurifieTechnology[i].flage
      }
    }
    if (this.waterPurifierTechnologySelected.length == 0) {
      this.waterPurifierTechnologySelected.push(value);
      this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.waterPurifierTechnologySelected) {
        if (this.waterPurifierTechnologySelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.waterPurifierTechnologySelected.push(value);
        this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;
      } else {
        this.waterPurifierTechnologySelected.splice(removeItemIndex, 1);
        this.waterPurifierTechnologySelectedLength = this.waterPurifierTechnologySelected.length;
      }
    }
  }

  clickOnWaterPurifierModelName(value) {
    for (let i in this.waterPurifieModelName) {
      if (this.waterPurifieModelName[i].value == value) {
        this.waterPurifieModelName[i].flage = !this.waterPurifieModelName[i].flage
      }
    }
    if (this.waterPurifierModelNameSelected.length == 0) {
      this.waterPurifierModelNameSelected.push(value);
      this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.waterPurifierModelNameSelected) {
        if (this.waterPurifierModelNameSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.waterPurifierModelNameSelected.push(value);
        this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;
      } else {
        this.waterPurifierModelNameSelected.splice(removeItemIndex, 1);
        this.waterPurifierModelNameSelectedLength = this.waterPurifierModelNameSelected.length;
      }
    }
  }

  clickOnWaterPurifierCapacity(value) {
    for (let i in this.waterPurifieCapacity) {
      if (this.waterPurifieCapacity[i].value == value) {
        this.waterPurifieCapacity[i].flage = !this.waterPurifieCapacity[i].flage
      }
    }
    if (this.waterPurifierCapacitySelected.length == 0) {
      this.waterPurifierCapacitySelected.push(value);
      this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.waterPurifierCapacitySelected) {
        if (this.waterPurifierCapacitySelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.waterPurifierCapacitySelected.push(value);
        this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;
      } else {
        this.waterPurifierCapacitySelected.splice(removeItemIndex, 1);
        this.waterPurifierCapacitySelectedLength = this.waterPurifierCapacitySelected.length;
      }
    }
  }

  clickOnWaterPurifierPrice(value) {
    for (let i in this.waterPurifieMRP) {
      if (this.waterPurifieMRP[i].value == value) {
        this.waterPurifieMRP[i].flage = !this.waterPurifieMRP[i].flage
      }
    }
    if (this.waterPurifierPriseSelected.length == 0) {
      this.waterPurifierPriseSelected.push(value);
      this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.waterPurifierPriseSelected) {
        if (this.waterPurifierPriseSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.waterPurifierPriseSelected.push(value);
        this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;
      } else {
        this.waterPurifierPriseSelected.splice(removeItemIndex, 1);
        this.waterPurifierPriseSelectedLength = this.waterPurifierPriseSelected.length;
      }
    }
  }

  clickOnAirCoolerType(value) {
    for (let i in this.airCoolerType) {
      if (this.airCoolerType[i].value == value) {
        this.airCoolerType[i].flage = !this.airCoolerType[i].flage
      }
    }
    if (this.airCoolerTypeSelected.length == 0) {
      this.airCoolerTypeSelected.push(value);
      this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length;
      // console.log("this.airCoolerTypeSelected 1",this.airCoolerTypeSelectedLength);
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airCoolerTypeSelected) {
        if (this.airCoolerTypeSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airCoolerTypeSelected.push(value);
        this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length;
        // console.log("this.airCoolerTypeSelected 2",this.airCoolerTypeSelectedLength);
      } else {
        this.airCoolerTypeSelected.splice(removeItemIndex, 1);
        this.airCoolerTypeSelectedLength = this.airCoolerTypeSelected.length;
        // console.log("this.airCoolerTypeSelected 3",this.airCoolerTypeSelectedLength);
      }
    }
  }

  clickOnAirCoolerPrice(value) {
    for (let i in this.airCoolerMRP) {
      if (this.airCoolerMRP[i].value == value) {
        this.airCoolerMRP[i].flage = !this.airCoolerMRP[i].flage
      }
    }
    if (this.airCoolerPriceSelected.length == 0) {
      this.airCoolerPriceSelected.push(value);
      this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length;
      // console.log("airCoolerPriceSelectedLength",this.airCoolerPriceSelectedLength);
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airCoolerPriceSelected) {
        if (this.airCoolerPriceSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airCoolerPriceSelected.push(value);
        this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length;
        // console.log("airCoolerPriceSelectedLength",this.airCoolerPriceSelectedLength);
      } else {
        this.airCoolerPriceSelected.splice(removeItemIndex, 1)
        this.airCoolerPriceSelectedLength = this.airCoolerPriceSelected.length;
        // console.log("airCoolerPriceSelectedLength",this.airCoolerPriceSelectedLength);
      }
    }
  }

  clickOnAirCoolerCapacity(value) {
    for (let i in this.airCoolerCapacity) {
      if (this.airCoolerCapacity[i].value == value) {
        this.airCoolerCapacity[i].flage = !this.airCoolerCapacity[i].flage
      }
    }
    if (this.airCoolerCapacitySelected.length == 0) {
      this.airCoolerCapacitySelected.push(value);
      this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length;
      // console.log("airCoolerCapacitySelectedLength",this.airCoolerCapacitySelectedLength);
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airCoolerCapacitySelected) {
        if (this.airCoolerCapacitySelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airCoolerCapacitySelected.push(value);
        this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length;
        // console.log("airCoolerCapacitySelectedLength",this.airCoolerCapacitySelectedLength);
      } else {
        this.airCoolerCapacitySelected.splice(removeItemIndex, 1);
        this.airCoolerCapacitySelectedLength = this.airCoolerCapacitySelected.length;
        // console.log("airCoolerCapacitySelectedLength",this.airCoolerCapacitySelectedLength);
      }
    }
  }

  clickOnAirConditionerSubCategory(value) {
    for (let i in this.airConditionerSubCategory) {
      if (this.airConditionerSubCategory[i].value == value) {
        this.airConditionerSubCategory[i].flage = !this.airConditionerSubCategory[i].flage
      }
    }
    if (this.airConditionerSubCategorySelected.length == 0) {
      this.airConditionerSubCategorySelected.push(value);
      this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length;
      // console.log("airConditionerSubCategorySelectedLenght",this.airConditionerSubCategorySelectedLenght);
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airConditionerSubCategorySelected) {
        if (this.airConditionerSubCategorySelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airConditionerSubCategorySelected.push(value);
        this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length;
        // console.log("airConditionerSubCategorySelectedLenght",this.airConditionerSubCategorySelectedLenght);
      } else {
        this.airConditionerSubCategorySelected.splice(removeItemIndex, 1);
        this.airConditionerSubCategorySelectedLenght = this.airConditionerSubCategorySelected.length;
        // console.log("airConditionerSubCategorySelectedLenght",this.airConditionerSubCategorySelectedLenght);
      }
    }
  }

  clickOnAirConditionerCapacity(value) {
    for (let i in this.airConditionerCapacity) {
      if (this.airConditionerCapacity[i].value == value) {
        this.airConditionerCapacity[i].flage = !this.airConditionerCapacity[i].flage
      }
    }
    if (this.airConditionerCapacitySelected.length == 0) {
      this.airConditionerCapacitySelected.push(value);
      this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length;
      // console.log("airConditionerCapacitySelectedLength",this.airConditionerCapacitySelectedLength);
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airConditionerCapacitySelected) {
        if (this.airConditionerCapacitySelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airConditionerCapacitySelected.push(value);
        this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length;
        // console.log("airConditionerCapacitySelectedLength",this.airConditionerCapacitySelectedLength);
      } else {
        this.airConditionerCapacitySelected.splice(removeItemIndex, 1);
        this.airConditionerCapacitySelectedLength = this.airConditionerCapacitySelected.length;
        // console.log("airConditionerCapacitySelectedLength",this.airConditionerCapacitySelectedLength);
      }
    }
  }

  clickOnAirConditionerSeries(value) {
    for (let i in this.airConditionerSeries) {
      if (this.airConditionerSeries[i].value == value) {
        this.airConditionerSeries[i].flage = !this.airConditionerSeries[i].flage
      }
    }
    if (this.airConditionerSeriesSelected.length == 0) {
      this.airConditionerSeriesSelected.push(value);
      this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airConditionerSeriesSelected) {
        if (this.airConditionerSeriesSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airConditionerSeriesSelected.push(value);
        this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;
      } else {
        this.airConditionerSeriesSelected.splice(removeItemIndex, 1)
        this.airConditionerSeriesSelectedLength = this.airConditionerSeriesSelected.length;
      }
    }
  }

  clickOnAirConditionerStarRating(value) {
    for (let i in this.airConditionerStarRating) {
      if (this.airConditionerStarRating[i].value == value) {
        this.airConditionerStarRating[i].flage = !this.airConditionerStarRating[i].flage
      }
    }
    if (this.airConditionerStarRatingSelected.length == 0) {
      this.airConditionerStarRatingSelected.push(value);
      this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airConditionerStarRatingSelected) {
        if (this.airConditionerStarRatingSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airConditionerStarRatingSelected.push(value);
        this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;
      } else {
        this.airConditionerStarRatingSelected.splice(removeItemIndex, 1);
        this.airConditionerStarRatingSelectedLength = this.airConditionerStarRatingSelected.length;
      }
    }
  }

  clickOnAirConditionerPrise(value) {
    for (let i in this.airConditionerMRP) {
      if (this.airConditionerMRP[i].value == value) {
        this.airConditionerMRP[i].flage = !this.airConditionerMRP[i].flage
      }
    }
    if (this.airConditionerPriseSelected.length == 0) {
      this.airConditionerPriseSelected.push(value);
      this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;
    } else {
      let count = 0
      let removeItemIndex = 0;
      for (let index in this.airConditionerPriseSelected) {
        if (this.airConditionerPriseSelected[index] == value) {
          count = 1
          removeItemIndex = Number(index)
        }
      }
      if (count == 0) {
        this.airConditionerPriseSelected.push(value);
        this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;
      } else {
        this.airConditionerPriseSelected.splice(removeItemIndex, 1);
        this.airConditionerPriseSelectedLength = this.airConditionerPriseSelected.length;
      }
    }
  }

  getFilterAirConditioner() {
    this.route.queryParams.subscribe(params => {
      if (params && params.title) {
        this.title = params.title
        let param = {};
        if (params.title == "Air Conditioners") {
          param = { air_conditioner: "Air Conditioners" }
        }
        this.loadingController.create({
          message: 'Please wait',
        }).then((res) => {
          res.present();
          if (navigator.onLine) {
            this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe((response) => {
              Object.keys(response).map(key => {
                //console.log("Res******", response[key]);
                this.airConditionerSubCategory = []
                this.airConditionerCapacity = []
                this.airConditionerSeries = []
                this.airConditionerStarRating = []
                this.airConditionerMRP = []
                for (let index in response[key]) {
                  if (index == "capacity") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.airConditionerCapacity.push(object)
                    }
                  }
                  if (index == "mrp") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      if (response[key][index][1][i].replace(/,/g, '').includes("and above")) {
                        let array = response[key][index][1][i].replace(/,/g, '').split(" ")
                        object.value = array[0] + "-" + "99999999"
                        object.showValue = response[key][index][1][i]
                        this.airConditionerMRP.push(object)
                      } else {
                        object.value = response[key][index][1][i].replace(/,/g, '')
                        object.showValue = response[key][index][1][i]
                        this.airConditionerMRP.push(object)
                      }
                    }
                  }
                  if (index == "series") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.airConditionerSeries.push(object)
                    }
                  }
                  if (index == "star_rating") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i] + " star"
                      object.showValue = response[key][index][1][i]
                      this.airConditionerStarRating.push(object)
                    }
                  }
                  if (index == "subcategory") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.airConditionerSubCategory.push(object)
                    }
                  }
                }
                this.assignFilterPreveas()
                res.dismiss();
              })
            }, err => {
              res.dismiss();
              console.log("err.........", JSON.stringify(err))
            });
          } else {
            res.dismiss();
            console.log("no internat connection")
          }
        });
      }
    })
  }

  getFilterAirCooler() {
    this.route.queryParams.subscribe(params => {
      if (params && params.title) {
        this.title = params.title
        let param = {};
        if (params.title == "Air Coolers") {
          param = { air_cooler: "Air Coolers" }
        }
        this.loadingController.create({
          message: 'Please wait',
        }).then((res) => {
          res.present();
          if (navigator.onLine) {
            this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe((response) => {
              Object.keys(response).map(key => {
                //console.log("Res******", response[key]);
                this.airCoolerType = []
                this.airCoolerMRP = []
                this.airCoolerCapacity = []
                for (let index in response[key]) {
                  if (index == "capacity") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.airCoolerCapacity.push(object)
                    }
                  }
                  if (index == "mrp") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      if (response[key][index][1][i].replace(/,/g, '').includes("and above")) {
                        let array = response[key][index][1][i].replace(/,/g, '').split(" ")
                        object.value = array[0] + "-" + "99999999"
                        object.showValue = response[key][index][1][i]
                        this.airCoolerMRP.push(object)
                      } else {
                        object.value = response[key][index][1][i].replace(/,/g, '')
                        object.showValue = response[key][index][1][i]
                        this.airCoolerMRP.push(object)
                      }
                    }
                  }
                  if (index == "type") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.airCoolerType.push(object)
                    }
                  }
                }
                this.assignFilterPreveas()
                res.dismiss();
              })
            }, err => {
              res.dismiss();
              console.log("err.........", JSON.stringify(err))
            });
          } else {
            res.dismiss();
            console.log("no internat connection")
          }
        });
      }
    })
  }

  getFilterAirPurifier() {
    this.route.queryParams.subscribe(params => {
      if (params && params.title) {
        this.title = params.title
        let param = {};
        if (params.title == "Air Purifiers") {
          param = { air_purifier: "Air Purifiers" }
        }
        this.loadingController.create({
          message: 'Please wait',
        }).then((res) => {
          res.present();
          if (navigator.onLine) {
            this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe((response) => {
              Object.keys(response).map(key => {
                //console.log("Res******", response[key]);
                this.airPurifierMRP = []
                this.airPurifierCoverageArea = []
                this.airPurifierCADR = []
                for (let index in response[key]) {
                  if (index == "area_cover") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.airPurifierCoverageArea.push(object)
                    }
                  }
                  if (index == "mrp") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      if (response[key][index][1][i].replace(/,/g, '').includes("and above")) {
                        let array = response[key][index][1][i].replace(/,/g, '').split(" ")
                        object.value = array[0] + "-" + "99999999"
                        object.showValue = response[key][index][1][i]
                        this.airPurifierMRP.push(object)
                      } else {
                        object.value = response[key][index][1][i].replace(/,/g, '')
                        object.showValue = response[key][index][1][i]
                        this.airPurifierMRP.push(object)
                      }
                    }
                  }
                  if (index == "cadr") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.airPurifierCADR.push(object)
                    }
                  }
                }
                this.assignFilterPreveas()
                res.dismiss();
              })
            }, err => {
              res.dismiss();
              console.log("err.........", JSON.stringify(err))
            });
          } else {
            res.dismiss();
            console.log("no internat connection")
          }
        });
      }
    })
  }

  getFilterWaterPurifier() {
    this.route.queryParams.subscribe(params => {
      if (params && params.title) {
        this.title = params.title
        let param = {};
        if (params.title == "Water Purifiers") {
          param = { water_purifier: "Water Purifiers" }
        }
        this.loadingController.create({
          message: 'Please wait',
        }).then((res) => {
          res.present();
          if (navigator.onLine) {
            this.http.post(this.categoriesPage.apiBaseUrl + '/bluestar_api/category_filter', param).subscribe((response) => {
              Object.keys(response).map(key => {
                //console.log("Res******", response[key]);
                this.waterPurifieTechnology = []
                this.waterPurifieMRP = []
                this.waterPurifieCapacity = []
                this.waterPurifieModelName = []
                for (let index in response[key]) {
                  if (index == "purification_technology") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.waterPurifieTechnology.push(object)
                    }
                  }
                  if (index == "mrp") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      if (response[key][index][1][i].replace(/,/g, '').includes("and above")) {
                        let array = response[key][index][1][i].replace(/,/g, '').split(" ")
                        object.value = array[0] + "-" + "99999999"
                        object.showValue = response[key][index][1][i]
                        this.waterPurifieMRP.push(object)
                      } else {
                        object.value = response[key][index][1][i].replace(/,/g, '')
                        object.showValue = response[key][index][1][i]
                        this.waterPurifieMRP.push(object)
                      }
                    }
                  }
                  if (index == "storage_capacity") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.waterPurifieCapacity.push(object)
                    }
                  }
                  if (index == "model_series") {
                    for (let i in response[key][index][1]) {
                      let object = {
                        value: "",
                        showValue: "",
                        flage: false,
                      }
                      object.value = response[key][index][1][i]
                      object.showValue = response[key][index][1][i]
                      this.waterPurifieModelName.push(object)
                    }
                  }
                }
                this.assignFilterPreveas()
                res.dismiss();
              })
            }, err => {
              res.dismiss();
              console.log("err.........", JSON.stringify(err))
            });
          } else {
            res.dismiss();
            console.log("no internat connection")
          }
        });
      }
    })
  }
}



