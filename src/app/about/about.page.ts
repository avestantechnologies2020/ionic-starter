import { Component } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: [
    './styles/about.page.scss',
    './styles/about.shell.scss'
  ]
})
export class AboutPage { }
